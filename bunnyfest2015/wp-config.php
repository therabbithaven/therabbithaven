<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'rabbit_wpbf');

/** MySQL database username */
define('DB_USER', 'rabbit_wpbf');

/** MySQL database password */
define('DB_PASSWORD', '8.[7mS2Ps9');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'zj7buylublgljej5vkw23rzdhztajoyqxs1dkksvpmzgatn4imuodpt9pogkv9jw');
define('SECURE_AUTH_KEY',  'dc999yoyyp8u3u8ksjbnicuert0794pxp7h9g1xrn3gp2sfutjbsngxmazaq6ues');
define('LOGGED_IN_KEY',    'sbpqoxckmb3tmm2kzfscstdz0tvwqjzgskths54ptgryhyayhrcltipghfydsyqv');
define('NONCE_KEY',        'tq9mwcubdgaetbkgi3jb0rafxep6zh1hdeqsylplvs8jcjy0icys2nrd1z8rlmn4');
define('AUTH_SALT',        'vxannx5wmktwq9bhvutr2qeapa1l4i4i8b3yskncgn4bvytq9glwx4297mc1c8yz');
define('SECURE_AUTH_SALT', 'ggwmslhy5xykcdgp2t8csuqr0h763gv5yst94jqt8ei2s2zn5qzosvne3rvn3wjg');
define('LOGGED_IN_SALT',   'szhwsceac7uom3hdn0ije96anungjvqurre7xkoslk0ypkny1fqwdrnrygjtgwde');
define('NONCE_SALT',       '9qhy49kqktpqrhkjxm9b089sarimdtoqlpqc9u0acphm15i3wkjmyhv4jbqd2ivy');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
