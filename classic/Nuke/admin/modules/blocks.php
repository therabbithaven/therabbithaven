<?php
// ----------------------------------------------------------------------
// Copyright (c) 2002-2004 Shawn McKenzie
// http://spidean.mckenzies.net
// ----------------------------------------------------------------------
// LICENSE
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License (GPL)
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// To read the license please visit http://www.gnu.org/copyleft/gpl.html
// ----------------------------------------------------------------------

if (!eregi("admin.php", $_SERVER['PHP_SELF'])) { die ("Access Denied"); }

global $autoblock;

$thename = $GLOBALS['thename'];
$version = $GLOBALS['Version_Num'];
$abnuke = "abnuke.php";

if ($version >= 7) {
    $abnuke = "abnuke70.php";
}
elseif ($version >= 6.5) {
    $abnuke = "abnuke65.php";
}
@include("modules/AutoTheme/autotheme.cfg");
@include("themes/$thename/autoblock.cfg");
@include("themes/$thename/theme.cfg");
include(dirname(__FILE__)."/$abnuke");

?>
