<?php
// ----------------------------------------------------------------------
// Copyright (c) 2002-2004 Shawn McKenzie
// http://spidean.mckenzies.net
// ----------------------------------------------------------------------
// LICENSE
//
// This software is the copyrighted material of Shawn McKenzie. AutoTheme is
// licensed for use on (1) web site installation only. See the license.txt for your
// specific license.
// 
// YOU AGREE NOT TO COPY OR DISTRIBUTE THIS SOFTWARE OR CHANGE THE SOURCE CODE
// WITHOUT PRIOR WRITTEN PERMISSION. PLEASE SEND ANY QUESTIONS OR COMMENTS TO
// AUTOTHEME@MCKENZIES.NET.
// 
// SHAWN MCKENZIE, TAKES NO RESPONSIBILITY FOR ANY DAMAGE THAT THIS SOFTWARE MIGHT
// CAUSE.
//
// Additional licensing information is available from autotheme@mckenzies.net.
// 
// If you disagree with any of these terms, you are not authorized to use this
// software. If you do agree with these terms, please enjoy.
// ----------------------------------------------------------------------

$modversion['name'] = 'AutoTheme';
$modversion['version'] = '1.7';
$modversion['description'] = 'HTML Theme System';
$modversion['credits'] = 'docs/LICENSE.TXT';
$modversion['help'] = 'docs/INSTALL.TXT';
$modversion['changelog'] = 'docs/INSTALL.TXT';
$modversion['license'] = 'docs/LICENSE.TXT';
$modversion['official'] = 0;
$modversion['author'] = 'Shawn McKenzie';
$modversion['contact'] = 'http://spidean.mckenzies.net';
$modversion['admin'] = 1;
$modversion['securityschema'] = array('AutoTheme::' => '::');

?>
