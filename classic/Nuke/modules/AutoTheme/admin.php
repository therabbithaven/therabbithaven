<?php
// ----------------------------------------------------------------------
// Copyright (c) 2002-2004 Shawn McKenzie
// http://spidean.mckenzies.net
// ----------------------------------------------------------------------
// LICENSE
//
// This software is the copyrighted material of Shawn McKenzie. AutoTheme is
// licensed for use on (1) web site installation only. See the license.txt for your
// specific license.
// 
// YOU AGREE NOT TO COPY OR DISTRIBUTE THIS SOFTWARE OR CHANGE THE SOURCE CODE
// WITHOUT PRIOR WRITTEN PERMISSION. PLEASE SEND ANY QUESTIONS OR COMMENTS TO
// AUTOTHEME@MCKENZIES.NET.
// 
// SHAWN MCKENZIE, TAKES NO RESPONSIBILITY FOR ANY DAMAGE THAT THIS SOFTWARE MIGHT
// CAUSE.
//
// Additional licensing information is available from autotheme@mckenzies.net.
// 
// If you disagree with any of these terms, you are not authorized to use this
// software. If you do agree with these terms, please enjoy.
// ----------------------------------------------------------------------

if (!eregi("admin.php", $_SERVER['PHP_SELF'])) { die ("Access Denied"); }

define('AUTOTHEME_ADMIN_LOADED', TRUE);
define('MOD_NAME', 'AutoTheme');

$atdir = "modules/AutoTheme/";

include_once($atdir."includes/atAPI.php");
atAdminInit($atdir);

$GLOBALS['HTTP_GET_VARS'] = $_GET;
$GLOBALS['HTTP_POST_VARS'] = $_POST;

/* main admin function */
function AutoTheme_admin_main($var)
{
    $autoconfig = atGetAutoConfig();
    extract($autoconfig);
	
	extract($var);

    $adminlinks = "admin.php?module=". MOD_NAME;
    $modlist = list_mods();
    
    AutoThemeHeader();    
    OpenTable();
    echo "<b><a href=\"$adminlinks&op=athemes\">"._AT_AUTOTHEMES."</a></b></td><td>"._AT_ATDESCR."</td></tr>\n"
    ."<tr><td><b><a href=\"$adminlinks&op=cmdedit\">"._AT_COMMANDS."</a></b></td><td>"._AT_CMDDESCR."</td></tr>\n"
    ."<tr><td><b><a href=\"$adminlinks&op=extras\">"._AT_EXTRAS."</a></b></td><td>"._AT_EXTRASDESCR;
    CloseTable();
    
    if ($cache['on']) {
    	$oyes = "checked";
    }
    else {
    	$ono ="checked";
    }
    /*if (!isset($cache['db']) || $cache['db']) {
    	$dyes = "checked";
    }
    else {
    	$dno ="checked";
    }*/
    if (!$cache['expire']) {
    	$cache_expire = 3600;
    }
    else {
    	$cache_expire = $cache['expire'];
    }
    OpenTable();
    echo "<b>"._AT_CACHEOPTIONS."</b>\n";
    OpenTable();
    echo "<form method=\"POST\" action=\"admin.php\">\n"
    ."  <input type=\"hidden\" name=\"op\" value=\"updatemain\">\n"
    ."  <input type=\"hidden\" name=\"module\" value=\"". MOD_NAME."\">\n"
    . _AT_ENABLECACHE
    ."</td><td>"
    ."<input type=\"radio\" name=\"cache_on\" value=\"1\" $oyes>"._YES."\n"
    ."<input type=\"radio\" name=\"cache_on\" value=\"0\" $ono>"._NO."\n"
    ."</td></tr><tr><td>"
    ._AT_CACHETIME
    ."</td><td>"
    ."<input type=\"text\" name=\"cache_expire\" value=\"$cache_expire\">\n"
    /*."</td></tr><tr><td>"
    ._AT_EXPIREONDB
    ."</td><td>"
     ."<input type=\"radio\" name=\"db_expire\" value=\"1\" $dyes>"._YES."\n"
    ."<input type=\"radio\" name=\"db_expire\" value=\"0\" $dno>"._NO."\n"*/
    ."</td></tr><tr><td>"
    ."<input type=\"submit\" value=\""._AT_SAVE."\" name=\"B1\"></form>\n"
    ."</td><td>";
    CloseTable();
    echo "<br />\n";
    
    OpenTable();
    echo "<b>"._AT_MODULENOCACHE."</b><br /><br />\n";
    foreach ($cache['exclude'] as $mod) {
    	echo "$mod&nbsp;<a href=\"$adminlinks&op=updatemain&mod=$mod\">[ "._AT_REMOVE." ]</a><br />";
    }
    echo "<br />";
    echo "<form method=\"POST\" action=\"admin.php\">\n"
    ."<b>"._AT_ADDEXCLUDEMODULE."</b><br />\n"
    ."<select name=\"mod\">\n";
    
    foreach ($modlist as $mod) {
        if (!in_array($mod, $cache['exclude'])) {
        	echo "  <option>$mod</option>\n";
        }
    }
    echo "</select><br /><br />\n"
    ."<input type=\"hidden\" name=\"op\" value=\"updatemain\">\n"
    ."<input type=\"hidden\" name=\"module\" value=\"". MOD_NAME."\">\n"
    ."<input type=\"submit\" value=\""._AT_ADD."\" name=\"add\">\n"
    ."</form>\n";
    CloseTable();
    CloseTable();
    AutoThemeFooter();
}

function AutoTheme_admin_updatemain($var)
{
	extract($var);
	
	$autoconfig = atGetAutoConfig();
	extract($autoconfig);
	
	if (isset($mod)) {
		if (isset($add)) {
			$cache['exclude'][] = $mod;
		}
		else {
			$key = array_search($mod, $cache['exclude']);
			unset($cache['exclude'][$key]);
		}
	}
	elseif (isset($cache_on)) {
		$cache['on'] = $cache_on;
		$cache['expire'] = $cache_expire;
		/*$cache['db'] = $db_expire;*/
	}
	$var = compact("cache");	
	SaveATConfig($var);
	
	Header("Location: admin.php?module=". MOD_NAME ."&op=main");
}

function AutoTheme_admin_extras($var)
{
    $autoconfig = atGetAutoConfig();
    extract($autoconfig);

    extract($var);
    AutoThemeHeader(_AT_EXTRAS);
    
	OpenTable();
    echo "<form method=\"POST\" action=\"admin.php\">\n"
    ."  <input type=\"hidden\" name=\"op\" value=\"updateextras\">\n"
    ."  <input type=\"hidden\" name=\"module\" value=\"". MOD_NAME."\">\n"
    ."  <b>"._AT_EXTRANAME."</b></td><td><b>"._AT_DESCRIPTION."</b></td><td><b>"._AT_ENABLE."</b></td><td><b>"._AT_ACTION."</b></td></tr><tr><td>\n";
    
    if (!$ext = atAutoGetVar("extra")) {
        $ext = atExtraScan($extradir);
    }
    if (!$ext) {
        return;
    }
    ksort($ext);
    foreach ($ext as $id => $info) {
        $yes = $no = "";
        
        $extraname = $id;
                
        $name = "_AT_".strtoupper($extraname)."_NAME";
        $descr = "_AT_".strtoupper($extraname)."_DESCRIPTION";
        
        if (!defined($descr)) {
        	$descr = $info['description'];
        }
        else {
        	$descr = constant($descr);
        }
        if (!defined($name)) {
        	$name = $info['name'];
        }
        else {
        	$name = constant($name);
        }
        if ($autoextra[$id]) {
            $yes = "checked";
        }
        else {
            $no = "checked";
        }
        echo "$name</td><td>$descr</td><td>"
        ."<input type=\"hidden\" name=\"extra[]\" value=\"$id\">\n"
        ."<input type=\"radio\" name=\"$id\" value=\"1\" $yes>"._YES."\n"
        ."<input type=\"radio\" name=\"$id\" value=\"0\" $no>"._NO."</td><td>";

        if ($info['atadmin'] && function_exists($info['atadmin']) && $yes == "checked") {
        	echo "[ <a href=\"admin.php?module=". MOD_NAME."&op=extraops&extraname=".htmlentities(urlencode($id))."\">"._AT_CONFIGURE."</a> ]";
        }
        elseif ($info['atadmin'] && function_exists($info['atadmin']) && $yes != "checked") {
            echo "[ "._AT_CONFIGURE." ]";
        }
        if ($info['themeadmin'] && function_exists($info['themeadmin'])) {
            echo "*";
        }
        if ($info['modadmin'] && function_exists($info['modadmin'])) {
            echo "**";
        }
        echo "</td></tr><tr><td>";
    }
    echo "</td></tr><tr><td><input type=\"submit\" value=\""._AT_SAVE."\" name=\"B1\">\n"
    ."</form></td><td>&nbsp;\n";

    CloseTable();
    echo _AT_EXTRANOTE;
    AutoThemeFooter();
}
function AutoTheme_admin_extraops($var)
{	
	$autoconfig = atGetAutoConfig();
    extract($autoconfig);

    extract($var);
	
	$extra = atExtraLoad($extraname);
	
	$description = "_AT_".strtoupper($extraname)."_DESCRIPTION";
    $name = "_AT_".strtoupper($extraname)."_NAME";
    $help = "_AT_".strtoupper($extraname)."_HELP";
    
    if (!defined($description)) {
    	$description = $extra['description'];
    }
    else {
    	$description = constant($description);
    }
    if (!defined($name)) {
    	$name = $extra['name'];
    }
    else {
    	$name = constant($name);
    }    
    if (!defined($help)) {
    	$help = _AT_NOHELP;
    }
    else {
    	$help = constant($help);
    }
    if ($thememod) {
        AutoTheme_admin_links($themedir, "$themedir  >  $thememod > $modops > $name");
        $extrafunc = $extra['modadmin'];
        
        $themepath = get_theme_path($themedir);

        $themeconfig = atLoadThemeConfig($themepath);
        extract($themeconfig);
    }
	elseif ($themedir) {
        AutoTheme_admin_links($themedir, "$themedir  >  ".$extra['name']);
        $extrafunc = $extra['themeadmin'];
        
        $themepath = get_theme_path($themedir);

        $themeconfig = atLoadThemeConfig($themepath);
        extract($themeconfig);
    }
    elseif (!$themedir) {
        AutoThemeHeader($extra['name']);	
	    $extrafunc = $extra['atadmin'];
    }
    if (is_array($$extraname)) {
        if ($thememod) {
        	if ($modops) {
        		$args = ${$extraname}[$thememod][$modops];
        	}
        	else {
        		$args = ${$extraname}['default'];
			}
        }
        else {
        	$args = $$extraname;
        }
    }
    else {
        $args = array();
    }
    if (function_exists("$extrafunc")) {
    	$output = $extrafunc($args);
    }
    else {
        Header("Location: admin.php?module=". MOD_NAME."&op=extras");
    }
    OpenTable();
    
    echo "<b>"._AT_DESCRIPTION."</b>: $description<br />";
    echo "<b>"._AT_VERSION."</b>: ".$extra['version']."<br />";
    echo "<b>"._AT_AUTHOR."</b>: ".$extra['author']."<br />";
    echo "<b>"._AT_CONTACT."</b>: ".$extra['contact']."<br />";
    CloseTable();
    
    echo "<br /><b>$name</b>: ".$help."<br /><br />";    
    
    OpenTable();
    echo "<form method=\"POST\" action=\"admin.php\">\n"
    ."  <input type=\"hidden\" name=\"op\" value=\"updateextraops\">\n"
	."  <input type=\"hidden\" name=\"module\" value=\"". MOD_NAME ."\">\n"
	."  <input type=\"hidden\" name=\"themedir\" value=\"$themedir\">\n"
	."  <input type=\"hidden\" name=\"extraname\" value=\"$extraname\">\n"
	."  <input type=\"hidden\" name=\"thememod\" value=\"$thememod\">\n"
	."  <input type=\"hidden\" name=\"modops\" value=\"$modops\">\n"
    .$output."<br />"
	."  <input type=\"submit\" value=\""._AT_SAVE."\" name=\"B1\">\n"
    ."</form>\n";
    CloseTable();
    AutoThemeFooter();
}

/* Update optional settings */
function AutoTheme_admin_updateextraops($var)
{
    extract($var);

    $extraname = $var['extraname'];
    
    if ($themedir) {
    	$themepath = get_theme_path($themedir);
    	$themeconfig = atLoadThemeConfig($themepath);
    	extract($themeconfig);
    }
    unset($var['op'], $var['module'], $var['extraname'], $var['themedir'], $var['B1'], $var['thememod'], $var['modops']);

    foreach ($var as $key => $val) {
        if (is_array($val)) {
            foreach ($val as $k => $v) {
                if ($v != "") {
                    $extra[$key][$k] = $v;
                }
            }
        }
        else {
            $extra[$key] = $val;
        }
    }
    if ($themedir) {
    	if ($thememod) {
    		if ($modops) {
    			${$extraname}[$thememod][$modops] = $extra;
    		}
    		else {
    			${$extraname}['default'] = $extra;
    		}    			
    	}
    	else {
    		${$extraname} = $extra;
    	}
    	$var = compact($extraname);
        SaveThemeConfig($themedir, $var);
        Header("Location: admin.php?module=". MOD_NAME."&op=atmain&themedir=$themedir");
    }
    else {
    	$extravars = $extra;
    	$$extraname = $extravars;  	
        $var = compact($extraname);
        SaveATConfig($var);
        Header("Location: admin.php?module=". MOD_NAME."&op=extras");
    }
}

/* Update main settings */
function AutoTheme_admin_updateextras($var)
{
    extract($var);
    
    foreach ($extra as $name) {
    	$autoextra[$name] = $$name;
    }
    $var = compact("autoextra");
    SaveATConfig($var);

    Header("Location: admin.php?module=". MOD_NAME."&op=extras");
}

/* Main autoblock settings form */
function AutoTheme_admin_ablocks($var)
{
    extract($var);
    
    $autoconfig = atGetAutoConfig();
    extract($autoconfig);
    
    AutoTheme_admin_links($themedir, "$themedir  >  "._AT_ACTIVEAUTOBLOCKS);
    $themepath = get_theme_path($themedir);
    $themeconfig = atLoadThemeConfig($themepath);
    extract($themeconfig);

    if (($posfield = AutoThemeCheckTable()) !== true) {
        OpenTable();
        echo "<center><b>"._AT_AUTOBLOCKSNOTACTIVE."</b><br />\n"
        ."[ <a href=\"admin.php?module=". MOD_NAME."&op=updatetable&posfield=$posfield&themedir=$themedir\">"._AT_UPDATETABLE."</a> ]</center>\n";
        CloseTable();
        AutoThemeFooter();
        exit;
    }
    OpenTable();
    echo "<form method=\"POST\" action=\"admin.php\">\n"
    ."  <input type=\"hidden\" name=\"op\" value=\"updateablock\">\n"
    ."  <input type=\"hidden\" name=\"themedir\" value=\"$themedir\">\n"
    ."  <input type=\"hidden\" name=\"module\" value=\"". MOD_NAME."\">\n"
    ."<b>"._AT_AUTOBLOCKS."</b></td><td><b>"._AT_NAME."</b></td><td><b>"._AT_ACTION."</b></td></tr><tr><td>\n";

    if ($autoblock){
        ksort($autoblock);
        foreach ($autoblock as $key => $ablock){
            echo "autoblock$key</td><td><input type=\"text\" name=\"ablocktemp[$key]\" value=\"".atDisplayVar($ablock)."\" size=\"20\"></td><td>\n"
            ."[ <a href=\"admin.php?module=". MOD_NAME."&op=delablock&ablock=$key&themedir=".htmlentities(urlencode($themedir))."\">"._AT_REMOVE."</a> ]\n"
            ."</td></tr><tr><td>\n";
        }
        echo "<input type=\"submit\" value=\""._AT_SAVE."\" name=\"B1\"></td><td>&nbsp;\n";
    }
    else {
        echo _AT_NOACTIVEAUTOBLOCKS;
    }
    CloseTable();
    echo "</form><br />\n";
    OpenTable();
    echo "<form method=\"POST\" action=\"admin.php\">\n"
    ."<b>"._AT_ADDAUTOBLOCK."</b><br />\n"
    ."<input type=\"text\" name=\"ablock\" size=\"20\">\n"
    ."<input type=\"hidden\" name=\"op\" value=\"addablock\">\n"
    ."<input type=\"hidden\" name=\"themedir\" value=\"$themedir\">\n"
    ."<input type=\"hidden\" name=\"module\" value=\"". MOD_NAME."\">\n"
    ."<input type=\"submit\" value=\""._AT_ADD."\" name=\"B1\">\n"
    ."</form>\n";
    CloseTable();
    AutoThemeFooter();
}

/* Update autoblock settings */
function AutoTheme_admin_updateablock($var)
{
    extract($var);
    
    $autoconfig = atGetAutoConfig();
    extract($autoconfig);
    
    if ($themedir) {
        $themeconfig = atLoadThemeConfig("themes/$themedir");
        extract($themeconfig);
    }
    else {
    	$themedir = "";
    }
    foreach ($ablocktemp as $key => $ablock){
        if ($ablock) {
            $autoblock[$key] = atExportVar($ablock);
        }
    }
    $autoblock = array_unique($autoblock);

    $var = compact("autoblock");
    SaveThemeConfig($themedir, $var);
    
    Header("Location: admin.php?module=". MOD_NAME."&op=ablocks&themedir=$themedir");
}

/* Add an autoblock */
function AutoTheme_admin_addablock($var)
{
    extract($var);
    $autoconfig = atGetAutoConfig();
    extract($autoconfig);
    
    $ablock = atExportVar($ablock);
    
    if ($themedir) {
        $themeconfig = atLoadThemeConfig("themes/$themedir");
        extract($themeconfig);
    }
    else {
    	$themedir = "";
    }
    if ($autoblock) {
        if ($ablock && !in_array($ablock, $autoblock)) {
            $autoblock[] = $ablock;
        }
    }
    elseif ($ablock) {
        $autoblock[1] = $ablock;
    }
    $var = compact("autoblock");
    SaveThemeConfig($themedir, $var);
    
    Header("Location: admin.php?module=". MOD_NAME."&op=ablocks&themedir=$themedir");
}

/* Delete an autoblock */
function AutoTheme_admin_delablock($var)
{
    extract($var);
    
    if (!isset($confirmed)) {
        AutoTheme_admin_confirmdel($var);
        exit;
    }
    extract($confirmed);

    if ($del == _YES) {
    	$autoconfig = atGetAutoConfig();
    	extract($autoconfig);
    	
    	if ($themedir) {
    		$themeconfig = atLoadThemeConfig("themes/$themedir");
    		extract($themeconfig);
    	}
    	else {
    		$themedir = "";
    	}
        unset($autoblock[$ablock]);
    
        $var = compact("autoblock");
        SaveThemeConfig($themedir, $var);
    }
    Header("Location: admin.php?module=". MOD_NAME."&op=ablocks&themedir=$themedir");
}

/* Main autotheme selection form */
function AutoTheme_admin_athemes($var)
{
    $themelist = list_themes();
    
    extract($var);
    
    AutoThemeHeader(_AT_ACTIVEAUTOTHEMES);
    OpenTable();
    
    echo "<b>"._AT_AUTOTHEMES."</b></td><td><b>"._AT_MULTISITE."</b></td><td><b>"._AT_ACTION."</b></td></tr><tr><td>\n";

    if (is_array($themelist)) {
        foreach ($themelist as $key => $themedir) {
            $prevtheme = get_theme_name($themedir);

            if (($multisite = get_multisite_name($themedir)) === false) {
                $multisite = "&nbsp;";
            }
            echo "$prevtheme</td><td>$multisite</td><td>\n"
            ."[ <a href=\"index.php?theme=$prevtheme\" target=\"_AT_blank\">"._AT_PREVIEW."</a> | \n"
            ."[ <a href=\"admin.php?module=". MOD_NAME."&op=atmain&themedir=".htmlentities(urlencode($themedir))."\">"._AT_CONFIGURE."</a> ]\n"
            ."</td></tr><tr><td>\n";
        }
    }
    else {
        echo _AT_NOACTIVEAUTOTHEMES;
    }
    CloseTable();
    echo "<br />\n";
    OpenTable();
    echo "<form method=\"POST\" action=\"admin.php\">\n"
    ."<b>"._AT_CREATENEWTHEME."</b><br />\n"
    ."<input type=\"text\" name=\"themedir\" size=\"20\">\n"
    ."<input type=\"hidden\" name=\"op\" value=\"addatheme\">\n"
    ."<input type=\"hidden\" name=\"module\" value=\"".MOD_NAME."\">\n"
    ."<input type=\"submit\" name=\"action\" value=\""._AT_BLANK."\">\n"
    ."<input type=\"submit\" name=\"action\" value=\""._AT_EXAMPLE."\">\n"
    ."</form>\n";
    CloseTable();
    AutoThemeFooter();
}

function AutoTheme_admin_addatheme($var)
{
    extract($var);

    $old_mask = umask(0);

    if (file_exists("themes/$themedir")) {
        display_error("<b>"._AT_ERROR."</b>: themes/$themedir/ "._AT_DIREXISTS);
    }
    if (!is_writeable("themes/")) {
        display_error("<b>"._AT_ERROR."</b>: themes/ "._AT_NOTWRITABLE);
    }
    mkdir("themes/$themedir");
    mkdir("themes/$themedir/images");
    mkdir("themes/$themedir/style");
    
    if ($action == _AT_BLANK) {
        $fromdir = "modules/AutoTheme/tools/themes/AT-Blank";
    }
    if ($action == _AT_EXAMPLE) {
        $fromdir = "modules/AutoTheme/tools/themes/AT-Example";
    }
    if ($handle = @opendir("$fromdir/images/")) {
            while (false !== ($file = @readdir($handle))) {
                if ($file == "." || $file == "..") {
                    continue;
                }
                copy("$fromdir/images/$file", "themes/$themedir/images/$file");
            }
            closedir($handle);
    }
    if ($handle = @opendir("$fromdir/style/")) {
            while (false !== ($file = @readdir($handle))) {
                if ($file == "." || $file == "..") {
                    continue;
                }
                copy("$fromdir/style/$file", "themes/$themedir/style/$file");
            }
            closedir($handle);
    }
    if ($handle = @opendir("$fromdir/")) {
            while (false !== ($file = @readdir($handle))) {
                if ($file == "." || $file == "..") {
                    continue;
                }
                copy("$fromdir/$file", "themes/$themedir/$file");
            }
            closedir($handle);
    }
    Header("Location: admin.php?module=".MOD_NAME."&op=general&themedir=".htmlentities(urlencode($themedir)));
}

/* Main autotheme admin function */
function AutoTheme_admin_atmain($var)
{
    extract($var);
    
    if (@file_exists("themes/$themedir/theme.cfg")) {
        include("themes/$themedir/theme.cfg");
    }
    elseif (@file_exists("$themedir/theme.cfg")) {
        include("$themedir/theme.cfg");
    }
    if (isset($import)) {
        if ($import == "skip") {
            $var = compact("themedir");
            SaveThemeConfig($themedir, $var);
            return AutoTheme_admin_atmain($var);
        }
        else {
            $var = compact("themedir", "import");
            AutoTheme_admin_import($var);
            $var = compact("themedir");
            return AutoTheme_admin_atmain($var);
        }
    }
    AutoTheme_admin_links($themedir, $themedir);
    $adminlinks = "admin.php?module=". MOD_NAME;

    OpenTable();
    $pathparts = explode("/", $themedir);
    $thistheme = array_pop($pathparts);

    if (atRunningGetVar("thename") == $thistheme && !isset($edit)) {
        echo "<b>"._AT_NOTICE.":</b> "._AT_ACTIVETHEMEMSG."<br /><br />"
        ."[ <a href=\"admin.php?module=".MOD_NAME."&op=atmain&themedir=$themedir&edit=1\">"._AT_CONTINUE."</a> ]"
        ."  [ <a href=\"admin.php?module=".MOD_NAME."&op=athemes\"><b>"._AT_CANCEL."</b></a> ]";

        CloseTable();
        AutoThemeFooter();

        return;
    }
    if (is_array($block_display) || is_array($miscellaneous)) {
        echo "<b>"._AT_NOTICE.":</b> AT-Lite .6 - "._AT_OLDTHEMEMSG."<br /><br />"
        ."[ <a href=\"admin.php?module=".MOD_NAME."&op=atmain&themedir=$themedir&import=lite_6\">"._AT_IMPORT."</a> ]"
        ."  [ <a href=\"admin.php?module=".MOD_NAME."&op=atmain&themedir=$themedir&import=skip\">"._AT_SKIP."</a> ]"
        ."  [ <a href=\"admin.php?module=".MOD_NAME."&op=athemes\"><b>"._AT_CANCEL."</b></a> ]";

        CloseTable();
        AutoThemeFooter();

        return;
    }
    elseif (!isset($atversion)) {
        echo "<b>"._AT_NOTICE.":</b> AutoTheme 1.0 - "._AT_OLDTHEMEMSG."<br /><br />"
        ."[ <a href=\"admin.php?module=".MOD_NAME."&op=atmain&themedir=$themedir&import=1_0\">"._AT_IMPORT."</a> ]"
        ."  [ <a href=\"admin.php?module=".MOD_NAME."&op=atmain&themedir=$themedir&import=skip\">"._AT_SKIP."</a> ]"
        ."  [ <a href=\"admin.php?module=".MOD_NAME."&op=athemes\"><b>"._AT_CANCEL."</b></a> ]";

        CloseTable();
        AutoThemeFooter();

        return;
    }
    echo "<b><a href=\"$adminlinks&op=general&themedir=".htmlentities(urlencode($themedir))."\">"._AT_GENERAL."</a></b></td><td>"._AT_GENDESCR."</td></tr>\n"
    ."<tr><td><b><a href=\"$adminlinks&op=modmain&themedir=".htmlentities(urlencode($themedir))."\">"._AT_CUSTMODULES."</a></b></td><td>"._AT_MODDESCR."</td></tr>\n"
    ."<tr><td><b><a href=\"$adminlinks&op=ablocks&themedir=".htmlentities(urlencode($themedir))."\">"._AT_AUTOBLOCKS."</a></b></td><td>"._AT_THEMEABDESCR."</td></tr>\n"
    ."<tr><td><b><a href=\"$adminlinks&op=cmdedit&themedir=".htmlentities(urlencode($themedir))."\">"._AT_COMMANDS."</a></b></td><td>"._AT_THEMECMDDESCR;

    $autoconfig = atGetAutoConfig();
    extract($autoconfig);

    if (!$ext = atAutoGetVar("extra")) {
        list($ext, $cmd) = atExtraScan($extradir);
    }
    if (!$ext) {
        return;
    }
    foreach ($ext as $id => $info) {
        if ($info['themeadmin'] && function_exists($info['themeadmin']) && $autoextra[$id]) {
        	echo "<tr><td><b><a href=\"$adminlinks&op=extraops&extraname=".htmlentities(urlencode($id))."&themedir=".htmlentities(urlencode($themedir))."\">".$info['name']."</a></b></td><td>".$info['description']."</td></tr>\n";
        }
    }
    CloseTable();
    AutoThemeFooter();
    FixupThemeConfig($themedir);
}

/* Import an autotheme */
function AutoTheme_admin_import($var)
{
    extract($var);
    
    if (@file_exists("themes/$themedir/theme.cfg")) {
        include("themes/$themedir/theme.cfg");
        @copy("themes/$themedir/theme.cfg", "themes/$themedir/theme.cfg.bak");
    }
    elseif (@file_exists("$themedir/theme.cfg")) {
        include("$themedir/theme.cfg");
        @copy("$themedir/theme.cfg", "$themedir/theme.cfg.bak");
    }
    if ($import == "1_0") {
        $temp['default'] = $template['default'];
        $tempdisplay['default'] = $blockdisplay['default'];
        $tempstyle['default'] = $style['default'];
        
        foreach ($template as $thememod => $innerarray) {
            if ($thememod != "default" && !isset($template[$thememod]['default'])) {
                $temp[$thememod]['default'] = $template[$thememod];
                $tempdisplay[$thememod]['default'] = $blockdisplay[$thememod];
                $tempstyle[$thememod]['default'] = $style[$thememod];
            }
            elseif ($thememod != "default") {
                $temp[$thememod] = $template[$thememod];
                $tempdisplay[$thememod] = $blockdisplay[$thememod];
                $tempstyle[$thememod] = $style[$thememod];
            }
        }
        unset($template, $blockdisplay, $style);
        $template = $temp;
        $blockdisplay = $tempdisplay;
        $style = $tempstyle;

        $var = compact("template", "blockdisplay", "style", "blocktemplate");
        SaveThemeConfig($themedir, $var);
        return;
    }
    foreach ($template as $name => $value) {
        $key = str_replace("area", "Area", $name);
        $temp[$key] = $value;
    }
    foreach ($block_display as $name => $value) {
        $key = str_replace("area", "Area", $name);
        $block[$key] = (string)(int)$value;
    }
    unset($template);

    $template['default'] = $temp;
    $template['default']['altsummary'] = (string)(int)$alternate_summary;
    $blockdisplay['default'] = $block;

    extract($miscellaneous);
    $color1 = $bgcolor1;
    $color2 = $bgcolor2;
    $color3 = $bgcolor3;
    $color4 = $bgcolor4;
    $color5 = $textcolor1;
    $color6 = $textcolor2;
    $logoimg = $logo;
    $striphead = (int)$strip_head;
    $style['default'] = compact("stylesheet", "logoimg", "color1", "color2", "color3", "color4", "color5", "color6", "striphead");

    for ($index = 0; $index <= 1; $index++) {
        if ($custom_module) {
            foreach ($custom_module as $thememod => $innerarray) {
				if (is_array($thememod)) {
					foreach ($thememod as $name => $value) {
                    	$key = str_replace("area", "Area", $name);

                    	if (eregi("block", $key)) {
                        	$temp[$key] = $value;
                    	}
                    	elseif (eregi("Area", $key)) {
                        	$block[$key] = (string)(int)$value;
                    	}
                	}
                }
                if ($index == 1) {
                    $thememod = "*HomePage";
                }
                $template[$thememod]['default'] = $temp;
                $blockdisplay[$thememod]['default'] = $block;

                $template[$thememod]['default'] = array_merge((array)$template['default'], (array)$template[$thememod]['default']);
                $blockdisplay[$thememod]['default'] = array_merge((array)$blockdisplay['default'], (array)$blockdisplay[$thememod]['default']);

                extract($custom_module);
                $color1 = $bgcolor1;
                $color2 = $bgcolor2;
                $color3 = $bgcolor3;
                $color4 = $bgcolor4;
                $color5 = $textcolor1;
                $color6 = $textcolor2;
                $logoimg = $logo;
                $style[$thememod]['default'] = compact("stylesheet", "logoimg", "color1", "color2", "color3", "color4", "color5", "color6", "striphead");
                $style[$thememod]['default'] = array_merge((array)$style['default'], (array)$style[$thememod]['default']);
            }
        }
    }
    $blocktemplate = $custom_block;

    $var = compact("template", "blockdisplay", "style", "blocktemplate");
    SaveThemeConfig($themedir, $var);
}

/* General theme settings */
function AutoTheme_admin_general($var)
{
    extract($var);
    AutoTheme_admin_links($themedir, "$themedir  >  "._AT_GENERAL);
    
    $adminlinks = "admin.php?module=". MOD_NAME;    
    echo "[ <a href=\"$adminlinks&op=blockmain&themedir=".htmlentities(urlencode($themedir))."\">"._AT_CUSTBLOCKS."</a> ]";

    $autoconfig = atGetAutoConfig();
    extract($autoconfig);
    
    if (!$ext = atAutoGetVar("extra")) {
        $ext = atExtraScan($extradir);
    }    
    foreach ($ext as $id => $info) {
        if ($info['modadmin'] && function_exists($info['modadmin']) && $autoextra[$id]) {
        	echo "[ <a href=\"$adminlinks&op=extraops&extraname=".htmlentities(urlencode($id))."&themedir=".htmlentities(urlencode($themedir))."&thememod=default\">".$info['name']."</a> ]";
        }
    }
    if (@file_exists("themes/$themedir/theme.cfg")) {
        include("themes/$themedir/theme.cfg");
    }
    elseif (@file_exists("$themedir/theme.cfg")) {
        include("$themedir/theme.cfg");
    }
    $config = array_merge((array)$template['default'], (array)$blockdisplay['default'], (array)$style['default']);
    $temps = $template['default'];
    $formvars = compact("temps", "config");
    
    AutoTheme_admin_generalform($var, $formvars);
    AutoThemeFooter();
}

/* Update general theme settings */
function AutoTheme_admin_updategeneral($var)
{
    extract($var);

    /*if (@file_exists("modules/". MOD_NAME."/autotheme.cfg")) {
        include("modules/". MOD_NAME."/autotheme.cfg");
    }*/
    if (@file_exists("themes/$themedir/theme.cfg")) {
        include("themes/$themedir/theme.cfg");
    }
    elseif (@file_exists("$themedir/theme.cfg")) {
        include("$themedir/theme.cfg");
    }
    $template['default'] = compact("main", "summary", "summary1", "summary2",
    "article", "altsummary", "leftblock", "centerblock", "rightblock", "table1", "table2");
    $blockdisplay['default'] = compact("left", "center", "right");
    $style['default'] = compact("stylesheet", "logoimg", "color1", "color2", "color3",
    "color4", "color5", "color6", "color7", "color8", "color9", "color10", "striphead", "head");

    if ($autoblock) {
        foreach ($autoblock as $key => $ablock) {
            $template['default']["autoblock".$key] = ${"autoblock".$key};
            $blockdisplay['default']["autoblock".$key] = $ablockdis[$key];
        }
    }
    $var = compact("altsummary", "template", "blockdisplay", "style");
    SaveThemeConfig($themedir, $var);

    Header("Location: admin.php?module=". MOD_NAME."&op=atmain&themedir=".htmlentities(urlencode($themedir)));
}

/* Main module selection form */
function AutoTheme_admin_modmain($var)
{
    extract($var);
    
    $modlist = list_mods();

    AutoTheme_admin_links($themedir, "$themedir  >  "._AT_CUSTMODULES);
    if (@file_exists("themes/$themedir/theme.cfg")) {
        include("themes/$themedir/theme.cfg");
    }
    elseif (@file_exists("$themedir/theme.cfg")) {
            include("$themedir/theme.cfg");
    }
    OpenTable();
    echo "<b>"._AT_MODULES."</b></td><td><b>"._AT_OPTIONS."<b></td><td><b>"._AT_ACTION."</b></td></tr><tr><td>\n";
    
    if (!$template['default']) {
        echo _AT_CONFGENERALFIRST;
        exit;
    }
    ksort($template);
    foreach ($template as $thememod => $innerarray){

       $pathparts = explode("/", $themedir);
       $prevtheme = array_pop($pathparts);

       if ($thememod != "default" && count($template) > 1) {
           foreach ($innerarray as $modops => $opsarray) {
                switch ($thememod) {
                    case "*AdminPages":
                    $thememod = "*AdminPages";
                    $prevpage = "admin.php?theme=$prevtheme";
                    break;
               
                    case "*HomePage":
                    $thememod = "*HomePage";
                    $prevpage = "index.php?theme=$prevtheme";
                    break;
               
                    case "*UserPages":
                    $thememod = "*UserPages";
                    $prevpage = "user.php?theme=$prevtheme";
                    break;

                    default:
                    $modstring = "&$modops";
                    if (atGetModStyle() == "new"){
                        $prevpage = "index.php?module=$thememod&theme=$prevtheme$modstring";
                    }
                    else {
                        $prevpage = "modules.php?op=modload&name=$thememod&file=index&theme=$prevtheme$modstring";
                    }
                    break;
                }
                echo "$thememod</td><td>$modops</td><td>\n";
                
                if ($modops == "default") {
                	echo "[ <a href=\"$prevpage\" target=\"_AT_blank\">"._AT_PREVIEW."</a> | \n";
                }
                else {
                	echo "[ "._AT_PREVIEW." | \n";
                }
                echo "[ <a href=\"admin.php?module=". MOD_NAME."&op=modgeneral&themedir=".htmlentities(urlencode($themedir))."&thememod=".htmlentities(urlencode($thememod))."&modops=".htmlentities(urlencode($modops))."\">"._AT_CONFIGURE."</a> | \n"
                ."<a href=\"admin.php?module=". MOD_NAME."&op=delmod&themedir=".htmlentities(urlencode($themedir))."&thememod=".htmlentities(urlencode($thememod))."&modops=".htmlentities(urlencode($modops))."\">"._AT_REMOVE."</a> ]\n"
                ."</td></tr><tr><td>\n";
           }
       }
       elseif (count($template) <= 1) {
           echo _AT_NOCUSTMODULES;
       }
    }
    echo "&nbsp;</td><td>&nbsp</td><td>&nbsp;";
    CloseTable();
    echo "<br />\n";
    OpenTable();
    echo "<form method=\"POST\" action=\"admin.php\">\n"
    ."<b>"._AT_ADDMODULE."</b><br />\n"
    ."<select name=\"thememod\">\n";
    foreach ($modlist as $mod) {
        echo "  <option>$mod</option>\n";
    }
    echo "</select><br />\n"
    ."<b>"._AT_MODULEOPS."</b><br />\n"
    ."<input type=\"text\" name=\"modops\" size=\"20\">\n"
    ."<input type=\"hidden\" name=\"themedir\" value=\"$themedir\">\n"
    ."<input type=\"hidden\" name=\"op\" value=\"addmod\">\n"
    ."<input type=\"hidden\" name=\"module\" value=\"". MOD_NAME."\">\n"
    ."<input type=\"submit\" value=\""._AT_ADD."\" name=\"B1\">\n"
    ."</form>\n";
    CloseTable();
    AutoThemeFooter();
}

/* Add a module */
function AutoTheme_admin_addmod($var)
{
   extract($var);

   if (@file_exists("themes/$themedir/theme.cfg")) {
        include("themes/$themedir/theme.cfg");
    }
    elseif (@file_exists("$themedir/theme.cfg")) {
            include("$themedir/theme.cfg");
    }
    if (!$modops) {
        $modops = "default";
    }
    if (!$blocktemplate['default']) {
    	$temp = $blocktemplate;
    	unset($blocktemplate);
    	$blocktemplate['default'] = $temp;
    	if (!$modops != "default") {
    		$blocktemplate[$thememod][$modops] = $temp;
    	}
    	$var = compact("blocktemplate");
    	SaveThemeConfig($themedir, $var);
    }    	
    if (!$template[$thememod]['default']) {
        $tmptemplate = $template[$thememod];
        $tmpblockdisplay = $blockdisplay[$thememod];
        $tmpstyle = $style[$thememod];
        $tmpblocktemplate = $blocktemplate[$thememod];
        $template[$thememod] = $blockdisplay[$thememod] = $style[$thememod] = array();
        $template[$thememod]['default'] = $tmptemplate;
        $blockdisplay[$thememod]['default'] = $tmpblockdisplay;
        $style[$thememod]['default'] = $tmpstyle;
        $blocktemplate[$thememod]['default'] = $tmpblocktemplate;
    }
    if ($thememod && !$template[$thememod][$modops]) {
        $template[$thememod][$modops] = $template['default'];
        $blockdisplay[$thememod][$modops] = $blockdisplay['default'];
        $style[$thememod][$modops] = $style['default'];
        $blocktemplate[$thememod][$modops] = $blocktemplate['default'];
    }
    $var = compact("template", "blockdisplay", "style", "blocktemplate");
    SaveThemeConfig($themedir, $var);
   
   Header("Location: admin.php?module=". MOD_NAME."&op=modgeneral&themedir=".htmlentities(urlencode($themedir))."&thememod=".htmlentities(urlencode($thememod))."&modops=".htmlentities(urlencode($modops)));
}

/* Delete a module */
function AutoTheme_admin_delmod($var)
{
    extract($var);
    
    if (!isset($confirmed)) {
        AutoTheme_admin_confirmdel($var);
        exit;
    }
    extract($confirmed);

    if ($del == _YES) {
        if (@file_exists("themes/$themedir/theme.cfg")) {
            include("themes/$themedir/theme.cfg");
        }
        elseif (@file_exists("$themedir/theme.cfg")) {
            include("$themedir/theme.cfg");
        }
        if ($modops == "default" && !isset($template[$thememod][$modops])) {
            unset($template[$thememod], $blockdisplay[$thememod], $style[$thememod], $blocktemplate[$thememod]);
        }
        else {
            unset($template[$thememod][$modops], $blockdisplay[$thememod][$modops], $style[$thememod][$modops], $blocktemplate[$thememod][$modops]);
        }
        $var = compact("template", "blockdisplay", "style", "blocktemplate");
        SaveThemeConfig($themedir, $var);
    }
    Header("Location: admin.php?module=". MOD_NAME."&op=modmain&themedir=".htmlentities(urlencode($themedir)));
}

/* Module settings */
function AutoTheme_admin_modgeneral($var)
{
    extract($var);

    AutoTheme_admin_links($themedir, "$themedir  >  $thememod  >  $modops");
    
    $adminlinks = "admin.php?module=". MOD_NAME;    
    echo "[ <a href=\"$adminlinks&op=blockmain&themedir=".htmlentities(urlencode($themedir))."&thememod=".htmlentities(urlencode($thememod))."&modops=".htmlentities(urlencode($modops))."\">"._AT_CUSTBLOCKS."</a> ]";
    
    $autoconfig = atGetAutoConfig();
    extract($autoconfig);
    
    if (!$ext = atAutoGetVar("extra")) {
        $ext = atExtraScan($extradir);
    }    
    foreach ($ext as $id => $info) {
        if ($info['modadmin'] && function_exists($info['modadmin']) && $autoextra[$id]) {
        	echo "[ <a href=\"$adminlinks&op=extraops&extraname=".htmlentities(urlencode($id))."&themedir=".htmlentities(urlencode($themedir))."&thememod=".htmlentities(urlencode($thememod))."&modops=".htmlentities(urlencode($modops))."\">".$info['name']."</a> ]";
        }
    }
	if (@file_exists("themes/$themedir/theme.cfg")) {
        include("themes/$themedir/theme.cfg");
    }
    elseif (@file_exists("$themedir/theme.cfg")) {
            include("$themedir/theme.cfg");
    }
    if (isset($template[$thememod][$modops])) {
        $template = $template[$thememod][$modops];
    }
    elseif ($template[$thememod]) {
        $template = $template[$thememod];
    }
    if (isset($style[$thememod][$modops])) {
        $style = $style[$thememod][$modops];
    }
    elseif ($style[$thememod]) {
        $style = $style[$thememod];
    }
    if (isset($blockdisplay[$thememod][$modops])) {
        $blockdisplay = $blockdisplay[$thememod][$modops];
    }
    elseif ($blockdisplay[$thememod]) {
        $blockdisplay = $blockdisplay[$thememod];
    }
    $config = array_merge((array)$template, (array)$blockdisplay, (array)$style);
    $temps = $template;
    $formvars = compact("temps", "config");

    AutoTheme_admin_generalform($var, $formvars);
    AutoThemeFooter();
}

/* Update module settings */
function AutoTheme_admin_updatemod($var)
{
    extract($var);

    /*if (file_exists("modules/". MOD_NAME."/autotheme.cfg")) {
        include("modules/". MOD_NAME."/autotheme.cfg");
    }*/
    if (file_exists("themes/$themedir/theme.cfg")) {
        include("themes/$themedir/theme.cfg");
    }
    elseif (file_exists("$themedir/theme.cfg")) {
            include("$themedir/theme.cfg");
    }
    /*$styleparts = explode("/", $stylesheet);
    $logoparts = explode("/", $logoimg);
    $stylesheet = array_pop($styleparts);
    $logoimg = array_pop($logoparts);*/
    
    if (!$template[$thememod]['default']) {
        $tmptemplate = $template[$thememod];
        $tmpblockdisplay = $blockdisplay[$thememod];
        $tmpstyle = $style[$thememod];
        $template[$thememod] = $blockdisplay[$thememod] = $style[$thememod] = array();
        $template[$thememod]['default'] = $tmptemplate;
        $blockdisplay[$thememod]['default'] = $tmpblockdisplay;
        $style[$thememod]['default'] = $tmpstyle;
    }
    $template[$thememod][$modops] = compact("main", "leftblock", "centerblock", "rightblock",
    "summary", "summary1", "summary2", "article", "altsummary", "table1", "table2");
    $blockdisplay[$thememod][$modops] = compact("left", "center", "right");
    $style[$thememod][$modops] = compact("stylesheet", "logoimg", "color1", "color2", "color3",
    "color4", "color5", "color6", "color7", "color8", "color9", "color10", "striphead", "head");

    if ($autoblock) {
        foreach ($autoblock as $key => $ablock) {
            $template[$thememod][$modops]["autoblock".$key] = ${"autoblock".$key};
            $blockdisplay[$thememod][$modops]["autoblock".$key] = $ablockdis[$key];
        }
    }
    $var = compact("template", "blockdisplay", "style");
    SaveThemeConfig($themedir, $var);
    
    Header("Location: admin.php?module=". MOD_NAME."&op=modmain&themedir=".htmlentities(urlencode($themedir))."&thememod=$thememod");
}

/* Main block and settings form */
function AutoTheme_admin_blockmain($var)
{
    $blocklist = atBlockList();

    extract($var);
    
    if (file_exists("themes/$themedir/theme.cfg")) {
        $filelist = list_files("themes/$themedir", "htm");
        include("themes/$themedir/theme.cfg");
    }
    elseif (file_exists("$themedir/theme.cfg")) {
        $filelist = list_files($themedir, "htm");
        include("$themedir/theme.cfg");
    }    	
    if ($modops) {
    	$where = "$thememod  >  $modops";
    }
    else {
    	$thememod = "default";
    	$where = _AT_GENERAL;
    }
    if (!is_array($blocktemplate['default'])) {
    	$temp = $blocktemplate;
    	unset($blocktemplate);
    	$blocktemplate['default'] = $temp;
    	if ($modops) {
    		$blocktemplate[$thememod][$modops] = $temp;
    	}
    	else {
    		$blocktemplate[$thememod]= $temp;
    	}    		
    	$var = compact("blocktemplate");
    	SaveThemeConfig($themedir, $var);
    }    	
    AutoTheme_admin_links($themedir, "$themedir  >  $where > "._AT_CUSTBLOCKS);
    
    OpenTable();
    echo "<form method=\"POST\" action=\"admin.php\">\n"
    ."  <input type=\"hidden\" name=\"op\" value=\"updateblock\">\n"
    ."  <input type=\"hidden\" name=\"module\" value=\"". MOD_NAME."\">\n"
    ."  <input type=\"hidden\" name=\"themedir\" value=\"$themedir\">\n"
    ."  <input type=\"hidden\" name=\"thememod\" value=\"$thememod\">\n"
    ."  <input type=\"hidden\" name=\"modops\" value=\"$modops\">\n"
    ."<b>"._AT_BLOCKS."</b></td><td><b>"._AT_FILENAME."</b></td><td><b>"._AT_ACTION."</b></td></tr><tr><td>\n";

    if ($modops) {
    	$blocktemplate = $blocktemplate[$thememod][$modops];
    }
    else {
    	$blocktemplate = $blocktemplate[$thememod];
    }    
    if ($blocktemplate){
        ksort($blocktemplate);
        
        foreach ($blocktemplate as $themeblock => $filename){
            $blockvar = strtolower(preg_replace("^\W|_^", "", $themeblock));
            $varval = htmlentities(urlencode($themeblock));
            echo $themeblock."</td>\n"
            .select_file($blockvar."file", $filename, $filelist)."<td>\n"
            ."[ <a href=\"admin.php?module=". MOD_NAME."&op=delblock&themedir=".htmlentities(urlencode($themedir))."&themeblock=$varval&thememod=".htmlentities(urlencode($thememod))."&modops=".htmlentities(urlencode($modops))."\">"._AT_REMOVE."</a> ]\n"
            ."</td></tr><tr><td>\n";
        }
        echo "<input type=\"submit\" value=\""._AT_SAVE."\" name=\"B1\">\n"
        ."</form></td><td>\n";
    }
    else {
        echo _AT_NOCUSTBLOCKS;
    }
    echo "&nbsp;</td><td>&nbsp;";
    CloseTable();
    echo "<br />\n";
    OpenTable();
    echo "<form method=\"POST\" action=\"admin.php\">\n"
    ."<b>"._AT_ADDBLOCK."</b><br />\n"
    ."<select name=\"themeblock\">\n";
    foreach ($blocklist as $block) {
        if (!array_key_exists($block, $blocktemplate)) {
            echo "<option>$block</option>";
        }
    }
    echo "</select>"
    ."<input type=\"hidden\" name=\"themedir\" value=\"$themedir\">\n"
    ."  <input type=\"hidden\" name=\"thememod\" value=\"$thememod\">\n"
    ."  <input type=\"hidden\" name=\"modops\" value=\"$modops\">\n"
    ."<input type=\"hidden\" name=\"op\" value=\"addblock\">\n"
    ."<input type=\"hidden\" name=\"module\" value=\"". MOD_NAME."\">\n"
    ."<input type=\"submit\" value=\""._AT_ADD."\" name=\"B1\">\n"
    ."</form>\n";
    CloseTable();
    AutoThemeFooter();
}

/* Add a block */
function AutoTheme_admin_addblock($var)
{
    extract($var);
    
    if (file_exists("themes/$themedir/theme.cfg")) {
        include("themes/$themedir/theme.cfg");
    }
    elseif (file_exists("$themedir/theme.cfg")) {
            include("$themedir/theme.cfg");
    }    
    if ($modops) {
    	if (!$blocktemplate[$thememod][$modops][$themeblock]) {
    		$blocktemplate[$thememod][$modops][addslashes($themeblock)] = "";
    	}
    	else {
    		Header("Location: admin.php?module=". MOD_NAME."&op=blockmain&themedir=".htmlentities(urlencode($themedir))."&thememod=$thememod&modops=$modops");
        	exit;
    	}    		
    }
    else {
    	if (!$blocktemplate[$thememod][$themeblock]) {
    		$blocktemplate[$thememod][addslashes($themeblock)] = "";
    	}
    	else {
        	Header("Location: admin.php?module=". MOD_NAME."&op=blockmain&themedir=".htmlentities(urlencode($themedir))."&thememod=$thememod");
        	exit;
    	}
    }
    $var = compact("blocktemplate");
    SaveThemeConfig($themedir, $var);
    
    Header("Location: admin.php?module=". MOD_NAME."&op=blockmain&themedir=".htmlentities(urlencode($themedir))."&thememod=$thememod&modops=$modops");
}

/* Delete a block */
function AutoTheme_admin_delblock($var)
{
    extract($var);
    
    if (!isset($confirmed)) {
        AutoTheme_admin_confirmdel($var);
        exit;
    }
    extract($confirmed);

    if ($del == _YES) {
        if (file_exists("themes/$themedir/theme.cfg")) {
            include("themes/$themedir/theme.cfg");
        }
        elseif (file_exists("$themedir/theme.cfg")) {
            include("$themedir/theme.cfg");
        }
        if ($modops) {
    		$themeblock = stripslashes(stripslashes($themeblock));
    		unset($blocktemplate[$thememod][$modops][$themeblock]);
    	}
    	else {
    		$themeblock = stripslashes(stripslashes($themeblock));
  	        unset($blocktemplate[$thememod][$themeblock]);
    	}    	
        $var = compact("blocktemplate");
        SaveThemeConfig($themedir, $var);
    }
    Header("Location: admin.php?module=". MOD_NAME."&op=blockmain&themedir=".htmlentities(urlencode($themedir))."&thememod=$thememod&modops=$modops");
}

/* Update block settings */
function AutoTheme_admin_updateblock($var)
{
    extract($var);
    
    if (file_exists("themes/$themedir/theme.cfg")) {
        include("themes/$themedir/theme.cfg");
    }
    elseif (file_exists("$themedir/theme.cfg")) {
            include("$themedir/theme.cfg");
    }
    if ($modops) {
    	$theblocks = $blocktemplate[$thememod][$modops];
    	unset($blocktemplate[$thememod][$modops]);
    }
    else {
    	$theblocks = $blocktemplate[$thememod];
    	unset($blocktemplate[$thememod]);
    }
    foreach ($theblocks as $themeblock => $filename){
        $blockvar = strtolower(preg_replace("^\W|_^", "", $themeblock));
        if ($modops) {
        	$blocktemplate[$thememod][$modops][addslashes($themeblock)] = ${$blockvar."file"};
        }
        else {
        	$blocktemplate[$thememod][addslashes($themeblock)] = ${$blockvar."file"};
        }        	
    }
    $var = compact("blocktemplate");
    SaveThemeConfig($themedir, $var);
    
    Header("Location: admin.php?module=". MOD_NAME."&op=blockmain&themedir=".htmlentities(urlencode($themedir))."&thememod=$thememod&modops=$modops");
}

/* Main command editor */
function AutoTheme_admin_cmdedit($var)
{
    extract($var);
	
    atCommandLoad();
    $autoconfig = atGetAutoConfig();
    extract($autoconfig);

    if ($themedir) {
        AutoTheme_admin_links($themedir, "$themedir  >  "._AT_COMMANDS);
        $themeconfig = atLoadThemeConfig("themes/$themedir");
        extract($themeconfig);
        if (is_array($themecmd)) {
            $autocmd = $themecmd;
        }
    }
    else {
    	AutoThemeHeader(_AT_COMMANDS);
    	$themedir = "";
    }
    OpenTable();
    echo "<b>"._AT_CONFCOMMANDS."</b></td><td><b>"._AT_APPLIESTO."</b></td><td><b>"._AT_ACTION."</b></td></tr><tr><td>\n";
    
	if (!is_array($autocmd['all']) && !is_array($autocmd['anonymous']) && !is_array($autocmd['admin']) && !is_array($autocmd['loggedin'])) {
		$newacmd['all'] = $autocmd;
		$autocmd = $newacmd;
	}
    ksort($autocmd);
	$display = 0;
    foreach ($autocmd as $type => $c_array) {
        foreach ($c_array as $cmd => $action) {
            $display = 1;
            $lang_type = constant("_AT_".strtoupper($type));
            echo "$cmd</td><td>$lang_type</td><td>\n"
           	."[ <a href=\"admin.php?module=". MOD_NAME."&op=editcmd&cmd=".htmlentities(urlencode($cmd))."&type=".htmlentities(urlencode($type))."&themedir=".htmlentities(urlencode($themedir))."\">"._AT_CONFIGURE."</a> | \n"
           	."<a href=\"admin.php?module=". MOD_NAME."&op=delcmd&cmd=".htmlentities(urlencode($cmd))."&type=".htmlentities(urlencode($type))."&themedir=".htmlentities(urlencode($themedir))."\">"._AT_REMOVE."</a> ]\n"
           	."</td></tr><tr><td>\n";
       	}
    }
    if (!$display) {
        echo _AT_NOUSERCONFCOMMANDS;
    }
    CloseTable();
    echo "<br />\n";
    
    $appliesto = "<select name=\"type\">\n"
    ."<option value=\"admin\">"._AT_ADMIN."</option>\n"
    ."<option value=\"all\" selected>"._AT_ALL."</option>\n"
    ."<option value=\"anonymous\">"._AT_ANONYMOUS."</option>\n"
    ."<option value=\"loggedin\">"._AT_LOGGEDIN."</option>\n"
    ."</select>\n";
    
    OpenTable();
    echo "<form method=\"POST\" action=\"admin.php\">\n"
    ."<b>"._AT_ADDCOMMAND."</b><br />\n"
    ."<input type=\"text\" name=\"cmd\" size=\"20\"><br />\n"
    ."<b>"._AT_APPLIESTO."</b><br />"
    .$appliesto."<br />"
    ."<b>"._AT_ACTION."</b><br />\n"
    ."<textarea rows=\"5\" style=\"width: 100%\" name=\"action\"></textarea>\n"
    ."<input type=\"hidden\" name=\"op\" value=\"addcmd\">\n"
    ."<input type=\"hidden\" name=\"module\" value=\"". MOD_NAME."\">\n"
    ."<input type=\"hidden\" name=\"themedir\" value=\"$themedir\">\n"
    ."<input type=\"submit\" value=\""._AT_ADD."\" name=\"B1\">\n"
    ."</form>\n";
    CloseTable();
    AutoThemeFooter();
}

/* Edit a command */
function AutoTheme_admin_editcmd($var)
{
    extract($var);
    atCommandLoad();
    $autoconfig = atGetAutoConfig();
    extract($autoconfig);
    
    if ($themedir) {
        AutoTheme_admin_links($themedir, "$themedir  >  "._AT_COMMANDS);
        $themeconfig = atLoadThemeConfig("themes/$themedir");
        extract($themeconfig);
        $autocmd = $themecmd;        
    }
    else {
    	AutoThemeHeader(_AT_COMMANDS);
    	$themedir = "";
    }
    
	foreach ($autocmd as $key => $val) {
    	if (!is_array($val)) {
    		$newacmd['all'] = $autocmd;
    		$autocmd = $newacmd;
    		break;
    	}
    }
    $thiscmd = atDisplayVar($autocmd[$type][$cmd]);

    $t_var = $type."_sel";
    $$t_var =  " selected";
    
    $appliesto = "<select name=\"type\">\n"
    ."<option value=\"admin\"$admin_sel>"._AT_ADMIN."</option>\n"
    ."<option value=\"all\"$all_sel>"._AT_ALL."</option>\n"
    ."<option value=\"anonymous\"$anonymous_sel>"._AT_ANONYMOUS."</option>\n"
    ."<option value=\"loggedin\"$loggedin_sel>"._AT_LOGGEDIN."</option>\n"
    ."</select>\n";

    OpenTable();
    echo "<form method=\"POST\" action=\"admin.php\">\n"
    ."<b>"._AT_APPLIESTO."</b>: $appliesto<br /><br /><b>"._AT_COMMAND."</b>: $cmd<br /><br />\n"
    ."<b>"._AT_ACTION."</b><br />\n"
    ."<textarea wrap=\"virtual\" rows=\"10\" style=\"width: 100%\" name=\"action\">\n"
    .$thiscmd
    ."</textarea>\n"
    ."<input type=\"hidden\" name=\"cmd\" value=\"$cmd\">\n"
    ."<input type=\"hidden\" name=\"oldtype\" value=\"$type\">\n"
    ."<input type=\"hidden\" name=\"op\" value=\"updatecmd\">\n"
    ."<input type=\"hidden\" name=\"themedir\" value=\"$themedir\">\n"
    ."<input type=\"hidden\" name=\"module\" value=\"". MOD_NAME."\">\n"
    ."<input type=\"submit\" value=\""._AT_SAVE."\" name=\"B1\">\n"
    ."</form>";
    CloseTable();
    AutoThemeFooter();
}

/* View a command */
function AutoTheme_admin_viewcmd($var)
{
    extract($var);
    AutoThemeHeader(_AT_NONCONFCOMMANDS);
    
    $command = atCommandLoad();
    
    OpenTable();
    $lang_type = constant("_AT_".strtoupper($type));
    echo "<b>"._AT_APPLIESTO."</b>: $lang_type<br /><br /><b>"._AT_COMMAND."</b>: $cmd<br /><br />\n"
    ."<b>"._AT_ACTION."</b><br />\n"
    ."<textarea wrap=\"virtual\" rows=\"10\" style=\"width: 100%\" name=\"action\" readonly>\n"
    .htmlentities($command[$type][$cmd])
    ."</textarea><br />\n"
    ."[ <a href=\"admin.php?module=". MOD_NAME."&op=cmdedit\">"._AT_BACKTOCMDS."</a> ]";
    CloseTable();
    AutoThemeFooter();
}

/* Add command */
function AutoTheme_admin_addcmd($var)
{
    extract($var);
    
    atCommandLoad();
    $autoconfig = atGetAutoConfig();
    extract($autoconfig);
        	
    if ($themedir) {
        $themeconfig = atLoadThemeConfig("themes/$themedir");
        extract($themeconfig);
        $autocmd = $themecmd;
    }
    else {
        $themedir = "";
    }    

    foreach ($autocmd as $key => $val) {
    	if (!is_array($val)) {
    		$newacmd['all'] = $autocmd;
    		$autocmd = $newacmd;
    		break;
    	}
    }
    $type = strtolower($type);
    
    if ($cmd && !$autocmd[$type][$cmd] && !$command[$type][$cmd]) {
        $autocmd[$type][$cmd] = atExportVar($action);
    }
    else {
        Header("Location: admin.php?module=". MOD_NAME."&op=cmdedit&themedir=$themedir");
    }
    if ($themedir) {
    	$themecmd = $autocmd;
    	$var = compact("themecmd");
    	SaveThemeConfig($themedir, $var);
    }
    else {
    	$var = compact("autocmd");
    	SaveATConfig($var);
    }
    Header("Location: admin.php?module=". MOD_NAME."&op=cmdedit&themedir=$themedir");
}

/* Delete command */
function AutoTheme_admin_delcmd($var)
{
    extract($var);

    if (!$confirmed) {
        AutoTheme_admin_confirmdel($var);
        exit;
    }
    extract($confirmed);

    if ($del == _YES) {
    	
    	atCommandLoad();
    	$autoconfig = atGetAutoConfig();
    	extract($autoconfig);
    	
    	if ($themedir) {
    		$themeconfig = atLoadThemeConfig("themes/$themedir");
    		extract($themeconfig);
    		$autocmd = $themecmd;
    	}
    	else {
    		$themedir = "";
    	}
    	
        foreach ($autocmd as $key => $val) {
	    	if (!is_array($val)) {
    			$newacmd['all'] = $autocmd;
    			$autocmd = $newacmd;
    			break;
    		}
    	}
        unset($autocmd[$type][$cmd]);

        if ($themedir) {
        	$themecmd = $autocmd;
        	$var = compact("themecmd");
        	SaveThemeConfig($themedir, $var);
        }
        else {
        	$var = compact("autocmd");
        	SaveATConfig($var);
        }
    }
    Header("Location: admin.php?module=". MOD_NAME."&op=cmdedit&themedir=$themedir");
}

/* Update a command */
function AutoTheme_admin_updatecmd($var)
{
    extract($var);

    atCommandLoad();
    $autoconfig = atGetAutoConfig();
    extract($autoconfig);
        	
    if ($themedir) {
        $themeconfig = atLoadThemeConfig("themes/$themedir");
        extract($themeconfig);
        $autocmd = $themecmd;
    }
    else {
        $themedir = "";
    }
    
    foreach ($autocmd as $key => $val) {
    	if (!is_array($val)) {
   			$newacmd['all'] = $autocmd;
   			$autocmd = $newacmd;
   			break;
   		}
   	}
   	unset($autocmd[$oldtype][$cmd]);
    $autocmd[$type][$cmd] = atExportVar($action);

    if ($themedir) {
    	$themecmd = $autocmd;
    	$var = compact("themecmd");
    	SaveThemeConfig($themedir, $var);
    }
    else {
    	$var = compact("autocmd");
    	SaveATConfig($var);
    }

    Header("Location: admin.php?module=". MOD_NAME."&op=cmdedit&themedir=$themedir");
}

/* Admin links for sub pages */
function AutoTheme_admin_links($themedir, $title)
{
    echo "<script type=\"text/javascript\" src='modules/". MOD_NAME."/javascript/picker.js'></script>\n"
    ."<script>\n"
    ."   var JS_PATH = 'modules/". MOD_NAME."/javascript/';\n"
    ."   var PU_PATH = JS_PATH + 'popups/';\n"
    ."</script>\n\n";

    include("header.php");

    $adminlinks = "admin.php?module=". MOD_NAME;
    OpenTable();
    OpenTable();
    echo "<div align=\"center\">"
    ."<a class=\"pn-title\" href =\"admin.php\">"._AT_ADMINMENU."</a>"
    ."</div><br />\n"
    ."<div align=\"center\">"
    ."[ <a href =\"$adminlinks&op=main\">"._AT_ATADMIN."</a> ]"
    ."<div align=\"center\">\n"
    ."</div>\n";
    
    $adminlinks = "admin.php?module=". MOD_NAME;
    CloseTable();
    echo "<br /><div class=\"pn-pagetitle title\" align=\"center\">$title</div>\n"
    ."<div align=\"center\"><br />\n"   
    ."[ <a href=\"$adminlinks&op=general&themedir=".htmlentities(urlencode($themedir))."\">"._AT_GENERAL."</a> | "
    ."<a href=\"$adminlinks&op=modmain&themedir=".htmlentities(urlencode($themedir))."\">"._AT_CUSTMODULES."</a> | "
    ."<a href=\"$adminlinks&op=ablocks&themedir=".htmlentities(urlencode($themedir))."\">"._AT_AUTOBLOCKS."</a> | "
    ."<a href=\"$adminlinks&op=cmdedit&themedir=".htmlentities(urlencode($themedir))."\">"._AT_COMMANDS."</a> ]<br />";

    $autoconfig = atGetAutoConfig();
    extract($autoconfig);
    
    if (!$ext = atAutoGetVar("extra")) {
        $ext = atExtraScan($extradir);
    }
    if (!$ext) {
        return;
    }
    ksort($ext);
    foreach ($ext as $id => $info) {
        if ($info['themeadmin'] && function_exists($info['themeadmin']) && $autoextra[$id]) {
        	echo "[ <a href=\"admin.php?module=". MOD_NAME."&op=extraops&extraname=".htmlentities(urlencode($id))."&themedir=".htmlentities(urlencode($themedir))."\">".$info['name']."</a> ]";
        }
    }
    echo "</div><br />\n";
}

/* Confirm delete of something */
function AutoTheme_admin_confirmdel($var)
{
    extract($var);
    AutoThemeHeader(_AT_CONFIRMDEL);
    OpenTable();
    
    echo "<b>"._AT_AREYOUSUREDEL."</b><br />\n"
    ."<form method=\"POST\" action=\"admin.php\">\n"
    ."<input type=\"hidden\" name=\"op\" value=\"$op\">\n";
    foreach ($var as $thevar => $theval) {
        echo "<input type=\"hidden\" name=\"confirmed[$thevar]\" value=\"$theval\">\n";
    }
    echo "<input type=\"hidden\" name=\"module\" value=\"". MOD_NAME."\">\n"
    ."<input type=\"submit\" value=\""._YES."\" name=\"del\">\n"
    ."<input type=\"submit\" value=\""._NO."\" name=\"del\">\n"
    ."</form>\n";

    CloseTable();
    AutoThemeFooter();
}

/* Admin header */
function AutoThemeHeader($title="")
{
    include("header.php");
    $adminlinks = "admin.php?module=". MOD_NAME;
    OpenTable();
    OpenTable();
    echo "<div align=\"center\">"
    ."<a class=\"pn-title\" href =\"admin.php\">"._AT_ADMINMENU."</a>"
    ."</div><br />\n"
    ."<div align=\"center\">"
    ."[ <a href =\"$adminlinks&op=main\">"._AT_ATADMIN."</a> ]"
    ."<div align=\"center\">\n"
    ."</div><br />\n"
    ."[ <a href=\"$adminlinks&op=athemes\">"._AT_AUTOTHEMES."</a> | "
    ."<a href=\"$adminlinks&op=cmdedit\">"._AT_COMMANDS."</a> | "
    ."<a href=\"$adminlinks&op=extras\">"._AT_EXTRAS."</a> ]\n"
    ."</div>\n";
    CloseTable();
    if ($title) {
        echo "<br />";
    }
    echo "<div class=\"pn-pagetitle title\" align=\"center\">$title</div>\n"
    ."<br />\n";
}

/* Admin footer */
function AutoThemeFooter()
{
    $adminlinks = "admin.php?module=". MOD_NAME;
    CloseTable();
    echo "<br />";
    OpenTable();
    echo "<div align=\"center\">\n"
    ."AutoTheme 1.7<br />\n"
    ."Copyright (c) 2002-2004 Shawn McKenzie<br />\n"
    ."<a target=\"_blank\" href=\"http://spidean.mckenzies.net\">http://spidean.mckenzies.net</a>\n"
    ."</div>\n";
    CloseTable();

    include("footer.php");
}

function AutoTheme_admin_generalform($var, $formvars)
{
	extract($var);
	
	if (@file_exists("themes/$themedir/theme.cfg")) {
		include("themes/$themedir/theme.cfg");
		$filelist = list_files("themes/$themedir", "htm");
		$csslist = list_files("themes/$themedir", "css");
		$imglist = array_merge(
		(array)list_files("themes/$themedir", "gif"),
		(array)list_files("themes/$themedir", "jpg"),
		(array)list_files("themes/$themedir", "png")
		);
	}
	elseif (@file_exists("$themedir/theme.cfg")) {
		include("$themedir/theme.cfg");
		$filelist = list_files($themedir, "htm");
		$csslist = list_files($themedir, "css");
		$imglist = array_merge(
		(array)list_files($themedir, "gif"),
		(array)list_files($themedir, "jpg"),
		(array)list_files($themedir, "png")
		);
	}
	extract($formvars);
	extract($config);
	
	$ysh = $nsh = $yalt = $nalt = $yleft = $nleft = $ycenter = $ncenter = $yright = $nright = "";
	
	if ($striphead){ $ysh = "checked"; } else { $nsh = "checked"; }
	if ($altsummary){ $yalt = "checked"; } else { $nalt = "checked"; }
	if ($left){ $yleft = "checked"; } else { $nleft = "checked"; }
	if ($center){ $ycenter = "checked"; } else { $ncenter = "checked"; }
	if ($right){ $yright = "checked"; } else { $nright = "checked"; }
	
	if (!isset($modops)) {
		$modops = "default";
	}
	if (isset($thememod)) {
		$thisfrm = $thememod;
		$def = $thememod;
		$blockshow = _AT_BLOCKSHOW;
		$temp = $thememod;
	}
	else {
		$thisfrm = _AT_MAIN;
		$def = _AT_DEF;
		$blockshow = _AT_BLOCKDEFAULT;
		$temp = "default";
	}
	OpenTable();
	echo "<form name=\"general\" method=\"POST\" action=\"admin.php\">\n";
	if (isset($thememod)) {
		echo "  <input type=\"hidden\" name=\"op\" value=\"updatemod\">\n"
		."  <input type=\"hidden\" name=\"thememod\" value=\"$thememod\">\n"
		."  <input type=\"hidden\" name=\"modops\" value=\"$modops\">\n";
	}
	else {
		echo "  <input type=\"hidden\" name=\"op\" value=\"updategeneral\">\n";
	}
	echo "  <input type=\"hidden\" name=\"module\" value=\"".$GLOBALS['module']."\">\n"
	."  <input type=\"hidden\" name=\"themedir\" value=\"$themedir\">\n"
	."  <table border=\"0\" width=\"100%\" cellpadding=\"0\" style=\"border-collapse: collapse\" bordercolor=\"#111111\" cellspacing=\"0\">\n"
	."    <tr>\n"
	."      <td><b>$def "._AT_THEME." "._AT_TEMPLATES."</b></td>\n"
	."      <td><b>"._AT_FILENAME."</b></td>\n"
	."      <td>&nbsp;</td>\n"
	."    </tr>\n"
	."    <tr>\n"
	."      <td>"._AT_PAGETEMPLATE."</td>\n"
	.select_file("main", $temps['main'], $filelist)
	."      <td><b>"._AT_STRIPTOHEAD."</b></td>\n"
	."      <td>&nbsp;</td>\n"
	."    </tr>\n"
	."    <tr>\n"
	."      <td>&nbsp;</td>\n"
	."      <td>&nbsp;</td>\n"
	."      <td><input type=\"radio\" name=\"striphead\" value=\"1\" $ysh>"._YES."\n"
	."          <input type=\"radio\" name=\"striphead\" value=\"0\" $nsh>"._NO."</td>\n"
	."    </tr>\n";
	
	if (!isset($thememod) || $thememod == "News" || $thememod == "*HomePage") {
		echo "    <tr>\n"
		."      <td>"._AT_SUMMARY." "._AT_ARTICLE."</td>\n"
		.select_file("summary", $temps['summary'], $filelist)
		."      <td>&nbsp;</td>\n"
		."    </tr>\n"
		."    <tr>\n"
		."      <td>"._AT_FIRST." "._AT_ALTERNATING." "._AT_SUMMARY."</td>\n"
		.select_file("summary1", $temps['summary1'], $filelist)
		."      <td><b>"._AT_ALTSUMMARY."</b></td>\n"
		."    </tr>\n"
		."    <tr>\n"
		."      <td>"._AT_SECOND." "._AT_ALTERNATING." "._AT_SUMMARY."</td>\n"
		.select_file("summary2", $temps['summary2'], $filelist)
		."      <td><input type=\"radio\" name=\"altsummary\" value=\"1\" $yalt>"._YES."\n"
		."          <input type=\"radio\" name=\"altsummary\" value=\"0\" $nalt>"._NO."</td>\n"
		."    </tr>\n"
		."    <tr>\n"
		."      <td>"._AT_FULL." "._AT_ARTICLE."</td>\n"
		.select_file("article", $temps['article'], $filelist)
		."      <td>&nbsp;</td>\n"
		."    </tr>\n";
	}
	else {
		echo "    <tr>\n"
		."      <td>&nbsp;</td>\n"
		."      <td>&nbsp;</td>\n"
		."      <td>&nbsp;</td>\n"
		."    </tr>\n";
	}
	echo "    <tr>\n"
	."      <td>&nbsp;</td>\n"
	."      <td>&nbsp;</td>\n"
	."      <td><b>$blockshow</b></td>\n"
	."    </tr>\n"
	."    <tr>\n"
	."      <td>"._AT_LEFT." "._AT_BLOCKS."</td>\n"
	.select_file("leftblock", $temps['leftblock'], $filelist)
	."      <td><input type=\"radio\" name=\"left\" value=\"1\" $yleft>"._YES."\n"
	."          <input type=\"radio\" name=\"left\" value=\"0\" $nleft>"._NO."</td>\n"
	."    </tr>\n"
	."    <tr>\n"
	."      <td>"._AT_CENTER." "._AT_BLOCKS."</td>\n"
	.select_file("centerblock", $temps['centerblock'], $filelist)
	."      <td><input type=\"radio\" name=\"center\" value=\"1\" $ycenter>"._YES."\n"
	."          <input type=\"radio\" name=\"center\" value=\"0\" $ncenter>"._NO."</td>\n"
	."    </tr>\n"
	."    <tr>\n"
	."      <td>"._AT_RIGHT." "._AT_BLOCKS."</td>\n"
	.select_file("rightblock", $temps['rightblock'], $filelist)
	."      <td><input type=\"radio\" name=\"right\" value=\"1\" $yright>"._YES."\n"
	."          <input type=\"radio\" name=\"right\" value=\"0\" $nright>"._NO."</td>\n"
	."    </tr>\n";
	if ($autoblock){
		asort($autoblock);
		if (isset($thememod)) {
			foreach ($autoblock as $key => $ablock){
				if ($blockdisplay[$temp][$modops]["autoblock".$key]) { $yes = "checked"; $no = ""; } else { $yes = ""; $no = "checked"; }
				echo "    <tr>\n"
				."      <td>$ablock "._AT_BLOCKS."</td>\n"
				.select_file("autoblock".$key, $temps["autoblock".$key], $filelist)
				."      <td><input type=\"radio\" name=\"ablockdis[$key]\" value=\"1\" $yes>"._YES."\n"
				."          <input type=\"radio\" name=\"ablockdis[$key]\" value=\"0\" $no>"._NO."</td>\n"
				."    </tr>\n";
			}
		}
		else {
			foreach ($autoblock as $key => $ablock){
				if ($blockdisplay[$temp]["autoblock".$key]) { $yes = "checked"; $no = ""; } else { $yes = ""; $no = "checked"; }
				echo "    <tr>\n"
				."      <td>$ablock "._AT_BLOCKS."</td>\n"
				.select_file("autoblock".$key, $temps["autoblock".$key], $filelist)
				."      <td><input type=\"radio\" name=\"ablockdis[$key]\" value=\"1\" $yes>"._YES."\n"
				."          <input type=\"radio\" name=\"ablockdis[$key]\" value=\"0\" $no>"._NO."</td>\n"
				."    </tr>\n";
			}
		}
	}
	echo "    <tr>\n"
	."      <td>&nbsp;</td>\n"
	."      <td>&nbsp;</td>\n"
	."      <td>&nbsp;</td>\n"
	."    </tr>\n"
	."    <tr>\n"
	."      <td>"._AT_FIRST." "._AT_TBL."</td>\n"
	.select_file("table1", $temps['table1'], $filelist)
	."      <td>&nbsp;</td>\n"
	."    </tr>\n"
	."    <tr>\n"
	."      <td>"._AT_SECOND." "._AT_TBL."</td>\n"
	.select_file("table2", $temps['table2'], $filelist)
	."      <td>&nbsp;</td>\n"
	."    </tr>\n"
	."    <tr>\n"
	."      <td>"._AT_STYLESHEET."</td>\n"
	.select_file("stylesheet", $stylesheet, $csslist, 0, 0)
	."      <td>&nbsp;</td>\n"
	."    </tr>\n"
	."    <tr>\n"
	."      <td>"._AT_LOGO." "._AT_IMG."</td>\n"
	.select_file("logoimg", $logoimg, $imglist, 0, 0)
	."      <td>&nbsp;</td>\n"
	."    </tr>\n"
	."    <tr>\n"
	."      <td>&nbsp;</td>\n"
	."      <td>&nbsp;</td>\n"
	."      <td>&nbsp;</td>\n"
	."    </tr>\n"
	."    <tr>\n"
	."      <td><b>$def "._AT_THEME." "._AT_COLORS."</b></td>\n"
	."      <td><b>"._AT_COLOR."</b></td>\n"
	."      <td>&nbsp;</td>\n"
	."    </tr>\n"
	."    <tr>\n"
	."      <td>"._AT_BG." "._AT_COLOR." 1</td>\n"
	."      <td><input type=\"text\" name=\"color1\" size=\"7\" value=\"$color1\"><a href=\"javascript:TCP.popup(document.forms['general'].elements['color1'], 0)\">"._AT_SELECT."</a></td>\n"
	."      <td bgcolor=\"$color1\">&nbsp;</td>\n"
	."    </tr>\n"
	."    <tr>\n"
	."      <td>"._AT_BG." "._AT_COLOR." 2</td>\n"
	."      <td><input type=\"text\" name=\"color2\" size=\"7\" value=\"$color2\"><a href=\"javascript:TCP.popup(document.forms['general'].elements['color2'], 0)\">"._AT_SELECT."</a></td>\n"
	."      <td bgcolor=\"$color2\">&nbsp;</td>\n"
	."    </tr>\n"
	."    <tr>\n"
	."      <td>"._AT_BG." "._AT_COLOR." 3</td>\n"
	."      <td><input type=\"text\" name=\"color3\" size=\"7\" value=\"$color3\"><a href=\"javascript:TCP.popup(document.forms['general'].elements['color3'], 0)\">"._AT_SELECT."</a></td>\n"
	."      <td bgcolor=\"$color3\">&nbsp;</td>\n"
	."    </tr>\n"
	."    <tr>\n"
	."      <td>"._AT_BG." "._AT_COLOR." 4</td>\n"
	."      <td><input type=\"text\" name=\"color4\" size=\"7\" value=\"$color4\"><a href=\"javascript:TCP.popup(document.forms['general'].elements['color4'], 0)\">"._AT_SELECT."</a></td>\n"
	."      <td bgcolor=\"$color4\">&nbsp;</td>\n"
	."    </tr>\n"
	."    <tr>\n"
	."      <td>"._AT_TXT." "._AT_COLOR." 1</td>\n"
	."      <td><input type=\"text\" name=\"color5\" size=\"7\" value=\"$color5\"><a href=\"javascript:TCP.popup(document.forms['general'].elements['color5'], 0)\">"._AT_SELECT."</a></td>\n"
	."      <td bgcolor=\"$color5\">&nbsp;</td>\n"
	."    </tr>\n"
	."    <tr>\n"
	."      <td>"._AT_TXT." "._AT_COLOR." 2</td>\n"
	."      <td><input type=\"text\" name=\"color6\" size=\"7\" value=\"$color6\"><a href=\"javascript:TCP.popup(document.forms['general'].elements['color6'], 0)\">"._AT_SELECT."</a></td>\n"
	."      <td  bgcolor=\"$color6\">&nbsp;</td>\n"
	."    </tr>\n"
	."    <tr>\n"
	."      <td>"._AT_TBL." "._AT_BORDER." "._AT_COLOR." 1</td>\n"
	."      <td><input type=\"text\" name=\"color7\" size=\"7\" value=\"$color7\"><a href=\"javascript:TCP.popup(document.forms['general'].elements['color7'], 0)\">"._AT_SELECT."</a></td>\n"
	."      <td  bgcolor=\"$color7\">&nbsp;</td>\n"
	."    </tr>\n"
	."    <tr>\n"
	."      <td>"._AT_TBL." "._AT_BG." "._AT_COLOR." 1</td>\n"
	."      <td><input type=\"text\" name=\"color8\" size=\"7\" value=\"$color8\"><a href=\"javascript:TCP.popup(document.forms['general'].elements['color8'], 0)\">"._AT_SELECT."</a></td>\n"
	."      <td  bgcolor=\"$color8\">&nbsp;</td>\n"
	."    </tr>\n"
	."    <tr>\n"
	."      <td>"._AT_TBL." "._AT_BORDER." "._AT_COLOR." 2</td>\n"
	."      <td><input type=\"text\" name=\"color9\" size=\"7\" value=\"$color9\"><a href=\"javascript:TCP.popup(document.forms['general'].elements['color9'], 0)\">"._AT_SELECT."</a></td>\n"
	."      <td  bgcolor=\"$color9\">&nbsp;</td>\n"
	."    </tr>\n"
	."    <tr>\n"
	."      <td>"._AT_TBL." "._AT_BG." "._AT_COLOR." 2</td>\n"
	."      <td><input type=\"text\" name=\"color10\" size=\"7\" value=\"$color10\"><a href=\"javascript:TCP.popup(document.forms['general'].elements['color10'], 0)\">"._AT_SELECT."</a></td>\n"
	."      <td  bgcolor=\"$color10\">&nbsp;</td>\n"
	."    </tr>\n"
	."    <tr>\n"
	."      <td>&nbsp;</td>\n"
	."      <td>&nbsp;</td>\n"
	."      <td>&nbsp;</td>\n"
	."    </tr>\n"
	."    <tr>\n"
	."      <td colspan=\"3\"><b>$def "._AT_HEAD." "._AT_CONTENT."</b><br />\n"
	."<textarea type=\"text\" wrap=\"virtual\" rows=\"5\" style=\"width: 100%\" name=\"head\">\n"
	.htmlentities($head)
	."</textarea></td>\n"
	."    </tr>\n"
	."    <tr>\n"
	."      <td><br /><input type=\"submit\" value=\""._AT_SAVE."\" name=\"B1\"></td>\n"
	."      <td>&nbsp;</td>\n"
	."      <td>&nbsp;</td>\n"
	."    </tr>\n"
	."  </table>\n"
	."</form>\n";
	CloseTable();
}

/* Write autotheme.cfg */
function SaveATConfig($var, $extraname="")
{
    if (file_exists("modules/". MOD_NAME."/autotheme.cfg")) {
       if (!is_writable("modules/". MOD_NAME."/autotheme.cfg")) {
           display_error("<b>"._AT_ERROR."</b>: autotheme.cfg "._NOTWRITABLE);
       }
    }
    elseif (!is_writable("modules/". MOD_NAME."/")) {
       display_error("<b>"._AT_ERROR."</b>: modules/AutoTheme/ "._NOTWRITABLE);
    }
    include("modules/". MOD_NAME."/autotheme.cfg");

    extract($var);

    if (!$autoblock) { $autoblock = array(); }
    if (!$autocmd) { $autocmd = array(); }
    if (!$autoextra) { $autoextra = array(); }

    $content = STARTPHP."\n"
    .'$cache = '
    .var_export($cache, TRUE)
    .";\n"
    .'$autoblock = '
    .var_export($autoblock, TRUE)
    .";\n"
    .'$autocmd = '
    .var_export($autocmd, TRUE)
    .";\n"
    .'$autoextra = '
    .var_export($autoextra, TRUE)
    .";\n";
    
    foreach ($autoextra as $k => $v) {
        if (is_array($$k)) {
            $content .= '$'.$k.' = '
            .var_export($$k, TRUE)
            .";\n";
        }
    }
    $content .= ENDPHP;

    $handle = fopen("modules/". MOD_NAME."/autotheme.cfg", "w");
	fwrite($handle, $content);
	fclose($handle);
	
	atCompileClear();
}

/* Write theme.cfg */
function SaveThemeConfig($themedir, $var)
{
    $themepath = get_theme_path($themedir);

    $themeconfig = atLoadThemeConfig($themepath);
    extract($themeconfig);

    if (!is_writable("$themepath/theme.cfg")) {
        display_error("<b>"._AT_ERROR."</b>: $themepath/theme.cfg "._NOTWRITABLE);
    }
    extract($var);

    if (!$template) { $template = array(); }
    if (!$blockdisplay) { $blockdisplay = array(); }
    if (!$style) { $style = array(); }
    if (!$blocktemplate) { $blocktemplate = array(); }
    if (!$autoblock) { $autoblock = array(); }
    if (!$themecmd) { $themecmd = array(); }

    $content = STARTPHP."\n"
    .'$atversion = \'1.7\';'
    ."\n"
    .'$template = '
    .var_export($template, TRUE)
    .";\n"
    .'$blockdisplay = '
    .var_export($blockdisplay, TRUE)
    .";\n"
    .'$style = '
    .var_export($style, TRUE)
    .";\n"
    .'$blocktemplate = '
    .var_export($blocktemplate, TRUE)
    .";\n"
    .'$autoblock = '
    .var_export($autoblock, TRUE)
    .";\n"
    .'$themecmd = '
    .var_export($themecmd, TRUE)
    .";\n";
    
    $autoconfig = atGetAutoConfig();
    $ext = atExtraScan($autoconfig['extradir']);
    
    foreach ($ext as $k => $v) {
        if (is_array($$k) && (isset($ext[$k]['themeadmin']) || isset($ext[$k]['modadmin']))) {        	
            $content .= '$'.$k.' = '
            .var_export($$k, TRUE)
            .";\n";
        }
    }
    $content .= ENDPHP;

    $handle = fopen("$themepath/theme.cfg", "w");
	fwrite($handle, $content);
	fclose($handle);
	
	atCompileClear();
}

/* fix theme config for new features */
function FixupThemeConfig($themedir)
{
	$autocmd = array();
    $autoblock = array();

    $autoconfig = atGetAutoConfig();
    extract($autoconfig);
    
    $tempab = $autoblock;
    $tempcmd = $autocmd;

    $themepath = get_theme_path($themedir);

    $themeconfig = atLoadThemeConfig($themepath);
    extract($themeconfig);
    
    if (empty($autoblock)) {
        $autoblock = $tempab;
    }
    if (empty($themecmd)) {
        $themecmd = $tempcmd;
    }
    /* fix old custom module names */
    if (array_key_exists("pnHome", $template)) {
        $template['*HomePage'] = $template['pnHome'];
        $blockdisplay['*HomePage'] = $blockdisplay['pnHome'];
        $style['*HomePage'] = $style['pnHome'];
        unset($template['pnHome'], $blockdisplay['pnHome'], $style['pnHome']);
    }
    if (array_key_exists("pnAdmin", $template)) {
        $template['*AdminPages'] = $template['pnAdmin'];
        $blockdisplay['*AdminPages'] = $blockdisplay['pnAdmin'];
        $style['*AdminPages'] = $style['pnAdmin'];
        unset($template['pnAdmin'], $blockdisplay['pnAdmin'], $style['pnAdmin']);
    }
    if (array_key_exists("pnUser", $template)) {
        $template['*UserPages'] = $template['pnUser'];
        $blockdisplay['*UserPages'] = $blockdisplay['pnUser'];
        $style['*UserPages'] = $style['pnUser'];
        unset($template['pnUser'], $blockdisplay['pnUser'], $style['pnUser']);
    }
    if (array_key_exists("nukeHome", $template)) {
        $template['*HomePage'] = $template['nukeHome'];
        $blockdisplay['*HomePage'] = $blockdisplay['nukeHome'];
        $style['*HomePage'] = $style['nukeHome'];
        unset($template['nukeHome'], $blockdisplay['nukeHome'], $style['nukeHome']);
    }
    if (array_key_exists("nukeAdmin", $template)) {
        $template['*AdminPages'] = $template['nukeAdmin'];
        $blockdisplay['*AdminPages'] = $blockdisplay['nukeAdmin'];
        $style['*AdminPages'] = $style['nukeAdmin'];
        unset($template['nukeAdmin'], $blockdisplay['nukeAdmin'], $style['nukeAdmin']);
    }
    if (array_key_exists("nukeUser", $template)) {
        $template['*UserPages'] = $template['nukeUser'];
        $blockdisplay['*UserPages'] = $blockdisplay['nukeUser'];
        $style['*UserPages'] = $style['nukeUser'];
        unset($template['nukeUser'], $blockdisplay['nukeUser'], $style['nukeUser']);
    }
    /* fix block templates for custom modules */
    if (!is_array($blocktemplate['default'])) {
    	$temp = $blocktemplate;
    	unset($blocktemplate);
    	$blocktemplate['default'] = $temp;
    	
        foreach ($template as $thememod => $a) {
    		if ($thememod != 'default') {
    		    foreach ($a as $modops => $v) {
                    $blocktemplate[$thememod][$modops] = $temp;
                }
            }
    	}
    }
    /* fix autoblock templates and blockdisplay keys */
    foreach ($template as $mod => $arr1) {
        foreach ($arr1 as $modop => $arr2) {
            if ($mod == "default") {
                foreach ($autoblock as $num => $name) {
                    if (!$template['default']['autoblock'.$num]) {
                        $template['default']['autoblock'.$num] = $template['default'][$name.'block'];
                        $blockdisplay['default']['autoblock'.$num] = $blockdisplay['default'][$name];
                        unset($template['default'][$name.'block']);
                        unset($blockdisplay['default'][$name]);
                    }
                }
            }
            else {
                foreach ($arr2 as $k => $v) {
                    foreach ($autoblock as $num => $name) {
                        if (!$template[$mod][$modop]['autoblock'.$num]) {
                            $template[$mod][$modop]['autoblock'.$num] = $template[$mod][$modop][$name.'block'];
                            $blockdisplay[$mod][$modop]['autoblock'.$num] = $blockdisplay[$mod][$modop][$name];
                            unset($template[$mod][$modop][$name.'block']);
                            unset($blockdisplay[$mod][$modop][$name]);
                        }
                    }
                }
            }
        }
    }
    $var = compact("template", "blockdisplay", "style", "autoblock", "themecmd", "blocktemplate");
    SaveThemeConfig($themepath, $var);
}

function AutoTheme_admin_editfile($var)
{
    extract($var);
	
    $jspath ="modules/". MOD_NAME."/javascript/";
    $thefile = "themes/$themedir/$themefile";

    if ($action) {
        if (@file_exists($thefile)) {
            if (!is_writable($thefile)) {
                display_error("<b>"._AT_ERROR."</b>: $thefile "._AT_NOTWRITABLE);
            }
        }
        elseif (!is_writable("themes/$themedir/")) {
            display_error("<b>"._AT_ERROR."</b>: themes/$themedir/ "._AT_NOTWRITABLE);
        }
        $content = "";

        if ($head) {
            $content .= "<html>\n<head>\n$head\n</head>\n";
        }
        if ($body) {
            $content .= "$body\n";
        }
        $content .= $HTML;
        $content = trim(atExportVar($content));
        $content = trim($content);
        
        $handle = fopen($thefile, "w");
        fwrite($handle, $content);
	    fclose($handle);
	    
	    echo "$thefile saved...";
        exit;
    }
    if ($themefile) {

        $HTML = atTemplateRead($thefile);
        
        $head = spliti('\<head\>', $HTML);
        $head = spliti('\<\/head\>', $head[1]);
        $head = trim($head[0]);
        
        eregi('(\<body[^\>]+\>)(.*)', $HTML, $parts);
        
        if (is_array($parts)) {
            $body = $parts[1];
            $HTML = $parts[2];
        }
	}
    include("modules/". MOD_NAME."/admin/atEditor.php");
}

function select_file($name, $val, $filelist, $create=1, $link=1)
{
    $output = "<td><select name=\"$name\">\n";
    $output .= "<option></option>\n";

    foreach ($filelist as $file) {
        $sel = "";
        if ($file == $val) { $sel = " selected"; }
        $output .= "<option$sel>$file</option>\n";
    }
    $output .= "</select>\n";

    if ($link) {
        $output .= edit_link($val, $create);
    }
    $output .= "</td>\n";

    return $output;
}

function edit_link($file, $create)
{
    global $themedir;

	if ($file) {
        $action = _AT_EDIT;
    }
    elseif ($create) {
        $action = _AT_CREATE;
    }
    else {
    	return;
    }
    $link = "<a target=\"_blank\" href=\"admin.php?module=". MOD_NAME."&op=editfile&themedir=".htmlentities(urlencode($themedir))."&themefile=$file\">$action</a>";

    return $link;
}

function edit_prep($filename, $striphead=1)
{
    $HTML = atTemplateRead($filename);
    if ($striphead) {
        $HTML = preg_split('/\<\/head\>/', $HTML);
    }
    else {
        $HTML = preg_split('/\<head\>/', $HTML);
    }
    if (isset($HTML[1])) {
        $HTML = $HTML[1];
    }
    else {
        $HTML = $HTML[0];
    }
    $HTML = preg_split('/\<\/body\>/', $HTML);
    $HTML = trim($HTML[0]);

    return $HTML;
}

?>
