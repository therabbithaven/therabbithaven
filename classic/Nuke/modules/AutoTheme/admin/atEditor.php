<?php
// ----------------------------------------------------------------------
// Copyright (c) 2002-2004 Shawn McKenzie
// http://spidean.mckenzies.net
// ----------------------------------------------------------------------
// LICENSE
//
// This software is the copyrighted material of Shawn McKenzie. AutoTheme is
// licensed for use on (1) web site installation only. See the license.txt for your
// specific license.
// 
// YOU AGREE NOT TO COPY OR DISTRIBUTE THIS SOFTWARE OR CHANGE THE SOURCE CODE
// WITHOUT PRIOR WRITTEN PERMISSION. PLEASE SEND ANY QUESTIONS OR COMMENTS TO
// AUTOTHEME@MCKENZIES.NET.
// 
// SHAWN MCKENZIE, TAKES NO RESPONSIBILITY FOR ANY DAMAGE THAT THIS SOFTWARE MIGHT
// CAUSE.
//
// Additional licensing information is available from autotheme@mckenzies.net.
// 
// If you disagree with any of these terms, you are not authorized to use this
// software. If you do agree with these terms, please enjoy.
// ----------------------------------------------------------------------

$lang = atGetLang();

?>

<html>
<head>

<title>AutoTheme Editor :: <?php echo "$themedir > $themefile"; ?></title>

<style type="text/css">
@import url(<?php echo $jspath; ?>htmlarea.css);
html, body { margin: 0px; border: 0px; background-color: buttonface; }
</style>

<script type="text/javascript" src="<?php echo $jspath; ?>htmlarea.js"></script>
<script type="text/javascript" src="<?php echo "modules/AutoTheme/lang/$lang/"; ?>htmlarea-lang.js"></script>
<script type="text/javascript" src="<?php echo $jspath; ?>dialog.js"></script>

<script type="text/javascript">
var editor = null;

function resize_editor() {
  var newHeight;
  if (document.all) {
    newHeight = document.body.offsetHeight - editor._toolbar.offsetHeight - 30;
    if (newHeight < 0) { newHeight = 0; }
  } else {
    newHeight = window.innerHeight - editor._toolbar.offsetHeight - 30;
  }
  editor._textArea2.style.height = editor._iframe.style.height = newHeight + "px";
}

function init() {
  var config         = new HTMLArea.Config();
  config.editorURL   = "<?php echo $jspath; ?>";
  config.width       = "100%";
  config.height      = "auto";

  config.toolbar = [
             [ "fontname", "space" ],
			 [ "fontsize", "space" ],
			 [ "formatblock", "space"],
			 [ "bold", "italic", "underline", "separator" ],
			 [ "strikethrough", "subscript", "superscript", "linebreak" ],
			 [ "justifyleft", "justifycenter", "justifyright", "justifyfull", "separator" ],
			 [ "orderedlist", "unorderedlist", "outdent", "indent", "separator" ],
			 [ "forecolor", "backcolor", "textindicator", "separator" ],
			 [ "horizontalrule", "createlink", "insertimage", "inserttable", "htmlmode" ]
  ];

  editor = new HTMLArea("HTML", config);
  editor.generate();
  resize_editor();
  editor._iframe.style.width = "100%";
  editor._textArea2.style.width = "100%";
}
</script>

</head>

<body onload="init()">

<form method="POST" action="admin.php" style="margin: 0px; border: 1px solid; border-color: threedshadow threedhighlight threedhighlight threedshadow;">
<input type="hidden" name="module" value="<?php echo MOD_NAME; ?>">
<input type="hidden" name="op" value="editfile">
<input type="hidden" name="themedir" value="<?php echo $themedir; ?>">

<?php if ($head) { ?>
<textarea name="head" id="head" style="width:100%" rows="4" cols="20"><?php echo $head; ?></textarea>
<?php } ?>
<?php if ($body) { ?>
<textarea name="body" id="body" style="width:100%" rows="4" cols="20"><?php echo $body; ?></textarea>
<?php } ?>
<textarea name="HTML" id="HTML" style="width:100%; height:600px" rows="1" cols="20"><?php echo $HTML; ?></textarea>

<div align="center">
<?php
if ($themefile) {
	echo "<input type=\"submit\" name=\"action\" value=\"Save\">"
	."<input type=\"hidden\" name=\"themefile\" value=\"$themefile\">";
}
else {
	echo "<input type=\"submit\" name=\"action\" value=\"Save As\">"
	."<input type=\"text\" name=\"themefile\" size=\"20\">";
}
?>
</div>
</form>

</body></html>
