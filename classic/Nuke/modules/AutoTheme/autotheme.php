<?php
// ----------------------------------------------------------------------
// Copyright (c) 2002-2004 Shawn McKenzie
// http://spidean.mckenzies.net
// ----------------------------------------------------------------------
// LICENSE
//
// This software is the copyrighted material of Shawn McKenzie. AutoTheme is
// licensed for use on (1) web site installation only. See the license.txt for your
// specific license.
// 
// YOU AGREE NOT TO COPY OR DISTRIBUTE THIS SOFTWARE OR CHANGE THE SOURCE CODE
// WITHOUT PRIOR WRITTEN PERMISSION. PLEASE SEND ANY QUESTIONS OR COMMENTS TO
// AUTOTHEME@MCKENZIES.NET.
// 
// SHAWN MCKENZIE, TAKES NO RESPONSIBILITY FOR ANY DAMAGE THAT THIS SOFTWARE MIGHT
// CAUSE.
//
// Additional licensing information is available from autotheme@mckenzies.net.
// 
// If you disagree with any of these terms, you are not authorized to use this
// software. If you do agree with these terms, please enjoy.
// ----------------------------------------------------------------------

$mtime = explode(" ", microtime());
$GLOBALS['renderstart'] = $mtime[1] + $mtime[0];

define('AUTOTHEME_MOD_LOADED', TRUE);

$atdir = "modules/AutoTheme/";

include_once($atdir."includes/atAPI.php");
atThemeInit($atdir, $thename);

$runningconfig = atGetRunningConfig();
extract($runningconfig);

if (atGetModStyle() == "old") {
	ob_start();
}

if (!function_exists("OpenTable")) {
    function OpenTable()
    {
        $runningconfig = atGetRunningConfig();
        extract($runningconfig);

        if ($template['table1']) {
            $file = $template['table1'];
			
            $template = atTemplateCompile($themepath.$file);
            list($output, $close) = atTemplateSplit($template, "table-content");
            atTemplateDisplay($output);
        }
        else {
            echo "<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"0\" bgcolor=\"$tblcolor1\"><tr><td>\n";
            echo "<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"4\" bgcolor=\"$tblcolor2\"><tr><td>\n";
        }
    }
}

if (!function_exists("CloseTable")) {
    function CloseTable()
    {
        $runningconfig = atGetRunningConfig();
        extract($runningconfig);

        if ($template['table1']) {
            $file = $template['table1'];

            $template = atTemplateCompile($themepath.$file);
            list($open, $output) = atTemplateSplit($template, "table-content");
            atTemplateDisplay($output);
        }
        else {
            echo "</td></tr></table></td></tr></table>\n";
        }
    }
}

if (!function_exists("OpenTable2")) {
    function OpenTable2()
    {
        $runningconfig = atGetRunningConfig();
        extract($runningconfig);

        if ($template['table2']) {
            $file = $template['table2'];

            $template = atTemplateCompile($themepath.$file);
            list($output, $close) = atTemplateSplit($template, "table-content");
            atTemplateDisplay($output);
        }
        else {
            echo "<table border=\"0\" cellspacing=\"1\" cellpadding=\"0\" bgcolor=\"$tblcolor3\" align=\"center\"><tr><td>\n";
            echo "<table border=\"0\" cellspacing=\"1\" cellpadding=\"4\" bgcolor=\"$tblcolor4\"><tr><td>\n";
        }
    }
}

if (!function_exists("CloseTable2")) {
    function CloseTable2()
    {
        $runningconfig = atGetRunningConfig();
        extract($runningconfig);

        if ($template['table2']) {
            $file = $template['table2'];

            $template = atTemplateCompile($themepath.$file);
            list($open, $output) = atTemplateSplit($template, "table-content");
            atTemplateDisplay($output);
        }
        else {
            echo "</td></tr></table></td></tr></table>\n";
        }
    }
}

?>
