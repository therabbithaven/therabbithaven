<?php 

// Extra for all platforms
//
// How to register an extra and the functions that it performs and when to perform them (at operation)
//
// $extra = array ( 'at operation' => 'extra function' );
//
$extra['headcontent'] = array (
	'name' => 'Head content',
	'description' => 'Head content, title, meta tags and description for each page.',
	'version' => '1.7',
	'author' => 'Shawn McKenzie',
	'contact' => 'http://spidean.mckenzies.net',
	'themepreprocess' => 'at_headcontent',
	'modadmin' => 'at_admin_headcontent',
);

// Extra functions
//
function at_headcontent($display)
{
	$runningconfig = atGetRunningConfig();
	extract($runningconfig);
	
	if ($headcontent[$modtemplate][$modops]) {
		extract($headcontent[$modtemplate][$modops]);
		$doit = 1;
	}
	elseif ($headcontent['default']) {
		extract($headcontent['default']);
		$doit = 1;
	}		
	elseif (!isset($doit)) {
		return $display;
	}
	$search = array(
		"|\<title\>[^\<]+\<\/title\>|",
		"|\<META NAME=\"KEYWORDS\" CONTENT=\"[^\"]+\"|",
		"|\<META NAME=\"DESCRIPTION\" CONTENT=\"[^\"]+\"|",
		"|\<META NAME=\"GENERATOR\" CONTENT=\"([^\"]+)\"|",
	);
	
	$replace = array(
		"<title>$title</title>",
		"<META NAME=\"KEYWORDS\" CONTENT=\"$keywords\"",
		"<META NAME=\"DESCRIPTION\" CONTENT=\"$description\"",
		'<META NAME="GENERATOR" CONTENT="$1 running AutoTheme HTML Theme System - http://spidean.mckenzies.net"',
	);
	
	$display = preg_replace($search, $replace, $display);
	
	return $display;

}

function at_admin_headcontent($headcontent)
{	
	extract($headcontent);
	
    $output = "      "._AT_TITLE."<br />\n"
    ."      <input type=\"text\" name=\"title\" size=\"50\" value=\"$title\"><br />\n"
	."      "._AT_KEYWORDS."<br />\n"
    ."      <textarea name=\"keywords\" rows=\"5\" cols=\"50\">$keywords</textarea><br />\n"
    ."      "._AT_DESCRIPTION."<br />\n"
    ."      <input type=\"text\" name=\"description\" size=\"50\" value=\"$description\"><br /><br />\n";

	return $output;	
}

/*
Current PostNuke head content

	echo '<title>'.pnConfigGetVar('sitename').' :: '.pnConfigGetVar('slogan')."</title>\n";
    echo '<meta name="KEYWORDS" content="'.pnConfigGetVar('metakeywords')."\">\n";
    echo '<meta name="DESCRIPTION" content="'.pnConfigGetVar('slogan')."\">\n";
    echo "<meta name=\"ROBOTS\" content=\"INDEX,FOLLOW\">\n";
    echo "<meta name=\"resource-type\" content=\"document\">\n";
    echo "<meta http-equiv=\"expires\" content=\"0\">\n";
    echo '<meta name="author" content="'.pnConfigGetVar('sitename')."\">\n";
    echo '<meta name="copyright" content="Copyright (c) 2003 by '.pnConfigGetVar('sitename')."\">\n";
    echo "<meta name=\"revisit-after\" content=\"1 days\">\n";
    echo "<meta name=\"distribution\" content=\"Global\">\n";
    echo '<meta name="generator" content="PostNuke '._PN_VERSION_NUM." - http://postnuke.com\">\n";
    echo "<meta name=\"rating\" content=\"General\">\n";
*/

?>
