<?php 

// Extra for all platforms
//
// How to register an extra and the functions that it performs and when to perform them (at operation)
//
// $extra = array ( 'at operation' => 'extra function' );
//
$extra['themesonadate'] = array (
	'name' => 'Themes on a Date',
	'description' => 'Display themes on specific dates',
	'version' => '1.7',
	'author' => 'Shawn McKenzie',
	'contact' => 'http://spidean.mckenzies.net',
	'themeopen' => 'at_themesonadate',
	'atadmin' => 'at_admin_themesonadate'
);

// Extra functions
//
function at_themesonadate($vars)
{
    extract($vars);
    
    $themesonadate = atAutoGetVar("themesonadate");
    
    if (!$themesonadate) {
    	return;
    }
    foreach ($themesonadate['date'] as $k => $v) {
    	$newarray[$v] = $themesonadate['theme'][$k];
    }    
    $themesonadate = $newarray;
    $now = time();    	
    
    ksort($themesonadate);
    
    foreach ($themesonadate as $date => $theme) {
    	$changedate = strtotime($date);

    	if ($now >= $changedate) {
    		$newtheme = $theme;
    	}
    }
        
    if (isset($newtheme) && @file_exists("themes/$newtheme/theme.cfg")) {
    	global $prefix, $db;
        $db->sql_query("UPDATE ".$prefix."_config SET Default_Theme='$newtheme'");
    }
    
}	

function at_admin_themesonadate($themesonadate)
{
    foreach ($themesonadate['date'] as $k => $date) {
    	$theme = $themesonadate['theme'][$k];
        $output .= _AT_DATE." <input type=\"text\" name=\"date[]\" value=\"$date\" maxlength=\"10\">\n";
        $output .= _AT_THEME." <input type=\"text\" name=\"theme[]\" value=\"$theme\" maxlength=\"50\"><br />\n";
    }
    $output .= _AT_DATE." <input type=\"text\" name=\"date[]\" value=\"\" maxlength=\"10\">\n";
        $output .= _AT_THEME." <input type=\"text\" name=\"theme[]\" value=\"\" maxlength=\"50\"><br />\n";
    
	return $output;
}

?>
