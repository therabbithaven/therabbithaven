<?php
// ----------------------------------------------------------------------
// Copyright (c) 2002-2004 Shawn McKenzie
// http://spidean.mckenzies.net
// ----------------------------------------------------------------------
// LICENSE
//
// This software is the copyrighted material of Shawn McKenzie. AutoTheme is
// licensed for use on (1) web site installation only. See the license.txt for your
// specific license.
// 
// YOU AGREE NOT TO COPY OR DISTRIBUTE THIS SOFTWARE OR CHANGE THE SOURCE CODE
// WITHOUT PRIOR WRITTEN PERMISSION. PLEASE SEND ANY QUESTIONS OR COMMENTS TO
// AUTOTHEME@MCKENZIES.NET.
// 
// SHAWN MCKENZIE, TAKES NO RESPONSIBILITY FOR ANY DAMAGE THAT THIS SOFTWARE MIGHT
// CAUSE.
//
// Additional licensing information is available from autotheme@mckenzies.net.
// 
// If you disagree with any of these terms, you are not authorized to use this
// software. If you do agree with these terms, please enjoy.
// ----------------------------------------------------------------------

/* define('AUTOTHEME_DEBUG_ENABLED', TRUE); */

if (defined('AUTOTHEME_DEBUG_ENABLED') && AUTOTHEME_DEBUG_ENABLED) {
    error_reporting(E_ALL^E_NOTICE);
}
else {
    error_reporting(E_ALL^E_WARNING^E_NOTICE);
}
define('AUTOTHEME_API_LOADED', TRUE);
define('STARTPHP', str_replace('&lt;', '<', '&lt;?php'));
define('ENDPHP', str_replace('&gt;', '>', '?&gt;'));

function atInit($atdir, $thename)
{
	atThemeInit($atdir, $thename);
}

function atAPIInit($atdir)
{
	if (defined('AUTOTHEME_API_INITIALIZED')) {
        return atGetGlobalConfig();
    }
    define('AUTOTHEME_API_INITIALIZED', TRUE);

    $incdir = $atdir."includes/";
    $extradir = $atdir."extras/";
    $compiledir = $atdir."_compile/";
    $cachedir = $atdir."_cache/";
    $platform = atGetPlatform();
    $platformdir = $incdir."$platform/";

    include_once($incdir."atExtended.php");
    include_once($platformdir."atAPI.php");

    $platformconfig = atPlatformAPIInit($atdir);
    extract($platformconfig);

    atLoadAutoConfig($atdir);

    atAutoSetVar("atdir", $atdir);
    atAutoSetVar("incdir", $incdir);
    atAutoSetVar("extradir", $extradir);
    atAutoSetVar("compiledir", $compiledir);
    atAutoSetVar("cachedir", $cachedir);
    atAutoSetVar("platform", $platform);
    atAutoSetVar("platformdir", $platformdir);

    atModLangLoad('user');

    atExtraLoadAll();

    return atGetGlobalConfig();
}

function atThemeInit($atdir, $thename)
{
    if (defined('AUTOTHEME_THEME_INITIALIZED')) {
        return atGetRunningConfig();
    }
    define('AUTOTHEME_THEME_INITIALIZED', TRUE);

    $globalconfig = atAPIInit($atdir);
    extract($globalconfig);

    $platformconfig = atPlatformThemeInit($thename);
    extract($platformconfig);

    if (defined('WHERE_IS_PERSO') &&
    	WHERE_IS_PERSO &&
    	@file_exists(WHERE_IS_PERSO."themes/$thename/theme.cfg"))
    {
        $multipath = WHERE_IS_PERSO;
    }
    else {
        $multipath = "";
    }
    $lang = atGetLang();
    $themepath = $multipath."themes/$thename/";
    $imagepath = $themepath."images/";
    $imagelangpath = $imagepath."$lang/";

    atThemeSetVar("lang", $lang);
    atThemeSetVar("thename", $thename);
    atThemeSetVar("themepath", $themepath);
    atThemeSetVar("imagepath", $imagepath);
    atThemeSetVar("imagelangpath", $imagelangpath);
    atThemeSetVar("multipath", $multipath);

    include_once($platformdir."atFuncs.php");

    atLoadThemeConfig($themepath);
    atThemeLangLoad($themepath);

    atLoadRunningConfig();
    
    atExtendedInit();

    return atGetRunningConfig();
}

function atAdminInit($atdir)
{
    if (defined('AUTOTHEME_ADMIN_INITIALIZED')) {
        return atGetGlobalConfig();
    }
    define('AUTOTHEME_ADMIN_INITIALIZED', TRUE);

    $globalconfig = atAPIInit($atdir, $thename);
    extract($globalconfig);

    include_once($platformdir."atAdmin.php");

    atModLangLoad('admin');
    atLoadAutoConfig($atdir);

    return atGetGlobalConfig();
}

function atLoadRunningConfig()
{
    $globalconfig = atGetGlobalConfig();
    extract($globalconfig);

    atRunningMultiSetVars($globalconfig);

    $modname = atGetModName();
    $modtemplate = atTemplateGetType($template);

    atCommandLoad();
    atRunningSetVar("command", atGetCommands());

    atRunningSetVar("atdir", $atdir);
    atRunningSetVar("incdir", $incdir);
    atRunningSetVar("extradir", $extradir);
    atRunningSetVar("compiledir", $compiledir);
    atRunningSetVar("platform", $platform);
    atRunningSetVar("platformdir", $platformdir);
    atRunningSetVar("thename", $thename);
    atRunningSetVar("themepath", $themepath);
    atRunningSetVar("imagepath", $imagepath);
    atRunningSetVar("imagelangpath", $imagelangpath);
    atRunningSetVar("imgpath", $imagepath);
    atRunningSetVar("multipath", $multipath);
    atRunningSetVar("modname", $modname);
    atRunningSetVar("modtemplate", $modtemplate);
    atRunningSetVar("language", atGetLang());
    atRunningSetVar("modtype", atGetModType());
    atRunningSetVar("username", atGetUserName());
    atRunningSetVar("is_loggedin", atIsLoggedIn());
    atRunningSetVar("is_admin", atIsAdminUser());
    atRunningSetVar("is_home", atIsHomePage());

    /* 1.7 cfg */
    if (isset($template[$modtemplate]['default'])) {
        $modops = "default";
        $matchlen = 0;
        if ($_SERVER['QUERY_STRING']) {
            foreach ($template[$modtemplate] as $ops => $vals) {
                if ((strlen($ops) > $matchlen) && eregi($ops, $_SERVER['QUERY_STRING'])) {
                    $modops = $ops;
                    $matchlen = strlen($modops);
                }
            }
        }
        $template = array_merge((array)$template['default'], (array)$template[$modtemplate][$modops]);
        $blockdisplay = array_merge((array)$blockdisplay['default'], (array)$blockdisplay[$modtemplate][$modops]);
        $style = array_merge((array)$style['default'], (array)$style[$modtemplate][$modops]);
        if ($blocktemplate['default']) {
        	$blocktemplate = array_merge((array)$blocktemplate['default'], (array)$blocktemplate[$modtemplate][$modops]);
        }
    }
    /* 1.0 cfg */
    elseif (isset($template[$modtemplate])) {
        $template = array_merge((array)$template['default'], (array)$template[$modtemplate]);
        $blockdisplay = array_merge((array)$blockdisplay['default'], (array)$blockdisplay[$modtemplate]);
        $style = array_merge((array)$style['default'], (array)$style[$modtemplate]);
    }
    /* No custom module */
    elseif (!isset($template[$modtemplate])) {
        $template = $template['default'];
        $blockdisplay = $blockdisplay['default'];
        $style = $style['default'];
        if ($blocktemplate['default']) {
            $blocktemplate = $blocktemplate['default'];
        }
    }
    if (!$modops) {
    	$modops = "default";
    }
    atRunningSetVar("modops", $modops);

    extract($style);

    atRunningSetVar("logoimg", $logoimg);
    atRunningSetVar("bgcolor1", $color1);
    atRunningSetVar("bgcolor2", $color2);
    atRunningSetVar("bgcolor3", $color3);
    atRunningSetVar("bgcolor4", $color4);
    atRunningSetVar("textcolor1", $color5);
    atRunningSetVar("textcolor2", $color6);
    atRunningSetVar("tblcolor1", $color7);
    atRunningSetVar("tblcolor2", $color8);
    atRunningSetVar("tblcolor3", $color9);
    atRunningSetVar("tblcolor4", $color10);
    atRunningSetVar("striphead", $striphead);

    $runningconfig = compact(
        "template",
        "blockdisplay",
        "style",
        "blocktemplate",
        "themeversion",
        "autoblock",
        "autolang"
    );

    atRunningMultiSetVars($runningconfig);
    atRunningMultiSetVars(atGetLangVars());
}

function atGetLangVars()
{
    $const = get_defined_constants();
    foreach ($const as $k => $v) {
        if ($k[0] == "_") {
            $result["LANG$k"] = $v;
        }
    }
    return $result;
}

function atIsAPISupported($api)
{
    $platform = atGetPlatform();

    if ($api == $platform) {
        return true;
    }
    switch ($api) {
        case "postnuke":
            if ($platform == "postnuke" ||
	           $platform == "envolution" ||
	           $platform == "md-pro")
            {
               return true;
            }
            else {
               return false;
            }

        default:
            return false;
    }
}

function atTemplateGetType($template)
{
    if (atIsHomePage()) {
        if (isset($template['pnHome'])) {
            $type = "pnHome";
        }
        if (isset($template['nukeHome'])) {
            $type = "nukeHome";
        }
        if (isset($template['*HomePage'])) {
            $type = "*HomePage";
        }
    }
    $modtype = atGetModType();

    if ($modtype == "admin") {
        if (isset($template['pnAdmin'])) {
            $type = "pnAdmin";
        }
        if (isset($template['nukeAdmin'])) {
            $type = "nukeAdmin";
        }
        if (isset($template['*AdminPages'])) {
            $type = "*AdminPages";
        }
    }
    if ($modtype == "user") {
        if (isset($template['pnUser'])) {
            $type = "pnUser";
        }
        if (isset($template['nukeUser'])) {
            $type = "nukeUser";
        }
        if (isset($template['*UserPages'])) {
            $type = "*UserPages";
        }
    }
    if (!isset($type)) {
        $type = atGetModName();
    }
    return $type;
}

function atGetCommands()
{
    $globalconfig = atGetGlobalConfig();
    extract($globalconfig);

    if (atIsLoggedIn()) {
        $result = $command['loggedin'];
    }
    else {
        $result = $command['anonymous'];
    }
    if (atIsAdminUser()) {
        $result = array_merge((array)$result, (array)$command['admin']);
    }
    $result = array_merge((array)$command['all'], (array)$result);

    return $result;
}

function atCommandLoad()
{
    $globalconfig = atGetGlobalConfig();
    extract($globalconfig);

    include($incdir."atCommands.php");
    include($platformdir."atCommands.php");

    $extracmd = atExtraCommandLoad($extradir);
    //$lang = atGetLang();

    $result['admin'] = array_merge(
            (array)$command['admin'],
            (array)$platformcmd['admin'],
            (array)$extracmd['admin'],
            (array)$autocmd['admin'],
            (array)$themecmd['admin']);

    $result['all'] = array_merge(
            (array)$command['all'],
            (array)$platformcmd['all'],
            (array)$extracmd['all'],
            (array)$autocmd['all'],
            (array)$themecmd['all']);

    $result['anonymous'] = array_merge(
            (array)$command['anonymous'],
            (array)$platformcmd['anonymous'],
            (array)$extracmd['anonymous'],
            (array)$autocmd['anonymous'],
            (array)$themecmd['anonymous']);

    $result['loggedin'] = array_merge(
            (array)$command['loggedin'],
            (array)$platformcmd['loggedin'],
            (array)$extracmd['loggedin'],
            (array)$autocmd['loggedin'],
            (array)$themecmd['loggedin']);

    atAutoSetVar("command", $result);

    return $result;
}

function atCommandAdd($name, $command)
{
    $GLOBALS['AT_RUNNING']['command'][$name] = $command;
}

function atCommandMultiAdd($commands, $prefix="")
{
    foreach ($commands as $key => $val) {
        /* atCommandAdd("$key", 'echo $'.$prefix.'["'.$key.'"];'); */
        atCommandAdd("$key", $val);
    }
}

function atCommandReturn($commands, $prefix="")
{
    if (!$prefix) {
        $prefix = strtolower(atGetModName());
    }
    atRunningSetVar($prefix, $commands);

    foreach ($commands as $key => $val) {
        $cmd["$prefix:$key"] = 'echo $'.$prefix.'["'.$key.'"];';
    }
    return $cmd;
}

function atCommandBuild($commands, $prefix="")
{
    $cmd = atCommandReturn($commands, $prefix);
    atCommandMultiAdd($cmd, $prefix);
}

function atCommandReplace($tmpcontent, $commands=array())
{
    $runningconfig = atGetRunningConfig();
    extract($runningconfig);

    /* $search = '/{repeat:([\w\d\_]+):([\w\d\:\_]+)}(.*){repeat}/';
    $replace = STARTPHP.' for(\$i=0; \$i<count(\$$1["$2"]); \$i++) { eval(\''.ENDPHP.'$3\'); } '.ENDPHP;
    $tmpcontent = preg_replace($search, $replace, $tmpcontent); */

    $commands = array_merge((array)$command, (array)$commands);

    foreach ($commands as $cmd => $action) {
        $search = array(
            "<!-- [$cmd] -->",
            "<!--[$cmd]-->",
            "<!-- {".$cmd."} -->",
            "<!--{".$cmd."}-->",
            "{".$cmd."}",
        );

        $replace = STARTPHP." $action ".ENDPHP;
        $tmpcontent = str_replace($search, $replace, $tmpcontent);
    }
    foreach ($runningconfig as $cmd => $action) {
        $search = array(
            "<!-- [$cmd] -->",
            "<!--[$cmd]-->",
            "<!-- {".$cmd."} -->",
            "<!--{".$cmd."}-->",
            "{".$cmd."}",
        );
        $replace = STARTPHP." echo \$$cmd; ".ENDPHP;

        $tmpcontent = str_replace($search, $replace, $tmpcontent);
    }
    return $tmpcontent;
}

function atTemplateRead($file)
{
    $HTML = @file_get_contents($file);

    return $HTML;
}

function atTemplatePrep($filename, $striphead=1)
{
    if (defined('AUTOTHEME_DEBUG_ENABLED')) {
        if (is_dir($filename)) {
            return _AT_TEMPLATENOTDEFINED;
        }
        elseif (!is_file($filename)) {
            return _AT_TEMPLATENOTFOUND.": $filename<br />";
        }
    }
    $HTML = atTemplateRead($filename);

    if ($striphead) {
        $HTML = spliti('\<\/head\>', $HTML);
    }
    else {
        $HTML = spliti('\<head\>', $HTML);
    }
    if (isset($HTML[1])) {
        $HTML = $HTML[1];
    }
    else {
        $HTML = $HTML[0];
    }
    $HTML = spliti('\<\/body\>', $HTML);
    $HTML = trim($HTML[0]);

    return $HTML;
}

function atTemplateSplit($content, $spliton)
{
    $parts = preg_split("/(\<\!--[ ]*\[|{)$spliton(}|\][ ]*--\>)/", $content);

    return array($parts[0], $parts[1]);
}

function atTemplateDisplay($tmpcontent)
{
    $runningconfig = atGetRunningConfig();
    extract($runningconfig);

    eval(ENDPHP.$tmpcontent);
}

function atModLangLoad($type)
{
    $lang = atGetLang();

    if (@file_exists("modules/AutoTheme/lang/$lang/$type.php")) {
        @include("modules/AutoTheme/lang/$lang/$type.php");
    }
    else {
        @include("modules/AutoTheme/lang/english/$type.php");
    }
}

function atThemeLangLoad($themepath)
{
    $lang = atGetLang();

    if (@file_exists("$themepath/lang/$lang/global.php")) {
        @include("$themepath/lang/$lang/global.php");
    }
    else {
        @include("$themepath/lang/eng/global.php");
    }
}

function atExportVar($var)
{
    if (get_magic_quotes_gpc()) {
        $var = stripslashes($var);
    }
    return $var;
}

function atDisplayVar($var)
{
    return htmlentities($var);
}

function atLoadAutoConfig($path)
{
    @include($path."/autotheme.cfg");

    $autoconfig = compact("cache", "cache_expire", "autotheme", "autoblock", "autolang", "autocmd", "autoextra");

    foreach ($autoextra as $k => $v) {
        if (isset($$k)) {
            $autoconfig[$k] = $$k;
        }
    }
    atAutoMultiSetVars($autoconfig);

    return $autoconfig;
}

function atGetAutoConfig()
{
    if (isset($GLOBALS['AT_AUTO'])) {
        return $GLOBALS['AT_AUTO'];
    }
    else {
        return false;
    }
}

function atAutoMultiSetVars($vars)
{
    foreach ($vars as $name => $val) {
        atAutoSetVar($name, $val);
    }
}

function atAutoSetVar($name, $val)
{
	$GLOBALS['AT_AUTO'][$name] = $val;
}

function atAutoGetVar($var)
{
    if (isset($GLOBALS['AT_AUTO'][$var])) {
        return $GLOBALS['AT_AUTO'][$var];
    }
    else {
        return false;
    }
}

function atLoadThemeConfig($path, $atdir="modules/AutoTheme")
{
    @include($atdir."/autotheme.cfg");
    @include("modules/Blocks/autoblock.cfg");
    @include($path."/autoblock.cfg");
	@include($path."/theme.cfg");

    $themeconfig = compact("template", "blockdisplay", "style", "blocktemplate", "autoblock", "themecmd", "themeversion");

    foreach ($autoextra as $k => $v) {
        if (isset($$k)) {
            $themeconfig[$k] = $$k;
        }
    }
    atThemeMultiSetVars($themeconfig);

    return $themeconfig;
}

function atGetThemeConfig()
{
    return $GLOBALS['AT_THEME'];
}

function atThemeMultiSetVars($vars)
{
    foreach ($vars as $name => $val) {
        atThemeSetVar($name, $val);
    }
}

function atThemeSetVar($name, $val)
{
    $GLOBALS['AT_THEME'][$name] = $val;
}

function atThemeGetVar($var)
{
    return $GLOBALS['AT_THEME'][$var];
}

function atGetRunningConfig()
{
    return $GLOBALS['AT_RUNNING'];
}

function atRunningMultiSetVars($vars)
{
    foreach ($vars as $name => $val) {
        atRunningSetVar($name, $val);
    }
}

function atRunningSetVar($name, $val)
{
     $GLOBALS['AT_RUNNING'][$name] = $val;
}

function atRunningGetVar($var)
{
    return $GLOBALS['AT_RUNNING'][$var];
}

function atGetGlobalConfig()
{
    $autoconfig = $themeconfig = array();

    if (isset($GLOBALS['AT_AUTO'])) {
        $autoconfig = $GLOBALS['AT_AUTO'];
        $auto = 1;
    }
    if (isset($GLOBALS['AT_THEME'])) {
        $themeconfig = $GLOBALS['AT_THEME'];
        $theme = 1;
    }
    if (!isset($auto) && !isset($theme)) {
        return false;
    }
    else {
        return array_merge((array)$autoconfig, (array)$themeconfig);
    }
}

function atGetBaseDir()
{
    return $_SERVER['DOCUMENT_ROOT']."/";
}

function atGetPlatform()
{
    if (isset($GLOBALS['mainfile']) && $GLOBALS['mainfile'] == 1) {
        $platform = "PHP-Nuke";
    }
    if (defined('CPG_NUKE')) {
        $platform = "CPG-Nuke";
    }
    if (function_exists("pnConfigGetVar")) {
        $platform = pnConfigGetVar('Version_ID');
    }
    $platform = strtolower($platform);

    return $platform;
}

function atThemeOpen()
{
	$runningconfig = atGetRunningConfig();

    if (!isset($runningconfig['extra'])) {
        return;
    }
    $extra = $runningconfig['extra'];

    foreach ($extra as $name) {
		if (isset($name['themepreprocess'])) {
			$themeprefuncs[] = $name['themepreprocess'];
		}
		if (isset($name['themeopen'])) {
            $func = $name['themeopen'];
            if (function_exists("$func")) {
            	$func($runningconfig);
            }
        }
    }
    if (isset($themeprefuncs)) {
    	atThemePreProcess($themeprefuncs);
    }
}

function atThemePreProcess($funcs)
{
	$display = ob_get_contents();
    ob_end_clean();
    ob_start();

	foreach ($funcs as $func) {
		if (function_exists("$func")) {
            $display = $func($display);
        }
    }
    echo $display;
}

function atBodyOpen()
{
    $runningconfig = atGetRunningConfig();

    if (!isset($runningconfig['extra'])) {
        return;
    }
    $extra = $runningconfig['extra'];

    foreach ($extra as $name) {
		if (isset($name['bodypreprocess'])) {
			$bodyprefuncs[] = $name['bodypreprocess'];
		}
		if (isset($name['bodyopen'])) {
            $func = $name['bodyopen'];
            if (function_exists("$func")) {
            	$func($runningconfig);
            }
        }
    }
    if (isset($bodyprefuncs)) {
    	atBodyPreProcess($bodyprefuncs);
    }
}

function atBodyPreProcess($funcs)
{
	$display = ob_get_contents();
    ob_end_clean();
    ob_start();

	foreach ($funcs as $func) {
		if (function_exists("$func")) {
            $display = $func($display);
        }
    }
    echo $display;
}

function atModOpen()
{
	if (atGetModStyle() == "new") {
    	echo $GLOBALS['return'];
    	$GLOBALS['return'] = "";
    }

    $runningconfig = atGetRunningConfig();

    if (!isset($runningconfig['extra'])) {
        return;
    }
    $extra = $runningconfig['extra'];

    foreach ($extra as $name) {
		if (isset($name['modpreprocess'])) {
			$modprefuncs[] = $name['modpreprocess'];
		}
		if (isset($name['modopen'])) {
            $func = $name['modopen'];
            if (function_exists("$func")) {
            	$func($runningconfig);
            }
        }
    }
    if (isset($modprefuncs)) {
    	atModPreProcess($modprefuncs);
    }
    if (atGetModStyle() == "new") {
    	ob_start();
    }
}

function atModPreProcess($funcs)
{
    $display = ob_get_contents();
    ob_end_clean();
    ob_start();

	foreach ($funcs as $func) {
		if (function_exists("$func")) {
            $display = $func($display);
        }
    }
    echo $display;
}

function atModClose()
{
	if (atGetModStyle() == "new") {
    	ob_end_clean();
    }
    $runningconfig = atGetRunningConfig();

    if (!isset($runningconfig['extra'])) {
        return;
    }
    $extra = $runningconfig['extra'];

    foreach ($extra as $name) {
		if (isset($name['modpostprocess'])) {
			$modpostfuncs[] = $name['modpostprocess'];
		}
		if (isset($name['modclose'])) {
            $func = $name['modclose'];
            if (function_exists("$func")) {
            	$func($runningconfig);
            }
        }
    }
    if (isset($modpostfuncs)) {
    	atModPostProcess($modpostfuncs);
    }
}

function atModPostProcess($funcs)
{
    $display = ob_get_contents();
    ob_end_clean();
   	ob_start();

	foreach ($funcs as $func) {
		if (function_exists("$func")) {
            $display = $func($display);
        }
    }
    echo $display;
}

function atBlockOpen()
{
    $runningconfig = atGetRunningConfig();

    if (!isset($runningconfig['extra'])) {
        return;
    }
    $extra = $runningconfig['extra'];

	foreach ($extra as $name) {
		if (isset($name['blockpreprocess'])) {
			$blockprefuncs[] = $name['blockpreprocess'];
		}
		if (isset($name['blockopen'])) {
            $func = $name['blockopen'];
            if (function_exists("$func")) {
            	$func($runningconfig);
            }
        }
    }
    if (isset($blockprefuncs)) {
    	atBlockPreProcess($blockprefuncs);
    }
}

function atBlockPreProcess($funcs)
{
    $display = ob_get_contents();
    ob_end_clean();
    ob_start();

	foreach ($funcs as $func) {
		if (function_exists("$func")) {
            $display = $func($display);
        }
    }
    echo $display;
}

function atBlockClose()
{
    $runningconfig = atGetRunningConfig();

    if (!isset($runningconfig['extra'])) {
        return;
    }
    $extra = $runningconfig['extra'];

	foreach ($extra as $name) {
		if (isset($name['blockpostprocess'])) {
			$blockpostfuncs[] = $name['blockpostprocess'];
		}
		if (isset($name['blockclose'])) {
            $func = $name['blockclose'];
            if (function_exists("$func")) {
            	$func($runningconfig);
            }
        }
    }
    if (isset($blockpostfuncs)) {
    	atBlockPostProcess($blockpostfuncs);
    }
}

function atBlockPostProcess($funcs)
{
    $display = ob_get_contents();
    ob_end_clean();
    ob_start();

	foreach ($funcs as $func) {
		if (function_exists("$func")) {
            $display = $func($display);
        }
    }
    echo $display;
}

function atThemePostProcess($funcs)
{
	$display = ob_get_contents();
    ob_end_clean();
    ob_start();

    foreach ($funcs as $func) {
		if (function_exists("$func")) {
            $display = $func($display);
        }
    }
    echo $display;
}

function atThemeHeader()
{
	atThemeOpen();
 	atThemeAddHeader();
	atBodyOpen();

    $runningconfig = atGetRunningConfig();
    extract($runningconfig);

    $file = $template['main'];

    $template = atTemplateCompile($themepath.$file, $striphead);
    list($output, $footer) = atTemplateSplit($template, "modules");

    atTemplateDisplay($output);

    atModOpen();
}

function atThemeFooter()
{
	atModClose();

    $runningconfig = atGetRunningConfig();
    extract($runningconfig);

    $file = $template['main'];

    $template = atTemplateCompile($themepath.$file, $striphead);
    list($header, $output) = atTemplateSplit($template, "modules");

    atTemplateDisplay($output);

    atThemeAddFooter();
	atThemeClose();
}

function atNewsSummary($text, $url, $html)
{
    foreach ($text as $name => $val) {
        $news["text:$name"] = $val;
    }
    foreach ($url as $name => $val) {
        $news["url:$name"] = $val;
    }
    foreach ($html as $name => $val) {
        $news["html:$name"] = $val;
    }
    atCommandBuild($news, "news");

    $runningconfig = atGetRunningConfig();
    extract($runningconfig);

    if (!isset($summary_count)) {
        static $summary_count = "";
    }
    $summary_count ++;

    if ($template['altsummary'] && $template['summary1'] && $template['summary2']) {
        if ($summary_count % 2 == 0) {
            $file = $template['summary1'];
        }
        else {
            $file = $template['summary2'];
        }
    }
    else {
        $file = $template['summary'];
    }
    $output = atTemplateCompile($themepath.$file);

    atTemplateDisplay($output);
}

function atNewsArticle($text, $url, $html)
{
    foreach ($text as $name => $val) {
        $news["text:$name"] = $val;
    }
    foreach ($url as $name => $val) {
        $news["url:$name"] = $val;
    }
    foreach ($html as $name => $val) {
        $news["html:$name"] = $val;
    }
    atCommandBuild($news, "news");

    $runningconfig = atGetRunningConfig();
    extract($runningconfig);

    $file = $template['article'];

    $output = atTemplateCompile($themepath.$file);

    atTemplateDisplay($output);
}

function atThemeBlock($block)
{
    atCommandBuild($block, "block");
	atBlockOpen();

    $runningconfig = atGetRunningConfig();
    extract($runningconfig);

    $location = $block['position'];
    $blocktitle = trim(strip_tags($block['title'], ""));

    if ($block['modname'] == "Admin_Messages" && $block['bkey'] == "messages") {
    	$blocktitle = "Administration Messages";
    }

    switch ($location) {
		case 'l':
            $file = $template['leftblock'];
            break;

        case 'r':
            $file = $template['rightblock'];
            break;

        case 'c':
            $file = $template['centerblock'];
            break;

        default:
            if (isset($autoblock[$location])) {
        	   $file = $template["autoblock".$location];
            }
            break;
    }
    if (isset($blocktemplate[$blocktitle])) {
		$file = $blocktemplate[$blocktitle];
    }
    $output = atTemplateCompile($themepath.$file);

    atTemplateDisplay($output);

    atBlockClose();
}

function atBlockDisplay($location="", $title="")
{
    $runningconfig = atGetRunningConfig();
    extract($runningconfig);

    $display = 0;

    if ($location) {
        switch ($location) {
            case 'l':
                if ($blockdisplay['left']) {
                    $display = 1;
                }
                break;

            case 'r':
                if ($blockdisplay['right']) {
        	       $display = 1;
                }
                break;

            case 'c':
                if ($blockdisplay['center']) {
                    $display = 1;
                }
                break;

            case 'd':
                if ($blockdisplay['center']) {
                    $display = 1;
                }
                break;

            default:
                if (isset($autoblock[$location]) && $blockdisplay["autoblock".$location]) {
                    $display = 1;
                }
                break;
        }
        if ($display) {
            atBlockLoad($location);
        }
    }
    elseif ($title) {
        atBlockLoad("", $title);
    }
}

function atExtraCommandLoad($dir)
{
    $atdir = atAutoGetVar("atdir");
    $platform = atGetPlatform();
    $lang = atGetLang();

    if ($handle = @opendir($dir)) {
        while (false !== ($file = @readdir($handle))) {
            if (eregi(".cmd.php", $file)) {
                $extracmd = array();

                $parts = explode(".", $file);
                $name = $parts[0];
                @include_once($atdir."lang/$lang/$name.php");
                @include_once($dir.$file);

                foreach ($extracmd as $type => $cmds) {
                    foreach ($cmds as $cmd => $action) {
                        $cmdresult[$type][$cmd] = $extracmd[$type][$cmd];
                    }
                }
            }
        }
        closedir($handle);
    }
    if ($handle = @opendir($dir.$platform)) {
        while (false !== ($file = @readdir($handle))) {
            if (eregi(".cmd.php", $file)) {
                $extracmd = array();

                $parts = explode(".", $file);
                $name = $parts[0];
                @include_once($atdir."lang/$lang/$name.php");
                @include_once($dir.$platform."/$file");

                foreach ($extracmd as $type => $cmds) {
                    foreach ($cmds as $cmd => $action) {
                        $cmdresult[$type][$cmd] = $extracmd[$type][$cmd];
                    }
                }
            }
        }
        closedir($handle);
    }
    return $cmdresult;
}

function atExtraScan($dir)
{
    $extra = atRunningGetVar("extra");
    $atdir = atAutoGetVar("atdir");
    $platform = atGetPlatform();
    $lang = atGetLang();

    if ($handle = @opendir($dir.$platform)) {
        while (false !== ($file = @readdir($handle))) {
            if (eregi(".ext.php", $file)) {
                $parts = explode(".", $file);
                $name = $parts[0];
                $loaded[$name] = 1;
                @include_once($atdir."lang/$lang/$name.php");
                @include_once($dir.$platform."/$file");
            }
        }
        closedir($handle);
    }
    if ($handle = @opendir($dir)) {
        while (false !== ($file = @readdir($handle))) {
            if (eregi(".ext.php", $file)) {
                $parts = explode(".", $file);
                $name = $parts[0];
                if (!$loaded[$name]) {
                    @include_once($atdir."lang/$lang/$name.php");
                    @include_once($dir.$file);
                }
            }
        }
        closedir($handle);
    }
    atAutoSetVar("extra", $extra);

    return $extra;
}

function atExtraLoadAll()
{
    $globalconfig = atGetGlobalConfig();
    extract($globalconfig);

    $result = array();
    $lang = atGetLang();

    foreach ($autoextra as $name => $val) {
        if ($val) {
            $extra = atExtraLoad($name);
            array_push($result, $extra);
        }
    }
    return $result;
}

function atExtraLoad($name)
{
    $globalconfig = atGetGlobalConfig();
    extract($globalconfig);

    $extra = atRunningGetVar("extra");
    $platform = atGetPlatform();
    $lang = atGetLang();

    @include_once($atdir."lang/$lang/$name.php");
    if (@file_exists($extradir.$platform."/$name.ext.php")) {
        @include_once($extradir.$platform."/$name.ext.php");
    }
    else {
        @include_once($extradir."$name.ext.php");
    }
    atRunningSetVar("extra", $extra);

    return $extra[$name];
}

/* compile functions */
function atCompileRead($filename, $modifier="")
{
    $globalconfig = atGetGlobalConfig();
    extract($globalconfig);

    $oldmask = umask(0);

    $filetime =  @filemtime($incdir."atCommands.php")
				+@filemtime($platformdir."atCommands.php")
               	+@filemtime($atdir."autotheme.cfg")
               	+@filemtime($themepath."theme.cfg")
               	+@filemtime($filename);

    $filepre = atGetCompileFilename($filename, $modifier);
    $filename = $filepre."_".$filetime;

    if ($output = atTemplateRead($compiledir.$filename)) {
    	return $output;
    }
    else {
        return false;
    }
}

function atCompileWrite($filename, $content, $modifier="")
{
    $globalconfig = atGetGlobalConfig();
    extract($globalconfig);

    $oldmask = umask(0);

    $filetime =  @filemtime($incdir."atCommands.php")
				+@filemtime($platformdir."atCommands.php")
               	+@filemtime($atdir."autotheme.cfg")
               	+@filemtime($themepath."theme.cfg")
               	+@filemtime($filename);

    $filepre = atGetCompileFilename($filename, $modifier);
    $filename = $filepre."_".$filetime;

    if ($handle = @opendir($compiledir)) {
       while (false !== ($file = @readdir($handle))) {
            if (false !== strpos($file, $filepre)) {
                @unlink($compiledir.$file);

                if (function_exists('atcacheclear')) {
    				atCacheClear();
    			}
                break;
            }
        }
        closedir($handle);
    }
    if ($handle = @fopen($compiledir.$filename, "w")) {
        fwrite($handle, $content);
	    fclose($handle);
    }
}

function atCompileClear()
{
	$compiledir = atAutoGetVar('compiledir');

    if ($handle = @opendir($compiledir)) {
            while (false !== ($file = @readdir($handle))) {
                @unlink($compiledir.$file);
            }
            closedir($handle);
    }
    if (function_exists('atcacheclear')) {
    	atCacheClear();
    }
}

function atGetCompileFilename($filename, $modifier="")
{
	$thename = atRunningGetVar('thename');

	if (atIsLoggedIn()) {
        $user = 1;
    }
    else {
        $user = 0;
    }
    if (atIsAdminUser()) {
        $admin = 1;
    }
    else {
        $admin = 0;
    }
    $temp = $user.$admin.$thename.$filename.$modifier;
	$filepre = md5($temp);

	return $filepre;
}

function atTemplateCompile($file, $striphead=1)
{
    $globalconfig = atGetGlobalConfig();
    extract($globalconfig);

    if (!$output = atCompileRead($file)) {
            $HTML = atTemplatePrep($file, $striphead);
            $output = atCommandReplace($HTML, $command);
            atCompileWrite($file, $output);
    }
    return $output;
}

/* function to get output from pnHTML and build command from it */
function atModCommand($command, &$output)
{
    $$command = $output->GetOutput();

    atCommandBuild(compact("$command"));
}

/* function to build commands from module */
function atCommandBuild2($name, $commands, $prefix="")
{
    if (!$prefix) {
        $prefix = strtolower(atGetModName());
    }
    $$name = $commands;
    $commands = compact("$name");

    atRunningSetVar($prefix, $commands);

    foreach ($commands as $key => $val) {
        foreach ($val as $num => $cmd) {
            foreach($cmd as $cname => $action) {
            atCommandAdd("$prefix:$key:$cname", 'echo $'.$prefix.'["'.$key.'"][$i]["'.$cname.'"];');
            }
        }
    }
}

function atModOutput($file)
{
    $globalconfig = atGetGlobalConfig();
    extract($globalconfig);

    $modname = atGetModName();

    if (file_exists($themepath."modules/$modname/$file")) {
        $file = $themepath."modules/$modname/$file";
    }
    elseif (file_exists("modules/$modname/templates/$file")) {
        $file = "modules/$modname/templates/$file";
    }
    else {
        return false;
    }
    $output = atTemplateCompile($file);

    ob_start();
    atTemplateDisplay($output);
    $display = ob_get_contents();
    ob_end_clean();

    return $display;
}

/* function to add an array and a function that loops through the array and displays a template */
function atCommandLoopAdd($function, $array)
{
    /* maybe need to add commandadd for array and func
    maybe not use this, use command add and template display or combination, why dedicated func? */
    $globalconfig = atGetGlobalConfig();
    extract($globalconfig);

    eval('function '.$function.'(\$array, \$file)
    {
        $globalconfig = atGetGlobalConfig();
        extract($globalconfig);

        $modname = atGetModName();

        if (file_exists($themepath."modules/$modname/$file")) {
           $file = $themepath."modules/$modname/$file";
           }
        elseif (file_exists("modules/$modname/templates/$file")) {
           $file = "modules/$modname/templates/$file";
        }
        else {
           return false;
        }
        $output = atTemplateCompile($file);
        atTemplateDisplay($output);
    }');
}

/**********************************/
/* Utility functions              */
/**********************************/
function array_display($var)
{
    foreach ($var as $k => $v) {
        if (is_array($v)) {
            echo $k;
            array_display($v);
            continue;
        }
        echo "<pre>". htmlentities("$k = $v") ."</pre>";
    }
}

/**********************************/
/* Compatibility functions        */
/**********************************/
if (!function_exists("file_get_contents")) {
    function file_get_contents($filename)
    {
        if (@is_file($filename)) {
            $result = "";
            $handle = @fopen($filename, "r");
            if (!$handle) {
                return false;
            }
            while (!feof($handle)) {
                $result .= fread($handle, 4096);
            }
            return $result;
        }
        else {
            return false;
        }
    }
}

if (!function_exists("var_export")) {
    function var_export($var)
    {
        $result = "";

        switch (gettype($var)) {
            case "array":
                reset($var);
                $result = "array(\n";
                foreach ($var as $k => $v) {
                    $result .= "  " . var_export($k)." => " . var_export($v).",\n";
                }
                $result .= ")";
                break;

            case "string":
                $result = "'$var'";
                break;

            case "boolean":
                $result = ($var) ? "true" : "false";
                break;

            default:
                if (empty($var)) {
                    $result = "''";
                }
                else {
                    $result = $var;
                }
                break;
        }
        return $result;
    }
}

/**********************************/
/* Administration functions       */
/**********************************/
function list_themes($dir="themes")
{
    $themelist = list_core_themes($dir);

    if ($multibase = get_multisite_base()) {
        $multilist = list_multisite_themes($multibase);
        $themelist = array_merge((array)$themelist, (array)$multilist);
    }
    return $themelist;
}

function list_core_themes($dir="themes")
{
    if ($handle = @opendir($dir)) {
        while (false !== ($subdir = @readdir($handle))) {
            if (@is_dir("$dir/$subdir") &&
                $subdir !== '.' &&
                $subdir !== '..' &&
                @file_exists("$dir/$subdir/theme.cfg"))
            {
                $themelist[] = $subdir;
            }
        }
        closedir($handle);
    }
    return $themelist;
}

function list_multisite_themes($dir="parameters")
{
    $themelist = array();

    if ($handle = @opendir($dir)) {
        while (false !== ($subdir = @readdir($handle))) {
            if (@is_dir("$dir/$subdir/themes") &&
                $subdir !== '.' &&
                $subdir !== '..')
            {
                $multilist[$subdir] = list_core_themes("$dir/$subdir/themes");
            }
        }
        closedir($handle);
    }
    foreach ($multilist as $multidir => $list) {
        foreach ($list as $themedir) {
            $themelist[] = "$dir/$multidir/themes/$themedir";
        }
    }
    return $themelist;
}

function get_multisite_base()
{
    if (defined('WHERE_IS_PERSO')) {
        $pathparts = explode("/", WHERE_IS_PERSO);
        array_pop($pathparts);
        array_pop($pathparts);
        $multibase = implode("/", $pathparts);

        return $multibase;
    }
    else {
        return false;
    }
}

function get_multisite_name($path)
{
    $pathparts = explode("/", $path);

    if ($pathparts) {
        $multisite = $pathparts[count($pathparts)- 3];
        return $multisite;
    }
    else {
        return false;
    }
}

function get_theme_name($path)
{
    $pathparts = explode("/", $path);
    $themename = array_pop($pathparts);

    return $themename;
}

function get_theme_path($themedir)
{
    if (@file_exists("themes/$themedir")) {
        $thepath = "themes/$themedir/";
    }
    elseif (@file_exists("$themedir")) {
        $thepath = "$themedir/";
    }
    return $thepath;
}

function list_files($dir, $ext)
{
    $filelist = $newlist = array();

    if ($handle = @opendir($dir)) {
        while (false !== ($item = @readdir($handle))) {
            if (@!is_dir($dir . $item) && eregi(".$ext", $item)) {
                $filelist[] = $item;
            }
            elseif (@is_dir("$dir/$item") && $item !== '.' && $item !== '..') {
                $sublist = list_files("$dir/$item", $ext);
                if (is_array($sublist)) {
                    foreach ($sublist as $file) {
                        $newlist[] = "$item/$file";
                    }
                }
            }
        }
        closedir($handle);

        $filelist = array_merge((array)$filelist, (array)$newlist);

        return $filelist;
    }
    else {
        return false;
    }
}

function display_error($message)
{
    AutoThemeHeader();
    echo $message;
    AutoThemeFooter();
    die();
}

function input_field($opt)
{
	$input .= "<input";

	unset($opt['label']);

	foreach ($opt as $k => $v) {
		$input .= " $k=\"$v\"";
	}
	$input .= ">";

	return $input;
}

?>
