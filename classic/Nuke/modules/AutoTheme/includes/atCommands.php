<?php
// ----------------------------------------------------------------------
// Copyright (c) 2002-2004 Shawn McKenzie
// http://spidean.mckenzies.net
// ----------------------------------------------------------------------
// LICENSE
//
// This software is the copyrighted material of Shawn McKenzie. AutoTheme is
// licensed for use on (1) web site installation only. See the license.txt for your
// specific license.
// 
// YOU AGREE NOT TO COPY OR DISTRIBUTE THIS SOFTWARE OR CHANGE THE SOURCE CODE
// WITHOUT PRIOR WRITTEN PERMISSION. PLEASE SEND ANY QUESTIONS OR COMMENTS TO
// AUTOTHEME@MCKENZIES.NET.
// 
// SHAWN MCKENZIE, TAKES NO RESPONSIBILITY FOR ANY DAMAGE THAT THIS SOFTWARE MIGHT
// CAUSE.
//
// Additional licensing information is available from autotheme@mckenzies.net.
// 
// If you disagree with any of these terms, you are not authorized to use this
// software. If you do agree with these terms, please enjoy.
// ----------------------------------------------------------------------

$command['anonymous'] = array (
);

$command['loggedin'] = array (
);

$command['admin'] = array (
);

$command['all'] = array (
	'theme-path' => 'echo $themepath;',
	'image-path' => 'echo $imagepath;',
	'footer-msg' => 'echo footmsg();',
	'open-table' => 'OpenTable();',
	'close-table' => 'CloseTable();',
	'open-table2' => 'OpenTable2();',
	'close-table2' => 'CloseTable2();',
	'user' => 'echo $username;',
	'user-welcome' => 'echo _AT_WELCOME." ".$username;',
	'logo-image' => 'if (isset($logoimg) && file_exists($themepath.$logoimg) && !is_dir($themepath.$logoimg)) {'
	.'echo "<img src=\"".$themepath.$logoimg."\" alt=\"\" border=\"0\">\n";}',
	'block-title' => 'echo $block["title"];',
	'block-content' => 'echo $block["content"];',
	'left-blocks' => 'atBlockDisplay("l");',
	'center-blocks' => 'atBlockDisplay("c");',
	'right-blocks' => 'atBlockDisplay("r");',
	'color1' => 'echo $bgcolor1;',
	'color2' => 'echo $bgcolor2;',
	'color3' => 'echo $bgcolor3;',
	'color4' => 'echo $bgcolor4;',
	'color5' => 'echo $textcolor1;',
	'color6' => 'echo $textcolor2;',
	'color7' => 'echo $tblcolor1;',
	'color8' => 'echo $tblcolor2;',
	'color9' => 'echo $tblcolor3;',
	'color10' => 'echo $tblcolor4;',
);

if (isset($autoblock)) {
    foreach ($autoblock as $key => $ablock){
            $command['all']['autoblock'.$key.'-blocks'] = 'atBlockDisplay("'.$key.'");';
			$command['all'][$ablock.'-blocks'] = 'atBlockDisplay("'.$key.'");';
            $command['all'][strtolower($ablock.'-blocks')] = 'atBlockDisplay("'.$key.'");';
    }
}

?>
