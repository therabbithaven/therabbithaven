<?php
// ----------------------------------------------------------------------
// Copyright (c) 2002-2004 Shawn McKenzie
// http://spidean.mckenzies.net
// ----------------------------------------------------------------------
// LICENSE
//
// This software is the copyrighted material of Shawn McKenzie. AutoTheme is
// licensed for use on (1) web site installation only. See the license.txt for your
// specific license.
// 
// YOU AGREE NOT TO COPY OR DISTRIBUTE THIS SOFTWARE OR CHANGE THE SOURCE CODE
// WITHOUT PRIOR WRITTEN PERMISSION. PLEASE SEND ANY QUESTIONS OR COMMENTS TO
// AUTOTHEME@MCKENZIES.NET.
// 
// SHAWN MCKENZIE, TAKES NO RESPONSIBILITY FOR ANY DAMAGE THAT THIS SOFTWARE MIGHT
// CAUSE.
//
// Additional licensing information is available from autotheme@mckenzies.net.
// 
// If you disagree with any of these terms, you are not authorized to use this
// software. If you do agree with these terms, please enjoy.
// ----------------------------------------------------------------------

function atExtendedInit()
{
	if ($content = atCacheRead()) {
		echo $content;
		
		$mtime = explode(" ", microtime());
		$renderstop = $mtime[1] + $mtime[0];
		$rendertime = ($renderstop - $GLOBALS['renderstart']);
		
		printf('<div align="center">'._AT_READFROMCACHE." %f "._AT_SECONDS.'</div>', $rendertime);
		
		die("\n</body>\n</html>");
	}
}
	
function atThemeClose()
{    
    $runningconfig = atGetRunningConfig();

    if (!isset($runningconfig['extra'])) {
        return;
    }
    $extra = $runningconfig['extra'];

    foreach ($extra as $name) {
		if (isset($name['themepostprocess'])) {
			$themepostfuncs[] = $name['themepostprocess'];
		}
		if (isset($name['themeclose'])) {
            $func = $name['themeclose'];
            if (function_exists("$func")) {
            	$func($runningconfig);
            }
        }
    }
    if (isset($themepostfuncs)) {
    	atThemePostProcess($themepostfuncs);
    }
    atCacheWrite();
    
    if (atGetModStyle() == "new") {
    	ob_start();
    }
}

function atThemeAddHeader()
{
    $runningconfig = atGetRunningConfig();
    extract($runningconfig);

    extract($style);

    $head .= "\n<!--\n"
    ."************************ AutoTheme 1.7 **************************\n"
    ."-->\n\n";
    
    $head .= "\n<link rel=\"stylesheet\" href=\"".$atdir."style/style.css\" type=\"text/css\">\n";
    
    if (isset($stylesheet)) {
        if (@file_exists($themepath.$stylesheet) && !@is_dir($themepath.$stylesheet)) {
            $head .= "\n<link rel=\"stylesheet\" href=\"".$themepath.$stylesheet."\" type=\"text/css\">\n";
        }
    }
    if ($striphead) {
		$head .= "</head>\n\n";
	}
	echo $head;
}

function atThemeAddFooter()
{
    $runningconfig = atGetRunningConfig();
    extract($runningconfig);

    $footer = "\n<!--\n"
    ."************************ AutoTheme 1.7 **************************\n"
    ."-->\n";

	echo $footer;
}

function atCacheRead()
{
	$runningconfig = atGetRunningConfig();
    extract($runningconfig);
        
	$cache_on = $cache['on'];
	$cache_expire = $cache['expire'];
	$db_expire = $cache['db'];
	$exclude = $cache['exclude'];
	$oldmask = umask(0); 
	
	if (!$cache_on) {
		return false;
	}
	elseif (in_array($modname, $exclude) || in_array($modtemplate, $exclude)) {
		return false;
	} 
    $filename = atGetCacheFilename();
    
    if (@filemtime($cachedir.$filename) >= (time() - $cache_expire)) {
    	if ($output = atTemplateRead($cachedir.$filename)) {
    		return $output;
    	}
    	else {
    		return false;
    	}
    }
    else {
    	return false;
    }
}

function atCacheWrite()
{
    $runningconfig = atGetRunningConfig();
    extract($runningconfig);
        
	$cache_on = $cache['on'];
    $cache_expire = $cache['expire'];
    $db_expire = $cache['db'];
	$exclude = $cache['exclude'];
	$oldmask = umask(0);
	
	if (!$cache_on) {
		return false;
	}
	elseif (in_array($modname, $exclude) || in_array($modtemplate, $exclude)) {
		return false;
	}
    $filename = atGetCacheFilename();

    if (@file_exists($cachedir.$filename)) {
    	@unlink($cachedir.$filename);
    }
     
    $content = ob_get_contents();
    
    if ($handle = @fopen($cachedir.$filename, "w")) {
        fwrite($handle, $content);
	    fclose($handle);
    }
}

function atCacheClear()
{
    $cachedir = atAutoGetVar('cachedir');

    if ($handle = @opendir($cachedir)) {
            while (false !== ($file = @readdir($handle))) {
                @unlink($cachedir.$file);
            }
            closedir($handle);
    }
}

function atGetCacheFilename()
{	
	$username = atGetUserName();
	$thename = atRunningGetVar('thename');
	
	$getvars = serialize($_GET);
	$postvars = serialize($_POST);
	$temp = $username.$thename.$_SERVER['PHP_SELF'].$getvars.$postvars;
	$filename = md5($temp);
	
	return $filename;
}

function atGetDBUpdateTime()
{
	list($dbconn) = pnDBGetConn();
	
	$time = 0;
	
	switch ($dbconn->databaseType) {
		case "oci8":
			$result = $dbconn->Execute("SELECT last_ddl_time FROM user_objects ORDER BY last_ddl_time DESC");
			$row = $result->GetRowAssoc(false);
        	$time = strtotime($row['last_ddl_time']);		
		break;		
		
		default:
			$result = $dbconn->Execute("SHOW TABLE STATUS");		
			while (!$result->EOF) {
        		$row = $result->GetRowAssoc(false);
        		echo $row['update_time']."<br>";
        		$temp = strtotime($row['update_time']);
        
        		if ($temp > $time) {
        			$time = $temp;
        		}        
        		$result->MoveNext();
    		}
			break;
		}	
    	return $time;
}

?>
