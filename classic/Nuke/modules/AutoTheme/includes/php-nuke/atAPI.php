<?php
// ----------------------------------------------------------------------
// Copyright (c) 2002-2004 Shawn McKenzie
// http://spidean.mckenzies.net
// ----------------------------------------------------------------------
// LICENSE
//
// This software is the copyrighted material of Shawn McKenzie. AutoTheme is
// licensed for use on (1) web site installation only. See the license.txt for your
// specific license.
// 
// YOU AGREE NOT TO COPY OR DISTRIBUTE THIS SOFTWARE OR CHANGE THE SOURCE CODE
// WITHOUT PRIOR WRITTEN PERMISSION. PLEASE SEND ANY QUESTIONS OR COMMENTS TO
// AUTOTHEME@MCKENZIES.NET.
// 
// SHAWN MCKENZIE, TAKES NO RESPONSIBILITY FOR ANY DAMAGE THAT THIS SOFTWARE MIGHT
// CAUSE.
//
// Additional licensing information is available from autotheme@mckenzies.net.
// 
// If you disagree with any of these terms, you are not authorized to use this
// software. If you do agree with these terms, please enjoy.
// ----------------------------------------------------------------------

function atPlatformAPIInit($atdir)
{
    return array();
}

function atPlatformThemeInit($thename)
{
    $GLOBALS['home_page'] = $GLOBALS['home'];
    $GLOBALS['home'] = 0;

    if (isset($_GET['theme'])) {
        $oldtheme = $thename;
        $thename = $_GET['theme'];
        $GLOBALS['oldtheme'] = $oldtheme;
    }
    $vars = compact("thename");

    return $vars;
}

function atGetPlatformVersion()
{
    return $GLOBALS['Version_Num'];
}

function atGetHomeMod()
{
        $module = $GLOBALS['main_module'];

    return $module;
}

function atIsHomePage()
{
    if ($GLOBALS['home_page'] == 1) {
        return true;
    }
    else {
        return false;
    }
}

function atIsAdminUser()
{
    if (is_admin($GLOBALS['admin'])) {
        return true;
    }
    else {
        return false;
    }
}

function atIsLoggedIn()
{
    if (is_user($GLOBALS['user'])) {
        return true;
    }
    else {
        return false;
    }
}

function atGetUserName()
{
    if (!atIsLoggedIn()) {
        $username = _AT_GUEST;
    }
    else {
        $cookie = cookiedecode($GLOBALS['user']);
        $username = $cookie[1];
    }
    return $username;
}

function atGetModType()
{
    $type = "";
    
    if (eregi("admin.php", $_SERVER['PHP_SELF'])) {
        $type = "admin";
    }
    elseif (atGetModName() == "Your_Account") {
        $type = "user";
    }
    return $type;
}

function atGetModStyle()
{
    $style = "old";

    return $style;
}

function atGetModName()
{
    return $GLOBALS['module_name'];
}

function atGetLang()
{
    $lang = $GLOBALS['currentlang'];
    
    include_once("modules/AutoTheme/lang/lang.php");
        
    if (defined($lang)) {
        $lang = constant($lang);
    }
    return $lang;
}

function atBlockLoad($location="", $title="")
{
    $dbi = $GLOBALS['dbi'];
    $prefix = $GLOBALS['prefix'];

    if ($GLOBALS['multilingual']) {
    	$querylang = "AND (blanguage='".atGetLang()."' OR blanguage='')";
    }
    else {
    	$querylang = "";
    }
    $version = atGetPlatformVersion();
    
    if ($version >= 6.5) {
        $posfield = "bposition";
    }
    else {
        $posfield = "position";
    }
    if ($location) {
        $result = sql_query("SELECT bid, bkey, title, content, url, blockfile, view, ".$posfield." FROM ".$prefix."_blocks WHERE ".$posfield."='$location' AND active='1' $querylang ORDER BY weight ASC", $dbi);
    }
    elseif ($title) {
        $result = sql_query("SELECT bid, bkey, title, content, url, blockfile, view, ".$posfield." FROM ".$prefix."_blocks WHERE title='$title' AND active='1' $querylang ORDER BY weight ASC", $dbi);
    }
    while (list($bid, $bkey, $title, $content, $url, $blockfile, $view, $position) = sql_fetch_row($result, $dbi)) {
        $block = compact("bid", "bkey", "title", "content", "position", "url", "blockfile", "view");

		atRunningSetVar("block", $block);

        if ($bkey == "admin") {
            adminblock();
        }
        elseif ($bkey == "userbox") {
            userblock();
        }
        elseif ($bkey == "") {
            $displaythis = 0;

            if ($view == 0) {
                $displaythis = 1;
            }
            elseif ($view == 1 && atIsLoggedIn() || atIsAdminUser()) {
                $displaythis = 1;
            }
            elseif ($view == 2 && atIsAdminUser()) {
                $displaythis = 1;
            }
            elseif ($view == 3 && !atIsLoggedIn() || atIsAdminUser()) {
                $displaythis = 1;
            }
            if ($displaythis) {
                if ($url == "") {
                    if ($blockfile == "") {
                        themesidebox($title, $content);
                    }
                    else {
                        blockfileinc($title, $blockfile);
                    }
                }
                else {
                    headlines($bid);
                }
            }
        }
    }
}

function atBlockList()
{
    $dbi = $GLOBALS['dbi'];
    $prefix = $GLOBALS['prefix'];

    $result = sql_query("SELECT title FROM ".$prefix."_blocks ORDER BY title", $dbi);

    while (list($title) = sql_fetch_row($result, $dbi)) {
        $blocklist[] = $title;
    }
    return $blocklist;
}

?>
