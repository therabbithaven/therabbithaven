<?php
// ----------------------------------------------------------------------
// Copyright (c) 2002-2004 Shawn McKenzie
// http://spidean.mckenzies.net
// ----------------------------------------------------------------------
// LICENSE
//
// This software is the copyrighted material of Shawn McKenzie. AutoTheme is
// licensed for use on (1) web site installation only. See the license.txt for your
// specific license.
// 
// YOU AGREE NOT TO COPY OR DISTRIBUTE THIS SOFTWARE OR CHANGE THE SOURCE CODE
// WITHOUT PRIOR WRITTEN PERMISSION. PLEASE SEND ANY QUESTIONS OR COMMENTS TO
// AUTOTHEME@MCKENZIES.NET.
// 
// SHAWN MCKENZIE, TAKES NO RESPONSIBILITY FOR ANY DAMAGE THAT THIS SOFTWARE MIGHT
// CAUSE.
//
// Additional licensing information is available from autotheme@mckenzies.net.
// 
// If you disagree with any of these terms, you are not authorized to use this
// software. If you do agree with these terms, please enjoy.
// ----------------------------------------------------------------------

function AutoThemeCheckTable()
{
    $dbi = $GLOBALS['dbi'];
    $prefix = $GLOBALS['prefix'];
    
    $version = atGetPlatformVersion();
    
    if ($version >= 6.5) {
        $posfield = "bposition";
    }
    else {
        $posfield = "position";
    }
    $result = sql_query("SELECT ".$posfield." from ".$prefix."_blocks", $dbi);
    
    if (mysql_field_len($result, 0) == 255) {
        return true;
    }
    else {
        return $posfield;
    }
}

function AutoTheme_admin_updatetable($var)
{
    extract($var);

    $dbi = $GLOBALS['dbi'];
    $prefix = $GLOBALS['prefix'];

    sql_query("ALTER TABLE `".$prefix."_blocks` CHANGE `".$posfield."` `".$posfield."` VARCHAR( 255 ) NOT NULL", $dbi)
        or die(_DBMODIFYERROR);
    
    Header("Location: admin.php?module=". MOD_NAME ."&op=ablocks&themedir=$themedir");
}

function list_mods($dir="modules")
{
    $modlist = array(
        "*AdminPages",
        "*HomePage",
        "*UserPages"
    );
    
    if ($handle = @opendir($dir)) {
        while (false !== ($subdir = @readdir($handle))) {
            if (@is_dir("$dir/$subdir") &&
                $subdir !== '.' &&
                $subdir !== '..' &&
                $subdir !== 'AutoTheme')
            {
                $modlist[] = $subdir;
            }
        }
        closedir($handle);
    }
    return $modlist;
}

?>
