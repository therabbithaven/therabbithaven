<?php
// ----------------------------------------------------------------------
// Copyright (c) 2002-2004 Shawn McKenzie
// http://spidean.mckenzies.net
// ----------------------------------------------------------------------
// LICENSE
//
// This software is the copyrighted material of Shawn McKenzie. AutoTheme is
// licensed for use on (1) web site installation only. See the license.txt for your
// specific license.
// 
// YOU AGREE NOT TO COPY OR DISTRIBUTE THIS SOFTWARE OR CHANGE THE SOURCE CODE
// WITHOUT PRIOR WRITTEN PERMISSION. PLEASE SEND ANY QUESTIONS OR COMMENTS TO
// AUTOTHEME@MCKENZIES.NET.
// 
// SHAWN MCKENZIE, TAKES NO RESPONSIBILITY FOR ANY DAMAGE THAT THIS SOFTWARE MIGHT
// CAUSE.
//
// Additional licensing information is available from autotheme@mckenzies.net.
// 
// If you disagree with any of these terms, you are not authorized to use this
// software. If you do agree with these terms, please enjoy.
// ----------------------------------------------------------------------

function themeheader()
{
    $display = ob_get_contents();
    ob_end_clean();
    $newdisplay = str_replace("</head>", "", $display);

    if (isset($GLOBALS['oldtheme'])) {
        $thename = atThemeGetVar("thename");
        $oldtheme = $GLOBALS['oldtheme'];

        $newdisplay = str_replace(
            "themes/$oldtheme/",
            "themes/$thename/",
            $newdisplay
        );
    }
    ob_start();
    
    echo $newdisplay;

    atThemeHeader();
}

function themefooter()
{
    atThemeFooter();
}

function themeindex($aid, $informant, $datetime, $title, $counter, $topic, $hometext, $notes, $morelink, $topicname, $topicimage, $topictext)
{
    $sid = $GLOBALS['sid'];
    $vars = compact("sid", "aid", "informant", "datetime", "title", "counter", "topic", "hometext", "notes", "morelink", "topicname", "topicimage", "topictext");
    list($preformat, $info, $links) = themeformatnews($vars);

    atRunningMultiSetVars($vars);
    
    atNewsSummary($info, $links, $preformat);
}

function themearticle($aid, $informant, $datetime, $title, $bodytext, $topic, $topicname, $topicimage, $topictext)
{
    $sid = $GLOBALS['sid'];
    $vars = compact("sid", "aid", "informant", "datetime", "title", "bodytext", "topic", "topicname", "topicimage", "topictext");
    list($preformat, $info, $links) = themeformatnews($vars);

    atRunningMultiSetVars($vars);

    atNewsArticle($info, $links, $preformat);
}

function themesidebox($title, $content)
{
    $block = atRunningGetVar("block");
    $block['title'] = $title;
    $block['content'] = $content;

    if ($block['position'] == "d") {
    	$block['position'] = "c";
    }
    atThemeBlock($block);
}

function themeformatnews($vars)
{
    extract($vars);
    
    $preformat = array (
	   'bodytext' => $bodytext,
	   'bytesmore' => $morelink,
	   'category' => $title,
	   'comment' => '?',
	   'hometext' => $hometext,
	   'notes' => $notes,
	   'searchtopic' => $topicimage,
	   'print' => $morelink,
	   'readmore' => $morelink,
	   'send' => $morelink,
	   'title' => $title,
	   'version' => '?',
	   'more' => $morelink,
	   'catandtitle' => $title,
	   'maintext' => $hometext,
	   'fulltext' => $bodytext,
    );

    $info = array (
	   'aid' => $aid,
	   'bodytext' => $bodytext,
	   'catthemeoverride' => '?',
	   'cid' => '?',
	   'cattitle' => $title,
	   'comments' => '',
	   'counter' => $counter,
	   'hometext' => $hometext,
	   'informant' => $informant,
	   'notes' => $notes,
	   'sid' => $sid,
	   'themeoverride' => '?',
	   'tid' => $topic,
	   'time' => $datetime,
	   'title' => $title,
	   'topicname' => $topicname,
	   'topicimage' => $topicimage,
	   'topictext' => $topictext,
	   'tcounter' => '?',
	   'unixtime' => '?',
	   'withcomm' => '?',
	   'skins' => '?',
	   'topicid' => $topic,
	   'topic' => $topic,
	   'catid' => '?',
	   'version' => '?',
	   'longdatetime' => $datetime,
	   'briefdatetime' => $datetime,
	   'longdate' => $datetime,
	   'briefdate' => $datetime,
	   'catandtitle' => $title,
	   'maintext' => $hometext,
	   'fulltext' => $bodytext,
    );
    
    $links = array (
	   'category' => '?',
	   'comment' => '?',
	   'fullarticle' => '?',
	   'searchtopic' => '?',
	   'print' => '?',
	   'send' => '?',
	   'version' => '?',
    );

    return array($preformat, $info, $links);

}

?>
