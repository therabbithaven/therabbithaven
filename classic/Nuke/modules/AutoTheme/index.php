<?php
// ----------------------------------------------------------------------
// Copyright (c) 2002-2004 Shawn McKenzie
// http://spidean.mckenzies.net
// ----------------------------------------------------------------------
// LICENSE
//
// This software is the copyrighted material of Shawn McKenzie. AutoTheme is
// licensed for use on (1) web site installation only. See the license.txt for your
// specific license.
// 
// YOU AGREE NOT TO COPY OR DISTRIBUTE THIS SOFTWARE OR CHANGE THE SOURCE CODE
// WITHOUT PRIOR WRITTEN PERMISSION. PLEASE SEND ANY QUESTIONS OR COMMENTS TO
// AUTOTHEME@MCKENZIES.NET.
// 
// SHAWN MCKENZIE, TAKES NO RESPONSIBILITY FOR ANY DAMAGE THAT THIS SOFTWARE MIGHT
// CAUSE.
//
// Additional licensing information is available from autotheme@mckenzies.net.
// 
// If you disagree with any of these terms, you are not authorized to use this
// software. If you do agree with these terms, please enjoy.
// ----------------------------------------------------------------------

if (!eregi("index.php|modules.php", $_SERVER['PHP_SELF'])) {
    die ("Access Denied");
}
include("header.php");

OpenTable();

echo "<div align=\"center\"><b><a href=\"http://spidean.mckenzies.net\">AutoTheme 1.7</a></b><br />";
echo "Copyright (c) 2002-2004 Shawn McKenzie<br /><a href=\"http://spidean.mckenzies.net\">http://spidean.mckenzies.net</a><br /><br />";
echo "This site is running the AutoTheme HTML Theme System.  AutoTheme runs as a transparent user module.  It can be configured from the Administration interface.</div><br /><br />";

CloseTable();

include("footer.php");

?>
