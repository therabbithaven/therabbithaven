<?php

// This is the three letter abbreviation for this language as defined by ISO 639-2
// The list can be found at http://lcweb.loc.gov/standards/iso639-2/englangn.html
// This must be defined to use the multi-language image features and others
//
define('_ISO639_2', 'eng');

define('_AT_ABDESCR', 'This is where you can add and configure your AutoBlocks.  AutoBlocks allow you to define unlimited block positions in addition to  Left, Right and Center.');
define('_AT_ACTION', 'Action');
define('_AT_ACTIVEAUTOBLOCKS', 'Active AutoBlocks');
define('_AT_ACTIVEAUTOTHEMES', 'Active AutoThemes');
define('_AT_ACTIVETHEMEMSG', 'This theme is currently set as the active theme.  Editing the active theme could have adverse effects and is not recommended.  You should copy this theme to a new theme directory and edit that copy!');
define('_AT_ADD', 'Add');
define('_AT_ADDAUTOBLOCK', 'Add an AutoBlock');
define('_AT_ADDAUTOTHEME', 'Add an AutoTheme');
define('_AT_ADDBLOCK', 'Add a Custom Block');
define('_AT_ADDCOMMAND', 'Add a Command');
define('_AT_ADDLANGUAGE', 'Add a Language');
define('_AT_ADDMODULE', 'Add a Custom Module');
define('_AT_ADMINMENU', 'Administration Menu');
define('_AT_ADMIN', 'Admin');
define('_AT_ALL', 'All');
define('_AT_ALTERNATING', 'Alternating');
define('_AT_ALTSUMMARY', 'Use Alternating Summaries?');
define('_AT_ANONYMOUS', 'Anonymous');
define('_AT_APPLIESTO', 'Applies to users');
define('_AT_AREYOUSUREDEL', 'Are you sure you want to remove?');
define('_AT_ARTICLE', 'Article');
define('_AT_ATADMIN', 'AutoTheme Administration');
define('_AT_AUTOBLOCKS', 'AutoBlocks');
define('_AT_AUTOBLOCKSNOTACTIVE', 'AutoBlocks have not been activated.');
define('_AT_AUTOTHEMES', 'AutoThemes');
define('_AT_BACKTOCMDS', 'Back to Commands');
define('_AT_BG', 'Background');
define('_AT_BLOCK', 'Block');
define('_AT_BLOCKS', 'Blocks');
define('_AT_BLOCKDEFAULT', 'Show block by default?');
define('_AT_BLOCKSHOW', 'Show block for this module?');
define('_AT_BORDER', 'Border');
define('_AT_CENTER', 'Center');
define('_AT_COLOR', 'Color');
define('_AT_COLORS', 'Colors');
define('_AT_COMMAND', 'Command');
define('_AT_COMMANDS', 'Commands');
define('_AT_CONFCOMMANDS', 'User Configurable Commands');
define('_AT_CONFGENERALFIRST', 'You must configure the general theme settings first!');
define('_AT_CONFIRMDEL', 'Confirm Removal');
define('_AT_CONTENT', 'Content');
define('_AT_CREATE', 'Create');
define('_AT_CREATENEWTHEME', 'Create new theme from');
define('_AT_CUSTBLOCKS', 'Custom Blocks');
define('_AT_CUSTMODULES', 'Custom Modules');
define('_AT_DEF', 'Default');
define('_AT_DIREXISTS', 'directory already exists!');
define('_AT_EDIT', 'Edit');
define('_AT_ENSHORTURLS', 'Enable AutoURLs (requires Apache and mod_rewrite)?');
define('_AT_FILENAME', 'File name');
define('_AT_FILESMISSING', 'Module files missing!');
define('_AT_BLANK', 'Blank');
define('_AT_EXAMPLE', 'Example');
define('_AT_GENERAL', 'General');
define('_AT_GETSHORTURLS', 'Click here to download the necessary file');
define('_AT_HEAD', 'HEAD');
define('_AT_IMG', 'Image');
define('_AT_IMPORT', 'Import');
define('_AT_IMPORTFROM', 'from AT-Lite .6');
define('_AT_LANGRTL', 'Languages that read Right-To-Left');
define('_AT_LANGUAGES', 'Languages');
define('_AT_LEFT', 'Left');
define('_AT_LOGGEDIN', 'Loggedin');
define('_AT_LOGO', 'Logo');
define('_AT_MAIN', 'Main');
define('_AT_MANUAL', 'Manual');
define('_AT_MODULES', 'Modules');
define('_AT_MODULEOPS', 'Module Options');
define('_AT_NAME', 'Name');
define('_AT_NOACTIVEAUTOBLOCKS', 'No Active AutoBlocks!');
define('_AT_NOACTIVEAUTOTHEMES', 'No Active AutoThemes!');
define('_AT_NOCUSTBLOCKS', 'No Custom Blocks!');
define('_AT_NOCUSTMODULES', 'No Custom Modules!');
define('_AT_NOIMPORT', 'This is not an importable theme!  The theme.cfg is not from AutoTheme Beta 5 or AT-Lite .6!');
define('_AT_NONCONFCOMMANDS', 'Non-configurable Commands');
define('_AT_NORTLLANG', 'No RTL Languages!');
define('_AT_NOTHEMECFG', 'No theme.cfg found in that directory!');
define('_AT_NOTHEMEDIR', 'No theme directory found!');
define('_AT_NOTICE', 'NOTICE');
define('_AT_NOTWRITABLE', 'is not writable!');
define('_AT_NOUSERCONFCOMMANDS', 'No User Configured Commands!');
define('_AT_OR', 'or');
define('_AT_PAGETEMPLATE', 'Page Template');
define('_AT_PREVIEW', 'Preview');
define('_AT_REMOVE', 'Remove');
define('_AT_RIGHT', 'Right');
define('_AT_SAVE', 'Save');
define('_AT_SHORTURLSNOTACTIVE', 'AutoURLs have not been installed.');
define('_AT_STRIPTOHEAD', 'Strip out head content (from &lt;head&gt; to &lt;/head&gt;?');
define('_AT_STYLESHEET', 'Style Sheet');
define('_AT_SUMMARY', 'Summary');
define('_AT_SUPPORT', 'Support');
define('_AT_TBL', 'Table');
define('_AT_TEMPLATES', 'Templates');
define('_AT_THEME', 'Theme');
define('_AT_THEMEABDESCR', 'This is where you can add and configure your AutoBlocks specific to this theme.  AutoBlocks allow you to define unlimited block positions in addition to  Left, Right and Center.');
define('_AT_THEMECMDDESCR', 'This is where you can add and configure your own custom commands that you can use in your AutoTheme templates specifically for this theme. You can also view the built-in AutoTheme commands.');
define('_AT_TXT', 'Text');
define('_AT_UPDATETABLE', 'Click here to update the database to support AutoBlocks');
define('_AT_VIEW', 'View');
define('_AT_FIRST', 'First');
define('_AT_FULL', 'Full');
define('_AT_SECOND', 'Second');
define('_AT_SKIP', 'Skip');
define('_AT_INFO', 'Information');
define('_AT_CONTACT', 'Contact');
define('_AT_VERSION', 'Version');
define('_AT_DESCRIPTION', 'Description');
define('_AT_AUTHOR', 'Author');
define('_AT_SELECT', 'Select');
define('_AT_DBMODIFYERROR', 'An error was encountered while updating the database.');
define('_AT_AUTOPRINT', 'Theme to use for AutoPrint (printer friendly page):');
define('_AT_ENABLE', 'Enable');
define('_AT_EXTRAS', 'Extras');
define('_AT_EXTRANAME', 'Extra Name');
define('_AT_INFODESCR', 'This is where you can configure information for your theme (i.e. Name, Version, Description, Author, Contact).  You can also choose to have this information included in the HTML source of your theme.');
define('_AT_EXTRASDESCR', 'This is where you can configure extra options for AutoTheme (i.e. AutoURLs, AutoPrint, etc...).');
define('_AT_MULTISITE', 'Multi-Site');
define('_AT_INCLUDEINFO', 'Include this information as a comment in the HTML source?');
define('_AT_OPTIONS', 'Options');
define('_AT_CONFIGURE', 'Configure');
define('_AT_ATDESCR', 'This is where you can add and configure your AutoThemes.  You can configure default theme templates, block display and colors, as well as custom module templates, block display and colors and custom block templates.');
define('_AT_CMDDESCR', 'This is where you can add and configure your own custom commands that you can use in your AutoTheme templates. You can also view the built-in AutoTheme commands.');
define('_AT_LANGDESCR', 'This is where you can add and configure custom theme actions based on the current language settings, such as Right-To-Left text display.');
define('_AT_GENDESCR', 'This is where you configure the default theme templates, block display and colors for your AutoTheme.');
define('_AT_MODDESCR', 'This is where you configure custom theme templates, block display and colors for specific modules and other areas of your site.');
define('_AT_BLOCKDESCR', 'This is where you configure custom theme templates for specific blocks on your site.');
define('_AT_CANCEL', 'Cancel');
define('_AT_CONTINUE', 'Continue');
define('_AT_ERROR', 'ERROR');
define('_AT_OLDTHEMEMSG', 'This theme appears to be from a previous version of AutoTheme.  If this theme is from a previous version, it can be automatically imported by clicking '._AT_IMPORT.'.  This theme may work without importing, in which case click '._AT_SKIP.'.');
define('_AT_EXTRANOTE', '*&nbsp;&nbsp;This Extra is configured for each individual theme.<br />**&nbsp;This Extra is configured for each individual Custom Module.');
define('_AT_NOHELP', 'No additional information is available for this Extra.');

define('_AT_CACHEOPTIONS', 'Caching Options');
define('_AT_ENABLECACHE', 'Enable cache');
define('_AT_CACHETIME', 'Cache expiration (seconds)');
define('_AT_MODULENOCACHE', 'Modules to exclude from caching');
define('_AT_ADDEXCLUDEMODULE', 'Add a module');
define('_AT_EXPIREONDB', 'Expire cache on DB update');

?>
