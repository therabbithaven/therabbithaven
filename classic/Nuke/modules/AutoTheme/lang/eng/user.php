<?php

// This is the three letter abbreviation for this language as defined by ISO 639-2
// The list can be found at http://lcweb.loc.gov/standards/iso639-2/englangn.html
// This must be defined to use the multi-language image features and others
//
define('_ISO639_2', 'eng');

define('_AT_GUEST','Guest');
define('_AT_MYACCOUNT','My Account');
define('_AT_NEWACCOUNT','Register');
define('_AT_REMEMBERME','Remember me');
define('_AT_WELCOME','Welcome');

define('_AT_TEMPLATENOTDEFINED', 'Template not defined in theme configuration!');
define('_AT_TEMPLATENOTFOUND', 'Template not found!');

define('_AT_SECONDS', 'Seconds');
define('_AT_READFROMCACHE', 'Page read from cache in');

?>
