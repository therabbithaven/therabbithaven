<?php

// This is the three letter abbreviation for this language as defined by ISO 639-2
// The list can be found at http://lcweb.loc.gov/standards/iso639-2/englangn.html
// This must be defined to use the multi-language image features and others
//
define('english', 'eng');
define('arabic', 'ara');
define('farsi', 'far');
define('hebrew', 'heb');

?>
