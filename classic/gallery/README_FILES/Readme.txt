  ==========================================================
                      4images Readme
  ==========================================================

  4images - Image Gallery Management System
  Version 1.7.1
  Copyright (C) 2002 Jan Sorgalla <jan@4homepages.de>


  Lizenz/Licence:
  ----------------------------
  Die Lizenzbestimmungen finden Sie in der Datei 
  "docs/Lizenz.deutsch.txt"
  ********************************************************
  Please see the "docs/Licence.english.txt" file


  Installation:
  ----------------------------
  Eine Installationsanleitung finden Sie in der
  Datei "docs/Installation.deutsch.txt"
  ********************************************************
  Please see the "docs/Installation.english.txt" file


  FAQ:
  ----------------------------
  Bei Problemen kontaktieren Sie bitte den FAQ-Bereich in
  unserem Support-Forum:
  http://www.4homepages.de/forum/viewforum.php?f=14
  ********************************************************
  If you have problems, see the FAQ section in our 
  support forum:
  http://www.4homepages.de/forum/viewforum.php?f=14


  Download:
  ----------------------------
  Die neueste Version von 4images kann unter http://www.4homepages.de
  heruntergeladen werden.
  ********************************************************
  You can get the newest version of 4images at http://www.4homepages.de


  Support:
  ----------------------------
  Das Supportforum finden Sie unter http://www.4homepages.de/forum
  ********************************************************
  There is a support forum under http://www.4homepages.de/forum