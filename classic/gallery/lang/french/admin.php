<?php
/**************************************************************************
 *                                                                        *
 *    4images - A Web Based Image Gallery Management System               *
 *    ----------------------------------------------------------------    *
 *                                                                        *
 *             File: admin.php                                            *
 *        Copyright: (C) 2002 Jan Sorgalla                                *
 *            Email: jan@4homepages.de                                    *
 *              Web: http://www.4homepages.de                             *
 *    Scriptversion: 1.7                                                  *
 *                                                                        *
 *    Never released without support from: Nicky (http://www.nicky.net)   *
 *                                                                        *
 **************************************************************************
 *                                                                        *
 *    Dieses Script ist KEINE Freeware. Bitte lesen Sie die Lizenz-       *
 *    bedingungen (Lizenz.txt) f�r weitere Informationen.                 *
 *    ---------------------------------------------------------------     *
 *    This script is NOT freeware! Please read the Copyright Notice       *
 *    (Licence.txt) for further information.                              *
 *                                                                        *
 *************************************************************************/

/**************************************************************************
 *                                                                        *
 *        French translation by: Daniel (http://www.sudbox.com)           *
 *                               GoL (http://www.wipe-fr.org)             *
 *                                                                        *
 *************************************************************************/

$lang['user_integration_delete_msg'] = "You are not using the 4images User-Database. User not deleted.";

//-----------------------------------------------------
//--- Main --------------------------------------------
//-----------------------------------------------------
$lang['no_admin'] = "Si vous n'�tes pas l'administrateur du site. Vous ne devez pas �tre ici.";
$lang['admin_login_redirect'] = "Vous �tes connect�. Redirection automatique...";
$lang['admin_no_lang'] = "Aucun language trouv�. S'il vous pla�t t�l�chargez le language <b>\"french\"</b>. A mettre dans votre r�pertoire <b>\"lang\"</b>.";
$lang['admin_login'] = "Connexion";
$lang['goto_homepage'] = "Retour � la  page Galerie";
$lang['online_users'] = "{num_total} utilisateur(s) connect�(s). {num_registered} utilisateur(s) enregistr�(s) et {num_guests} invit�(s).";
$lang['homestats_total'] = "Total:";
$lang['top_cat_hits'] = "Top 5 des cat�gories par hits";
$lang['top_image_hits'] = "Top 5 des images par hits";
$lang['top_image_downloads'] = "Top 5 des images par t�l�chargement";
$lang['top_image_rating'] = "Top 5 des images par r�sultats des votes";
$lang['top_image_votes'] = "Top 5 des images par votes";
$lang['yes'] = "Oui";
$lang['no'] = "Non";
$lang['search'] = "Cherche";
$lang['search_next_page'] = "Page suivante";
$lang['save_changes'] = "Sauver les changements";
$lang['reset'] = "Recommencer";
$lang['add'] = "Ajouter";
$lang['edit'] = "Editer";
$lang['delete'] = "Effacer";
$lang['options'] = "Options";
$lang['back_overview'] = "Retour arri�re";
$lang['back'] = "Retour";
$lang['sort_options'] = "Options";
$lang['order_by'] = "Classer par";
$lang['results_per_page'] = "R�sultats par page";
$lang['asc'] = "Ascendant";
$lang['desc'] = "Descendant";
$lang['found'] = "Trouv�: ";
$lang['showing'] = "Affich�: ";
$lang['date_format'] = "<br /><span class=\"smalltext\">(Format de date: aaaa-mm-jj hh:mm:ss)</span>";
$lang['date_desc'] = "<br /><span class=\"smalltext\">Laisser la case par d�faut.</span>";

$lang['userlevel_admin'] = "Administrateurs";
$lang['userlevel_registered'] = "Membres enregistr�s";
$lang['userlevel_registered_awaiting'] = "Membres enregistr�s (non activ�s)";

$lang['headline_whosonline'] = "Qui est connect�?";
$lang['headline_stats'] = "Statistiques";

$lang['images'] = "Images";
$lang['users'] = "Utilisateurs";
$lang['database'] = "Database";
$lang['media_directory'] = "R�pertoire M�dia";
$lang['thumb_directory'] = "R�pertoire Vignettes";
$lang['validate'] = "Valider";
$lang['images_awaiting_validation'] = "<br><b>{num_images}</b> images en attente de validation";

$lang['permissions'] = "Permissions";
$lang['all'] = "Tous";
$lang['private'] = "Priv�";
$lang['all_categories'] = "Toutes les Cat�gories";
$lang['no_category'] = "Aucune Cat�gorie";

$lang['reset_stats_desc'] = "Si vous voulez r�initialiser les stats � une valeur sp�cifique, entrez un nombre. Laisser le champ de la permission vide si vous ne voulez pas modifier les stats.";

//-----------------------------------------------------
//--- Email -------------------------------------------
//-----------------------------------------------------
$lang['send_emails'] = "Message aux membres";
$lang['send_emails_subject'] = "Sujet";
$lang['send_emails_message'] = "Message";
$lang['select_email_user'] = "Choix utilisateurs";
$lang['send_emails_success'] = "Emails envoy�s";
$lang['send_emails_error'] = "Erreur Email";

//-----------------------------------------------------
//--- Error Messages ----------------------------------
//-----------------------------------------------------
$lang['error'] = "ERREUR:";
$lang['error_log_desc'] = "Les erreurs suivantes se sont produites:";
$lang['lostfield_error'] = "S'il vous pla�t v�rifiez les champs marqu�s.";
$lang['parent_cat_error'] = "Vous ne pouvez pas assigner de cat�gorie comme sous-cat�gorie!";
$lang['invalid_email_error'] = "S'il vous pla�t v�rifiez votre Email!";
$lang['no_search_results'] = "Aucun r�sultat trouv�.";

//-----------------------------------------------------
//--- Fields ------------------------------------------
//-----------------------------------------------------
$lang['field_image_name'] = "Nom de l'image";
$lang['field_category_name'] = "Nom de la cat�gorie";
$lang['field_username'] = "Nom d'utilisateur";
$lang['field_password'] = "Mot de Passe";
$lang['field_userlevel'] = "Niveau Utilisateur";
$lang['field_password'] = "Mot de Passe";
$lang['field_password_ext'] = "Mot de Passe:<br /><span class=\"smalltext\">Laissez le champ libre du mot de passe � moins que vous ne vouliez le changer.</span>";
$lang['field_headline'] = "Titre de l'image";
$lang['field_email'] = "Email";
$lang['field_homepage'] = "Page Perso";
$lang['field_icq'] = "ICQ";
$lang['field_showemail'] = "Affiche son Email";
$lang['field_allowemails'] = "Afficher mon adresse Email:";
$lang['field_invisible'] = "Invisible";
$lang['field_date'] = "Date";
$lang['field_joindate'] = "Date d'inscription";
$lang['field_lastaction'] = "Derni�re visite";
$lang['field_ip'] = "IP";
$lang['field_comment'] = "Commentaires";
$lang['field_description'] = "Description";
$lang['field_description_ext'] = "Description<br /><span class=\"smalltext\">HTML autoris�.</span>";
$lang['field_parent'] = "Sous-cat�gorie dans";
$lang['field_hits'] = "Hits";
$lang['field_downloads'] = "T�l�chargements";
$lang['field_votes'] = "Votes";
$lang['field_rating'] = "R�sultats des votes";
$lang['field_category'] = "Cat�gorie";
$lang['field_keywords'] = "Mots-cl�s";
$lang['field_keywords_ext'] = "Mots-cl�s <br /><span class=\"smalltext\">Les mots-cl�s doivent �tre s�par�s par des espaces.</span>";
$lang['field_free'] = "Active";
$lang['field_allow_comments'] = "Autorise les commentaires";
$lang['field_image_file'] = "Nom de l'image";
$lang['field_thumb_file'] = "Nom de la vignette";
$lang['field_download_url'] = "URL de l'image";
$lang['field_usergroup_name'] = "Nom du groupe";

//-----------------------------------------------------
//--- Searchform Fields -------------------------------
//-----------------------------------------------------
$lang['field_image_id_contains'] = "L'image contient";
$lang['field_image_name_contains'] = "Le nom de l'image contient";
$lang['field_description_contains'] = "La description contient";
$lang['field_keywords_contains'] = "Les mots-cl�s";
$lang['field_username_contains'] = "L'utilisateur contient";
$lang['field_email_contains'] = "L'Email contient";
$lang['field_headline_contains'] = "Le titre contient";
$lang['field_comment_contains'] = "Le commentaire contient";
$lang['field_date_before'] = "Date avant le";
$lang['field_date_after'] = "Date apr�s le";
$lang['field_joindate_before'] = "Inscription avant le";
$lang['field_joindate_after'] = "Inscription apr�s le";
$lang['field_lastaction_before'] = "Derni�re visite avant le";
$lang['field_lastaction_after'] = "Derni�re visite apr�s le";
$lang['field_image_file_contains'] = "Le fichier image contient";
$lang['field_thumb_file_contains'] = "Le fichier image r�duit contient";
$lang['field_downloads_upper'] = "T�l�charg� avant le";
$lang['field_downloads_lower'] = "T�l�charg� apr�s le";
$lang['field_rating_upper'] = "R�sultat des votes plus grand que";
$lang['field_rating_lower'] = "R�sultat des votes plus petit que";
$lang['field_votes_upper'] = "Nb de votes plus grand que";
$lang['field_votes_lower'] = "Nb de votes plus petit que";
$lang['field_hits_upper'] = "Total des clics plus grand que";
$lang['field_hits_lower'] = "Total des clics plus petit que";

//-----------------------------------------------------
//--- Navigation --------------------------------------
//-----------------------------------------------------
$lang['nav_categories_main'] = "Cat�gories";
$lang['nav_categories_edit'] = "Editer les cat�gories";
$lang['nav_categories_add'] = "Ajouter des cat�gories";

$lang['nav_images_main'] = "Images";
$lang['nav_images_edit'] = "Editer les images";
$lang['nav_images_add'] = "Ajouter des images";
$lang['nav_images_validate'] = "Valider les Images";
$lang['nav_images_check'] = "Visualiser les nouvelles images";
$lang['nav_images_thumbnailer'] = "R�duire la taille de l'image automatiquement";
$lang['nav_images_resizer'] = "R�duire la taille de la vignette automatiquement";

$lang['nav_comments_main'] = "Commentaires";
$lang['nav_comments_edit'] = "Editer les commentaires";

$lang['nav_users_main'] = "Utilisateurs";
$lang['nav_users_edit'] = "Editer les utilisateurs";
$lang['nav_users_add'] = "Ajouter un utilisateur";
$lang['nav_usergroups'] = "Groupes d'utilisateurs";
$lang['nav_users_email'] = "Envoyer un Email";

$lang['nav_general_main'] = "G�n�ral";
$lang['nav_general_settings'] = "Configuration";
$lang['nav_general_templates'] = "Templates";
$lang['nav_general_backup'] = "Backup de la base SQL";
$lang['nav_general_stats'] = "Mise � z�ro des Stats";

//-----------------------------------------------------
//--- Categories --------------------------------------
//-----------------------------------------------------
$lang['category'] = "Cat�gorie";
$lang['main_category'] = "Cat�gorie Principale";
$lang['sub_categories'] = "Sous cat�gories";
$lang['no_categories'] = "pas de cat�gorie ajout�e";
$lang['select_category'] = "Choix de la cat�gorie";
$lang['add_subcategory'] = "Ajouter une sous cat�gorie";
$lang['no_subcategories'] = "Pas de sous cat�gorie ajout�e";
$lang['delete_cat_confirm'] = "Voulez-vous supprimer cette cat�gorie?<br />Toutes les images et commentaires de cette cat�gorie seront automatiquement supprim�s!";
$lang['delete_cat_files_confirm'] = "Supprimer tous les fichiers images du serveur?";
$lang['cat_add_success'] = "Cat�gorie ajout�e";
$lang['cat_add_error'] = "Erreur sur l'ajout de la cat�gorie";
$lang['cat_edit_success'] = "Cat�gorie �dit�e";
$lang['cat_edit_error'] = "Erreur sur l'�dition de la cat�gorie";
$lang['cat_delete_success'] = "Cat�gorie suprim�e";
$lang['cat_delete_error'] = "Erreur sur la suppression de la cat�gorie";
$lang['permissions_inherited'] = "Utilisation de permissions h�rit�es de la cat�gorie parent";
$lang['cat_order'] = "Ordre des cat�gories";
$lang['at_beginning'] = "En d�but";
$lang['at_end'] = "En fin";
$lang['after'] = "Apr�s";

//-----------------------------------------------------
//--- Images ------------------------------------------
//-----------------------------------------------------
$lang['image'] = "Image";
$lang['image_file'] = "Fichier image";
$lang['thumb'] = "Vignette";
$lang['thumb_file'] = "Fichier vignette";
$lang['delete_image_confirm'] = "Voulez-vous supprimer ce fichier image? Tous les commentaires attach�s � ce fichier seront automatiquement supprim�s.";
$lang['delete_image_files_confirm'] = "Supprimer tous les fichiers images du serveur?";
$lang['file_upload_error'] = "Erreur sur le transfert du fichier image";
$lang['thumb_upload_error'] = "Erreur sur le transfert du fichier vignette";
$lang['no_image_file'] = "Veuillez choisir un fichier image";
$lang['invalid_file_type'] = "Type de fichier non valide!";
$lang['invalid_image_width'] = "Largeur de fichier non valide!";
$lang['invalid_image_height'] = "Hauteur du fichier non valide!";
$lang['invalid_file_size'] = "taille du fichier non valide!";
$lang['file_already_exists'] = "Ce fichier existe d�j�!";
$lang['file_copy_error'] = "Erreur sur la copie. Veuillez v�rifier les permissions sur vos r�pertoires.";
$lang['file_upload_success'] = "Fichier image transf�r�";
$lang['file_delete_success'] = "Fichier image supprim�";
$lang['file_delete_error'] = "Erreur sur la suppression du fichier!";
$lang['error_image_deleted'] = "Erreur sur la suppression de l'image!";
$lang['thumb_upload_success'] = "Vignette transf�r�e";
$lang['thumb_delete_success'] = "Vignette supprim�e";
$lang['thumb_delete_error'] = "Erreur sur la suppression de la vignette!";
$lang['image_add_success'] = "Image ajout�e";
$lang['image_add_error'] = "Erreur sur l'ajout de l'image!";
$lang['image_edit_success'] = "Image �dit�e";
$lang['image_edit_error'] = "Erreur sur l'�dition de l'image!";
$lang['image_delete_success'] = "Image suprim�e";
$lang['image_delete_error'] = "Erreur sur la suppression de l'image!";
$lang['allowed_mediatypes_desc'] = "Les extensions valides sont: ";
$lang['no_thumb_found'] = "Pas de vignette trouv�e";
$lang['no_db_entry'] = "Pas de donn�es dans la base!";
$lang['check_all'] = "V�rification compl�te";
$lang['detailed_version'] = "D�tail de la Version";
$lang['num_newimages_desc'] = "Images affich�es: ";
$lang['num_addnewimages_desc'] = "Nombre d'images � afficher: ";
$lang['no_newimages'] = "Pas de nouvelle image trouv�e";
$lang['thumb_newimages_exists'] = "Vignette trouv�e";
$lang['no_thumb_newimages'] = "Pas de vignette trouv�e";
$lang['no_thumb_newimages_ext'] = "Pas de vignette trouv�e. Par d�faut une ic�ne sera affich�e.";
$lang['no_newimages_added'] = "Pas de nouvelle image ajout�e!";
$lang['no_image_found'] = "Les images avec un <b class=\"marqueur\">!</b> signale qu'elles ne se trouvent pas sur le serveur.";
$lang['upload_progress'] = "Transfert du fichier....";
$lang['upload_progress_desc'] = "Cette fen�tre se refermera automatiquement apr�s le chargement du fichier.";
$lang['upload_note'] = "<b>NOTE:</b> Dans le cas o� le nom du dossier de la vignette ne correspond pas avec le nom du dossier de l'image, celle-ci sera adapt�e au nom du dossier de l'image.";
$lang['checkimages_note'] = "Les images (<b>{num_all_newimages}</b>) ne sont pas dans le serveur.";
$lang['download_url_desc'] = "<br /><span class=\"smalltext\">Si vous remplissez ce champ, le t�l�chargement pointera sur l'URL que vous avez mise, autrement il pointera au dossier de l'image.</span>";
$lang['images_delete_success'] = "Images supprim�es";
$lang['images_delete_error'] = "Erreur lors de la suppression des images";

//-----------------------------------------------------
//--- Comments ----------------------------------------
//-----------------------------------------------------
$lang['comment'] = "Commentaire";
$lang['comments'] = "Commentaires";
$lang['delete_comment_confirm'] = "Efface ce commentaire?";
$lang['comment_edit_success'] = "Commentaire �dit�";
$lang['comment_edit_error'] = "Erreur sur l'�dition du commentaire.";
$lang['comment_delete_success'] = "Commentaire supprim�";
$lang['comment_delete_error'] = "Erreur sur la suppression du commentaire";
$lang['comments_delete_success'] = "Commentaires supprim�";
$lang['comments_delete_error'] = "Erreur sur la suppression des commentaires";

//-----------------------------------------------------
//--- User --------------------------------------------
//-----------------------------------------------------
$lang['user'] = "Utilisateur";
$lang['user_delete_confirm'] = "Effacer un utilisateur?";
$lang['user_delete_comments_confirm'] = "Effacer tous les commentaires de cet utilisateur?";
$lang['user_add_success'] = "Utilisateur ajout�";
$lang['user_add_error'] = "Erreur sur l'ajout de l'utilisateur";
$lang['user_edit_success'] = "Utilisateur modifi�";
$lang['user_edit_error'] = "Erreur sur l'�dition de l'utilisateur";
$lang['user_delete_success'] = "Utilisateur supprim�";
$lang['user_delete_error'] = "Erreur sur la suppression de l'utilisateur";
$lang['user_comments_update_success'] = "Mise � jour des commentaires utilisateur (remis � \"Invit�\")";
$lang['user_comments_update_error'] = "Erreur sur la mise � jour des commentaires utilisateur (Non remis � \"Invit�\")";
$lang['user_name_exists'] = "Ce nom d'utilisateur existe d�j�!";
$lang['user_email_exists'] = "Cet Email utilisateur existe d�j�!";
$lang['num_newusers_desc'] = "Combien d'utilisateurs voulez-vous ajouter?: ";
$lang['user_delete_images_confirm'] = "Supprimer toutes les images post�es par l'Utilisateur?";
$lang['user_images_update_success'] = "Mise � Jour des Images de l'Utilisateur (Utilisateur ID r�initialis� sur \"Invit�\")";
$lang['user_images_update_error'] = "Erreur lors de la mise � jour des images Utilisateurs (Utilisateur ID r�initialis� sur \"Invit�\")";

//-----------------------------------------------------
//--- Usergroups --------------------------------------
//-----------------------------------------------------
$lang['add_usergroup'] = "Ajouter un groupe";
$lang['member_of_usergroup'] = "Membre du groupe utilisateurs ci-dessous";
$lang['usergroup_add_success'] = "Groupe utilisateurs ajout�";
$lang['usergroup_add_error'] = "Erreur lors de l'ajout du Groupe Utilisateur";
$lang['usergroup_edit_success'] = "Mise � Jour du Groupe utilisateur";
$lang['usergroup_edit_error'] = "Erreur lors de la modification du Groupe Utilisateur";
$lang['usergroup_delete_success'] = "Groupe utilisateur supprim�";
$lang['usergroup_delete_error'] = "Erreur lors de la suppression du Groupe Utilisateur";
$lang['delete_group_confirm'] = "Supprimer ce groupe utilisateur?";
$lang['auth_viewcat'] = "Visualiser la Cat�gorie";
$lang['auth_viewimage'] = "Visualiser l'Image";
$lang['auth_download'] = "T�l�charger";
$lang['auth_upload'] = "Transf�rer";
$lang['auth_directupload'] = "T�l�chargement direct";
$lang['auth_vote'] = "Vote";
$lang['auth_sendpostcard'] = "Envoi d'une eCard";
$lang['auth_readcomment'] = "Lire les Commentaires";
$lang['auth_postcomment'] = "Poster des Commentaires";
$lang['permissions_edit_success'] = "Mise � Jour des Permissions";
$lang['activate_date'] = "Date d'Activation";
$lang['expire_date'] = "Date d'Expiration";
$lang['expire_date_desc'] = "<br /><span class=\"smalltext\">Si vous ne voulez pas une date d'expiration pour cet utilisateur, laisser le champs avec 0.</span>";

//-----------------------------------------------------
//--- Templates ---------------------------------------
//-----------------------------------------------------
$lang['no_template'] = "Pas de Template trouv�";
$lang['no_themes'] = "Pas de Template-Pack trouv�";
$lang['edit_template'] = "Editer un template";
$lang['edit_templates'] = "Editer un template";
$lang['choose_template'] = "Choix du template";
$lang['choose_theme'] = "Choix du Template-Pack";
$lang['load_theme'] = "Charger un th�me";
$lang['template_edit_success'] = "Template modifi�!";
$lang['template_edit_error'] = "Erreur sur la sauvegarde du template! v�rifier votre permissions (chmod 666).";

//-----------------------------------------------------
//--- Backup ------------------------------------------
//-----------------------------------------------------
$lang['do_backup'] = "Backup de la Base";
$lang['do_backup_desc'] = "Backup de la Base.<br /> <span class=\"smalltext\">La base de donn�es choisie pr�sente pour �tre mis � jour. Les tables exig�es sont pr�s�lectionn�es.";
$lang['list_backups'] = "Liste des backups en cours";
$lang['no_backups'] = "Aucun backup";
$lang['restore_backup'] = "Restaure";
$lang['delete_backup'] = "Efface";
$lang['download_backup'] = "T�l�charge";
$lang['show_backup'] = "Affiche";
$lang['make_backup_success'] = "Backup de la base r�ussie.";
$lang['make_backup_error'] = "Erreur sur le backup. V�rifier vos permissions (chmod 777).";
$lang['backup_delete_confirm'] = "Supprimer le backup?";
$lang['backup_delete_success'] = "Backup supprim�";
$lang['backup_delete_error'] = "Erreur sur la suppression du backup";
$lang['backup_restore_confirm'] = "Restaure la base?";
$lang['backup_restore_success'] = "base restaur�e";
$lang['backup_restore_error'] = "Erreurs sur la restauration:";

//-----------------------------------------------------
//--- Thumbnailer & Resizer ---------------------------
//-----------------------------------------------------
$lang['im_error'] = "Erreur sur ImageMagick. Mauvais chemin ou ImageMagick n'est pas install�.";
$lang['gd_error'] = "Erreur sur la librairie GD.";
$lang['netpbm_error'] = "Erreur sur NetPBM. Mauvais chemin ou NetPBM n'est pas install�.";
$lang['no_convert_module'] = "Choix du module pour cr�er les vignettes.";
$lang['check_module_settings'] = "V�rification des modules.";
$lang['check_thumbnails'] = "V�rification des vignettes";
$lang['check_thumbnails_desc'] = "V�rification dans la base des vignettes.";
$lang['create_thumbnails'] = "Cr�ation d'une vignette";
$lang['creating_thumbnail'] = "Cr�er une vignette pour: ";
$lang['creating_thumbnail_success'] = "Vignette cr��e!";
$lang['creating_thumbnail_error'] = "Erreur sur la cr�ation de la vignette!";
$lang['convert_thumbnail_dimension'] = "Taille de la vignette en pixel";
// <br /><span class=\"smalltext\">les proportions seront contraintes</span>
$lang['convert_thumbnail_quality'] = "Qualit� de la vignette<br /><span class=\"smalltext\">0 � 100</span>";
$lang['convert_options'] = "Conversion";
$lang['resize_images'] = "Redimentionnez les images";
$lang['resize_image_files'] = "Redimentionnez les fichiers images";
$lang['resize_thumb_files'] = "Redimentionnez les fichiers vignettes";
$lang['resize_org_size'] = "taille d'origine";
$lang['resize_new_size'] = "Nouvelle taille";
$lang['resize_new_quality'] = "Qualit� de l'image";
$lang['resize_image_type_desc'] = "Conversion des images ou des vignettes?";

$lang['resize_dimension_desc'] = "Taille de l'image en pixel.";
// <br /><span class=\"smalltext\">Si vous entrez 200 (max). la taille de l'image sera mise � 200 et sera dans les proportions.</span>
$lang['resize_proportions_desc'] = "Proportions";
$lang['resize_proportionally'] = "Redimentionner proportionnellement";
$lang['resize_fixed_width'] = "Redimentionner avec une largeur fixe";
$lang['resize_fixed_height'] = "Redimentionner avec une hauteur fixe";

$lang['resize_quality_desc'] = "Qualit� de l'image<br /><span class=\"smalltext\">0 � 100</span>";
$lang['resize_start'] = "D�marre la conversion";
$lang['resize_check'] = "Affiche l'image";
$lang['resizing_image'] = "Convertir l'image: ";
$lang['resizing_image_success'] = "Convertion de l'image r�ussie!";
$lang['resizing_image_error'] = "Erreur sur la convertion de l'image!";

//-----------------------------------------------------
//--- Settings ----------------------------------------
//-----------------------------------------------------
$lang['save_settings_success'] = "Sauvegarde de la configuration r�ussie";

/*-- Setting-Group 1 --*/
$setting_group[1]="Configuration g�n�rale";
$setting['site_name'] = "Nom du site";
$setting['site_email'] = "Email administrateur";
$setting['use_smtp'] = "Utilise le serveur SMTP";
$setting['smtp_host'] = "host du serveur SMTP";
$setting['smtp_username'] = "Login de serveur SMTP";
$setting['smtp_password'] = "Mot de Passe de serveur SMTP";
$setting['template_dir'] = "Choix du template";
$setting['language_dir'] = "Choix du language";
$setting['date_format'] = "Format de date";
$setting['time_format'] = "Format de l'heure";
$setting['convert_tool'] = "Utilitaire de conversion pour les vignettes<br /><span class=\"smalltext\">GD (http://www.boutell.com/gd)<br />ImageMagick (http://www.imagemagick.org)</span>";
$convert_tool_optionlist = array(
  "none"   => "Aucun",
  "im"     => "ImageMagick",
  "gd"     => "GD Bibliothek",
  "netpbm" => "NetPBM"
);
$setting['convert_tool_path'] = "Si vous avez s�lectionn� \"ImageMagick\" ou \"NetPBM\", entrez le chemin (path)";
$setting['gz_compress'] = "Utiliser le format de compression GZip<br /><span class=\"smalltext\">\"Zlib\" doit �tre install� sur votre serveur</span>";
$setting['gz_compress_level'] = "GZip niveau de compression<br /><span class=\"smalltext\">0-9, 0=aucun, 9=max</span>";

/*-- Setting-Group 2 --*/
$setting_group[2]="Gestion des groupes";
$setting['cat_cells'] = "Nombre de cellules par table";
$setting['cat_table_width'] = "Table dimension<br /><span class=\"smalltext\">en pourcentage</span>";
$setting['cat_table_cellspacing'] = "Cellspacing";
$setting['cat_table_cellpadding'] = "Cellpadding";
$setting['num_subcats'] = "Nombre de sous cat�gories";

/*-- Setting-Group 3 --*/
$setting_group[3]="Gestion des images";
$setting['image_order'] = "Classement des images par";
$image_order_optionlist = array(
  "image_name"      => "Nom",
  "image_date"      => "Date",
  "image_downloads" => "T�l�chargement",
  "image_votes"     => "Votes",
  "image_rating"    => "R�sultats",
  "image_hits"      => "Clics"
);
$setting['image_sort'] = "Ascendant/Descendant";
$image_sort_optionlist = array(
  "ASC"  => "Ascendant",
  "DESC" => "Descendant"
);
$setting['new_cutoff'] = "Nombre de jours pour afficher les nouvelles images";
$setting['image_border'] = "Bordure des vignettes";
$setting['image_cells'] = "Image table cells (cellules)";
$setting['default_image_rows'] = "Image table rows (lignes)";
$setting['custom_row_steps'] = "Option d'affichage dans le menu d�roulant du nombre d'images (permettre � utilisateurs de choisir le nombre d'images par page)";
$setting['image_table_width'] = "Largeur du tableau (d'images)<br /><span class=\"smalltext\">en absolu ou en pourcentage</span>";
$setting['image_table_cellspacing'] = "Cellspacing";
$setting['image_table_cellpadding'] = "Cellpadding";

/*-- Setting-Group 4 --*/
$setting_group[4]="T�l�chargement des images";
$setting['upload_mode'] = "Mode Transfert";
$upload_mode_optionlist = array(
  "1" => "Remplace fichiers",
  "2" => "Sauve fichier avec nouveau nom",
  "3" => "Pas de transfert"
);
$setting['allowed_mediatypes'] = "Extensions valides<br /><span class=\"smalltext\">D�limite avec une virgule, pas d'espace. Quand vous ajoutez de nouveaux types de fichiers, cr�ez une nouvelle vignette dans le r�pertoire correspondant au gabarit.</span>";
$setting['max_thumb_width'] = "Largeur Max. de la vignette en pixel";
$setting['max_thumb_height'] = "Hauteur Max. de la vignette en pixel";
$setting['max_thumb_size'] = "Taille Max. de la vignette en Ko";
$setting['max_image_width'] = "Largeur Max. de l'image en pixel";
$setting['max_image_height'] = "Hauteur Max. de l'image en pixel";
$setting['max_media_size'] = "Taille Max. de l'image en Ko";
$setting['upload_notify'] = "Notifier par email � chaque t�l�chargement";
$setting['upload_emails'] = "Adresses Email suppl�mentaires d'avis d'upload<br /><span class=\"smalltext\">S�paration des Emails par des virgules.</span>";
$setting['auto_thumbnail'] = "Cr�ation d'une vignette automatiquement";
$setting['auto_thumbnail_dimension'] = "Taille de la vignette en pixel";
$setting['auto_thumbnail_resize_type'] = "Proportions";
$auto_thumbnail_resize_type_optionlist = array(
  "1" => "Redimentionner proportionnellement",
  "2" => "Redimentionner avec une largeur fixe",
  "3" => "Redimentionner avec une hauteur fixe"
);
$setting['auto_thumbnail_quality'] = "Qualit� de la vignette<br /><span class=\"smalltext\">0 � 100</span>";

/*-- Setting-Group 5 --*/
$setting_group[5]="Gestion des commentaires";
$setting['badword_list'] = "Liste des Mauvais mots<br /><span class=\"smalltext\">Entrez les mots � bannir s�par�s par des espaces (pas de virgule). Si vous voulez bannir le mot \"test\", tous les mots avec \"test\" vont �tre bannis. \"Attest\" s'affichera avec \"At****\". Si vous d�sirez seulement bannir le mot \"test\", il faudra simplement le mettre sous cette forme {test}. Le mot  \"test\" Sera banni, mais \"attest\" s'affichera.</span>";
$setting['badword_replace_char'] = "Caract�re pour remplacer les mots bannis";
$setting['wordwrap_comments'] = "D�filement<br /><span class=\"smalltext\">Pour mettre un d�filement horizontal sur les commentaires. Mettre le nombre de caract�res par ligne. 0 annule le d�filement.</span>";
$setting['html_comments'] = "Autoriser l'HTML dans les commentaires";
$setting['bb_comments'] = "Autoriser les code BBCode dans les commentaires";
$setting['bb_img_comments'] = "Autoriser les images dans les commentaires (BB-Code)<br /><span class=\"smalltext\">Si vous s�lectionnez \"Non\", les images seront affich�es comme des liens.</span>";

/*-- Setting-Group 6 --*/
$setting_group[6]="Gestion des Pages et de la navigation";
$setting['category_separator'] = "D�limiteur de cat�gories (dans le chemin (paths))";
$setting['paging_range'] = "Nombre de page avec \"pr�cedent\" et \"suivant\" � afficher par page lors de la navigation";

/*-- Setting-Group 7 --*/
$setting_group[7]="Gestion des utilisateurs et Session";
$setting['user_edit_image'] = "Permettre aux utilisateurs d'�diter leurs propres images";
$setting['user_delete_image'] = "Permettre aux utilisateurs de supprimer leurs propres images";
$setting['user_edit_comments'] = "Permettre aux utilisateurs d'�diter les commentaires de leurs propres images";
$setting['user_delete_comments'] = "Permettre aux utilisateurs de supprimer les commentaires de leurs propres images";
$setting['account_activation'] = "Activation du compte";
$account_activation_optionlist = array(
  "0" => "Aucune",
  "1" => "Email",
  "2" => "Administrateur"
);
$setting['activation_time'] = "Nombre de jours en attente de r�ponse d'activation de compte.<br /><span class=\"smalltext\">0  annule la fonction et les comptes utilisateurs en attente de validatation ne seront pas supprim�s.</span>";
$setting['session_timeout'] = "Temps de la Session en minutes";
$setting['display_whosonline'] = "Affiche \"Qui est en ligne!\". Seulement visible par l'administrateur si d�sactiv�.";
$setting['highlight_admin'] = "Affiche les administrateurs en gras sur \"Qui est en ligne!\" ";
?>