<?php
/**************************************************************************
 *                                                                        *
 *    4images - A Web Based Image Gallery Management System               *
 *    ----------------------------------------------------------------    *
 *                                                                        *
 *             File: install.php                                          *
 *        Copyright: (C) 2002 Jan Sorgalla                                *
 *            Email: jan@4homepages.de                                    *
 *              Web: http://www.4homepages.de                             *
 *    Scriptversion: 1.7                                                  *
 *                                                                        *
 *    Never released without support from: Nicky (http://www.nicky.net)   *
 *                                                                        *
 **************************************************************************
 *                                                                        *
 *    Dieses Script ist KEINE Freeware. Bitte lesen Sie die Lizenz-       *
 *    bedingungen (Lizenz.txt) f�r weitere Informationen.                 *
 *    ---------------------------------------------------------------     *
 *    This script is NOT freeware! Please read the Copyright Notice       *
 *    (Licence.txt) for further information.                              *
 *                                                                        *
 *************************************************************************/

/**************************************************************************
 *                                                                        *
 *        French translation by Daniel (http://www.sudbox.com)            *
 *                                                                        *
 *************************************************************************/

$lang['start_install'] = "Installation";
$lang['start_install_desc'] = "Veuillez remplir le formulaire ci-dessous.";
$lang['lostfield_error'] = "Une erreur s'est produite. S'il vous pla�t v�rifiez les champs marqu�s!";

$lang['db'] = "Configuration de la base de donn�es";
$lang['db_servertype'] = "Type de serveur de base de donn�es";
$lang['db_host'] = "Nom (Hostname) du serveur de la base de donn�es";
$lang['db_name'] = "Nom de la base";
$lang['db_user'] = "Utilisateur (login) de la base";
$lang['db_password'] = "Mot de passe de la base";
$lang['table_prefix'] = "Prefixes pour la base de donn�es";

$lang['admin'] = "Configuration Administrateur";
$lang['admin_user'] = "Login Administrateur";
$lang['admin_password'] = "Mot de Passe Administrateur";
$lang['admin_password2'] = "Mot de Passe Administrateur (Confirmer)";

$lang['database_error'] = "Une erreur s'est produite en mettant � jour la base de donn�es:";
$lang['install_success'] = "Installation r�ussie!";
$lang['install_success_login'] = "Votre Login en mode Admin a �t� cr��.  A ce niveau d'installation, la base de donn�es est compl�te. S'il vous pla�t soyez s�r de v�rifier la Configuration G�n�rale en d�tail, faire les changements dans votre tableau de bord..<br /><b>&raquo; <a href=\"".ROOT_PATH."admin/index.php\">4images Panneau de Contr�le</a></b>";
$lang['config_download'] = "T�l�charger la configuration";
$lang['config_download_desc'] = "Votre fichier configuration \"config.php\" est bloqu� en �criture sur le serveur. Une copie du dossier du config sera t�l�charg� quand vous cliquerez sur le bouton ci-dessous. Vous devriez t�l�charger ce dossier dans le m�me r�pertoire comme 4images. Une fois que cela est fait, vous devriez vous connecter et utiliser le Login Administrateur et le mot de passe que vous avez fourni et visiter le tableau de bord pour v�rifier la configuration g�n�rale.";
?>