<?php
/**************************************************************************
 *                                                                        *
 *    4images - A Web Based Image Gallery Management System               *
 *    ----------------------------------------------------------------    *
 *                                                                        *
 *             File: main.php                                             *
 *        Copyright: (C) 2002 Jan Sorgalla                                *
 *            Email: jan@4homepages.de                                    *
 *              Web: http://www.4homepages.de                             *
 *    Scriptversion: 1.7                                                  *
 *                                                                        *
 *    Never released without support from: Nicky (http://www.nicky.net)   *
 *                                                                        *
 **************************************************************************
 *                                                                        *
 *    Dieses Script ist KEINE Freeware. Bitte lesen Sie die Lizenz-       *
 *    bedingungen (Lizenz.txt) f�r weitere Informationen.                 *
 *    ---------------------------------------------------------------     *
 *    This script is NOT freeware! Please read the Copyright Notice       *
 *    (Licence.txt) for further information.                              *
 *                                                                        *
 *************************************************************************/

/**************************************************************************
 *                                                                        *
 *        French translation by: Daniel (http://www.sudbox.com)           *
 *                               GoL (http://www.wipe-fr.org)             *
 *                                                                        *
 *************************************************************************/

$lang['no_settings'] = "ERREUR: Impossible de charger la configuration du script!";

//-----------------------------------------------------
//--- Templates ---------------------------------------
//-----------------------------------------------------
$lang['charset'] = "iso-8859-1";
$lang['direction'] = "ltr";

//-----------------------------------------------------
//--- Userlevel ---------------------------------------
//-----------------------------------------------------
$lang['userlevel_admin'] = "Admin";
$lang['userlevel_user'] = "Membre";
$lang['userlevel_guest'] = "Invit�";

//-----------------------------------------------------
//--- Categories --------------------------------------
//-----------------------------------------------------
$lang['no_categories'] = "Cat�gorie introuvable.";
$lang['no_images'] = "Il n'y a pas d'image dans cette cat�gorie.";
$lang['select_category'] = "Choix de la cat�gorie";

//-----------------------------------------------------
//--- Comments ----------------------------------------
//-----------------------------------------------------
$lang['name_required'] = "Veuillez mettre un nom.";
$lang['headline_required'] = "Veuillez mettre un titre.";
$lang['comment_required'] = "Veuillez ajouter un commentaire.";
$lang['spamming'] = "Vous ne pouvez pas re-poster un commentaire maintenant (anti-spamming), s'il vous pla�t r�-essayez d'ici quelques temps.";
$lang['comments'] = "Commentaires:";
$lang['no_comments'] = "Il n'y a pas de commentaire pour cette image.";
$lang['comments_deactivated'] = "Commentaires d�sactiv�s!";
$lang['post_comment'] = "Poster un commentaire";

//-----------------------------------------------------
//--- BBCode ------------------------------------------
//-----------------------------------------------------
$lang['bbcode'] = "BBCode";
$lang['tag_prompt'] = "Entrez votre texte:";
$lang['link_text_prompt'] = "Entrez le texte qui doit �tre affich� pour le lien (facultatif)";
$lang['link_url_prompt'] = "Entez l'URL compl�te du lien";
$lang['link_email_prompt'] = "Entez l'adresse Email pour le lien";
$lang['list_type_prompt'] = "Quel type de liste vous voulez? Entrez ' 1 ' pour une liste num�rot�e, entrez 'A' pour une liste alphab�tique, ou 'espace' pour une liste avec des points.";
$lang['list_item_prompt'] = "Entrez un article dans la liste. Laissez la bo�te vide ou cliquez sur 'Annuler' pour compl�ter la liste.";

//-----------------------------------------------------
//--- Image Details -----------------------------------
//-----------------------------------------------------
$lang['download_error'] = "Erreur de t�l�chargement!";
$lang['register_download'] = "Pour t�l�charger une image, vous devez �tre un membre enregistr�.<br />&raquo; <a href=\"{url_register}\">Inscrivez-vous</a>";
$lang['voting_success'] = "Merci d'avoir vot� pour cette image";
$lang['voting_error'] = "Vote invalide!";
$lang['already_voted'] = "D�sol� mais vous avez d�j� vot� pour cette image.";
$lang['prev_image'] = "Image pr�c�dente:";
$lang['next_image'] = "Image suivante:";
$lang['category'] = "Cat�gorie:";
$lang['description'] = "Description:";
$lang['keywords'] = "Mots-cl�s:";
$lang['date'] = "Date:";
$lang['hits'] = "Hits:";
$lang['downloads'] = "T�l�charger:";
$lang['rating'] = "Estimer:";
$lang['votes'] = "Votes:";
$lang['file_size'] = "Taille:";
$lang['author'] = "Auteur:";
$lang['name'] = "Nom:";
$lang['headline'] = "Titre:";
$lang['comment'] = "Commentaires:";
$lang['added_by'] = "Post�e par:";
$lang['allow_comments'] = "Autorise les commentaires:";

// IPTC Tags
$lang['iptc_caption'] = "Caption:";
$lang['iptc_caption_writer'] = "Caption writer:";
$lang['iptc_headline'] = "Headline:";
$lang['iptc_special_instructions'] = "Special instructions:";
$lang['iptc_byline'] = "Byline:";
$lang['iptc_byline_title'] = "Byline title:";
$lang['iptc_credit'] = "Credit:";
$lang['iptc_source'] = "Source:";
$lang['iptc_object_name'] = "Object name:";
$lang['iptc_date_created'] = "Date created:";
$lang['iptc_city'] = "City:";
$lang['iptc_state'] = "State:";
$lang['iptc_country'] = "Country:";
$lang['iptc_original_transmission_reference'] = "Original transmission reference:";
$lang['iptc_category'] = "Category:";
$lang['iptc_supplemental_category'] = "Supplemental category:";
$lang['iptc_keyword'] = "Keywords:";
$lang['iptc_copyright_notice'] = "Copyright Notice:";

//-----------------------------------------------------
//--- Postcards ---------------------------------------
//-----------------------------------------------------
$lang['send_postcard'] = "Envoyer une eCard (carte virtuelle)";
$lang['edit_postcard'] = "Modifier votre eCard";
$lang['preview_postcard'] = "Pr�visualiser votre eCard";
$lang['bg_color'] = "Couleur du fond d'�cran:";
$lang['border_color'] = "Couleur de la bordure:";
$lang['font_color'] = "Couleur de la Fonte (caract�re):";
$lang['font_face'] = "Style de fonte:";
$lang['recipient'] = "Destinataire";
$lang['sender'] = "Exp�diteur";
$lang['send_postcard_emailsubject'] = "Une eCard pour vous!";
$lang['send_postcard_success'] = "Merci! Votre eCard vient d'�tre envoy�e � son destinataire!";
$lang['back_to_gallery'] = "Retour � la Galerie";
$lang['invalid_postcard_id'] = "l'ID de cette eCard est Invalide.";

//-----------------------------------------------------
//--- Top Images --------------------------------------
//-----------------------------------------------------
$lang['top_image_hits'] = "Top 10 des images par hits";
$lang['top_image_downloads'] = "Top 10 des images par t�l�chargement";
$lang['top_image_rating'] = "Top 10 des images par r�sultats des votes";
$lang['top_image_votes'] = "Top 10 des images par votes";

//-----------------------------------------------------
//--- Users -------------------------------------------
//-----------------------------------------------------
$lang['send_password_emailsubject'] = "Envoyer un mot de passe du site {site_name}";     // Mail subject for password.
$lang['update_email_emailsubject'] = "MAJ Mot de passe sur le site {site_name}";         // Mail subject for activation code when changing email address
$lang['register_success_emailsubject'] = "Nouvelle Inscription sur le site {site_name}"; // Mail subject for activation code
$lang['admin_activation_emailsubject'] = "Activation de compte";                         // Mail subject for account activation by admin.
$lang['activation_success_emailsubject'] = "Compte activ�";                              // Mail subject after account activation by admin.

$lang['no_permission'] = "Vous n'�tes pas connect� ou vous n'avez pas d'autorisation pour entrer ici.!";
$lang['already_registered'] = "Vous �tes d�j� enregistr�. Si vous avez oubli� votre mot de passe, veuillez <a href=\"{url_lost_password}\">cliquer ici</a>.";
$lang['username_exists'] = "Ce nom d'utilisateur existe d�j�.";
$lang['email_exists'] = "Cette adresse Email existe d�j�.";
$lang['invalid_email_format'] = "Veuillez mettre une adresse Email valide.";
$lang['register_success'] = "Vous �tes maintenant enregistr�. Vous allez rapidemment recevoir un email avec votre code d'activation.";
$lang['register_success_admin'] = "Vous �tes maintenant enregistr�. Votre compte est inactif pour le moment, l'administrateur devra l'activer avant que vous ne puissiez vous logguer. Vous serez averti(e) une fois que votre compte aura �t� activ�.";
$lang['register_success_none'] = "Vous �tes maintenant enregistr�. S'il vous pla�t, logguez vous.";
$lang['missing_activationkey'] = "Il manque votre clef d'activation!.";
$lang['invalid_activationkey'] = "Compte non actif. Veuillez vous inscrire.</>";
$lang['activation_success'] = "Merci! Votre compte est activ�. Vous pouvez vous connecter au site.";
$lang['general_error'] = "Il y a une erreur. Veuillez SVP faire un <a href=javascript:history.go(-1)>retour arri�re</a> et recommencer. Si les probl�mes persistent, prenez contact avec l'administrateur du site.";
$lang['invalid_login'] = "Vous avez saisi un nom d'utilisateur ou un mot de passe invalide.";
$lang['update_email_error'] = "Veuillez entrer encore une fois votre Email!";
$lang['update_email_confirm_error'] = "L'adresse Email que vous avez mise ne correspond pas!";
$lang['update_profile_success'] = "Votre Profil vient d'�tre mis � jour!";
$lang['update_email_instruction'] = "Comme vous avez chang� votre adresse Email, vous devez r�activer votre compte. Le code d'activation a �t� envoy� � votre nouvelle adresse email!";
$lang['update_email_admin_instruction'] = "Comme vous avez chang� votre adresse email, l'administrateur a besoin de r�activer votre compte. Vous serez averti(e) une fois que votre compte aura �t� r�activ�.";
$lang['invalid_email'] = "Adresse Email non valide.";
$lang['send_password_success'] = "Votre mot de passe a �t� envoy� � votre adresse Email.";
$lang['update_password_error'] = "Vous avez mis un mot de passe non valide.";
$lang['update_password_confirm_error'] = "Les deux mots de passe doivent correspondre!";
$lang['update_password_success'] = "Votre Mot de Passe a �t� chang�.";
$lang['invalid_user_id'] = "Utilisateur introuvable!";
$lang['emailuser_success'] = "Le message par Email a �t� envoy�";
$lang['send_email_to'] = "Envoyer un message Email �:";
$lang['subject'] = "Sujet:";
$lang['message'] = "Message:";
$lang['profile_of'] = "Profil utilisateur de:";
$lang['edit_profile_msg'] = "Ici vous pouvez changer votre profil d'utilisateur et votre mot de passe.";
$lang['edit_profile_email_msg'] = "<br />Note: si vous changez votre adresse email vous devez r�activer votre compte. Le code d'activation sera envoy� � votre nouvelle adresse email.";
$lang['edit_profile_email_msg_admin'] = "<br />Note: Si vous changez votre adresse email, l'administrateur aura besoin de r�activer votre compte.";
$lang['join_date'] = "Enregistr� depuis le:";
$lang['last_action'] = "Derni�re visite le:";
$lang['email'] = "Email:";
$lang['email_confirm'] = "Confirmer son email:";
$lang['homepage'] = "Page Perso:";
$lang['icq'] = "ICQ:";
$lang['show_email'] = "Afficher mon adresse Email:";
$lang['allow_emails'] = "Recevez les emails des administrateurs:";
$lang['invisible'] = "Cacher ma situation en ligne:";
$lang['optional_infos'] = "Facultatif";
$lang['change_password'] = "Changer mon mot de passe";
$lang['old_password'] = "Ancien mot de passe:";
$lang['new_password'] = "Nouveau mot de passe:";
$lang['new_password_confirm'] = "Confirmer mon nouveau mot de passe:";
$lang['lost_password'] = "Recommencer votre mot de passe";
$lang['lost_password_msg'] = "Au cas o� vous avez oubli� votre mot de passe, entrez l'adresse Email que vous avez utilis� pour votre inscription.";
$lang['user_name'] = "Nom d'utilisateur:";
$lang['password'] = "Mot de passe:";

$lang['register_msg'] = "S'il vous pla�t remplissez tous les champs. Entrez une adresse Email valide o� nous pouvons vous fournir votre code d'activation.";
$lang['agreement'] = "Conditions d'utilisation:";
$lang['agreement_terms'] = "
            Les administrateurs de ce site entreprendront d'enlever ou
            d'�diter de mani�re g�n�rale et aussi rapidement que possible,
            tous les messages non accept�s par les termes d'utilisation.
            Il est impossible d'examiner chaque message. Par cons�quent vous
            reconnaissez que tous les messages post�s dans ce site expriment
            les vues et les opinions de l'auteur et pas des administrateurs ou des webmasters.
            Le site ne sera pas tenu responsable.
            <br /><br />
            Vous consentez � ne pas afficher des messages abusifs, obsc�nes,
            vulgaires, calomnieux, odieux. Des menaces ou toute autres choses qui
            peuvent violer les lois applicables dans ce pays. Vous consentez que le
            webmaster et l'administrateur de ce site ont le droit d'enlever ou d'�diter
            les sujets non conformes. Comme utilisateur vous consentez � appliquer
            toutes les informations au-dessus.
            Les donn�es sont entrepos�es dans une base. Ces informations ne seront
            pas divulgu�es sans votre consentement. Le webmaster ou l'administrateur
            ne peuvent pas �tre tenu responsables pour tous les messages post�s par les utilisateurs.
            <br /><br />
            Ce syst�me utilise des cookies pour stocker les informations sur votre ordinateur.
            Ces cookies ne contiennent pas d'informations importantes,
            ils servent � am�liorer vos connexions seulement.
            <br /><br />
            En cliquant, vous acceptez les conditions d'utilisation.";

$lang['agree'] = "J'accepte";
$lang['agree_not'] = "Je refuse";
$lang['show_user_images'] = "Affiche toutes les images post�es par {user_name}";

//-----------------------------------------------------
//--- Edit Image --------------------------------------
//-----------------------------------------------------
$lang['image_edit'] = "Editer image";
$lang['image_edit_success'] = "Image �dit�e";
$lang['image_edit_error'] = "Erreur dans l'�dition de l'image";
$lang['image_delete'] = "Supprimer image";
$lang['image_delete_success'] = "Image supprim�e";
$lang['image_delete_error'] = "Erreur dans la suppression de l'image";
$lang['image_delete_confirm'] = "Voulez-vous supprimer cette image?";

//-----------------------------------------------------
//--- Edit Comments -----------------------------------
//-----------------------------------------------------
$lang['comment_edit'] = "Editer le commentaire";
$lang['comment_edit_success'] = "Commentaire �dit�";
$lang['comment_edit_error'] = "Erreur dans l'�dition du commentaire.";
$lang['comment_delete'] = "Supprimer commentaire";
$lang['comment_delete_success'] = "Commentaire supprim�";
$lang['comment_delete_error'] = "Erreur dans la suppression du commentaire.";
$lang['comment_delete_confirm'] = "Supprimer ce commentaire?";

//-----------------------------------------------------
//--- Image Upload ------------------------------------
//-----------------------------------------------------
$lang['field_required'] = "Veuillez remplir le champ {field_name}!";
$lang['kb'] = "kb";
$lang['px'] = "px";
$lang['file_upload_error'] = "Erreur sur le transfert du fichier image";
$lang['thumb_upload_error'] = "Erreur sur le transfert du fichier vignette";
$lang['invalid_file_type'] = "Type de fichier non valide!";
$lang['invalid_image_width'] = "Largeur de fichier non valide!";
$lang['invalid_image_height'] = "Hauteur du fichier non valide!";
$lang['invalid_file_size'] = "taille du fichier non valide!";
$lang['image_add_success'] = "Image ajout�e";
$lang['allowed_mediatypes_desc'] = "Les extensions valides sont: ";
$lang['keywords_ext'] = "Mots-cl�s:<br /><span class=\"smalltext\">Les mots-cl�s doivent �tre s�par�s par des espaces.</span>";
$lang['user_upload'] = "Transf�rer une image";
$lang['image_name'] = "Nom de l'image:";
$lang['media_file'] = "Fichier image:";
$lang['thumb_file'] = "Vignette image:";
$lang['max_filesize'] = "Taille MAX. du fichier: ";
$lang['max_imagewidth'] = "Largeur MAX. de l'image: ";
$lang['max_imageheight'] = "Hauteur MAX de l'image: ";
$lang['image_file_required'] = "Choix de l'image!";
$lang['new_upload_emailsubject'] = "Nouveau transfert sur {site_name}";
$lang['new_upload_validate_desc'] = "Votre image est en attente de validation par l'Administrateur du site.";

//-----------------------------------------------------
//--- Lightbox ----------------------------------------
//-----------------------------------------------------
$lang['lightbox_no_images'] = "Vous n'avez pas d'image m�moris�e.";
$lang['lightbox_add_success'] = "Image ajout�e.";
$lang['lightbox_add_error'] = "Erreur sur l'ajout de l'image!";
$lang['lightbox_remove_success'] = "Image supprim�e de la page m�mo.";
$lang['lightbox_remove_error'] = "Erreur de suppression!";
$lang['lightbox_register'] = "Si vous d�sirez utiliser la fonction m�morisation d'image, vous devez �tre un membre enregsitr�. <br />&raquo; <a href=\"{url_register}\">Inscrivez-vous</a>";
$lang['lightbox_delete_success'] = "Suppression des images m�moris�es.";
$lang['lightbox_delete_error'] = "Erreur de suppression";
$lang['delete_lightbox'] = "Supprimer les images m�moris�es";
$lang['lighbox_lastaction'] = "Mise � jour de la fonction m�morisation:";
$lang['delete_lightbox_confirm'] = "Voulez-vous supprimer les images m�moris�es?";

//-----------------------------------------------------
//--- Misc --------------------------------------------
//-----------------------------------------------------
$lang['new'] = "Nouveau"; // Marks categories and images as "NEW"
$lang['home'] = "Menu Principal";
$lang['categories'] = "Cat�gories";
$lang['sub_categories'] = "Sous Cat�gories";
$lang['lightbox'] = "Images M�mos";
$lang['error'] = "Erreur";
$lang['register'] = "Inscription";
$lang['control_panel'] = "Panneau de contr�le";
$lang['profile'] = "Profil Utilisateur";
$lang['search'] = "Recherche";
$lang['advanced_search'] = "Recherche avanc�e";
$lang['new_images'] = "Nouvelles images";
$lang['top_images'] = "Top images";
$lang['registered_user'] = "Membres enregistr�s";
$lang['logout'] = "Quitter";
$lang['login'] = "Connexion";
$lang['lang_auto_login'] = "Identifiez-moi automatiquement lors de ma prochaine visite?";
$lang['lost_password'] = "Mot de passe oubli�";
$lang['random_image'] = "Image al�atoire";
$lang['site_stats'] = "<b>{total_images}</b> images dans <b>{total_categories}</b> cat�gories.";
$lang['lang_loggedin_msg'] = "Connecter �: <b>{loggedin_user_name}</b>";
$lang['go'] = "Go";
$lang['submit'] = "Envoyer";
$lang['reset'] = "Recommencer";
$lang['save'] = "Sauver";
$lang['yes'] = "Oui";
$lang['no'] = "Non";
$lang['images_per_page'] = "Images par page:";
$lang['user_online'] = "Utilisateurs connect�s: <b>{num_total_online}</b>";
$lang['user_online_detail'] = "Il y a actuellement <b>{num_registered_online}</b> membre(s) enregistr�(s) . (<b>{num_invisible_online}</b> utilisateurs invisibles) et <b>{num_guests_online}</b> invit�(s) connect�(s).";
$lang['lostfield_error'] = "S'il vous pla�t remplissez tous les champs!";
$lang['rate'] = "Vote";

//-----------------------------------------------------
//--- Paging ------------------------------------------
//-----------------------------------------------------
$lang['paging_stats'] = "R�sultats:  <b>{total_cat_images} </b> image(s) sur un total de <b> {total_pages}</b> page(s). Affiche:  image  <b>{first_page}</b> sur <b>{last_page}</b>.";
$lang['paging_next'] = "&raquo;";
$lang['paging_previous'] = "&laquo;";
$lang['paging_lastpage'] = "Derni�re page &raquo;";
$lang['paging_firstpage'] = "&laquo; Premi�re page";

//-----------------------------------------------------
//--- Search ------------------------------------------
//-----------------------------------------------------
$lang['search_no_results'] = "Aucun r�sultat dans votre recherche avec \"<font color=red>{keyword}</font>\".";
$lang['search_by_keyword'] = "Recherche par Mots-cl�s:<br /><span class=\"smalltext\">Vous pouvez utiliser AND, OR ou NOT pour d�finir les mots qui doivent �tre dans les r�sultats.</span>";
$lang['search_by_username'] = "Recherche par nom d'utilisateur:<br /><span class=\"smalltext\"></span>";
$lang['search_terms'] = "Termes de recherche:";
$lang['search_fields'] = "Cherche dans les champs suivants:";
$lang['new_images_only'] = "Afficher les nouvelles images seulement";
$lang['all_fields'] = "Tous";
$lang['name_only'] = "Par nom d'image";
$lang['description_only'] = "Par description";
$lang['keywords_only'] = "Par mots-cl�s";
$lang['and'] = "ET";
$lang['or'] = "OU";

//-----------------------------------------------------
//--- New Images --------------------------------------
//-----------------------------------------------------
$lang['no_new_images'] = "Actuellement, il n'y pas de nouvelle image.";

//-----------------------------------------------------
//--- Admin Links -------------------------------------
//-----------------------------------------------------
$lang['edit'] = "[Editer]";
$lang['delete'] = "[Supprimer]";
?>