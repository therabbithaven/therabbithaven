    function MSFPpreload(img) {
      var a=new Image(); a.src=img; return a; 
    }

    function doHover() {
       MSFPhover = (((navigator.appName == "Netscape") && 
                   (parseInt(navigator.appVersion) >= 3 )) || 
                   ((navigator.appName == "Microsoft Internet Explorer") && 
                   (parseInt(navigator.appVersion) >= 4 ))); 


       if(MSFPhover) {
          MSFPnav1n=MSFPpreload("/_derived/home_cmp_sunflower-w-white-background010_vbtn.gif");
          MSFPnav1h=MSFPpreload("/_derived/home_cmp_sunflower-w-white-background010_vbtn_a.gif");
          MSFPnav2n=MSFPpreload("/_derived/about_the_Haven.htm_cmp_sunflower-w-white-background010_vbtn.gif");
          MSFPnav2h=MSFPpreload("/_derived/about_the_Haven.htm_cmp_sunflower-w-white-background010_vbtn_a.gif"); 
          MSFPnav3n=MSFPpreload("/_derived/how_you_can_help.htm_cmp_sunflower-w-white-background010_vbtn.gif");
          MSFPnav3h=MSFPpreload("/_derived/how_you_can_help.htm_cmp_sunflower-w-white-background010_vbtn_a.gif");
          MSFPnav4n=MSFPpreload("/_derived/adoption_showcase.htm_cmp_sunflower-w-white-background010_vbtn.gif");
          MSFPnav4h=MSFPpreload("/_derived/adoption_showcase.htm_cmp_sunflower-w-white-background010_vbtn_a.gif");
          MSFPnav5n=MSFPpreload("/_derived/Success Stories.htm_cmp_sunflower-w-white-background010_vbtn.gif");
          MSFPnav5h=MSFPpreload("/_derived/Success Stories.htm_cmp_sunflower-w-white-background010_vbtn_a.gif");
          MSFPnav6n=MSFPpreload("/_derived/caring_for_your_rabbit.htm_cmp_sunflower-w-white-background010_vbtn.gif");
          MSFPnav6h=MSFPpreload("/_derived/caring_for_your_rabbit.htm_cmp_sunflower-w-white-background010_vbtn_a.gif");
          MSFPnav7n=MSFPpreload("/_derived/news_links.htm_cmp_sunflower-w-white-background010_vbtn.gif");
          MSFPnav7h=MSFPpreload("/_derived/news_links.htm_cmp_sunflower-w-white-background010_vbtn_a.gif");
       }
  }
