Curly Sue
Estimated birthdate 2/24/06

She is a very fancy Rex female & the only one we�ve ever seen with curly hair. It has gotten straighter as she�s grown up, but when she was very little it looked like she had a permanent wave.

Curly Sue has the longest ears, very stylized fine-boned features. She looks regal and demands that everyone pay attention to her. If you don�t, she�ll thump and nudge you to remind you to watch her play. She�s one of the thumpiest bunnies we�ve ever met, but oh so very cute because she demands your attention.

She�s very curious and will need an active household with room to roam. She prefers to run and jump and explore rather than be confined�but she enjoys being petted too.