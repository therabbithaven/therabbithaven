About this package
******************************************

This is the source package of TYPO3. It is highly recommended that you also
download one of the site packages listed at
http://typo3.org/download/packages/

These archives also contain an installation manual which will surely help to
on your way of installing TYPO3.

-- Michael Stucki <michael@typo3.org>  Fri,  01 Apr 2005 08:15:25 +0200
