typo3/ext/

Globally available extensions for TYPO3
The idea of importing extensions to the global directory is that they are available for all sites using the same TYPO3 core source code (only possible with symlinked source on Unix/Linux).

You can import your own extensions globally.