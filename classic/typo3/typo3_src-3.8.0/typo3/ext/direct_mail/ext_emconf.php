<?php

########################################################################
# Extension Manager/Repository config file for ext: "direct_mail"
# 
# Auto generated 23-05-2005 02:03
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'Web>Plugins, Direct Mail',
	'description' => 'Advanced Direct Mail/Newsletter mailer system with sofisticated options for personalization of emails including response statistics.',
	'category' => 'module',
	'shy' => 0,
	'dependencies' => 'cms,tt_address',
	'conflicts' => '',
	'priority' => '',
	'lockType' => 'G',
	'module' => '',
	'state' => 'beta',
	'internal' => 0,
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => 'tt_content,tt_address,fe_users',
	'clearCacheOnLoad' => 0,
	'author' => 'Kasper Sk�rh�j',
	'author_email' => 'kasper@typo3.com',
	'author_company' => 'Curby Soft Multimedia',
	'private' => 0,
	'download_password' => '',
	'version' => '1.0.8',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:14:{s:12:"ext_icon.gif";s:4:"a143";s:14:"ext_tables.php";s:4:"946b";s:14:"ext_tables.sql";s:4:"cabf";s:26:"locallang_csh_sysdmail.php";s:4:"7c8a";s:27:"locallang_csh_sysdmailg.php";s:4:"b9a4";s:17:"locallang_tca.php";s:4:"c991";s:12:"doc/TODO.txt";s:4:"2114";s:14:"mod/attach.gif";s:4:"5559";s:24:"mod/class.mailselect.php";s:4:"ade5";s:27:"mod/class.mod_web_dmail.php";s:4:"422f";s:20:"mod/dmailerd.phpcron";s:4:"83c7";s:17:"mod/locallang.php";s:4:"fb86";s:12:"mod/mail.gif";s:4:"34a5";s:20:"mod/returnmail.phpsh";s:4:"9b93";}',
);

?>