<?php
/**
* Default  TCA_DESCR for "sys_dmail"
*/

$LOCAL_LANG = Array (
	'default' => Array (
		'.description' => 'Direct mail campaigns ',
		'.details' => 'The "Direct mail" module in TYPO3 lets you send highly customized and personalized newsletters to subscribers either as HTML or Plain text, with or without attachments.
A \'Direct mail\' record contains information about a direct mail campaign such as subject, sender, priority, attachments and whether HTML or Plain text content is allowed. Furthermore it also holds the compiled mail content which is to be sent to the subscribers.
Everything regarding this module is controlled through the Web>Modules backend module.',
		'_.seeAlso' => 'sys_dmail_group
tt_content:list_type',
	),
);
?>