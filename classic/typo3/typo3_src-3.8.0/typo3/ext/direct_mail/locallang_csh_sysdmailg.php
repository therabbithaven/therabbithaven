<?php
/**
* Default  TCA_DESCR for "sys_dmail_group"
*/

$LOCAL_LANG = Array (
	'default' => Array (
		'.description' => 'Defines a group of recipients of Direct mails.',
		'.details' => 'You can compile the group of either single individuals, all Address records within a page or branch, all results or an SQL query or maybe the result from other Direct mail groups.',
		'_.seeAlso' => 'sys_dmail',
	),
);
?>