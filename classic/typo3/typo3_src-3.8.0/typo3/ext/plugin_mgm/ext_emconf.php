<?php

########################################################################
# Extension Manager/Repository config file for ext: "plugin_mgm"
# 
# Auto generated 23-05-2005 02:03
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'Web>Plugins',
	'description' => 'The plugin manager provides a framework for management of the installed frontend plugins.',
	'category' => 'module',
	'shy' => 0,
	'dependencies' => 'cms',
	'conflicts' => '',
	'priority' => '',
	'module' => 'modules',
	'state' => 'beta',
	'internal' => 1,
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author' => 'Kasper Sk�rh�j',
	'author_email' => 'kasperYYYY@typo3.com',
	'author_company' => 'Curby Soft Multimedia',
	'private' => 0,
	'download_password' => '',
	'version' => '0.0.5',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:9:{s:12:"ext_icon.gif";s:4:"61f5";s:14:"ext_tables.php";s:4:"c508";s:17:"modules/clear.gif";s:4:"cc11";s:16:"modules/conf.php";s:4:"7165";s:17:"modules/index.php";s:4:"4f42";s:21:"modules/locallang.php";s:4:"e688";s:25:"modules/locallang_mod.php";s:4:"2422";s:19:"modules/modules.gif";s:4:"61f5";s:12:"doc/TODO.txt";s:4:"f485";}',
);

?>