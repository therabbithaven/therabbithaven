<?php

########################################################################
# Extension Manager/Repository config file for ext: "quickhelp"
# 
# Auto generated 23-05-2005 02:07
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'Help>Quick Help',
	'description' => 'Generic user manual online from typo3.org',
	'category' => 'module',
	'shy' => 0,
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'module' => 'quick',
	'state' => 'stable',
	'internal' => 0,
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author' => 'Kasper Sk�rh�j',
	'author_email' => 'kasperYYYY@typo3.com',
	'author_company' => 'Curby Soft Multimedia',
	'private' => 0,
	'download_password' => '',
	'version' => '0.0.4',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:8:{s:12:"ext_icon.gif";s:4:"66b6";s:14:"ext_tables.php";s:4:"fc86";s:15:"quick/clear.gif";s:4:"cc11";s:14:"quick/conf.php";s:4:"e2d0";s:14:"quick/help.gif";s:4:"66b6";s:15:"quick/index.php";s:4:"db08";s:23:"quick/locallang_mod.php";s:4:"14d6";s:12:"doc/TODO.txt";s:4:"418c";}',
);

?>