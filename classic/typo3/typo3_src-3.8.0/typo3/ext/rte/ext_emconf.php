<?php

########################################################################
# Extension Manager/Repository config file for ext: "rte"
# 
# Auto generated 23-05-2005 02:09
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'Rich Text Editor',
	'description' => 'Rich Text Editor based on MSIE Active X. This is the one that has been around all the time. Now it\'s just an extension...',
	'category' => 'be',
	'shy' => 0,
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'TYPO3_version' => '3.6.0rc2-0.0.1',
	'PHP_version' => '0.0.1-0.0.1',
	'module' => 'app',
	'state' => 'stable',
	'internal' => 1,
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => 'G',
	'author' => 'Kasper Sk�rh�j',
	'author_email' => 'kasperYYYY@typo3.com',
	'author_company' => 'Curby Soft Multimedia',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'private' => 0,
	'download_password' => '',
	'version' => '0.0.10',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:22:{s:21:"class.tx_rte_base.php";s:4:"718b";s:17:"ext_localconf.php";s:4:"6fa5";s:13:"app/clear.gif";s:4:"cc11";s:12:"app/conf.php";s:4:"e95b";s:17:"app/locallang.php";s:4:"7598";s:34:"app/locallang_rte_select_image.php";s:4:"5227";s:26:"app/locallang_rte_user.php";s:4:"0886";s:10:"app/rte.js";s:4:"d803";s:11:"app/rte.php";s:4:"2dd1";s:16:"app/rte_bold.gif";s:4:"802d";s:19:"app/rte_cleaner.php";s:4:"8468";s:16:"app/rte_copy.gif";s:4:"11a1";s:15:"app/rte_cut.gif";s:4:"2950";s:15:"app/rte_div.gif";s:4:"d998";s:19:"app/rte_italics.gif";s:4:"1231";s:17:"app/rte_paste.gif";s:4:"083c";s:24:"app/rte_select_image.php";s:4:"9c1d";s:16:"app/rte_tbEN.gif";s:4:"321f";s:17:"app/rte_under.gif";s:4:"b2f0";s:16:"app/rte_user.php";s:4:"c993";s:12:"doc/TODO.txt";s:4:"2851";s:14:"doc/manual.sxw";s:4:"7ec5";}',
);

?>