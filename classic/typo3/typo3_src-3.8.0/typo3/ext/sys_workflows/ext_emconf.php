<?php

########################################################################
# Extension Manager/Repository config file for ext: "sys_workflows"
# 
# Auto generated 23-05-2005 02:04
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'User>Task Center, Tasks, Workflow',
	'description' => 'Workflow system based upon the To-Do list. It allows a straight line workflow with redirects, groups, reviewers and an editor for finalizing.',
	'category' => 'module',
	'shy' => 0,
	'dependencies' => 'sys_todos',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => 0,
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author' => 'Kasper Sk�rh�j',
	'author_email' => 'kasper@typo3.com',
	'author_company' => 'Curby Soft Multimedia',
	'private' => 0,
	'download_password' => '',
	'version' => '1.0.4',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:10:{s:36:"class.tx_sysworkflows_definition.php";s:4:"02ed";s:34:"class.tx_sysworkflows_executor.php";s:4:"58bf";s:12:"ext_icon.gif";s:4:"1960";s:15:"ext_icon__h.gif";s:4:"ac4d";s:15:"ext_php_api.dat";s:4:"ecf0";s:14:"ext_tables.php";s:4:"d2d7";s:14:"ext_tables.sql";s:4:"b552";s:26:"locallang_csh_sysworkf.php";s:4:"f008";s:17:"locallang_tca.php";s:4:"048a";s:7:"tca.php";s:4:"ba6b";}',
);

?>