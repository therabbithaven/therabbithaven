<?php

########################################################################
# Extension Manager/Repository config file for ext: "taskcenter_recent"
# 
# Auto generated 23-05-2005 02:05
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'User>Task Center, Recent',
	'description' => 'Lists most recently edited pages and records in Task Center',
	'category' => 'module',
	'shy' => 1,
	'dependencies' => 'taskcenter',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => 1,
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author' => 'Kasper Sk�rh�j',
	'author_email' => 'kasper@typo3.com',
	'author_company' => 'Curby Soft Multimedia',
	'private' => 0,
	'download_password' => '',
	'version' => '0.0.6',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:4:{s:29:"class.tx_taskcenterrecent.php";s:4:"dfbe";s:12:"ext_icon.gif";s:4:"9c47";s:14:"ext_tables.php";s:4:"a95c";s:13:"locallang.php";s:4:"2396";}',
);

?>