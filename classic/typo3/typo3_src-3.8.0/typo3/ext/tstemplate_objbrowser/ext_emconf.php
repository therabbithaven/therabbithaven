<?php

########################################################################
# Extension Manager/Repository config file for ext: "tstemplate_objbrowser"
# 
# Auto generated 23-05-2005 02:02
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'Web>Template, Object Browser',
	'description' => 'The Object Browser writes out the TypoScript configuration in an object tree style.',
	'category' => 'module',
	'shy' => 1,
	'dependencies' => 'tstemplate',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => 0,
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author' => 'Kasper Sk�rh�j',
	'author_email' => 'kasperYYYY@typo3.com',
	'author_company' => 'Curby Soft Multimedia',
	'private' => 0,
	'download_password' => '',
	'version' => '0.0.3',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:4:{s:33:"class.tx_tstemplateobjbrowser.php";s:4:"5a60";s:12:"ext_icon.gif";s:4:"4226";s:14:"ext_tables.php";s:4:"7ff6";s:12:"doc/TODO.txt";s:4:"6bb0";}',
);

?>