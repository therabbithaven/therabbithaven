<?php

########################################################################
# Extension Manager/Repository config file for ext: "tt_calender"
# 
# Auto generated 23-05-2005 02:00
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'Calendar',
	'description' => 'Enter dates in the tables and they can be displayed on the webpage. Very simple. Not so useful.',
	'category' => 'plugin',
	'shy' => 0,
	'dependencies' => 'cms',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'beta',
	'internal' => 0,
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 1,
	'lockType' => '',
	'author' => 'Kasper Sk�rh�j',
	'author_email' => 'kasperYYYY@typo3.com',
	'author_company' => 'Curby Soft Multimedia',
	'private' => 0,
	'download_password' => '',
	'version' => '1.0.4',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:16:{s:12:"ext_icon.gif";s:4:"ef1e";s:15:"ext_icon__h.gif";s:4:"ce83";s:16:"ext_icon__ht.gif";s:4:"587c";s:15:"ext_icon__t.gif";s:4:"662a";s:15:"ext_icon__x.gif";s:4:"4555";s:14:"ext_tables.php";s:4:"0328";s:14:"ext_tables.sql";s:4:"2f1b";s:28:"ext_typoscript_constants.txt";s:4:"651e";s:24:"ext_typoscript_setup.txt";s:4:"417c";s:25:"locallang_csh_ttcalen.php";s:4:"7da2";s:26:"locallang_csh_ttcalenc.php";s:4:"79d6";s:17:"locallang_tca.php";s:4:"0581";s:7:"tca.php";s:4:"7b52";s:15:"pi/calendar.inc";s:4:"9f59";s:25:"pi/calendar_template.tmpl";s:4:"af0f";s:14:"doc/manual.sxw";s:4:"30a8";}',
);

?>