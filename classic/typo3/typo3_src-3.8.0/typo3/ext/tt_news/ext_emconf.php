<?php

########################################################################
# Extension Manager/Repository config file for ext: "tt_news"
# 
# Auto generated 23-05-2005 01:58
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'News',
	'description' => 'Website news with front page teasers and article handling inside.',
	'category' => 'plugin',
	'shy' => 0,
	'dependencies' => 'cms',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => 0,
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 1,
	'lockType' => '',
	'author' => 'Kasper Sk�rh�j',
	'author_email' => 'kasperYYYY@typo3.com',
	'author_company' => 'Curby Soft Multimedia',
	'private' => 0,
	'download_password' => '',
	'version' => '1.0.4',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:23:{s:12:"ext_icon.gif";s:4:"5e2a";s:15:"ext_icon__h.gif";s:4:"e1bc";s:16:"ext_icon__ht.gif";s:4:"dbd6";s:17:"ext_icon__htu.gif";s:4:"7975";s:16:"ext_icon__hu.gif";s:4:"865e";s:15:"ext_icon__t.gif";s:4:"ef88";s:16:"ext_icon__tu.gif";s:4:"22a2";s:15:"ext_icon__u.gif";s:4:"0fd4";s:15:"ext_icon__x.gif";s:4:"ede5";s:14:"ext_tables.php";s:4:"6838";s:14:"ext_tables.sql";s:4:"603b";s:28:"ext_typoscript_constants.txt";s:4:"05e7";s:24:"ext_typoscript_setup.txt";s:4:"0e4b";s:24:"locallang_csh_ttnews.php";s:4:"20e2";s:25:"locallang_csh_ttnewsc.php";s:4:"a38f";s:17:"locallang_tca.php";s:4:"5893";s:7:"tca.php";s:4:"1db7";s:14:"doc/manual.sxw";s:4:"a3c2";s:22:"pi/class.tx_ttnews.php";s:4:"29aa";s:10:"pi/new.gif";s:4:"7f00";s:17:"pi/news_help.tmpl";s:4:"6121";s:17:"pi/news_help1.gif";s:4:"d8fb";s:21:"pi/news_template.tmpl";s:4:"5387";}',
);

?>