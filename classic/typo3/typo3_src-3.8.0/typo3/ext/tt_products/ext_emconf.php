<?php

########################################################################
# Extension Manager/Repository config file for ext: "tt_products"
# 
# Auto generated 23-05-2005 01:58
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'Shop system',
	'description' => 'Simple shop system with categories.',
	'category' => 'plugin',
	'shy' => 0,
	'dependencies' => 'cms',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => 0,
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 1,
	'lockType' => '',
	'author' => 'Kasper Sk�rh�j',
	'author_email' => 'kasperYYYY@typo3.com',
	'author_company' => 'Curby Soft Multimedia',
	'private' => 0,
	'download_password' => '',
	'version' => '1.2.7',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:32:{s:31:"class.tx_ttproducts_wizicon.php";s:4:"7b77";s:12:"ext_icon.gif";s:4:"eb61";s:15:"ext_icon__h.gif";s:4:"d98f";s:16:"ext_icon__ht.gif";s:4:"40c5";s:17:"ext_icon__htu.gif";s:4:"4628";s:16:"ext_icon__hu.gif";s:4:"3b58";s:15:"ext_icon__t.gif";s:4:"04a3";s:16:"ext_icon__tu.gif";s:4:"fcac";s:15:"ext_icon__u.gif";s:4:"ef5a";s:15:"ext_icon__x.gif";s:4:"3998";s:14:"ext_tables.php";s:4:"94c3";s:14:"ext_tables.sql";s:4:"9812";s:28:"ext_typoscript_constants.txt";s:4:"e974";s:24:"ext_typoscript_setup.txt";s:4:"0715";s:13:"locallang.php";s:4:"4486";s:24:"locallang_csh_ttprod.php";s:4:"51ee";s:25:"locallang_csh_ttprodc.php";s:4:"9dea";s:17:"locallang_tca.php";s:4:"1e24";s:15:"productlist.gif";s:4:"a6c1";s:7:"tca.php";s:4:"bd41";s:14:"doc/manual.sxw";s:4:"f0d8";s:26:"pi/class.tx_ttproducts.php";s:4:"ac0f";s:19:"pi/payment_DIBS.php";s:4:"bb33";s:29:"pi/payment_DIBS_template.tmpl";s:4:"0eb3";s:32:"pi/payment_DIBS_template_uk.tmpl";s:4:"96f9";s:31:"pi/products_comp_calcScript.inc";s:4:"046b";s:21:"pi/products_help.tmpl";s:4:"7586";s:21:"pi/products_help1.gif";s:4:"46d1";s:20:"pi/products_mail.inc";s:4:"94f2";s:25:"pi/products_template.tmpl";s:4:"47dd";s:28:"pi/products_template_de.tmpl";s:4:"1c6a";s:34:"pi/products_template_htmlmail.tmpl";s:4:"aa8a";}',
);

?>