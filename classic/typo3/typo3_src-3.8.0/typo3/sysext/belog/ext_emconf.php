<?php

########################################################################
# Extension Manager/Repository config file for ext: "belog"
# 
# Auto generated 23-05-2005 02:07
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'Tools>Log',
	'description' => 'Displays backend log, both per page and systemwide. Available as the module Tools>Log (system wide overview) and Web>Info/Log (page relative overview).',
	'category' => 'module',
	'shy' => 1,
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'TYPO3_version' => '3.7.0-',
	'PHP_version' => '0.0.1-0.0.1',
	'module' => 'mod',
	'state' => 'stable',
	'internal' => 0,
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author' => 'Kasper Sk�rh�j',
	'author_email' => 'kasperYYYY@typo3.com',
	'author_company' => 'Curby Soft Multimedia',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'private' => 0,
	'download_password' => '',
	'version' => '0.1.2',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:11:{s:26:"class.tx_belog_webinfo.php";s:4:"e53c";s:12:"ext_icon.gif";s:4:"a61e";s:14:"ext_tables.php";s:4:"694b";s:13:"locallang.xml";s:4:"f816";s:12:"doc/TODO.txt";s:4:"1631";s:13:"mod/clear.gif";s:4:"cc11";s:12:"mod/conf.php";s:4:"694e";s:13:"mod/index.php";s:4:"387b";s:17:"mod/locallang.xml";s:4:"f3e9";s:21:"mod/locallang_mod.xml";s:4:"7382";s:11:"mod/log.gif";s:4:"a61e";}',
);

?>