<?php

########################################################################
# Extension Manager/Repository config file for ext: "beuser"
# 
# Auto generated 23-05-2005 02:06
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'Tools>User Admin',
	'description' => 'Backend user administration and overview. Allows you to compare the settings of users and verify their permissions.',
	'category' => 'module',
	'shy' => 1,
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'TYPO3_version' => '3.7.0-',
	'PHP_version' => '0.0.1-0.0.1',
	'module' => 'mod',
	'state' => 'stable',
	'internal' => 0,
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author' => 'Kasper Sk�rh�j',
	'author_email' => 'kasperYYYY@typo3.com',
	'author_company' => 'Curby Soft Multimedia',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'private' => 0,
	'download_password' => '',
	'version' => '0.1.2',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:8:{s:12:"ext_icon.gif";s:4:"2804";s:14:"ext_tables.php";s:4:"c05c";s:12:"doc/TODO.txt";s:4:"93f7";s:14:"mod/beuser.gif";s:4:"2804";s:13:"mod/clear.gif";s:4:"cc11";s:12:"mod/conf.php";s:4:"8ab5";s:13:"mod/index.php";s:4:"4103";s:21:"mod/locallang_mod.xml";s:4:"d194";}',
);

?>