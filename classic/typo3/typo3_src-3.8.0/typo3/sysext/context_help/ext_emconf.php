<?php

########################################################################
# Extension Manager/Repository config file for ext: "context_help"
# 
# Auto generated 23-05-2005 02:10
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'Context Sensitive Help',
	'description' => 'Provides context sensitive help to tables, fields and modules in the system languages.',
	'category' => 'be',
	'shy' => 1,
	'dependencies' => 'cms',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'TYPO3_version' => '3.7.0-',
	'PHP_version' => '0.0.1-0.0.1',
	'module' => '',
	'state' => 'beta',
	'internal' => 1,
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author' => 'Kasper Sk�rh�j',
	'author_email' => 'kasperYYYY@typo3.com',
	'author_company' => 'Curby Soft Multimedia',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'private' => 0,
	'download_password' => '',
	'version' => '1.0.9',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:31:{s:12:"ext_icon.gif";s:4:"d050";s:14:"ext_tables.php";s:4:"4bcf";s:27:"locallang_csh_fe_groups.xml";s:4:"3acf";s:26:"locallang_csh_fe_users.xml";s:4:"f083";s:23:"locallang_csh_pages.xml";s:4:"be0d";s:26:"locallang_csh_pageslol.xml";s:4:"81c4";s:27:"locallang_csh_statictpl.xml";s:4:"9902";s:27:"locallang_csh_sysdomain.xml";s:4:"5861";s:25:"locallang_csh_systmpl.xml";s:4:"35c3";s:27:"locallang_csh_ttcontent.xml";s:4:"399d";s:24:"cshimages/fegroups_3.png";s:4:"5596";s:24:"cshimages/fegroups_4.png";s:4:"1023";s:23:"cshimages/feusers_1.png";s:4:"219b";s:23:"cshimages/feusers_2.png";s:4:"1b35";s:25:"cshimages/hidden_page.gif";s:4:"b7d1";s:25:"cshimages/hidden_page.png";s:4:"f56a";s:27:"cshimages/page_shortcut.gif";s:4:"3dc0";s:27:"cshimages/page_shortcut.png";s:4:"06fd";s:21:"cshimages/pages_1.png";s:4:"f2dc";s:21:"cshimages/pages_2.png";s:4:"20d1";s:20:"cshimages/static.png";s:4:"7d17";s:25:"cshimages/systemplate.png";s:4:"7f27";s:26:"cshimages/systemplate1.png";s:4:"18a9";s:26:"cshimages/systemplate2.png";s:4:"80a5";s:25:"cshimages/ttcontent_1.png";s:4:"2b09";s:25:"cshimages/ttcontent_2.png";s:4:"8865";s:25:"cshimages/ttcontent_3.png";s:4:"4100";s:25:"cshimages/ttcontent_4.png";s:4:"e572";s:25:"cshimages/ttcontent_5.png";s:4:"211e";s:25:"cshimages/ttcontent_6.png";s:4:"f075";s:25:"cshimages/ttcontent_7.png";s:4:"05e7";}',
);

?>