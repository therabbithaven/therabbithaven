<?php

########################################################################
# Extension Manager/Repository config file for ext: "css_styled_content"
# 
# Auto generated 23-05-2005 02:01
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'CSS styled content',
	'description' => 'Contains configuration for CSS content-rendering of the table "tt_content". This is meant as a modern substitute for the classic "content (default)" template which was based more on <font>-tags, while this is pure CSS. It is intended to work with all modern browsers (which excludes the NS4 series).',
	'category' => 'fe',
	'shy' => 0,
	'dependencies' => 'cms',
	'conflicts' => '',
	'priority' => 'top',
	'loadOrder' => '',
	'TYPO3_version' => '3.7.0-',
	'PHP_version' => '0.0.1-0.0.1',
	'module' => '',
	'state' => 'beta',
	'internal' => 1,
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 1,
	'lockType' => '',
	'author' => 'Kasper Sk�rh�j',
	'author_email' => 'kasperYYYY@typo3.com',
	'author_company' => 'Curby Soft Multimedia',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'private' => 0,
	'download_password' => '',
	'version' => '0.2.2',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:18:{s:21:"ext_conf_template.txt";s:4:"44ff";s:12:"ext_icon.gif";s:4:"1845";s:17:"ext_localconf.php";s:4:"b9d5";s:15:"ext_php_api.dat";s:4:"f62a";s:14:"ext_tables.php";s:4:"c1f8";s:16:"pageTSconfig.txt";s:4:"a3eb";s:15:"css/example.css";s:4:"9804";s:24:"css/example_outlines.css";s:4:"e656";s:14:"css/readme.txt";s:4:"ee9d";s:31:"css/img/background_gradient.gif";s:4:"45d7";s:28:"css/img/red_arrow_bullet.gif";s:4:"82d6";s:12:"doc/TODO.txt";s:4:"6534";s:14:"doc/manual.sxw";s:4:"85e4";s:37:"pi1/class.tx_cssstyledcontent_pi1.php";s:4:"75c2";s:17:"pi1/locallang.xml";s:4:"06f0";s:20:"static/constants.txt";s:4:"ff7a";s:20:"static/editorcfg.txt";s:4:"8d1c";s:16:"static/setup.txt";s:4:"49b4";}',
);

?>