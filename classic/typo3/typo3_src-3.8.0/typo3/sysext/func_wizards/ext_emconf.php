<?php

########################################################################
# Extension Manager/Repository config file for ext: "func_wizards"
# 
# Auto generated 23-05-2005 02:03
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'Web>Func, Wizards',
	'description' => 'Adds the \'Wizards\' item to the function menu in Web>Func. This is just a framework for wizard extensions.',
	'category' => 'module',
	'shy' => 1,
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'TYPO3_version' => '3.7.0-',
	'PHP_version' => '-',
	'module' => '',
	'state' => 'stable',
	'internal' => 0,
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author' => 'Kasper Sk�rh�j',
	'author_email' => 'kasperYYYY@typo3.com',
	'author_company' => 'Curby Soft Multimedia',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'private' => 0,
	'download_password' => '',
	'version' => '0.1.0',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:5:{s:32:"class.tx_funcwizards_webfunc.php";s:4:"67e3";s:12:"ext_icon.gif";s:4:"b4bf";s:14:"ext_tables.php";s:4:"17f9";s:13:"locallang.xml";s:4:"fe2e";s:17:"locallang_csh.xml";s:4:"205a";}',
);

?>