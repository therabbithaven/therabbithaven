<?php

########################################################################
# Extension Manager/Repository config file for ext: "indexed_search"
# 
# Auto generated 23-05-2005 01:59
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'Indexed Search Engine',
	'description' => 'Indexed Search Engine for TYPO3 pages, PDF-files, Word-files, HTML and text files. Provides a backend module for statistics of the indexer and a frontend plugin.',
	'category' => 'plugin',
	'shy' => 0,
	'dependencies' => 'cms',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'TYPO3_version' => '3.7.0-',
	'PHP_version' => '0.0.1-0.0.1',
	'module' => 'mod,cli',
	'state' => 'stable',
	'internal' => 1,
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 1,
	'lockType' => '',
	'author' => 'Kasper Sk�rh�j',
	'author_email' => 'kasperYYYY@typo3.com',
	'author_company' => 'Curby Soft Multimedia',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'private' => 0,
	'download_password' => '',
	'version' => '2.1.3',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:49:{s:9:"ChangeLog";s:4:"38e1";s:25:"class.doublemetaphone.php";s:4:"718e";s:25:"class.external_parser.php";s:4:"81a4";s:17:"class.indexer.php";s:4:"b3e2";s:15:"class.lexer.php";s:4:"4e81";s:21:"ext_conf_template.txt";s:4:"79d3";s:12:"ext_icon.gif";s:4:"4cbf";s:17:"ext_localconf.php";s:4:"b600";s:14:"ext_tables.php";s:4:"c7b4";s:14:"ext_tables.sql";s:4:"2cf1";s:28:"ext_typoscript_editorcfg.txt";s:4:"1614";s:24:"ext_typoscript_setup.txt";s:4:"c337";s:13:"locallang.xml";s:4:"32fc";s:16:"locallang_db.xml";s:4:"c954";s:7:"tca.php";s:4:"2eae";s:12:"cli/conf.php";s:4:"bbcd";s:21:"cli/indexer_cli.phpsh";s:4:"d236";s:12:"doc/TODO.txt";s:4:"a7dc";s:14:"doc/manual.sxw";s:4:"88e9";s:24:"example/class.pihook.php";s:4:"52a3";s:13:"mod/clear.gif";s:4:"cc11";s:12:"mod/conf.php";s:4:"9062";s:13:"mod/index.php";s:4:"c51a";s:15:"mod/isearch.gif";s:4:"4cbf";s:21:"mod/locallang_mod.xml";s:4:"8b99";s:44:"modfunc1/class.tx_indexedsearch_modfunc1.php";s:4:"5a8c";s:22:"modfunc1/locallang.xml";s:4:"5e52";s:44:"modfunc2/class.tx_indexedsearch_modfunc2.php";s:4:"d6f8";s:22:"modfunc2/locallang.xml";s:4:"dd93";s:29:"pi/class.tx_indexedsearch.php";s:4:"f000";s:21:"pi/considerations.txt";s:4:"52ea";s:16:"pi/locallang.xml";s:4:"625e";s:14:"pi/res/csv.gif";s:4:"e413";s:14:"pi/res/doc.gif";s:4:"0975";s:15:"pi/res/html.gif";s:4:"5647";s:14:"pi/res/jpg.gif";s:4:"23ac";s:17:"pi/res/locked.gif";s:4:"c212";s:16:"pi/res/pages.gif";s:4:"1923";s:14:"pi/res/pdf.gif";s:4:"9451";s:14:"pi/res/pps.gif";s:4:"926b";s:14:"pi/res/ppt.gif";s:4:"ada5";s:14:"pi/res/rtf.gif";s:4:"f660";s:14:"pi/res/sxc.gif";s:4:"00a6";s:14:"pi/res/sxi.gif";s:4:"ef83";s:14:"pi/res/sxw.gif";s:4:"4a8f";s:14:"pi/res/tif.gif";s:4:"533b";s:14:"pi/res/txt.gif";s:4:"c576";s:14:"pi/res/xls.gif";s:4:"4a22";s:14:"pi/res/xml.gif";s:4:"2e7b";}',
);

?>