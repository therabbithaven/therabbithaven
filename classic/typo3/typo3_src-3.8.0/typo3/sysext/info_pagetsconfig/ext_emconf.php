<?php

########################################################################
# Extension Manager/Repository config file for ext: "info_pagetsconfig"
# 
# Auto generated 23-05-2005 02:03
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'Web>Info, Page TSconfig',
	'description' => 'Displays the compiled Page TSconfig values relative to a page.',
	'category' => 'module',
	'shy' => 1,
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'TYPO3_version' => '3.7.0-',
	'PHP_version' => '-',
	'module' => '',
	'state' => 'stable',
	'internal' => 0,
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author' => 'Kasper Sk�rh�j',
	'author_email' => 'kasperYYYY@typo3.com',
	'author_company' => 'Curby Soft Multimedia',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'private' => 0,
	'download_password' => '',
	'version' => '0.1.0',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:11:{s:37:"class.tx_infopagetsconfig_webinfo.php";s:4:"fe2d";s:12:"ext_icon.gif";s:4:"04b0";s:14:"ext_tables.php";s:4:"935e";s:13:"locallang.xml";s:4:"3a15";s:25:"locallang_csh_webinfo.xml";s:4:"afe7";s:19:"cshimages/img_1.png";s:4:"729d";s:19:"cshimages/img_2.png";s:4:"3b0c";s:19:"cshimages/img_3.png";s:4:"3f9a";s:19:"cshimages/img_4.png";s:4:"2718";s:19:"cshimages/img_5.png";s:4:"f5bf";s:12:"doc/TODO.txt";s:4:"418c";}',
);

?>