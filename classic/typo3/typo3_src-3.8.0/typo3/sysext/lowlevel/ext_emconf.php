<?php

########################################################################
# Extension Manager/Repository config file for ext: "lowlevel"
# 
# Auto generated 23-05-2005 02:07
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'Tools>Config+DBint',
	'description' => 'Enables the \'Config\' and \'DBint\' modules for technical analysis of the system. This includes raw database search, checking relations, counting pages and records etc.',
	'category' => 'module',
	'shy' => 1,
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'TYPO3_version' => '3.7.0-',
	'PHP_version' => '0.0.1-0.0.1',
	'module' => 'config,dbint',
	'state' => 'stable',
	'internal' => 0,
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author' => 'Kasper Sk�rh�j',
	'author_email' => 'kasperYYYY@typo3.com',
	'author_company' => 'Curby Soft Multimedia',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'private' => 0,
	'download_password' => '',
	'version' => '1.1.2',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:13:{s:12:"ext_icon.gif";s:4:"4bcb";s:14:"ext_tables.php";s:4:"e778";s:16:"config/clear.gif";s:4:"cc11";s:15:"config/conf.php";s:4:"8bac";s:17:"config/config.gif";s:4:"2d41";s:16:"config/index.php";s:4:"2035";s:24:"config/locallang_mod.xml";s:4:"5153";s:15:"dbint/clear.gif";s:4:"cc11";s:14:"dbint/conf.php";s:4:"bb22";s:12:"dbint/db.gif";s:4:"4bcb";s:15:"dbint/index.php";s:4:"22b3";s:23:"dbint/locallang_mod.xml";s:4:"d185";s:12:"doc/TODO.txt";s:4:"4dcc";}',
);

?>