<?php

########################################################################
# Extension Manager/Repository config file for ext: "setup"
# 
# Auto generated 23-05-2005 02:05
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'User>Setup',
	'description' => 'Allows users to edit a limited set of options for their user profile, eg. preferred language and their name and email address.',
	'category' => 'module',
	'shy' => 1,
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'TYPO3_version' => '3.7.0-',
	'PHP_version' => '0.0.2-0.0.2',
	'module' => 'mod',
	'state' => 'stable',
	'internal' => 0,
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author' => 'Kasper Sk�rh�j',
	'author_email' => 'kasperYYYY@typo3.com',
	'author_company' => 'Curby Soft Multimedia',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'private' => 0,
	'download_password' => '',
	'version' => '0.0.16',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:23:{s:12:"ext_icon.gif";s:4:"6187";s:14:"ext_tables.php";s:4:"cc19";s:21:"locallang_csh_mod.xml";s:4:"af8b";s:18:"cshimages/lang.png";s:4:"237d";s:17:"cshimages/rte.png";s:4:"aaeb";s:20:"cshimages/setup1.png";s:4:"7e74";s:21:"cshimages/setup10.png";s:4:"4f5b";s:21:"cshimages/setup11.png";s:4:"2210";s:21:"cshimages/setup12.png";s:4:"7976";s:20:"cshimages/setup2.png";s:4:"8c62";s:20:"cshimages/setup3.png";s:4:"b6cf";s:20:"cshimages/setup4.png";s:4:"57e1";s:20:"cshimages/setup5.png";s:4:"7623";s:20:"cshimages/setup6.png";s:4:"2c85";s:20:"cshimages/setup7.png";s:4:"8543";s:20:"cshimages/setup8.png";s:4:"c12a";s:20:"cshimages/setup9.png";s:4:"42c9";s:13:"mod/clear.gif";s:4:"cc11";s:12:"mod/conf.php";s:4:"a8bf";s:13:"mod/index.php";s:4:"31e1";s:17:"mod/locallang.xml";s:4:"f99a";s:21:"mod/locallang_mod.xml";s:4:"c5c7";s:13:"mod/setup.gif";s:4:"6187";}',
);

?>