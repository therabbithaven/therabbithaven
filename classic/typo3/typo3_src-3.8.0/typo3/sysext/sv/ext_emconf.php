<?php

########################################################################
# Extension Manager/Repository config file for ext: "sv"
# 
# Auto generated 23-05-2005 01:57
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'TYPO3 System Services',
	'description' => 'The core/default sevices. This includes the default authentication services for now.',
	'category' => 'services',
	'shy' => 1,
	'dependencies' => '',
	'conflicts' => '',
	'priority' => 'top',
	'module' => '',
	'state' => 'stable',
	'internal' => 1,
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 1,
	'lockType' => 'S',
	'author' => 'Ren� Fritz',
	'author_email' => 'r.fritz@colorcube.de',
	'author_company' => 'Colorcube',
	'private' => 0,
	'download_password' => '',
	'version' => '1.0.0',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:5:{s:20:"class.tx_sv_auth.php";s:4:"730e";s:24:"class.tx_sv_authbase.php";s:4:"10e0";s:12:"ext_icon.gif";s:4:"87d7";s:17:"ext_localconf.php";s:4:"0ea3";s:14:"ext_tables.php";s:4:"d4b5";}',
);

?>