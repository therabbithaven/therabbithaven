<?php

########################################################################
# Extension Manager/Repository config file for ext: "taskcenter"
# 
# Auto generated 23-05-2005 02:05
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'User>Task Center',
	'description' => 'The Task Center is the framework for a host of other extensions, see below.',
	'category' => 'module',
	'shy' => 1,
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'TYPO3_version' => '3.7.0-',
	'PHP_version' => '0.0.1-0.0.1',
	'module' => 'task',
	'state' => 'stable',
	'internal' => 0,
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author' => 'Kasper Sk�rh�j',
	'author_email' => 'kasperYYYY@typo3.com',
	'author_company' => 'Curby Soft Multimedia',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'private' => 0,
	'download_password' => '',
	'version' => '0.1.2',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:9:{s:12:"ext_icon.gif";s:4:"fc53";s:14:"ext_tables.php";s:4:"837d";s:28:"task/class.mod_user_task.php";s:4:"d193";s:14:"task/clear.gif";s:4:"cc11";s:13:"task/conf.php";s:4:"1fae";s:14:"task/index.php";s:4:"78fe";s:18:"task/locallang.xml";s:4:"3e08";s:22:"task/locallang_mod.xml";s:4:"920d";s:13:"task/task.gif";s:4:"fc53";}',
);

?>