<?php

########################################################################
# Extension Manager/Repository config file for ext: "version"
# 
# Auto generated 23-05-2005 02:08
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'Versioning Management',
	'description' => 'Backend Interface for management of the versioning API.',
	'category' => 'be',
	'author' => 'Kasper Skaarhoj',
	'author_email' => 'kasperYYYY@typo3.com',
	'shy' => '',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'module' => 'cm1',
	'state' => 'beta',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author_company' => '',
	'private' => '',
	'download_password' => '',
	'version' => '0.0.0',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:14:{s:24:"class.tx_version_cm1.php";s:4:"a09d";s:12:"ext_icon.gif";s:4:"1bdc";s:14:"ext_tables.php";s:4:"d52c";s:13:"locallang.xml";s:4:"1e9d";s:16:"locallang_db.xml";s:4:"d145";s:13:"cm1/clear.gif";s:4:"cc11";s:15:"cm1/cm_icon.gif";s:4:"8074";s:12:"cm1/conf.php";s:4:"d1ef";s:13:"cm1/index.php";s:4:"104b";s:17:"cm1/locallang.xml";s:4:"6fc0";s:19:"doc/wizard_form.dat";s:4:"a254";s:20:"doc/wizard_form.html";s:4:"0652";s:38:"modfunc1/class.tx_version_modfunc1.php";s:4:"7742";s:22:"modfunc1/locallang.xml";s:4:"e934";}',
);

?>