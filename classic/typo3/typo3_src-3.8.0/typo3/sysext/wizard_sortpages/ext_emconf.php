<?php

########################################################################
# Extension Manager/Repository config file for ext: "wizard_sortpages"
# 
# Auto generated 23-05-2005 02:03
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'Web>Func, Wizards, Sort pages',
	'description' => 'A little utility to rearrange the sorting order of pages in the backend.',
	'category' => 'module',
	'shy' => 1,
	'dependencies' => 'func_wizards',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => 0,
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author' => 'Kasper Sk�rh�j',
	'author_email' => 'kasperYYYY@typo3.com',
	'author_company' => 'Curby Soft Multimedia',
	'private' => 0,
	'download_password' => '',
	'version' => '0.0.6',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:6:{s:38:"class.tx_wizardsortpages_webfunc_2.php";s:4:"e6de";s:12:"ext_icon.gif";s:4:"d638";s:14:"ext_tables.php";s:4:"f74d";s:13:"locallang.xml";s:4:"1f10";s:17:"locallang_csh.xml";s:4:"a1a0";s:23:"cshimages/wizards_1.png";s:4:"1ac8";}',
);

?>