<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2004 Mirko Schaal, SPLINELAB (ms@splinelab.com)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'CSS styled IMGTEXT with alt and title attributes' for the 'cron_cssstyledimgtext' extension.
 *
 * @author	Mirko Schaal, SPLINELAB <ms@splinelab.com>
 */


require_once(PATH_tslib.'class.tslib_content.php');

/**
 *
 */
class ux_tslib_cObj extends tslib_cObj {

	/**
	 * An abstraction method which creates an alt or title parameter for an HTML img tag.
	 * From the $conf array it implements the properties 'altText', 'titleText' and 'longdescURL'
	 *
	 * @param	array		TypoScript configuration properties
	 * @return	string		Parameter string containing alt and title parameters (if any)
	 * @see IMGTEXT(), cImage()
	 */
	function getAltParam($conf)	{
		// init
		$alttext='';

		// check if there are alredy alt or title tags
		// there seems to be a problem with the alt and title attributes in typo3 (i use 3.6.2)
		// images render like <img src="thegif.gif" alt="my alt" title="my title" alt="" title="" />
		// so i try to get rid of the empty attributes
		$hasAlt = eregi ('alt=', $conf['params']);
		$hasTitle = eregi ('title=', $conf['params']);

		// alt-attribute
		if (! $hasAlt ) {
			if (is_array($conf['altText.']))	{
				$altText = $this->stdWrap($this->cObjGet($conf['altText.'], 'altText.'),$conf['altText.']);
			} else {
				$altText = trim($this->stdWrap($conf['altText'], $conf['altText.']));
			}
			$alttextArray = array();
			if ($conf['altText.']['split'])	{
				$altSplit = $this->stdWrap($conf['altText.']['split.']['token'],$conf['altText.']['split.']['token.']);
				if (!$altSplit) {$altSplit=chr(10);}
				$alttext2 = $this->cObjGetSingle($conf['altText.']['split.']['cObject'],$conf['altText.']['split.']['cObject.'],'altText.split.cObject');
				$alttextArray = explode($altSplit,$alttext2);
				while(list($alt_key,$alt_val)=each($alttextArray))	{
					$alttextArray[$alt_key] = $this->stdWrap(trim($alttextArray[$alt_key]), $conf['altText.']['split.']['stdWrap.']);
				}
				$altText = $alttextArray[$GLOBALS['TSFE']->register['IMAGE_NUM']];
			}
		}

		// title-attribute
		if (! $hasTitle ) {
			if (is_array($conf['titleText.']))	{
				$titleText = trim($this->stdWrap($this->cObjGet($conf['titleText.'], 'titleText.'),$conf['titleText.']));
			} else {
				$titleText = trim($this->stdWrap($conf['titleText'], $conf['titleText.']));
			}
			$titletextArray = array();
			if ($conf['titleText.']['split'])	{
				$altSplit = $this->stdWrap($conf['titleText.']['split.']['token'],$conf['titleText.']['split.']['token.']);
				if (!$altSplit) {$altSplit=chr(10);}
				$titletext2= $this->cObjGetSingle($conf['titleText.']['split.']['cObject'],$conf['titleText.']['split.']['cObject.'],'titleText.split.cObject');
				$titletextArray=explode($altSplit,$titletext2);
				while(list($alt_key,$alt_val)=each($titletextArray))	{
					$titletextArray[$alt_key] = $this->stdWrap(trim($titletextArray[$alt_key]), $conf['titleText.']['split.']['stdWrap.']);
				}
				$titleText = $titletextArray[$GLOBALS['TSFE']->register['IMAGE_NUM']];
			}
		}

		$longDesc = trim($this->stdWrap($conf['longdescURL'],$conf['longdescURL.']));

		$parms = '';

			// 'alt':
		if (! $hasAlt) {
			$parms .= ' alt="'.htmlspecialchars(strip_tags($altText)).'"';
		}

			// 'title':
		$emptyTitleHandling = 'useAlt';
		if ($conf['emptyTitleHandling']) {
				// choices: 'keepEmpty' | 'useAlt' | 'removeAttr'
			$emptyTitleHandling = $conf['emptyTitleHandling'];
		}
		if (! $hasTitle ) {
			if ($titleText || $emptyTitleHandling == 'keepEmpty') {
				$parms.= ' title="'.htmlspecialchars(strip_tags($titleText)).'"';
			} else if (!$titleText && $emptyTitleHandling == 'useAlt') {
				$parms.= ' title="'.htmlspecialchars(strip_tags($altText)).'"';
			}
		}

			// 'longDesc' URL
		if ($longDesc)	{
			$parms.= ' longdesc="'.htmlspecialchars($longDesc).'"';
		}
		return $parms;
	}
}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/cron_cssstyledimgtext/class.ux_tslib_content.php']) {
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/cron_cssstyledimgtext/class.ux_tslib_content.php']);
}
?>