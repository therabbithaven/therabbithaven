<?php

########################################################################
# Extension Manager/Repository config file for ext: "cron_cssstyledimgtext"
# 
# Auto generated 16-04-2006 23:24
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'CSS styled IMGTEXT',
	'description' => 'CSS based implementation of the "Text with Image" and "Image" content types (from tt_content).',
	'category' => 'fe',
	'shy' => 0,
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'TYPO3_version' => '0.0.2-0.0.2',
	'PHP_version' => '0.0.2-0.0.2',
	'module' => '',
	'state' => 'beta',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 1,
	'lockType' => '',
	'author' => 'Ernesto Baschny',
	'author_email' => 'ernst@cron-it.de',
	'author_company' => 'cron IT GmbH',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'version' => '0.4.1',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:11:{s:26:"class.ux_tslib_content.php";s:4:"cc77";s:21:"ext_conf_template.txt";s:4:"2911";s:12:"ext_icon.gif";s:4:"1b90";s:17:"ext_localconf.php";s:4:"7657";s:14:"ext_tables.php";s:4:"a9f6";s:14:"ext_tables.sql";s:4:"5fba";s:16:"locallang_db.php";s:4:"a889";s:14:"doc/manual.sxw";s:4:"39b2";s:41:"pi1/class.tx_croncssstyledimgtext_pi1.php";s:4:"17fa";s:20:"static/constants.txt";s:4:"c7bd";s:16:"static/setup.txt";s:4:"4daa";}',
);

?>