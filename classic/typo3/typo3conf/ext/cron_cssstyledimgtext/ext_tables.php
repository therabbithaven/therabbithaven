<?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

t3lib_extMgm::addStaticFile($_EXTKEY,'static/','CSS Styled IMGTEXT');

$tempColumns = Array (
	'tx_croncssstyledimgtext_renderMethod' => Array (
		'exclude' => 1,
		'label' => 'LLL:EXT:cron_cssstyledimgtext/locallang_db.php:tt_content.tx_croncssstyledimgtext_renderMethod',
		'config' => Array (
			'type' => 'select',
			'items' => Array (
				Array('LLL:EXT:cron_cssstyledimgtext/locallang_db.php:tt_content.tx_croncssstyledimgtext_renderMethod.I.default', 'default'),
				Array('LLL:EXT:cron_cssstyledimgtext/locallang_db.php:tt_content.tx_croncssstyledimgtext_renderMethod.I.dl', 'dl'),
				Array('LLL:EXT:cron_cssstyledimgtext/locallang_db.php:tt_content.tx_croncssstyledimgtext_renderMethod.I.ul', 'ul'),
				Array('LLL:EXT:cron_cssstyledimgtext/locallang_db.php:tt_content.tx_croncssstyledimgtext_renderMethod.I.div', 'div'),
				Array('LLL:EXT:cron_cssstyledimgtext/locallang_db.php:tt_content.tx_croncssstyledimgtext_renderMethod.I.table', 'table'),
				Array('LLL:EXT:cron_cssstyledimgtext/locallang_db.php:tt_content.tx_croncssstyledimgtext_renderMethod.I.custom', 'custom'),
			),
			'default' => 'default'
		)
	),
	'tx_croncssstyledimgtext_altText' => Array (
		'exclude' => 1,
		'label' => 'LLL:EXT:cron_cssstyledimgtext/locallang_db.php:tt_content.tx_croncssstyledimgtext_altText',
		'config' => Array (
			'type' => 'text',
			'cols' => '30',
			'rows' => '3',
		)
	),
	'tx_croncssstyledimgtext_titleText' => Array (
		'exclude' => 1,
		'label' => 'LLL:EXT:cron_cssstyledimgtext/locallang_db.php:tt_content.tx_croncssstyledimgtext_titleText',
		'config' => Array (
			'type' => 'text',
			'cols' => '30',
			'rows' => '3',
		)
	),
	'tx_croncssstyledimgtext_longText' => Array (
		'exclude' => 1,
		'label' => 'LLL:EXT:cron_cssstyledimgtext/locallang_db.php:tt_content.tx_croncssstyledimgtext_longText',
		'config' => Array (
			'type' => 'text',
			'cols' => '30',
			'rows' => '3',
		)
	),
);

t3lib_div::loadTCA('tt_content');
t3lib_extMgm::addTCAcolumns('tt_content',$tempColumns,1);
t3lib_extMgm::addToAllTCAtypes('tt_content',
	'tx_croncssstyledimgtext_altText,'.
	'tx_croncssstyledimgtext_titleText,'.
# not working yet, idea would be to have this text shown in showpic.php (click-enlarge)
#	'tx_croncssstyledimgtext_longText,'.
	'tx_croncssstyledimgtext_renderMethod;;;;1-1-1,'.
	'','textpic,image');

?>