<?php
/**
 * Language labels for database tables/fields belonging to extension 'css_styled_imgtext'
 * 
 * This file is detected by the translation tool.
 */

$LOCAL_LANG = Array (
	'default' => Array (
		'tt_content.tx_croncssstyledimgtext_renderMethod' => 'Rendering Method:',
		'tt_content.tx_croncssstyledimgtext_renderMethod.I.default' => 'Default',
		'tt_content.tx_croncssstyledimgtext_renderMethod.I.dl' => 'Definition List',
		'tt_content.tx_croncssstyledimgtext_renderMethod.I.ul' => 'Unordered List',
		'tt_content.tx_croncssstyledimgtext_renderMethod.I.div' => 'List- and Table-less',
		'tt_content.tx_croncssstyledimgtext_renderMethod.I.table' => 'Table',
		'tt_content.tx_croncssstyledimgtext_renderMethod.I.custom' => 'Custom',
		'tt_content.tx_croncssstyledimgtext_altText' => 'Alt-text:',
		'tt_content.tx_croncssstyledimgtext_titleText' => 'Title-text:',
		'tt_content.tx_croncssstyledimgtext_longText' => 'Longtext (for click-enlarge):',
	),
	'de' => Array (
		'tt_content.tx_croncssstyledimgtext_renderMethod' => 'Ausgabenart:',
		'tt_content.tx_croncssstyledimgtext_renderMethod.I.default' => 'Standard',
		'tt_content.tx_croncssstyledimgtext_renderMethod.I.dl' => 'Definitionsliste',
		'tt_content.tx_croncssstyledimgtext_renderMethod.I.ul' => 'Ungeordnete Liste',
		'tt_content.tx_croncssstyledimgtext_renderMethod.I.div' => 'Listen- und Tabellenlos',
		'tt_content.tx_croncssstyledimgtext_renderMethod.I.table' => 'Tabelle',
		'tt_content.tx_croncssstyledimgtext_renderMethod.I.custom' => 'Eigene',
		'tt_content.tx_croncssstyledimgtext_altText' => 'Alternativer Text (alt):',
		'tt_content.tx_croncssstyledimgtext_titleText' => 'Titel (title):',
		'tt_content.tx_croncssstyledimgtext_longText' => 'Langer Text (bei Klick-Vergr&ouml;ssern):',
	),
);
?>