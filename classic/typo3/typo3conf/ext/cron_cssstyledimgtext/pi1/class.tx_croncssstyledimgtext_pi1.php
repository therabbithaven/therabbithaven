<?php
/***************************************************************
*  Copyright notice
*
*  (c) 1999-2005 Kasper Skaarhoj (kasperYYYY@typo3.com)
*  (c) 2005 Ingmar Schlecht for the Typo3 GUI Team (ingmars@web.de)
*  (c) 2005 Ernesto Baschny (ernst@cron-it.de)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*  A copy is found in the textfile GPL.txt and important notices to the license
*  from the author is found in LICENSE.txt distributed with these scripts.
*
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'CSS styled IMGTEXT'
 *
 * @author	Kasper Skaarhoj <kasperYYYY@typo3.com>
 * @author	Ingmar Schlecht for the Typo3 GUI Team (ingmars@web.de)
 * @author	Ernesto Baschny (ernst@cron-it.de)
 */

require_once(t3lib_extMgm::extPath('css_styled_content').'pi1/class.tx_cssstyledcontent_pi1.php');

/**
 * Plugin class - instantiated from TypoScript.
 * Rendering the IMGTEXT content elements from tt_content table.
 *
 * @author	Kasper Skaarhoj <kasperYYYY@typo3.com>
 * @author	Ingmar Schlecht for the Typo3 GUI Team (ingmars@web.de)
 * @author	Ernesto Baschny (ernst@cron-it.de)
 */
class tx_croncssstyledimgtext_pi1 extends tx_cssstyledcontent_pi1 {

	var $classDefinitions = array(
		'0' => 'csi-center csi-above', // 'textpic-above-centre',
		'1' => 'csi-right csi-above', // 'textpic-above-right',
		'2' => 'csi-left csi-above', // 'textpic-above-left',
		'8' => 'csi-center csi-below', // 'textpic-below-centre',
		'9' => 'csi-right csi-below', // 'textpic-below-right',
		'10' => 'csi-left csi-below', // 'textpic-below-left',
		'17' => 'csi-intext-right', // 'textpic-intext-right',
		'18' => 'csi-intext-left', // 'textpic-intext-left',
		'25' => 'csi-intext-right-nowrap', // 'textpic-intext-right-nowrap',
		'26' => 'csi-intext-left-nowrap', // 'textpic-intext-left-nowrap'
	);
	var $classCaptionAlign = array(
		'center' => 'csi-caption-c',
		'right' => 'csi-caption-r',
		'left' => 'csi-caption-l',
	);

	/**
	 * Rendering the IMGTEXT content element, called from TypoScript (tt_content.textpic.20)
	 * 
	 * @param	string		Content input. Not used, ignore.
	 * @param	array		TypoScript configuration. See TSRef "IMGTEXT". This function aims to be compatible.
	 * @return	string		HTML output.
	 * @access private
	 */
	 function render_textpic($content,$conf)	{
			// Look for hook before running default code for function
		if (method_exists($this, 'hookRequest') && $hookObj = &$this->hookRequest('render_textpic'))	{
			return $hookObj->render_textpic($content,$conf);
		}

			// Choose the rendering method
		$renderMethod = $this->cObj->stdWrap($conf['renderMethod'],$conf['renderMethod.']);
		if ($renderMethod == 'default' || $renderMethod == '' || $renderMethod == '0') {
			$renderMethod = $this->cObj->stdWrap($conf['defaultRenderMethod'],$conf['defaultRenderMethod.']);
		}

			// Render using the default IMGTEXT code (table-based)
		if ($renderMethod == 'table' || t3lib_div::GPVar('old'))  {
			return $this->cObj->IMGTEXT($conf);
		}

			// Specific configuration for the chosen rendering method
		if (is_array($conf['rendering.'][$renderMethod . '.'])) {
			$conf = $this->cObj->joinTSarrays($conf, $conf['rendering.'][$renderMethod . '.']);
		}

			// Image or Text with Image?
		if (is_array($conf['text.']))	{
			$content.= $this->cObj->stdWrap($this->cObj->cObjGet($conf['text.'],'text.'),$conf['text.']);	// this gets the surrounding content
		}
		
		$imgList=trim($this->cObj->stdWrap($conf['imgList'],$conf['imgList.']));	// gets images
		if ($imgList)	{
			$imgs = t3lib_div::trimExplode(',',$imgList);
			$imgStart = intval($this->cObj->stdWrap($conf['imgStart'],$conf['imgStart.']));
			$imgCount= count($imgs)-$imgStart;
			$imgMax = intval($this->cObj->stdWrap($conf['imgMax'],$conf['imgMax.']));
			if ($imgMax)	{
				$imgCount = t3lib_div::intInRange($imgCount,0,$conf['imgMax']);	// reduces the number of images.
			}

			$imgPath = $this->cObj->stdWrap($conf['imgPath'],$conf['imgPath.']);


				// Positioning
			$position=$this->cObj->stdWrap($conf['textPos'],$conf['textPos.']);

			$tmppos = $position&7;	// 0,1,2 = center,left,right
			$contentPosition = $position&24;	// 0,8,16,24 (above,below,intext,intext-wrap)
			$align = $this->cObj->align[$tmppos];
			$txtMarg = intval($this->cObj->stdWrap($conf['textMargin'],$conf['textMargin.']));
			if (!$conf['textMargin_outOfText'] && $contentPosition<16)	{
				$txtMarg=0;
			}

				// Captions
			$caption='';
			if (is_array($conf['caption.']))	{
				$caption= $this->cObj->stdWrap($this->cObj->cObjGet($conf['caption.'], 'caption.'),$conf['caption.']);
			}
			$captionArray=array();
			if ($conf['captionSplit'])	{
				$capSplit = $this->cObj->stdWrap($conf['captionSplit.']['token'],$conf['captionSplit.']['token.']);
				if (!$capSplit) { $capSplit=chr(10); }
				$captionArray = explode($capSplit, $this->cObj->cObjGetSingle($conf['captionSplit.']['cObject'],$conf['captionSplit.']['cObject.'],'captionSplit.cObject'));
				while (list($ca_key,$ca_val) = each($captionArray))	{
					$captionArray[$ca_key] = $this->cObj->stdWrap(trim($captionArray[$ca_key]), $conf['captionSplit.']['stdWrap.']);
				}
			}
			$cap = ($caption)?1:0;
			$caption_align = $this->cObj->stdWrap($conf['captionAlign'],$conf['captionAlign.']);
				// We default to left-align if nothing was provided. 'captionAlign' is used neither
				// by content (default) nor by css_styled_content!
			//if (!$caption_align) {
			//	$caption_align = $align;
			//}

			$colspacing = intval($this->cObj->stdWrap($conf['colSpace'],$conf['colSpace.']));
			$rowspacing = intval($this->cObj->stdWrap($conf['rowSpace'],$conf['rowSpace.']));

			$border = intval($this->cObj->stdWrap($conf['border'],$conf['border.'])) ? 1:0;
			$borderColor = $this->cObj->stdWrap($conf['borderCol'],$conf['borderCol.']);
			$borderThickness = intval($this->cObj->stdWrap($conf['borderThick'],$conf['borderThick.']));

			$borderColor=$borderColor?$borderColor:'black';
			$borderThickness=$borderThickness?$borderThickness:1;
			$borderSpace = (($conf['borderSpace']&&$border) ? intval($conf['borderSpace']) : 0);

				// generate cols
			$cols = intval($this->cObj->stdWrap($conf['cols'],$conf['cols.']));
			$colCount = ($cols > 1) ? $cols : 1;
			if ($colCount > $imgCount)	{$colCount = $imgCount;}
			$rowCount = ceil($imgCount / $colCount);

				// generate rows
			$rows = intval($this->cObj->stdWrap($conf['rows'],$conf['rows.']));
			if ($rows>1)  {
				$rowCount = $rows;
				if ($rowCount > $imgCount)	{$rowCount = $imgCount;}
				$colCount = ($rowCount>1) ? ceil($imgCount / $rowCount) : $imgCount;
			}

				// max Width
			$maxW = intval($this->cObj->stdWrap($conf['maxW'],$conf['maxW.']));
			$defaultW = intval($this->cObj->stdWrap($conf['defaultW'],$conf['defaultW.']));

			if ($contentPosition>=16)	{	// in Text
				$maxWInText = intval($this->cObj->stdWrap($conf['maxWInText'],$conf['maxWInText.']));
				if (!$maxWInText)	{	// If maxWInText is not set, it's calculated to the 50% of the max...
					$maxW = round($maxW/100*50);
				} else {
					$maxW = $maxWInText;
				}
				$defaultW = intval($this->cObj->stdWrap($conf['defaultWInText'],$conf['defaultWInText.']));
			}
			if (!$defaultW)	{ $defaultW = $maxW; }

				// Explicit setting in tt_content might override the maxW, if allowed:
			if ($this->cObj->data['imagewidth'])	{
				if ($this->cObj->data['imagewidth'] <= $maxW)	{
					$maxW = $this->cObj->data['imagewidth'];
				}
			}

				// All columns have the same width:
			$defaultColumnWidth = ceil(($maxW-$colspacing*($colCount-1)-$colCount*$border*$borderThickness*2)/$colCount);

				// Specify the maximum width for each column
			$columnWidths = array();  //SPEC
			$colRelations = trim($this->cObj->stdWrap($conf['colRelations'],$conf['colRelations.']));
			if (!$colRelations)	{
					// Default 1:1:... proportion, all columns same width
				for ($a=0;$a<$colCount;$a++)	{
					$columnWidths[$a] = $defaultColumnWidth;
				}
			} else {
					// We need another proportion
				$rel_parts = explode(':',$colRelations);
				$rel_total = 0;
				for ($a=0;$a<$colCount;$a++)	{
					$rel_parts[$a] = intval($rel_parts[$a]);
					$rel_total+= $rel_parts[$a];
				}
				if ($rel_total)	{
					for ($a=0;$a<$colCount;$a++)	{
						$columnWidths[$a] = round(($defaultColumnWidth*$colCount)/$rel_total*$rel_parts[$a]);
					}
					if (min($columnWidths)<=0 || max($rel_parts)/min($rel_parts)>10)	{
						// The difference in size between the largest and smalles must be within a factor of ten.
						for ($a=0;$a<$colCount;$a++)	{
							$columnWidths[$a] = $defaultColumnWidth;
						}
					}
				}
			}

			$image_compression = intval($this->cObj->stdWrap($conf['image_compression'],$conf['image_compression.']));
			$image_effects = intval($this->cObj->stdWrap($conf['image_effects'],$conf['image_effects.']));
			$image_frames = intval($this->cObj->stdWrap($conf['image_frames.']['key'],$conf['image_frames.']['key.']));

				// fetches pictures
			$splitArr=array();
			$splitArr['imgObjNum']=$conf['imgObjNum'];
			$splitArr = $GLOBALS['TSFE']->tmpl->splitConfArray($splitArr,$imgCount);

				// EqualHeight
			$equalHeight = intval($this->cObj->stdWrap($conf['equalH'],$conf['equalH.']));
			if ($equalHeight)	{	// Initiate gifbuilder object in order to get dimensions AND calculate the imageWidth's
				$gifCreator = t3lib_div::makeInstance('tslib_gifbuilder');
				$gifCreator->init();
				$relations_cols = Array();
				for($a=0;$a<$imgCount;$a++)	{
					$imgKey = $a+$imgStart;
					$imgInfo = $gifCreator->getImageDimensions($imgPath.$imgs[$imgKey]);
					$rel = $imgInfo[1] / $equalHeight;	// relationship between the original height and the wished height
					if ($rel)	{	// if relations is zero, then the addition of this value is omitted as the image is not expected to display because of some error.
						$relations_cols[floor($a/$colCount)] += $imgInfo[0]/$rel;	// counts the total width of the row with the new height taken into consideration.
					}
				}
			}

			$imageRowsFinalWidths = Array();	// contains the width of every image row
			$imageRowsMaxHeights = Array();
			$imgsTag=array();
			$origImages=array();
			for($a=0;$a<$imgCount;$a++)	{
				$GLOBALS['TSFE']->register['IMAGE_NUM'] = $a;
				
				$imgKey = $a+$imgStart;
				$totalImagePath = $imgPath.$imgs[$imgKey];
				$this->cObj->data[$this->cObj->currentValKey] = $totalImagePath;
				$imgObjNum = intval($splitArr[$a]['imgObjNum']);
				$imgConf = $conf[$imgObjNum.'.'];

				if ($equalHeight)	{
					$scale = 1;
					$totalMaxW = $defaultColumnWidth*$colCount;
					$rowTotalMaxW = $relations_cols[floor($a/$colCount)];
					if ($rowTotalMaxW > $totalMaxW)	{
						$scale = $rowTotalMaxW / $totalMaxW;
					}

						// transfer info to the imageObject. Please note, that 
					$imgConf['file.']['height'] = round($equalHeight/$scale);

						// other stuff will be calculated accordingly:
					unset($imgConf['file.']['width']);
					unset($imgConf['file.']['maxW']);
					unset($imgConf['file.']['maxH']);
					unset($imgConf['file.']['minW']);
					unset($imgConf['file.']['minH']);
					unset($imgConf['file.']['width.']);
					unset($imgConf['file.']['maxW.']);
					unset($imgConf['file.']['maxH.']);
					unset($imgConf['file.']['minW.']);
					unset($imgConf['file.']['minH.']);
				} else {
					$imgConf['file.']['maxW'] = $columnWidths[($a%$colCount)];
				}

					// do we want the title in the surrounding <img> tag?
				$titleInLink = $this->cObj->stdWrap($imgConf['titleInLink'], $imgConf['titleInLink.']);
				$titleInLinkAndImg = $this->cObj->stdWrap($imgConf['titleInLinkAndImg'], $imgConf['titleInLinkAndImg.']);
				$oldATagParms = $GLOBALS['TSFE']->ATagParams;
					// Place the title in the <img> tag, if desired
				if ($titleInLink)	{
					if (is_array($imgConf['titleText.']))	{
						$titleText = trim($this->cObj->stdWrap($this->cObj->cObjGet($imgConf['titleText.'], 'titleText.'),$imgConf['titleText.']));
					} else {
						$titleText = trim($this->cObj->stdWrap($imgConf['titleText'], $imgConf['titleText.']));
					}
					$titletextArray = array();
					if ($imgConf['titleText.']['split'])	{
						$split = $this->cObj->stdWrap($imgConf['titleText.']['split.']['token'],$imgConf['titleText.']['split.']['token.']);
						if (!$split) { $split=chr(10); }
						$titletextArray = explode($split, $this->cObj->cObjGetSingle($imgConf['titleText.']['split.']['cObject'],$imgConf['titleText.']['split.']['cObject.'],'titleText.split.cObject'));
						while (list($alt_key, $alt_val) = each($titletextArray))	{
							$titletextArray[$alt_key] = $this->cObj->stdWrap(trim($titletextArray[$alt_key]), $imgConf['titleText.']['split.']['stdWrap.']);
						}
						$titleText = $titletextArray[$a];
					}
					if ($titleText)	{
						$GLOBALS['TSFE']->ATagParams .= ' title="'. $titleText .'"';
					}
				}

				if ($imgConf || $imgConf['file']) {
					if ($this->cObj->image_effects[$image_effects])	{
						$imgConf['file.']['params'].= ' '.$this->cObj->image_effects[$image_effects];
					}
					if ($image_frames)	{
						if (is_array($conf['image_frames.'][$image_frames.'.']))	{
							$imgConf['file.']['m.'] = $conf['image_frames.'][$image_frames.'.'];
						}
					}
					if ($image_compression && $imgConf['file']!='GIFBUILDER')	{
						if ($image_compression==1)	{
							$tempImport = $imgConf['file.']['import'];
							$tempImport_dot = $imgConf['file.']['import.'];
							unset($imgConf['file.']);
							$imgConf['file.']['import'] = $tempImport;
							$imgConf['file.']['import.'] = $tempImport_dot;
						} elseif (isset($this->cObj->image_compression[$image_compression])) {
							$imgConf['file.']['params'].= ' '.$this->cObj->image_compression[$image_compression]['params'];
							$imgConf['file.']['ext'] = $this->cObj->image_compression[$image_compression]['ext'];
							unset($imgConf['file.']['ext.']);
						}
					}
					if ($titleInLink && ! $titleInLinkAndImg) {
							// Will be added by the IMAGE call to the a-tag:
						unset($imgConf['titleText']);
						unset($imgConf['titleText.']);
						$imgConf['emptyTitleHandling'] = 'removeAttr';
					}
					$imgsTag[$imgKey] = $this->cObj->IMAGE($imgConf);
				} else {
					$imgsTag[$imgKey] = $this->cObj->IMAGE(Array('file'=>$totalImagePath)); 	// currentValKey !!!
				}
					// Restore our ATagParams
				$GLOBALS['TSFE']->ATagParams = $oldATagParms;
					// Store the original filepath
				$origImages[$imgKey]=$GLOBALS['TSFE']->lastImageInfo;

				$imageRowsFinalWidths[floor($a/$colCount)] += $GLOBALS['TSFE']->lastImageInfo[0];
				if ($GLOBALS['TSFE']->lastImageInfo[1]>$imageRowsMaxHeights[floor($a/$colCount)])	{
					$imageRowsMaxHeights[floor($a/$colCount)] = $GLOBALS['TSFE']->lastImageInfo[1];
				}
			}
				// calculating the tableWidth:
				// TableWidth problems: It creates problems if the pictures are NOT as wide as the tableWidth.
			$tableWidth = max($imageRowsFinalWidths)+ $colspacing*($colCount) + $colCount*$border*($borderSpace+$borderThickness)*2;
			foreach ($columnWidths as $a => $w) {
				$allColumnsWidth += $w + $colspacing + $border*($borderSpace+$borderThickness)*2;
			}

				// noRows is in fact just one ROW, with the amount of columns specified, where
				//     the images are placed in.
				// noCols is just one COLUMN, each images placed side by side on each row
			$noRows = $this->cObj->stdWrap($conf['noRows'],$conf['noRows.']);
			$noCols = $this->cObj->stdWrap($conf['noCols'],$conf['noCols.']);
			if ($noRows) {$noCols=0;}	// noRows overrides noCols. They cannot exist at the same time.
			if ($equalHeight) {
				#$noCols=1;
				#$noRows=0;
			}

			$rowCount_temp=1;
			$colCount_temp=$colCount;
			if ($noRows)	{
				$rowCount_temp = $rowCount;
				$rowCount = 1;
			}
			if ($noCols)	{
				$colCount = 1;
				$columnWidths = array();
			}

				// col- and rowspans calculated
			$colspan = (($colspacing) ? $colCount*2-1 : $colCount);
			$rowspan = (($rowspacing) ? $rowCount*2-1 : $rowCount) + $cap;

				// Edit icons:
			$editIconsHTML = $conf['editIcons']&&$GLOBALS['TSFE']->beUserLogin ? $this->cObj->editIcons('',$conf['editIcons'],$conf['editIcons.']) : '';

				// Render the textpic the css-styled way
			$captionClass = '';
			if ($caption_align) {
				$captionClass = $this->classCaptionAlign[$caption_align];
			}
			$borderClass = '';
			if ($border) {
				$borderClass = 'csi-border';
			}

			$noWidth = $this->cObj->stdWrap($conf['noWidth'], $conf['noWidth.']);

				// Multiple classes for all properties, which one can then format in the CSS file
			$addClasses = $this->cObj->stdWrap($conf['addClasses'], $conf['addClasses.']);
			$class = 'csi-textpic ';
			$class .= $this->classDefinitions[$position];
			$class .= ($borderClass? ' '.$borderClass:'');
			$class .= ($captionClass? ' '.$captionClass:'');
			$class .= ($equalHeight? ' csi-equalheight':'');
			$class .= ($addClasses ? ' '.$addClasses:'');
			$imgWrapAttr = 'class="csi-imagewrap"';
			$imgWrapWidth = '';
			if ($position == 0 || $position == 8) {
					// For 'center' we always need a width, else the margin:auto trick won't work
				$imgWrapWidth = $tableWidth;
			}
			if ($rowCount > 1) {
					// For multiple rows we also need a width, so that the images will wrap
				$imgWrapWidth = $tableWidth;
			}

				// If noRows, we need multiple imagecolumn wraps
			$imageWrapCols = 1;
			if ($noRows) {
				$imageWrapCols = $colCount;
			}

				// User wants to separate the rows, but only do that if we do have rows
			$separateRows = $this->cObj->stdWrap($conf['separateRows'], $conf['separateRows.']);
			if ($noRows) {
				$separateRows = 0;
			}
			if ($rowCount == 1) {
				$separateRows = 0;
			}

				// Render the images
			$images = '<div '.$imgWrapAttr.((!$noWidth&&$imgWrapWidth) ? ' style="width:'.$imgWrapWidth.'px;"' : '' ).'>';
			for ($c = 0; $c < $imageWrapCols; $c++) {
				$GLOBALS['TSFE']->register['columnwidth'] = $columnWidths[$c];
				if ($noRows) {
						// Only needed to make columns, rather than rows:
					$thisColW = $columnWidths[$c] + $colspacing + $border*($borderSpace+$borderThickness)*2;
					$images .= '<div class="csi-imagecolumn"'. (!$noWidth ? 'style="width:'.$thisColW.'px;"' : '') . '>';
				}
				$allRows = '';
				for ($i = $c; $i<count($imgsTag); $i=$i+$imageWrapCols) {
					if ($separateRows && $i%$colCount == 0) {
						$thisRow = '';
					}

						// Render one image
					$GLOBALS['TSFE']->register['imagewidth'] = $origImages[$i][0];
					$GLOBALS['TSFE']->register['imageheight'] = $origImages[$i][1];
					$GLOBALS['TSFE']->register['rowwidth'] = $tableWidth;
					$thisImage = '';
					$thisImage .= $this->cObj->stdWrap($imgsTag[$i], $conf['imgTagStdWrap.']);
					if ($editIconsHTML) {
						$thisImage .= $this->cObj->stdWrap($editIconsHTML, $conf['editIconsStdWrap.']);
					}
					if ($captionArray[$i]) {
						$thisImage .= $this->cObj->stdWrap($captionArray[$i], $conf['captionStdWrap.']);
					}
					if ($conf['netprintApplicationLink']) {
						$thisImage = $this->cObj->netprintApplication_offsiteLinkWrap($thisImage, $origImages[$i], $conf['netprintApplicationLink.']);
					}
					$thisImage = $this->cObj->stdWrap($thisImage, $conf['oneImageStdWrap.']);

					if ($separateRows) {
						$thisRow .= $thisImage;
					} else {
						$allRows .= $thisImage;
					}
					if ($separateRows && ($i%$colCount == ($colCount-1) || $i+1==count($imgsTag))) {
						// Close this row at the end (colCount), or the last row at the final end
						$allRows .= $this->cObj->stdWrap($thisRow, $conf['imageRowStdWrap.']);
					}
				}
				if ($separateRows) {
					$images .= $allRows;
				} else {
					$images .= $this->cObj->stdWrap($allRows, $conf['noRowsStdWrap.']);
				}
				if ($noRows) {
					$images .= '</div>';
				}
			}
			$images .= '</div>';

			$content = '<div class="csi-text">'.$content.'</div>';

			switch ($position)	{
				case 0:  // textpic-above-centre
				case 1:  // textpic-above-right
				case 2:  // textpic-above-left
					$output = '<div class="'.$class.'">'.$images.$content.'</div><div class="csi-clear"></div>';
				break;

				case 8:  // textpic-below-centre
				case 9:  // textpic-below-right
				case 10: // textpic-below-left
					$output = '<div class="'.$class.'">'.$content.$images.'</div><div class="csi-clear"></div>';
				break;

				case 17: // textpic-intext-right
				case 18: // textpic-intext-left
					$output = '<div class="'.$class.'">'.$images.$content.'</div>';
				break;

				case 25: // textpic-intext-right-nowrap
					$output = '<div class="'.$class.'">'.$images.'<div style="margin-right:' . ($tableWidth) . 'px;">'.$content.'</div></div><div class="csi-clear"></div>';
				break;

				case 26: // textpic-intext-left-nowrap
					$output = '<div class="'.$class.'">'.$images.'<div style="margin-left:' . ($tableWidth) . 'px;">'.$content.'</div></div><div class="csi-clear"></div>';
				break;
			}

		} else {
			$output= $content;
		}

		if ($conf['stdWrap.']) {
			$output = $this->cObj->stdWrap($output, $conf['stdWrap.']);
		}
		
		return $output;
	}


	function basicRenderer($content, $conf) {
		return "Hello";
	}
}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/cron_cssstyledimgtext/pi1/class.tx_croncssstyledimgtext_pi1.php']) {
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/cron_cssstyledimgtext/pi1/class.tx_croncssstyledimgtext_pi1.php']);
}

?>