<?php

########################################################################
# Extension Manager/Repository config file for ext: "dk_picntext"
# 
# Auto generated 22-04-2006 23:43
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'Picture and Text',
	'description' => 'This is an extension for integrating scaleable pictures (needs ImageMagick) with header and description in your website. The pluggin can be styled with css.',
	'category' => 'plugin',
	'shy' => 0,
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'TYPO3_version' => '',
	'PHP_version' => '',
	'module' => '',
	'state' => 'alpha',
	'uploadfolder' => 1,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author' => 'Daniel Klemm',
	'author_email' => 'klemm@weitblick-it.de',
	'author_company' => 'Weitblick-IT',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'version' => '0.0.3',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:14:{s:9:"ChangeLog";s:4:"fab5";s:10:"README.txt";s:4:"ee2d";s:12:"ext_icon.gif";s:4:"1bdc";s:17:"ext_localconf.php";s:4:"7015";s:14:"ext_tables.php";s:4:"a8bd";s:14:"ext_tables.sql";s:4:"8fcb";s:27:"icon_tx_dkpicntext_main.gif";s:4:"475a";s:16:"locallang_db.php";s:4:"9464";s:7:"tca.php";s:4:"5695";s:19:"doc/wizard_form.dat";s:4:"f4ea";s:20:"doc/wizard_form.html";s:4:"402e";s:31:"pi1/class.tx_dkpicntext_pi1.php";s:4:"0cf0";s:17:"pi1/locallang.php";s:4:"52ef";s:24:"pi1/static/editorcfg.txt";s:4:"168a";}',
);

?>