<?php
/**
 * Language labels for database tables/fields belonging to extension "dk_picntext"
 *
 * This file is detected by the translation tool.
 */

$LOCAL_LANG = Array (
	'default' => Array (
		'tx_dkpicntext_main' => 'Weitblick-IT -- Picture and Text',	
		'tx_dkpicntext_main.header' => 'Header',	
		'tx_dkpicntext_main.picture' => 'Picture',	
		'tx_dkpicntext_main.description' => 'Description',	
		'tx_dkpicntext_main.thumbsize' => 'Width of thumbnail',	
		'tx_dkpicntext_main.popupsize' => 'Width of Popup',	
		'tt_content.list_type_pi1' => 'Weitblick-IT -- Picture and Text',	
	),
	'de' => Array (
		'tx_dkpicntext_main' => 'Weitblick-IT -- Bild mit Text',	
		'tx_dkpicntext_main.header' => 'Kopfzeile',	
		'tx_dkpicntext_main.picture' => 'Bild',	
		'tx_dkpicntext_main.description' => 'Beschreibung',	
		'tx_dkpicntext_main.thumbsize' => 'Breite des Vorschaubildes',	
		'tx_dkpicntext_main.popupsize' => 'Breite des Popups',	
		'tt_content.list_type_pi1' => 'Weitblick-IT -- Bild mit Text',	
	),
);
?>