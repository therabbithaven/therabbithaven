<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2005 Daniel Klemm <www.weitblick-IT.de> (klemm@weitblick-it.de)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'Weitblick-IT -- Picture and Text' for the 'dk_picntext' extension.
 *
 * @author	Daniel Klemm <www.weitblick-IT.de> <klemm@weitblick-it.de>
 */


require_once(PATH_tslib.'class.tslib_pibase.php');

class tx_dkpicntext_pi1 extends tslib_pibase {
	var $prefixId = 'tx_dkpicntext_pi1';		// Same as class name
	var $scriptRelPath = 'pi1/class.tx_dkpicntext_pi1.php';	// Path to this script relative to the extension dir.
	var $extKey = 'dk_picntext';	// The extension key.
	
	/**

	 * [Put your description here]

	 */

	function main($content,$conf)	{
		$this->conf=$conf;
		$this->pi_setPiVarDefaults();
		$this->pi_loadLL();
		$this->pi_USER_INT_obj=1;	// Configuring so caching is not expected. This value means that no cHash params are ever set. We do this, because it's a USER_INT object!

		
		// 001: Database query
    		$query = "SELECT pid,header,picture,description,thumbsize,popupsize " .
             	"FROM tx_dkpicntext_main " .
             	"WHERE pid=" . $GLOBALS["TSFE"]->id . $this->cObj->enableFields("tx_dkpicntext_main") . 
					" ORDER BY sorting";

    		// 002: Execute query
    		$res = mysql(TYPO3_db, $query);   

    		// 005: Some values
    		$row = mysql_fetch_array($res);
    		//$content = "query<br/>" . $query; 

			$content = "<!-- ext: dk_picntext -- by www.weitblick-it.de -->";

   		// 006: For all database rows from the query
    		while ($row) {
        			if ($row) {
          		// 010: Create one item
               $thsize = $row["thumbsize"];
               if ($thsize < 10 && $thsize > 0) {$thsize = 10;}
               elseif ($thsize > 1000) {$thsize = 500;}
               elseif ($thsize == 0) {$thsize = 100;}
					$thumbconf = array(
   						"file" => "uploads/tx_dkpicntext/" . $row["picture"],
   						"file." => array(
     							"width" => $thsize
   						),
					);

					$thumbnail["file"] = $this->cObj->IMG_RESOURCE($thumbconf);
					$thumbnail["titleText"] = "zum Vergrößern bitte klicken";
					$thumbnail["altText"] = "zum Vergrößern bitte klicken";
               $imsize = $row["popupsize"];
               if ($imsize < 10 && $imsize > 0) {$imsize = 10;}
               elseif ($imsize > 1000) {$imsize = 1000;}
               elseif ($imsize == 0) {$imsize = 500;}
					$bigimgconf = array(
   						"file" => "uploads/tx_dkpicntext/" . $row["picture"],
   						"file." => array(
     							"width" => $imsize
   						),
					);

					$bigimage["file"] = $this->cObj->IMG_RESOURCE($bigimgconf);
							$content = $content . "<div class=\"dk_picntext_wrap\">";
          				$content = $content . "<div class=\"dk_picntext_header\">" . $row["header"] . "</div>";
							$imgpath = $bigimage["file"];
							$size = GetImageSize($imgpath);
          				$content = $content .  "<div class=\"dk_picntext_thumbnail\"><a onClick=\"window.open('" . $imgpath . "', 'Image', 'width=" . $size[0] . ",height=" . $size[1] . "')\" target=\2_blank\">" . $this->cObj->IMAGE($thumbnail) . "</a></div>";
          				//$content = $content .  "<br/><br/>picture<br>" . $this->cObj->IMAGE($row["picture"]);
          				$content = $content . "<div class=\"dk_picntext_description\">" . $row["description"] . "</div>"; 
							$content = $content . "</div>";
        			} else {
          				$content = $content . "<br/>nischt<br/>";
        			} 

        			// 013: Fetch new row from the query
        			$row = mysql_fetch_array($res);
 	    	} //while   */

		return $this->pi_wrapInBaseClass($content);
	}
}



if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/dk_picntext/pi1/class.tx_dkpicntext_pi1.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/dk_picntext/pi1/class.tx_dkpicntext_pi1.php']);
}

?>