<?php

########################################################################
# Extension Manager/Repository config file for ext: "goof_fotoboek"
# 
# Auto generated 01-08-2006 22:45
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'Photo Book',
	'description' => 'Flexible photo gallery  which  renders multilevel plain directories into scaled, watermarked (IM6+) and thumbnailed images with a frontend interface to make multilangual comments per photo and directory.',
	'category' => 'plugin',
	'shy' => 0,
	'dependencies' => 'cms',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'TYPO3_version' => '0.0.16-0.0.16',
	'PHP_version' => '0.0.16-0.0.16',
	'module' => 'mod1',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => 'tt_content',
	'clearCacheOnLoad' => 1,
	'lockType' => '',
	'author' => 'Arco van Geest',
	'author_email' => 'arco@appeltaart.mine.nu',
	'author_company' => '',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'version' => '1.7.9',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:45:{s:13:"changelog.txt";s:4:"a128";s:12:"ext_icon.gif";s:4:"18f8";s:17:"ext_localconf.php";s:4:"2b72";s:15:"ext_php_api.dat";s:4:"1b55";s:14:"ext_tables.bak";s:4:"9baf";s:14:"ext_tables.php";s:4:"7518";s:14:"ext_tables.sql";s:4:"dbde";s:28:"ext_typoscript_constants.txt";s:4:"ca43";s:28:"ext_typoscript_editorcfg.txt";s:4:"2154";s:24:"ext_typoscript_setup.txt";s:4:"9822";s:12:"fotoboek.tpl";s:4:"6a79";s:15:"fotoboekdiv.tpl";s:4:"9481";s:16:"locallang_db.php";s:4:"6f42";s:19:"doc/wizard_form.dat";s:4:"4b79";s:20:"doc/wizard_form.html";s:4:"49a2";s:33:"pi1/class.tx_gooffotoboek_pi1.php";s:4:"60e1";s:17:"pi1/locallang.php";s:4:"ccf2";s:24:"pi1/static/editorcfg.txt";s:4:"2154";s:13:"res/empty.gif";s:4:"f801";s:17:"res/index_off.gif";s:4:"4154";s:16:"res/index_on.gif";s:4:"5234";s:14:"res/navend.gif";s:4:"71dc";s:16:"res/navstart.gif";s:4:"1d32";s:16:"res/next_off.gif";s:4:"ef25";s:15:"res/next_on.gif";s:4:"cc8d";s:16:"res/prev_off.gif";s:4:"7aa1";s:15:"res/prev_on.gif";s:4:"2fc6";s:18:"res/slidestart.gif";s:4:"c6b9";s:17:"res/slidestop.gif";s:4:"a267";s:14:"res/spacer.gif";s:4:"b5c1";s:17:"res/thumb_off.gif";s:4:"6537";s:16:"res/thumb_on.gif";s:4:"9c99";s:14:"res/up_off.gif";s:4:"ee71";s:13:"res/up_on.gif";s:4:"f2db";s:27:"res/watermark_bottomimg.gif";s:4:"7959";s:32:"res/watermark_bottomimg_mask.gif";s:4:"eee6";s:22:"res/watermark_mask.gif";s:4:"da4d";s:19:"extra/fbcomment.exe";s:4:"4e9f";s:22:"lang/locallang_csh.php";s:4:"be48";s:14:"mod1/clear.gif";s:4:"cc11";s:13:"mod1/conf.php";s:4:"23db";s:14:"mod1/index.php";s:4:"5e30";s:18:"mod1/locallang.php";s:4:"5079";s:18:"mod1/locallang.xml";s:4:"3a3d";s:22:"mod1/locallang_mod.php";s:4:"2de7";}',
);

?>