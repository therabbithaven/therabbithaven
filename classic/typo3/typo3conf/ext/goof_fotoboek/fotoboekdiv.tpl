SINGLE 	= template for single picture
THUMBTPL	= template for the thumbnails	
THUMBNAIL	= section for a single thumbnail (needs to be enabled in the config)

DIRTITLE	= Name of current directory or the comment for it.
PREV		= previous button
NEXT		= next button
INDEX		= button to go to the root of the photobook
UP		= go one level up
THUMB		= thumbnails button
DIRS		= list of subdirectories or their comment
IMAGE		= the photo itself
TITLE		= first row of the photo comment file
COMMENT	= rest of the comment file
THUMBNAILS  = Thumbnail images
NAVSTART	= Start image of navigation bar
NAVEND	= End image of navigation bar
FASTNAV	= Fast navigation bar (need to be enabled in the config)
PATH_TO_ORIGINAL = relative path to the original image
PATH_TO_SINGLE = relative path to the resized image 
DIRPATH	= will show a linked Rootline Navigation like: / Folder1 / SubFolder2 / SubSubFolder3
THUMBNAIL_IMAGE
THUMBNAIL_COMMMENT_HEADER
THUMBNAIL_FILESIZE
ORIENTATION
THUMBID

<!-- ###SINGLE### start -->
<div>
###DIRTITLE###
<div>###NAVSTART######PREV######SPACER######INDEX######SPACER######UP######SPACER######THUMBS######SPACER######NEXT######SPACER######SLIDESHOW######NAVEND###</div>
###DIRS###
###IMAGE###
###TITLE###
###COMMENT###
</div>
<!-- ###SINGLE### stop -->

<!-- ###THUMBTPL### start -->
<div>###DIRTITLE###
<div>###NAVSTART######PREV######SPACER######INDEX######SPACER######UP######SPACER######NEXT######NAVEND###</div>
###DIRS###
<div class="tabel">
###THUMBNAILS###
</div>
</div>
<!-- ###THUMBTPL### stop -->

<!-- ###THUMBNAIL### start -->
<div>
	###THUMBNAIL_IMAGE###<br />
 	###THUMBNAIL_COMMMENT_HEADER###<br />
<!--	###THUMBNAIL_FILESIZE###<br />
	###THUMBNAIL_FILENAME###<br />
	###THUMBID###<br />
	###ORIENTATION###<br /> -->
</div>
<!-- ###THUMBNAIL### stop -->

<!-- ###COMBINETPL### start -->
<div>###DIRTITLE###
<div>###NAVSTART######PREV######SPACER######INDEX######SPACER######UP######SPACER######NEXT######NAVEND###</div>
###DIRS###
<div class="tabel">
###THUMBNAILS###
</div>
<div class="single">###IMAGE###
###TITLE###
###COMMENT###
</div>
</div>
<!-- ###COMBINETPL### stop -->

