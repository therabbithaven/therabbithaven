<?php

class ux_SC_alt_doc extends SC_alt_doc {

	function init() {
		global $BE_USER;
		parent::init();

		$a = explode(chr(10),$BE_USER->user['TSconfig']);
		if(is_array($a)){
			foreach ($a as $index => $key){			
				if(substr($key,0,11)=='kj_calendar'){
					$b[] = substr($key,12);	
				}
			}
			
			if(is_array($b)){
				foreach ($b as $key){
					$a = explode('=',$key);
					$data[trim($a[0])] = trim($a[1]);
					
				}
			}else{
				$data['lang'] = 'en';
				$data['style'] = 'aqua';
			}
		}
		
		if($data['style'] == 'blue'){
			$style = 'calendar-blue.css';
		} elseif ($data['style'] == 'blue2'){
			$style = 'calendar-blue2.css';
		} elseif ($data['style'] == 'green'){
			$style = 'calendar-green.css';
		} elseif ($data['style'] == 'system'){
			$style = 'calendar-system.css';
		} elseif ($data['style'] == 'tas'){
			$style = 'calendar-tas.css';
		} elseif ($data['style'] == 'win2k'){
			$style = 'calendar-win2k-2.css';
		} elseif ($data['style'] == 'win2k-cold1'){
			$style = 'calendar-win2k-cold-1.css';
		} elseif ($data['style'] == 'win2k-cold2'){
			$style = 'calendar-win2k-cold-2.css';
		} elseif ($data['style'] == 'aqua'){
			$style = 'aqua/theme.css';
		} else {
			$style = 'aqua/theme.css';
		}
		
		// Styles: blue, blue2, green, system, tas, win2k, win2k2, win2k-cold1, win2k-cold2 and aqua
		
		if($data['lang'] != 'de' AND $data['lang'] != 'en'){
			$lang = 'en';
		}else{
			$lang = $data['lang'];			
		}
		
		// import the calendar script
		$this->doc->inDocStylesArray[] = '
			@import url('.t3lib_extMgm::extRelPath('kj_becalendar').'calendar/skins/'.$style.');
		';

		$this->doc->JScode .= '
			<!-- import the calendar script -->
			 <script type="text/javascript" src="'.t3lib_extMgm::extRelPath('kj_becalendar').'calendar/calendar.js"></script>

			   <!-- the following script defines the Calendar.setup helper function, which makes
			       adding a calendar a matter of 1 or 2 lines of code. -->
			  <script type="text/javascript" src="'.t3lib_extMgm::extRelPath('kj_becalendar').'calendar/calendar-setup.js"></script>
  
			<!-- import the language module -->
			<script type="text/javascript" src="'.t3lib_extMgm::extRelPath('kj_becalendar').'calendar/lang/calendar-'.$lang.'.js"></script>

			<!-- helper script that uses the calendar -->
			<script type="text/javascript">
				
				var oldLink = null;
				
				// code to change the active stylesheet
				function setActiveStyleSheet(link, title) {
				  var i, a, main;
				  for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {
				    if(a.getAttribute("rel").indexOf("style") != -1 && a.getAttribute("title")) {
				      a.disabled = true;
				      if(a.getAttribute("title") == title) a.disabled = false;
				    }
				  }
				  if (oldLink) oldLink.style.fontWeight = \'normal\';
				  oldLink = link;
				  link.style.fontWeight = \'bold\';
				  return false;
				}
				// This function gets called when the end-user clicks on some date.
				function selected(cal, date) {
				  cal.sel.value = date; // just update the date in the input field.
				  if (cal.dateClicked && (cal.sel.id == "sel1" || cal.sel.id == "sel3"))
				    // if we add this call we close the calendar on single-click.
				    // just to exemplify both cases, we are using this only for the 1st
				    // and the 3rd field, while 2nd and 4th will still require double-click.
				    cal.callCloseHandler();
				}
				
				// And this gets called when the end-user clicks on the _selected_ date,
				// or clicks on the "Close" button.  It just hides the calendar without
				// destroying it.
				function closeHandler(cal) {
				  cal.hide();                        // hide the calendar
				//  cal.destroy();
				  _dynarch_popupCalendar = null;
				}
				
				// This function shows the calendar under the element having the given id.
				// It takes care of catching "mousedown" signals on document and hiding the
				// calendar if the click was outside.
				function showCalendar(id, format, showsTime, showsOtherMonths) {
				  var el = document.getElementById(id);
				  if (_dynarch_popupCalendar != null) {
				    // we already have some calendar created
				    _dynarch_popupCalendar.hide();                 // so we hide it first.
				  } else {
				    // first-time call, create the calendar.
				    var cal = new Calendar(1, null, selected, closeHandler);
				    // uncomment the following line to hide the week numbers
				    // cal.weekNumbers = false;
				    if (typeof showsTime == "string") {
				      cal.showsTime = true;
				      cal.time24 = (showsTime == "24");
				    }
				    if (showsOtherMonths) {
				      cal.showsOtherMonths = true;
				    }
				    _dynarch_popupCalendar = cal;                  // remember it in the global var
				    cal.setRange(1900, 2070);        // min/max year allowed.
				    cal.create();
				  }
				  _dynarch_popupCalendar.setDateFormat(format);    // set the specified date format
				  _dynarch_popupCalendar.parseDate(el.value);      // try to parse the text in field
				  _dynarch_popupCalendar.sel = el;                 // inform it what input field we use
				
				  // the reference element that we pass to showAtElement is the button that
				  // triggers the calendar.  In this example we align the calendar bottom-right
				  // to the button.
				  _dynarch_popupCalendar.showAtElement(el.nextSibling, "Br");        // show the calendar
				
				  return false;
				}
				
				var MINUTE = 60 * 1000;
				var HOUR = 60 * MINUTE;
				var DAY = 24 * HOUR;
				var WEEK = 7 * DAY;
				
				
				</script>
		';
	}
}

?>
