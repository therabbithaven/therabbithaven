<?php

########################################################################
# Extension Manager/Repository config file for ext: "kj_imagelightbox"
# 
# Auto generated 22-04-2006 23:45
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'KJ: Image Lightbox',
	'description' => 'Extends the tt_content types image and image with text configuration with a checkbox. if this checkbox is activated, you ll gonna see a nice feature in the frontend. Also if you click on this picture, it will open in a Java Script window and the whole background will be in a grey colour.',
	'category' => 'fe',
	'shy' => 0,
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'TYPO3_version' => '',
	'PHP_version' => '',
	'module' => '',
	'state' => 'alpha',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author' => 'Julian Kleinhans',
	'author_email' => 'typo3@kj187.de',
	'author_company' => '',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'version' => '1.0.0',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:17:{s:23:"class.ux_sc_alt_doc.php";s:4:"6d69";s:26:"class.ux_tslib_content.php";s:4:"e547";s:12:"ext_icon.gif";s:4:"1740";s:17:"ext_localconf.php";s:4:"57f5";s:14:"ext_tables.php";s:4:"0f8d";s:14:"ext_tables.sql";s:4:"2c53";s:16:"locallang_db.php";s:4:"d699";s:18:"Lightbox/close.gif";s:4:"4c78";s:21:"Lightbox/lightbox.css";s:4:"0903";s:20:"Lightbox/lightbox.js";s:4:"251d";s:20:"Lightbox/loading.gif";s:4:"c77a";s:21:"Lightbox/loading2.gif";s:4:"0ed5";s:21:"Lightbox/loading3.gif";s:4:"d505";s:20:"Lightbox/overlay.png";s:4:"143d";s:14:"doc/manual.sxw";s:4:"3e3e";s:19:"doc/wizard_form.dat";s:4:"df25";s:20:"doc/wizard_form.html";s:4:"963d";}',
);

?>