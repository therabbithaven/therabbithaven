<?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');
$tempColumns = Array (
	"tx_kjimagelightbox_imagelightbox" => Array (		
		"exclude" => 1,		
		"label" => "LLL:EXT:kj_imagelightbox/locallang_db.php:tt_content.tx_kjimagelightbox_imagelightbox",		
		"config" => Array (
			"type" => "check",
		)
	),
);


t3lib_div::loadTCA("tt_content");
t3lib_extMgm::addTCAcolumns("tt_content",$tempColumns,1);
$GLOBALS['TCA']['tt_content']['palettes']['7']['showitem'] = 'image_link, image_zoom, tx_kjimagelightbox_imagelightbox';
?>