<?php
/**
 * Language labels for database tables/fields belonging to extension "kj_imagelightbox"
 *
 * This file is detected by the translation tool.
 */

$LOCAL_LANG = Array (
	'default' => Array (
		'tt_content.tx_kjimagelightbox_imagelightbox' => 'or Activate Imagelightbox',	
	),
	'de' => Array (
		'tt_content.tx_kjimagelightbox_imagelightbox' => 'oder aktiviere die Imagelightbox',	
	),
);
?>