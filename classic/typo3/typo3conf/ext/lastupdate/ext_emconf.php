<?php

########################################################################
# Extension Manager/Repository config file for ext: "lastupdate"
# 
# Auto generated 03-02-2006 23:07
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'Last update',
	'description' => 'Customize Last update function by selecting multiple tables from DB.',
	'category' => 'plugin',
	'shy' => 0,
	'dependencies' => 'cms',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'TYPO3_version' => '0.0.5-0.0.5',
	'PHP_version' => '0.0.5-0.0.5',
	'module' => '',
	'state' => 'stable',
	'internal' => 0,
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 1,
	'lockType' => '',
	'author' => 'Peter Foerger',
	'author_email' => 'p.foerger@vke-design.de',
	'author_company' => '',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'private' => 0,
	'download_password' => '',
	'version' => '3.1.7',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:18:{s:12:"ext_icon.gif";s:4:"7450";s:17:"ext_localconf.php";s:4:"a93c";s:15:"ext_php_api.dat";s:4:"d9cc";s:14:"ext_tables.php";s:4:"200f";s:14:"ext_tables.sql";s:4:"132f";s:28:"ext_typoscript_constants.txt";s:4:"e115";s:24:"ext_typoscript_setup.txt";s:4:"03bc";s:15:"flexform_ds.xml";s:4:"9920";s:29:"icon_tx_lastupdate_update.gif";s:4:"7450";s:17:"locallang_tca.php";s:4:"a561";s:17:"locallang_tca.xml";s:4:"31c7";s:7:"tca.php";s:4:"3e29";s:31:"pi1/class.tx_lastupdate_pi1.php";s:4:"b8e3";s:25:"pi1/class.user_tables.php";s:4:"9849";s:17:"pi1/locallang.php";s:4:"89cb";s:24:"pi1/static/editorcfg.txt";s:4:"203a";s:19:"doc/wizard_form.dat";s:4:"fe59";s:20:"doc/wizard_form.html";s:4:"fd6e";}',
);

?>