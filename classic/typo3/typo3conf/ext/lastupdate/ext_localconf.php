<?php
if (!defined ("TYPO3_MODE")) 	die ("Access denied.");
t3lib_extMgm::addUserTSConfig('
	options.saveDocNew.tx_lastupdate_custom=1
');

  ## Extending TypoScript from static template uid=43 to set up userdefined tag:
t3lib_extMgm::addTypoScript($_EXTKEY,"editorcfg","
	tt_content.CSS_editor.ch.tx_lastupdate_pi1 = < plugin.tx_lastupdate_pi1.CSS_editor
",43);
t3lib_extMgm::addPItoST43($_EXTKEY,"pi1/class.tx_lastupdate_pi1.php","_pi1","list_type",1);

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS'][$_EXTKEY.'/pi1/class.user_tables.php']) {
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS'][$_EXTKEY.'/pi1/class.user_tables.php']);
}
?>