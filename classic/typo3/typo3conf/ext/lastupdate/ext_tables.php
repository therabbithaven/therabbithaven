<?php
if (!defined ("TYPO3_MODE")) 	die ("Access denied.");

t3lib_extMgm::allowTableOnStandardPages("tx_lastupdate_custom");


t3lib_extMgm::addToInsertRecords("tx_lastupdate_custom");

$TCA["tx_lastupdate_custom"] = Array (
	"ctrl" => Array (
		"title" => "LLL:EXT:lastupdate/locallang_tca.php:tx_lastupdate_custom",		
		"label" => "uid",	
		"tstamp" => "tstamp",
		"crdate" => "crdate",
		"cruser_id" => "cruser_id",
		"default_sortby" => "ORDER BY crdate",	
		"delete" => "deleted",
		'prependAtCopy' => 'LLL:EXT:lang/locallang_general.php:LGL.prependAtCopy',
		
		'versioning' => TRUE,
		
		'copyAfterDuplFields' => 'sys_language_uid',
		'useColumnsForDefaultValues' => 'sys_language_uid',
		'transOrigPointerField' => 'l18n_parent',
		'transOrigDiffSourceField' => 'l18n_diffsource',
		'languageField' => 'sys_language_uid',
			
		"enablecolumns" => Array (		
			"disabled" => "hidden",
		),
		"dynamicConfigFile" => t3lib_extMgm::extPath($_EXTKEY)."tca.php",
		"iconfile" => t3lib_extMgm::extRelPath($_EXTKEY)."icon_tx_lastupdate_update.gif",
	),
	"feInterface" => Array (
		"fe_admin_fieldList" => "hidden, author_prefix, date_prefix, date_format, separate",
	)
);

t3lib_div::loadTCA("tt_content");

$TCA["tt_content"]["types"]["list"]["subtypes_excludelist"][$_EXTKEY."_pi1"]="layout,select_key,pages,recursive";
$TCA['tt_content']['types']['list']['subtypes_addlist'][$_EXTKEY.'_pi1']='pi_flexform';

t3lib_extMgm::addPlugin(Array("LLL:EXT:lastupdate/locallang_tca.php:tt_content.list_type_pi1", $_EXTKEY."_pi1"),"list_type");
t3lib_extMgm::addPiFlexFormValue($_EXTKEY.'_pi1', 'FILE:EXT:lastupdate/flexform_ds.xml');

t3lib_extMgm::addStaticFile($_EXTKEY,"pi1/static/","Last update");
?>