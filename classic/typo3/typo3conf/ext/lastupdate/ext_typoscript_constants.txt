

plugin.tx_lastupdate_pi1 {

    # cat=plugin.lastupdate//1; type=int+; label= General record storage page. Set only if you use an other sysFolder than the GRSP.
 storage = 

    # cat=plugin.lastupdate//1; type=int+; label= Display author/email: possible values are: 1 = author and date, 2 = author name only, 3 = date only.
  author_email = 1

    # cat=plugin.lastupdate//5; type=int+; label= Limit to updates from: possible values are: 1 = whole site, 2 = current page.
 pid_item = 1

    # cat=plugin.lastupdate//6; type=int+; label= Sequence Date-Author: possible values are: 1 = date first, 2 = authorname first.
 sequence = 1

}