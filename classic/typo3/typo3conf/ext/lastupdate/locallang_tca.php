<?php

$LOCAL_LANG = Array (
	'default' => Array (
		'tx_lastupdate_custom' => 'Last update',
		'tx_lastupdate_custom.author_prefix' => 'Prefix Author:',
		'tx_lastupdate_custom.date_prefix' => 'Prefix date:',
		'tx_lastupdate_custom.date_format' => 'Date format:',
		'tx_lastupdate_custom.separate' => 'Separator Date-Author:',
		'tt_content.list_type_pi1' => 'Last update',
		'tt_content.pi_flexform.sheet_general' => 'General Settings',
		'tt_content.pi_flexform.more_control' => 'More Control',
		'tt_content.pi_flexform.author_email' => 'Display author-date:',
		'tt_content.pi_flexform.author_email_1' => 'Author name and Date',
		'tt_content.pi_flexform.author_email_2' => 'Author name only',
		'tt_content.pi_flexform.author_email_3' => 'Date only',
		'tt_content.pi_flexform.page_author' => 'Fetch author name from page header:',
		'tt_content.pi_flexform.pid_item' => 'Limit to updates from:',
		'tt_content.pi_flexform.pid_item_1' => 'Whole Site',
		'tt_content.pi_flexform.pid_item_2' => 'Current page',
		'tt_content.pi_flexform.date_format' => 'Date format:',
		'tt_content.pi_flexform.separator' => 'Separator Date-Author:',
		'tt_content.pi_flexform.sequence' => 'Sequence:',
		'tt_content.pi_flexform.sequence_1' => 'Date-Author',
		'tt_content.pi_flexform.sequence_2' => 'Author-Date',
		'tt_content.pi_flexform.no_link' => 'No Mail Link',
		'tt_content.pi_flexform.display_link' => 'Show email address',
		'tt_content.pi_flexform.preferred_date' => 'Preferred date representation for the current locale',
		'tt_content.pi_flexform.date_time' => 'Date and time representation for the current locale ',
		'tt_content.pi_flexform.whole_site' => 'Whole site (ignore the selection above)',
		'tt_content.pi_flexform.storage_page' => 'Record storage page',
		'tt_content.pi_flexform.excludeSelection' => 'Exclude content from table: ',
	),
	'dk' => Array (
	),
	'de' => Array (
		'tx_lastupdate_custom' => 'Last update',
		'tt_content.list_type_pi1' => 'Last update',
		'tt_content.pi_flexform.sheet_general' => 'Generelle Einstellungen',
		'tt_content.pi_flexform.more_control' => 'Erweiterte Einstellungen',
		'tt_content.pi_flexform.author_email' => 'Anzeige Autor/Datum:',
		'tt_content.pi_flexform.author_email_1' => 'Autor und Datum',
		'tt_content.pi_flexform.author_email_2' => 'Autor',
		'tt_content.pi_flexform.author_email_3' => 'Datum',
		'tt_content.pi_flexform.page_author' => 'Autor wie im Seitentitel angegeben:',
		'tt_content.pi_flexform.pid_item' => 'Updates von:',
		'tt_content.pi_flexform.pid_item_1' => 'gesamter Website',
		'tt_content.pi_flexform.pid_item_2' => 'aktueller Seite',
		'tt_content.pi_flexform.date_format' => 'Datumsformat:',
		'tt_content.pi_flexform.separator' => 'Separator Datum-Autor:',
		'tt_content.pi_flexform.sequence' => 'Reihenfolge:',
		'tt_content.pi_flexform.sequence_1' => 'Datum-Autor',
		'tt_content.pi_flexform.sequence_2' => 'Autor-Datum',
		'tt_content.pi_flexform.no_link' => 'Kein Maillink',
		'tt_content.pi_flexform.display_link' => 'Mail Link anzeigen',
		'tt_content.pi_flexform.preferred_date' => 'Bevorzugte Zeitwiedergabe, abh�ngig von der gesetzten Umgebung',
		'tt_content.pi_flexform.date_time' => 'Datum und Zeit, abh�ngig von der gesetzten Umgebung',
	),
	'no' => Array (
	),
	'it' => Array (
	),
	'fr' => Array (
	),
	'es' => Array (
	),
	'nl' => Array (
	),
	'cz' => Array (
	),
	'pl' => Array (
	),
	'si' => Array (
	),
	'fi' => Array (
		'tx_lastupdate_custom' => 'Viime p�ivitys',
		'tx_lastupdate_custom.author_prefix' => 'Kirjoittajan etuliite:',
		'tx_lastupdate_custom.date_prefix' => 'P�iv�yksen etuliite:',
		'tx_lastupdate_custom.date_format' => 'P�iv�yksen muoto:',
		'tx_lastupdate_custom.separate' => 'P�iv�yksen-Kirjoittajan v�limerkki:',
		'tt_content.list_type_pi1' => 'Viimeinen  p�ivitys',
		'tt_content.pi_flexform.sheet_general' => 'Yleiset asetukset',
		'tt_content.pi_flexform.more_control' => 'Lis�kontrolli',
		'tt_content.pi_flexform.author_email' => 'N�yt� kirjoittajan nimi/email:',
		'tt_content.pi_flexform.author_email_1' => 'Kirjoittajan nimi (liitettyn� email osoite)',
		'tt_content.pi_flexform.author_email_2' => 'Ainoastaan kirjoittajan nimi',
		'tt_content.pi_flexform.author_email_3' => 'ei mit��n',
		'tt_content.pi_flexform.page_author' => 'Hae kirjoittajan nimi sivun otsikosta:',
		'tt_content.pi_flexform.pid_item' => 'Rajoita p�ivitykset :',
		'tt_content.pi_flexform.pid_item_1' => 'Koko sivusto',
		'tt_content.pi_flexform.pid_item_2' => 'T�m� sivu',
		'tt_content.pi_flexform.date_format' => 'P�iv�yksen muoto:',
		'tt_content.pi_flexform.separator' => 'P�iv�yksen-Kirjoittajan v�limerkki:',
		'tt_content.pi_flexform.sequence' => 'J�rjestys:',
		'tt_content.pi_flexform.sequence_1' => 'P�iv�ys-Kirjoittaja:',
		'tt_content.pi_flexform.sequence_2' => 'Kirjoittaja-P�iv�ys:',
		'tt_content.pi_flexform.no_link' => 'ei s�hk�postilinkki�',
		'tt_content.pi_flexform.display_link' => 'N�yt� email linkki',
		'tt_content.pi_flexform.preferred_date' => 'Haluttu p�iv�yksen esitysmuoto nykyiselle locale arvolle',
		'tt_content.pi_flexform.date_time' => 'P�iv�yksen ja ajan esitysmuoto nykyiselle locale arvolle',
		'tt_content.pi_flexform.whole_site' => 'Koko sivussto (ohita yll�oleva valinta)',
		'tt_content.pi_flexform.storage_page' => 'Tietojen tallennus sivu',
		'tt_content.pi_flexform.excludeSelection' => 'Sulje pois sis�lt� taulusta:',
	),
	'tr' => Array (
		'tt_content.list_type_pi1' => 'Son G�ncelleme',
		'tt_content.pi_flexform.sheet_general' => 'Genel ayarlar',
		'tt_content.pi_flexform.author_email' => 'Yazar�n ad�/email g�r�nt�lenmesi:',
		'tt_content.pi_flexform.author_email_1' => 'Yazar ad� (email adresiyle)',
		'tt_content.pi_flexform.author_email_2' => 'Yazar ad�',
		'tt_content.pi_flexform.author_email_3' => 'none',
		'tt_content.pi_flexform.pid_item' => 'G�ncellemerden s�n�rlama',
		'tt_content.pi_flexform.pid_item_1' => 'T�m site',
		'tt_content.pi_flexform.pid_item_2' => 'G�ncel sayfa',
		'tt_content.pi_flexform.date_format' => 'Tarih Format�:',
	),
	'se' => Array (
	),
	'pt' => Array (
	),
	'ru' => Array (
		'tt_content.list_type_pi1' => '��������� ����������',
		'tt_content.pi_flexform.sheet_general' => '����� ���������',
		'tt_content.pi_flexform.more_control' => '������ ��������',
		'tt_content.pi_flexform.author_email' => '���������� ������/����:',
		'tt_content.pi_flexform.author_email_1' => '��� ������ � ����',
		'tt_content.pi_flexform.author_email_2' => '������ ��� ������',
		'tt_content.pi_flexform.author_email_3' => '������ ����',
		'tt_content.pi_flexform.page_author' => '��������� ��� ������ �� ��������� ��������:',
		'tt_content.pi_flexform.pid_item' => '���������� ���������� �:',
		'tt_content.pi_flexform.pid_item_1' => '���� ����',
		'tt_content.pi_flexform.pid_item_2' => '������� ��������',
		'tt_content.pi_flexform.date_format' => '������ ����',
		'tt_content.pi_flexform.separator' => '����������� ����/�����:',
		'tt_content.pi_flexform.sequence' => '������������������:',
		'tt_content.pi_flexform.sequence_1' => '����/�����',
		'tt_content.pi_flexform.sequence_2' => '�����/����',
		'tt_content.pi_flexform.no_link' => '�� ���������� ���� �� e-mail',
		'tt_content.pi_flexform.display_link' => '���������� ���� �� e-mail',
	),
	'ro' => Array (
	),
	'ch' => Array (
	),
	'sk' => Array (
	),
	'lt' => Array (
	),
	'is' => Array (
	),
	'hr' => Array (
	),
	'hu' => Array (
	),
	'gl' => Array (
	),
	'th' => Array (
	),
	'gr' => Array (
	),
	'hk' => Array (
	),
	'eu' => Array (
	),
	'bg' => Array (
	),
	'br' => Array (
	),
	'et' => Array (
	),
	'ar' => Array (
	),
	'he' => Array (
	),
	'ua' => Array (
	),
	'lv' => Array (
	),
	'jp' => Array (
	),
	'vn' => Array (
	),
	'ca' => Array (
	),
	'ba' => Array (
	),
	'kr' => Array (
	),
	'eo' => Array (
	),
	'my' => Array (
	),
	'hi' => Array (
	),
);
?>