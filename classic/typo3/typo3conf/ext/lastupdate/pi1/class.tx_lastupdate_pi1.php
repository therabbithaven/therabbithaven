<?php
		/***************************************************************
		*  Copyright notice
		*
		*  (c) 2004 Peter Foerger (p.foerger@vke-design.de)
		*  All rights reserved
		*
		*  This script is part of the TYPO3 project. The TYPO3 project is
		*  free software; you can redistribute it and/or modify
		*  it under the terms of the GNU General Public License as published by
		*  the Free Software Foundation; either version 2 of the License, or
		*  (at your option) any later version.
		*
		*  The GNU General Public License can be found at
		*  http://www.gnu.org/copyleft/gpl.html.
		*
		*  This script is distributed in the hope that it will be useful,
		*  but WITHOUT ANY WARRANTY; without even the implied warranty of
		*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		*  GNU General Public License for more details.
		*
		*  This copyright notice MUST APPEAR in all copies of the script!
		***************************************************************/
		/**

		* Plugin 'Last update' for the 'lastupdate' extension.
		*
		* @author Peter Foerger <p.foerger@vke-design.de>
		*/
	
		/**
	 * [CLASS/FUNCTION INDEX of SCRIPT]
	 *
	 *
	 *
	 *   55: class tx_lastupdate_pi1 extends tslib_pibase
	 *   71:     function query_syslog()
	 *   92:     function check_hiddenRecords()
	 *  132:     function get_Author()
	 *  149:     function page_Author()
	 *  178:     function init($conf)
	 *  300:     function setDate($tstamp)
	 *  315:     function mailLink($author, $email)
	 *  342:     function main($content, $conf)
	 *
	 * TOTAL FUNCTIONS: 8
	 * (This index is automatically created/updated by the extension "extdeveval")
	 *
	 */
	
	
		require_once(PATH_tslib.'class.tslib_pibase.php');
	
	
	
		class tx_lastupdate_pi1 extends tslib_pibase {
			var $prefixId = 'tx_lastupdate_pi1';
			// Same as class name
			var $scriptRelPath = 'pi1/class.tx_lastupdate_pi1.php'; // Path to this script relative to the extension dir.
			var $extKey = 'lastupdate'; // The extension key.
			var $cObj;
			var $conf;
			var $tstamp;
			var $spaceAuthor = ' ';
			var $spaceDate = ' ';

		/**
	 * Check TCA for tablenames starting with 't*' like tt_content, tt_news, tx_foo.
	 *
	 * @return	array
	 */
			function tableNames() {
			$tmpArr  = array() ;
				$newArr = array();
				$arr = array();
				
				$tables = array_keys($GLOBALS['TCA']);
					foreach ($tables as $val){
					eregi('^t.*',$val,$tmpArr);
					$newArr = array_unique(array_merge($tmpArr, $newArr));
					}
					$excludeTables = explode(',',$this->config['exclude']);
					$newArr = array_diff($newArr,$excludeTables);
					
					foreach ($newArr as $val){
					$inString[] = str_replace($val,'\''.$val.'\'',$val);
					}
				$inString = implode(',',$inString);
			return $inString;
			
			}
			
			
			
			/**
		 * Query sys_log
		 * Thanks to Morten Hansen for his performance proposals
		 * @return array
		 */
		  function query_syslog($limit) {
		
		   $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
		    'sys_log.event_pid, sys_log.recuid, sys_log.tablename, sys_log.userid, sys_log.tstamp, be_users.realName, be_users.email',
		    'sys_log, be_users',
		    'sys_log.tablename IN ('.$this->tableNames().') AND sys_log.details_nr != 0 AND sys_log.userid = be_users.uid',
		    '',
		    'tstamp DESC',
		    $limit
		    
		   );
		  
			$numberOfResults = $GLOBALS['TYPO3_DB']->sql_num_rows($res);
			
		    $lastConditions = "";
		    $newConditions = "";
		    while ($row = mysql_fetch_assoc($res)) {
		       $newConditions = $row['tablename'].'.uid ='.$row['recuid'].' AND '.$row['tablename'].$this->config['pid'].$this->cObj->enableFields($row['tablename']);
		   
		       if($newConditions!=$lastConditions) {
		
		         $tmpRes = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
		          '*',
		          $row['tablename'],
		          $newConditions,
		          '',
		          '',
		          '1'
		          );
		          
		
		         if($GLOBALS['TYPO3_DB']->sql_num_rows($tmpRes)>0){;
		           $arr[] = $row;
		           
		           break;
		         } else unset($arr);
		
		         $lastConditions = $newConditions;
		
		      }
		    }
		
		    if($numberOfResults<1000 && !is_array($arr)) {
		      // we didnt find anything in syslog, so we are showing when the page was created (or last timestamped)
		      $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery('pages.tstamp', 'pages', 'uid = '.$GLOBALS['TSFE']->id, '', '', '');
		      $row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
		
		      // Default values for name and email
		      $row["realName"] = "Not Availiable";
		      $row["email"] = "";
		
		      $arr[] = $row;
		
		    }
		
		    return $arr;
		  } 
	
	
			function get_Author() {
				for ($i=0; ; $i+=1000){
					$arr = $this->query_syslog(''.$i.',1000');
					if (is_array($arr)){
					break;
					}
				}
				
				return $arr;
			}
	
			/**
	 * Get the author's name and mail address from page header
	 *
	 * @return	array
	 */
			function page_Author() {
				
				$arr = array();
				$res = $GLOBALS['TYPO3_DB']->exec_SELECTquery('pages.author, pages.author_email', 'pages', 'uid = '.$GLOBALS['TSFE']->id.' and author !=""', '', '', '1');
				$row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
	
				if (is_array($row)) {
							$arr[] = $row;
										
				}
				else
					{
					$arr = 'No Author specified in page header'.':';
				}
	
				return $arr;
			}
	
			/**
	 * Init function. Configuration values are stored in variables.
	 * Values can be added  by TS or FF with priority on FF.
	 *
	 * @param	array		$conf: configuration array from TS
	 * @return	void
	 */
			function init($conf) {
				$this->conf = $conf;
				$this->pi_setPiVarDefaults();
				$this->pi_loadLL();

				$this->pi_initPIflexForm();
				$this->tableNames();
				
				if (!$this->conf['storage']){
				$storagePid = $GLOBALS['TSFE']->getStorageSiterootPids();
				$this->conf['storage'] = $storagePid['_STORAGE_PID'];
				}
				
				$this->sys_language_uid = $GLOBALS['TSFE']->config['config']['sys_language_uid'] ? $GLOBALS['TSFE']->config['config']['sys_language_uid'] : '0';
				$this->config['storage'] = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'storage_page', 'sDEF');
				$this->config['storage']= ($this->config['storage']?$this->config['storage']:$this->conf['storage']);
				
				$res = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
											'*', 
											'tx_lastupdate_custom', 
											'pid='.$this->config['storage'].
											$this->cObj->enableFields('tx_lastupdate_custom'),
											'',
											'',
											'1'
											);
										
			if(!$GLOBALS['TYPO3_DB']->sql_num_rows($res)){echo 'No records found in storage folder!';}else{
			while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
				if ($GLOBALS['TSFE']->sys_language_content){
					$row = $GLOBALS['TSFE']->sys_page->getRecordOverlay('tx_lastupdate_custom', $row,$GLOBALS['TSFE']->sys_language_content, $GLOBALS['TSFE']->sys_language_contentOL, '');
	
				}
				
				// prefix author name
				$prefixAuthor = $row ['author_prefix'];
				if (!$prefixAuthor) {
					$this->spaceAuthor = '';
					}
				$this->config['prefixAuthor'] = $prefixAuthor;
				
				// prefix date
				$prefix = $row['date_prefix'];
				if (!$prefix) {
					$this->spaceDate = '';
				}
				$this->config['prefix'] = $prefix;
				
				// get date format
				$date = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'date_format', 'sDEF');
	
				switch ($date) {
					
					case '0':
					$date = '%x';//date
					break;
					
					case '1':
					$date = '%c'; //date and time
					break;
	
				
				}
				$customDate = $row['date_format'];
				$date = ($customDate?$customDate:$date);
				$this->config['date'] = $date;
				
				// get separator
				$separator = $row['separate'];
				if (!$separator) {
					$this->config['separate'] = ' ';
				} else {
					$this->config['separate'] = $separator;
				}
	
			}
				
				$author = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'author_email', 'sDEF');
				switch ($author) {
	
					case '1':
					$author = 'author_date';
					break;
	
					case '2':
					$author = 'author_only';
					break;
	
					case '3':
					$author = 'date_only';
					break;
				}
	
				$this->config['author'] = ($author?$author:$this->conf['author_email']);
	
				$pageAuthor = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'page_author', 'sDEF');
				$this->config['page_author'] = ($pageAuthor?$pageAuthor:$this->conf['page_author']);
	
				// get sequence
				$sequence = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'sequence', 'sDEF');
				switch ($sequence) {
	
					case '1':
					$sequence = 'date';
					break;
	
					case '2':
					$sequence = 'author';
					break;
				}
				$this->config['sequence'] = ($sequence?$sequence:$this->conf['sequence']);
				
				// whole site or current page
				$site = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'whole_site', 'sDEF');
				$this->config['site'] = ($site?$site:$this->conf['pid_item']);
				
				if ($this->config['site']== 1) {
					$this->config['pid']= '.pid > 0';//site
				}
				
				else {
							// pid_list
							$pid_list = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'pages', 'sDEF');
							$pid_list = $pid_list?$pid_list:trim($this->cObj->stdWrap($this->conf['pid_list'], $this->conf['pid_list.']));
							$pid_list = $pid_list ? implode(t3lib_div::intExplode(',', $pid_list), ','):$GLOBALS['TSFE']->id;
					
							$recursive = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'recursive', 'sDEF');
							$recursive = is_numeric($recursive)?$recursive:$this->cObj->stdWrap($conf['recursive'], $conf['recursive.']);
							// recursive level
							$this->pid_list = $this->pi_getPidList($pid_list, $recursive);
							$this->pid_list = $this->pid_list?$this->pid_list:0;
							
							$this->config['pid']= '.pid IN ('.$this->pid_list.')';
				}
	
	
				
	
				//display email address
				$displayLink = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'display_link', 'sDEF');
				$this->config['display_link'] = ($displayLink?$displayLink:$this->conf['display_link']);
	
				//no mail link
				$noLink = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'no_link', 'sDEF');
				$this->config['no_link'] = ($noLink?$noLink:$this->conf['no_link']);
				
				//exclude selection
				$exclude = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'excludeSelection', 'sDEF');
				$this->config['exclude']= ($exclude?$exclude:$this->conf['excludeSelection']);
			
			}
			}	
	
			/**
	 * date - humanreadable
	 *
	 * @param	integer		timestamp
	 * @return	string		date of lastupdate
	 */
			function setDate($tstamp) {
				
				$output = strftime($this->config['date'], $tstamp);
				return $output;
	
			}
			/**
	 * make mailLink
	 *
	 * if 'config.spamProtectEmailAddresses' is set maillink will be encrypted
	 *
	 * @param	string		$author: ...
	 * @param	string		$email: ...
	 * @return	string
	 */
			function mailLink($author, $email) {
				if ($email && !$this->config['no_link']) {
					if ($GLOBALS['TSFE']->spamProtectEmailAddresses) {
						$mailToUrl = "javascript:linkTo_UnCryptMailto('".$GLOBALS['TSFE']->encryptEmail('mailto:'.$email)."');";
						if ($this->config['display_link']) {
							$atLabel = trim($GLOBALS['TSFE']->config['config']['spamProtectEmailAddresses_atSubst']);
							$email = str_replace('@', $atLabel ? $atLabel : '(at)', $email);
							$author = $author.' <a href="'.htmlspecialchars($mailToUrl).'"'.'>&lt;'.$email.'&gt;</a>';
						} else {
							$author = '<a href="'.htmlspecialchars($mailToUrl).'"'.'>'.$author.'</a>';
						}
					} else {
						
						$author = '<a href="mailto:'.htmlspecialchars($email).'"'.'>'.$author.'</a>';
					}
	
				}
				return $author;
			}
	
	
			/**
	 * Main function
	 *
	 * @param	string		$content: function output is added to this
	 * @param	array		$conf: configuration array
	 * @return	string		content generated
	 */
			function main($content, $conf) {
				
				
				
				$this->init($conf);
				
				
				if ($this->config['author'] == 'author_date' || $this->config['author'] == '1') {
	
					if ($this->config['page_author']) {
						$userdata = $this->page_Author();
						foreach ($userdata as $val){
						$author = $val['author'];
						$email = $val['author_email'];
						}
						$date = $this->get_Author();
						foreach ($date as $val){
						$date = $this->setDate($val['tstamp']);
						
						}
						
					} else {
						$userdata = $this->get_Author();
						foreach ($userdata as $val){
						$author = $val['realName'];
						$email = $val['email'];
						$date = $this->setDate($val['tstamp']);
						}
				
						}
	
					
					$author = $this->mailLink($author, $email);
	
					if ($this->config['sequence'] == 'date'|| $this->config['sequence'] == '1' ) {
						$content = $this->config['prefix'].$this->spaceDate.$date.$this->config['separate'].$this->config['prefixAuthor'].$this->spaceAuthor.$author;
					} else {
						$content = $this->config['prefixAuthor'].$this->spaceAuthor.$author.$this->config['separate'].$this->config['prefix'].$this->spaceDate.$date;
					}
	
				}
	
	
				if ($this->config['author'] == 'author_only' || $this->config['author'] == '2') {
	
					if ($this->config['page_author']) {
						$userdata = $this->page_Author();
						foreach ($userdata as $val){
						$author = $val['author'];
						$email = $val['author_email'];
						}
						$date = $this->get_Author();
						foreach ($date as $val){
						$date = $this->setDate($val['tstamp']);
						
						}
						
					} else {
						$userdata = $this->get_Author();
						foreach ($userdata as $val){
						$author = $val['realName'];
						$email = $val['email'];
						$date = $this->setDate($val['tstamp']);
						}
					}
	
					$author = $this->mailLink($author, $email);
					$content = $this->config['prefixAuthor'].$this->spaceAuthor.$author;
	
	
	
				}
	
				if ($this->config['author'] == 'date_only' || $this->config['author'] == '3') {
	
					$userdata = $this->get_Author();
						foreach ($userdata as $val){
						$date = $this->setDate($val['tstamp']);
						}
					$content = $this->config['prefix'].$this->spaceDate.$date;
	
				}
	
	
			return $this->pi_wrapInBaseClass($content);
	
			}
			
		}
		
	
			if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/lastupdate/pi1/class.tx_lastupdate_pi1.php']) {
			include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/lastupdate/pi1/class.tx_lastupdate_pi1.php']);
		}
	
?>