<?php
if (!defined ("TYPO3_MODE")) 	die ("Access denied.");

$TCA["tx_lastupdate_custom"] = Array (
	"ctrl" => $TCA["tx_lastupdate_custom"]["ctrl"],
	"interface" => Array (
		"showRecordFieldList" => "hidden,author_prefix,date_prefix,date_format,separate"
	),
	"feInterface" => $TCA["tx_lastupdate_custom"]["feInterface"],
	"columns" => Array (
		"hidden" => Array (		
			"exclude" => 1,	
			"label" => "LLL:EXT:lang/locallang_general.php:LGL.hidden",
			"config" => Array (
				"type" => "check",
				"default" => "0"
			)
		),
		"author_prefix" => Array (		
			"exclude" => 1,
            'l10n_mode' => 'mergeIfNotBlank',		
			"label" => "LLL:EXT:lastupdate/locallang_tca.php:tx_lastupdate_custom.author_prefix",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",
			)
		),
		"date_prefix" => Array (		
			"exclude" => 1,		
          	'l10n_mode' => 'mergeIfNotBlank',	
			"label" => "LLL:EXT:lastupdate/locallang_tca.php:tx_lastupdate_custom.date_prefix",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",
			)
		),
		"date_format" => Array (		
			"exclude" => 1,		
            'l10n_mode' => 'mergeIfNotBlank',	
			"label" => "LLL:EXT:lastupdate/locallang_tca.php:tx_lastupdate_custom.date_format",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",
			)
		),
		"separate" => Array (		
			"exclude" => 1,		
            'l10n_mode' => 'mergeIfNotBlank',	
			"label" => "LLL:EXT:lastupdate/locallang_tca.php:tx_lastupdate_custom.separate",		
			"config" => Array (
				"type" => "input",	
				"size" => "30",
			)
		),
		'sys_language_uid' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.php:LGL.language',
			'config' => Array (
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => Array(
					Array('LLL:EXT:lang/locallang_general.php:LGL.allLanguages',-1),
					Array('LLL:EXT:lang/locallang_general.php:LGL.default_value',0)
				)
			)
		),
		'l18n_parent' => Array (
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.php:LGL.l18n_parent',
			'config' => Array (
				'type' => 'select',
				'items' => Array (
					Array('', 0),
				),
				'foreign_table' => 'tx_lastupdate_custom',
				'foreign_table_where' => 'AND tx_lastupdate_custom.uid=###REC_FIELD_l18n_parent### AND tx_lastupdate_custom.sys_language_uid IN (-1,0)',
				'wizards' => Array(
					'_PADDING' => 2,
					'_VERTICAL' => 1,
					
					'edit' => Array(
							'type' => 'popup',
							'title' => 'edit default language version of this record ',
							'script' => 'wizard_edit.php',
							'popup_onlyOpenIfSelected' => 1,
							'icon' => 'edit2.gif',
							'JSopenParams' => 'height=600,width=700,status=0,menubar=0,scrollbars=1,resizable=1',
					),
				),
			)
		),
		'l18n_diffsource' => Array('config'=>array('type'=>'passthrough')),
		't3ver_label' => Array (
			'displayCond' => 'EXT:version:LOADED:true',
			'label' => 'LLL:EXT:lang/locallang_general.php:LGL.versionLabel',
			'config' => Array (
				'type' => 'input',
				'size' => '30',
				'max' => '30',
			)
		)
	),
	
	"types" => Array (
		"0" => Array("showitem" => "hidden;;1;;1-1-1, author_prefix, date_prefix, date_format, separate")
	),
	"palettes" => Array (
		"1" => Array("showitem" => 'l18n_parent,sys_language_uid,t3ver_label')
		
	),

		
		
	
);
?>