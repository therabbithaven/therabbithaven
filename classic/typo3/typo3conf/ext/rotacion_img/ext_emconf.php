<?php

########################################################################
# Extension Manager/Repository config file for ext: "rotacion_img"
# 
# Auto generated 22-04-2006 23:46
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => '[Gobernalia]rotacion_imagenes',
	'description' => 'shows one image from a list of images changing them from time to time specified / muestra una imagen de una lista cambiandolas cada cierto tiempo especificado',
	'category' => 'plugin',
	'shy' => 0,
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'TYPO3_version' => '',
	'PHP_version' => '',
	'module' => '',
	'state' => 'beta',
	'uploadfolder' => 1,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author' => 'M.A Anton [Gobernalia Global Net S.A - GrupoBBVA]',
	'author_email' => 'admin@email.test',
	'author_company' => 'Gobernalia Global Net S.A (GrupoBBVA)',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'version' => '1.0.8',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:18:{s:9:"ChangeLog";s:4:"5523";s:10:"README.txt";s:4:"ee2d";s:12:"ext_icon.gif";s:4:"6ef6";s:17:"ext_localconf.php";s:4:"539e";s:14:"ext_tables.php";s:4:"5438";s:14:"ext_tables.sql";s:4:"c8ef";s:24:"ext_typoscript_setup.txt";s:4:"c15b";s:32:"icon_tx_rotacionimg_imagenes.gif";s:4:"4cae";s:16:"locallang_db.php";s:4:"85aa";s:7:"tca.php";s:4:"b80c";s:14:"doc/manual.sxw";s:4:"c65a";s:19:"doc/wizard_form.dat";s:4:"04a9";s:20:"doc/wizard_form.html";s:4:"aeed";s:32:"pi1/class.tx_rotacionimg_pi1.php";s:4:"0778";s:17:"pi1/locallang.php";s:4:"e95b";s:24:"pi1/static/editorcfg.txt";s:4:"abbf";s:20:".xvpics/ext_icon.gif";s:4:"38ef";s:40:".xvpics/icon_tx_rotacionimg_imagenes.gif";s:4:"d3c8";}',
);

?>