<?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

t3lib_extMgm::allowTableOnStandardPages("tx_rotacionimg_imagenes");


t3lib_extMgm::addToInsertRecords("tx_rotacionimg_imagenes");

$TCA["tx_rotacionimg_imagenes"] = Array (
	"ctrl" => Array (
		"title" => "LLL:EXT:rotacion_img/locallang_db.php:tx_rotacionimg_imagenes",		
		"label" => "nombre",
		"tstamp" => "tstamp",
		"crdate" => "crdate",
		"cruser_id" => "cruser_id",
		"default_sortby" => "ORDER BY nombre",	
		"delete" => "deleted",	
		"enablecolumns" => Array (		
			"disabled" => "hidden",	
			"starttime" => "starttime",	
			"endtime" => "endtime",
		),
		"dynamicConfigFile" => t3lib_extMgm::extPath($_EXTKEY)."tca.php",
		"iconfile" => t3lib_extMgm::extRelPath($_EXTKEY)."icon_tx_rotacionimg_imagenes.gif",
	),
	"feInterface" => Array (
		"fe_admin_fieldList" => "hidden, starttime, endtime, nombre,imagenes, enlaces,rotacion_alt,rotacion_title ",
	)
);


t3lib_div::loadTCA('tt_content');

t3lib_extMgm::addPlugin(Array('LLL:EXT:rotacion_img/locallang_db.php:tt_content.CType_pi1', $_EXTKEY.'_pi1'),'CType');

$tempColumns = Array (
	"tx_rotacionimg_reg_rotacion_img" => Array (		
		"exclude" => 1,
		"label" => "LLL:EXT:rotacion_img/locallang_db.php:tt_content.tx_rotacionimg_reg_rotacion_img",
		"config" => Array (
			"type" => "group",	
			"internal_type" => "db",	
			"allowed" => "tx_rotacionimg_imagenes",	
			"size" => 10,	
			"minitems" => 0,
			"maxitems" => 10,
		)
	),
	"tx_rotacionimg_tiempo_rotacion_img" => Array (		
		"exclude" => 1,		
		"label" => "LLL:EXT:rotacion_img/locallang_db.php:tt_content.tx_rotacionimg_tiempo_rotacion_img",
		"config" => Array (
			"type" => "input",
			"size" => "4",
			"max" => "4",
			"eval" => "int",
			"checkbox" => "0",
			"range" => Array (
				"upper" => "1000",
				"lower" => "1"
			),
			"default" => 0
		)
	),
);


t3lib_div::loadTCA("tt_content");
t3lib_extMgm::addTCAcolumns("tt_content",$tempColumns,1);
$TCA['tt_content']['types'][$_EXTKEY.'_pi1']['showitem']='CType;;4;button;1-1-1, header;;3;;2-2-2,tx_rotacionimg_reg_rotacion_img;;;;3-3-3, tx_rotacionimg_tiempo_rotacion_img';
//t3lib_extMgm::addToAllTCAtypes("tt_content","tx_rotacionimg_reg_rotacion_img;;;;1-1-1, tx_rotacionimg_tiempo_rotacion_img");

$TCA_DESCR['tx_rotacionimg_imagenes']=Array(
	'columns' => Array(
		'nombre' => Array(
			'description' => 'nombre identificativo',
			'details' => 'introduzca un nombre identificativo del grupo de imágenes',
			'seeAlso' => 'tx_rotacionimg_imagenes:imagenes,tx_rotacionimg_imagenes:enlaces,tx_rotacionimg_imagenes:rotacion_alt,tx_rotacionimg_imagenes:rotacion_title',
		),
		'imagenes' => Array(
			'description' => 'seleccion de imagenes',
			'details' => 'seleccione las imagenes que formarán parte de este grupo de imágenes, el orden que seleccione es en el que se presentaran las imagenes',
			'seeAlso' => 'tx_rotacionimg_imagenes:nombre,tx_rotacionimg_imagenes:enlaces,tx_rotacionimg_imagenes:rotacion_alt,tx_rotacionimg_imagenes:rotacion_title',
		),
		'enlaces' => Array(
			'description' => 'enlaces de imagenes',
			'details' => 'introduzca los enlaces para cada imagen separados por comas, los enlaces vacios dirigiran el enlace a la propia pagina en que se encuentre el contenido que las muestra',
			'seeAlso' => 'tx_rotacionimg_imagenes:nombre,tx_rotacionimg_imagenes:imagenes,tx_rotacionimg_imagenes:rotacion_alt,tx_rotacionimg_imagenes:rotacion_title',
		),
		'rotacion_alt' => Array(
			'description' => 'alt de las imagenes',
			'details' => 'introduzca el texto alternativo para las imagenes,cada linea corresponde con una imagen , si una imagen no tiene texto alternativo pero tiene su elemento correspondiente en el campo title, presentará este, y si no hay en ninguno presentará el nombre de la imagen',
			'seeAlso' => 'tx_rotacionimg_imagenes:nombre,tx_rotacionimg_imagenes:imagenes,tx_rotacionimg_imagenes:enlaces,tx_rotacionimg_imagenes:rotacion_title',
		),
		'rotacion_title' => Array(
			'description' => 'title de las imagenes',
			'details' => 'introduzca el titulo para las imagenes,cada linea corresponde con una imagen , si una imagen no tiene titulo pero tiene su texto alternativo correspondiente en el campo alt, presentará este, y si no hay en ninguno presentará el nombre de la imagen',
			'seeAlso' => 'tx_rotacionimg_imagenes:nombre,tx_rotacionimg_imagenes:imagenes,tx_rotacionimg_imagenes:enlaces,tx_rotacionimg_imagenes:rotacion_alt',
		),
	),
);

 $TCA_DESCR['tt_content']=Array(
	'columns' => Array(
		'tx_rotacionimg_reg_rotacion_img' => Array(
			'description' => 'seleccion del grupo de imagenes',
			'details' => 'seleccione los distintos grupos de imagenes pertenecientes al tipo de registro de rotación de imagenes, las imagenes se presentaran en el orden que se presenten los grupos seleccionados',
			'seeAlso' => 'tt_content:tx_rotacionimg_tiempo_rotacion_img'
		),
		'tx_rotacionimg_tiempo_rotacion_img' => Array(
			'description' => 'tiempo para cambio de imagenes',
			'details' => 'indique el tiempo en segundos durante el que se mostraran las distintas imagenes, pasado el cual se mostrara la siguiente imagen',
			'seeAlso' => 'tt_content:tx_rotacionimg_reg_rotacion_img'
		),
	),

);
?>
