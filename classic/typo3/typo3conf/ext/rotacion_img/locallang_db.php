<?php
/**
 * Language labels for database tables/fields belonging to extension "rotacion_img"
 *
 * This file is detected by the translation tool.
 */

$LOCAL_LANG = Array (
	'default' => Array (
		'tx_rotacionimg_imagenes' => 'Images Rotation',
		'tx_rotacionimg_imagenes.imagenes' => 'Images',
		'tx_rotacionimg_imagenes.enlaces' => 'Links',
		'tx_rotacionimg_imagenes.nombre' => 'Name',
		'tx_rotacionimg_imagenes.rotacion_alt' => 'Alt',
		'tx_rotacionimg_imagenes.rotacion_title' => 'Title',
		'tt_content.CType_pi1' => 'Images rotation',
		'tt_content.tx_rotacionimg_reg_rotacion_img' => 'Images group',
		'tt_content.tx_rotacionimg_tiempo_rotacion_img' => 'Time',
	),
	'es' => Array (
		'tx_rotacionimg_imagenes' => 'Rotacion de Imagenes',
		'tx_rotacionimg_imagenes.imagenes' => 'Imagenes',
		'tx_rotacionimg_imagenes.enlaces' => 'Enlaces',
		'tx_rotacionimg_imagenes.nombre' => 'Nombre descriptivo',
		'tx_rotacionimg_imagenes.rotacion_alt' => 'Alt',
		'tx_rotacionimg_imagenes.rotacion_title' => 'Title',
		'tt_content.CType_pi1' => 'rotacion de imagenes',	
		'tt_content.tx_rotacionimg_reg_rotacion_img' => 'Grupo de im�genes',
		'tt_content.tx_rotacionimg_tiempo_rotacion_img' => 'Tiempo',
	),
);
?>
