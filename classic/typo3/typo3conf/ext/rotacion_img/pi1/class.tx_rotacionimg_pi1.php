<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2005 MAAC [Gobernalia] (admin@email.test)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'rotacion de imagenes' for the 'rotacion_img' extension.
 *
 * @author	MAAC [Gobernalia] <admin@email.test>
 */


require_once(PATH_tslib.'class.tslib_pibase.php');

class tx_rotacionimg_pi1 extends tslib_pibase {
	var $prefixId = 'tx_rotacionimg_pi1';		// Same as class name
	var $scriptRelPath = 'pi1/class.tx_rotacionimg_pi1.php';	// Path to this script relative to the extension dir.
	var $extKey = 'rotacion_img';	// The extension key.
	var $pi_checkCHash = TRUE;
	
	/**
	 * [Put your description here]
	 */
	function main($content,$conf)	{
	//$GLOBALS['TSFE']->setJS('mi_js','hola');
	//debug($this->cObj->data);
	$this->pi_loadLL();
	//identificador para cada objeto
	$this->uid='_'.$this->cObj->data['uid'];

	//tomamos los datos de configuraci�n y montamos los elementos en funci�n de los mismos
	$this->conf=$conf;
	// datos de la caja
	$calto=$this->conf['caja.']['alto'];
	$cancho=$this->conf['caja.']['ancho'];
	$cborde=$this->conf['caja.']['borde'];
	$cbordepx=($this->conf['caja.']['anchobordepx']!=''?$this->conf['caja.']['anchobordepx']:'1');
	$cbordetipo=($this->conf['caja.']['tipoborde']!=''?$this->conf['caja.']['tipoborde']:'solid');
	$cbordecolor=($this->conf['caja.']['colorborde']!=''?$this->conf['caja.']['colorborde']:'red');
	//montamos el borde de la caja en su caso
	$bordecaja='border:'.$cbordepx.'px '.$cbordetipo.' '.$cbordecolor.';';
	$cborde=($this->conf['caja.']['borde']==1?$bordecaja:'border:0px;');
	$cmaxalto=($this->conf['caja.']['maxalto']!=''?'height:'.$this->conf['caja.']['maxalto'].';':'');
	$cmaxancho=($this->conf['caja.']['maxancho']!=''?'width:'.$this->conf['caja.']['maxancho'].';':'');
	$ccentrarh=($this->conf['caja.']['centrarhorizontal']==1?'text-align:center;':'');
	$ccentrarv=$this->conf['caja.']['centrarvertical'];
		# datos de la imagen
	$imbordepx=($this->conf['imag.']['anchobordepx']!=''?$this->conf['imag.']['anchobordepx']:'1');
	$imbordetipo=($this->conf['imag.']['tipoborde']!=''?$this->conf['imag.']['tipoborde']:'solid');
	$imbordecolor=($this->conf['imag.']['colorborde']!=''?$this->conf['imag.']['colorborde']:'blue');
	//montamos el borde de la imagen en su caso
	$bordeimagen='border:'.$imbordepx.'px '.$imbordetipo.' '.$imbordecolor.';';
	$imborde=($this->conf['imag.']['borde']==1?$bordeimagen:'border:0px;');

	//montamos los css en funcion de la configuraci�n
	$extracss='IMG.centro'.$this->uid.' {'.$cmaxalto.$cmaxancho.$imborde.'}
	DIV.grupo'.$this->uid.' {'.$ccentrarh.'height:'.$calto.';width:'.$cancho.';'.$cborde.'}';

	$this->gruposimagenes=$this->cObj->data['tx_rotacionimg_reg_rotacion_img'];
	$this->tiempo=($this->cObj->data['tx_rotacionimg_tiempo_rotacion_img'])?$this->cObj->data['tx_rotacionimg_tiempo_rotacion_img']:'5';
	$this->tiempo=($this->tiempo*1000);

	//asignamos valores a ciertas variables
		$this->tabla='tx_rotacionimg_imagenes';
		//el siguiente valor crea un texto para a�adir en las consultas, con las condiciones de que los registros no hayan sido borrados, est�n dentro de fechas, etc
		$this->enableFields = $this->cObj->enableFields($this->tabla);
		//tomamos la ruta de instalacion de la extensi�n para usarla posteriormente
		$this->rutaini=$GLOBALS['TYPO3_LOADED_EXT']['rotacion_img']['siteRelPath'];
		if($this->gruposimagenes!=''){$listagrupos= 'uid IN ('.$this->gruposimagenes.')';}
		$res = $GLOBALS['TYPO3_DB']->exec_SELECTquery('*', $this->tabla, $this->enableFields);	// sacamos las empresas dentro de las p�ginas seleccionadas y categor�a que recorremos.
	$res=$GLOBALS['TYPO3_DB']->exec_SELECTgetRows('*', $this->tabla, $listagrupos.$this->enableFields);
	//recorremos los resultados
	for ($list=0;$list<count($res);$list++){
		//comprobamos que haya y sacamos las imagenes
		if ($res[$list]['imagenes']!=''){
			$listaimagenes=explode(',',$res[$list]['imagenes']);
			//sacamos los enlaces
			if ($res[$list]['imagenes']!=''){
				$listaenlaces=explode(',',$res[$list]['enlaces']);
				$listaalt=explode(chr(10),$res[$list]["rotacion_alt"]);
				$listatitle=explode(chr(10),$res[$list]["rotacion_title"]);
			}
			//asignamos los valores en el array de imagen , enlace, alt y title
			for($listimg=0;$listimg<count($listaimagenes);$listimg++){
				$imagenactual=$listaimagenes[$listimg];
				$imagenes[]['imagen']=$imagenactual;
				$enlaceactual=$this->generaurl($listaenlaces[$listimg]);
				$numact=count($imagenes)-1;
				$imagenes[$numact]['enlace']=$enlaceactual;
				$alt_title=$this->alt_title($imagenactual,$listaalt[$listimg],$listatitle[$listimg]);
				$imagenes[$numact]['alt']=$alt_title['alt'];
				$imagenes[$numact]['title']=$alt_title['title'];
			}
		}
	}
	//creamos el javascript que recorre las imagenes
	//montamos un array con las im�genes y sus enlaces, alt y title
	$nimagenes=count($imagenes);
	$extrajs='listaimg'.$this->uid.'=Array();'."\n".
        'enlace'.$this->uid.'=Array();'."\n";
	for ($todas=0;$todas<$nimagenes;$todas++){
		$extrajs.='listaimg'.$this->uid.'['.$todas.']=new Image();
		listaimg'.$this->uid.'['.$todas.'].src="uploads/tx_rotacionimg/'.$imagenes[$todas]['imagen'].'";'."\n".
		'enlace'.$this->uid.'['.$todas.']="'.$imagenes[$todas]['enlace'].'";'."\n".
		'listaimg'.$this->uid.'['.$todas.'].alt="'.$imagenes[$todas]['alt'].'";'."\n".		'listaimg'.$this->uid.'['.$todas.'].title="'.$imagenes[$todas]['title'].'";'."\n";

	}
	//esta funci�n va mostrando las im�genes y las centra en funci�n del alto de la caja y de la imagen y lo asignamos al margin de la imagen
	$extrajs.='function muestra'.$this->uid.'(im){
		document.getElementById("foto'.$this->uid.'").src=listaimg'.$this->uid.'[im].src;
		document.getElementById("foto'.$this->uid.'").alt=listaimg'.$this->uid.'[im].alt;
		document.getElementById("foto'.$this->uid.'").title=listaimg'.$this->uid.'[im].title;';
		//en el caso de que hayamos indicado en la configuracion que centremos verticalmente escribimos la parte correspondiente
		if($ccentrarv==1){
			$extrajs.='var alto;
			alto=document.getElementById("foto'.$this->uid.'").height;
			document.getElementById("foto'.$this->uid.'").style.margin=('.intval($calto).'-alto)/2+"px 0px 0px 0px";';
		}
		$extrajs.='
		if(enlace'.$this->uid.'[im]!=""){
			document.getElementById("enlaceimg'.$this->uid.'").href=enlace'.$this->uid.'[im];
		}else{
			document.getElementById("enlaceimg'.$this->uid.'").href="javascript:return false;"
		}
		var siguiente'.$this->uid.';
		siguiente'.$this->uid.'=parseInt(im)+1;
		if(siguiente'.$this->uid.'=='.($nimagenes).'){siguiente'.$this->uid.'=0;}

		setTimeout("muestra'.$this->uid.'(\'"+siguiente'.$this->uid.'+"\')",'.$this->tiempo.');

	}';

	//pasamos el js y css a la variable para que lo escriba en la p�gina
	$GLOBALS['TSFE']->setJS('rota_'.$this->uid,$extrajs);
	$GLOBALS['TSFE']->setCSS('rota_'.$this->uid,$extracss);

	//escribimos la caja y la imagen original e inmediatamente llamamos a la funcion que recorre la imagen , con lo que nos aparece directamente la primera centrada.
	$codigotml='<div class="grupo'.$this->uid.'"><a id="enlaceimg'.$this->uid.'" ><img name="foto'.$this->uid.'" id="foto'.$this->uid.'" src="uploads/tx_rotacionimg/'.$imagenes[0]['imagen'].'" class="centro'.$this->uid.'" alt="" title=""></a></div>

	<script type="text/javascript">
		/*<![CDATA[*/
		<!--

			muestra'.$this->uid.'(0);
		// -->
		/*]]>*/
		</script>';

		return $codigotml;
	}

	function generaurl($valor){
		//si no pasamos nada hacemos el enlace a la propia p�gina
		$valorid=($_GET['id'] ? $_GET['id'] : $GLOBALS["TSFE"]->id);
		if ($valor==''){
			$valor=$valorid;
			return; //si quitamos este return devolver�amos el enlace con el propio id de la p�gina
		}
		$parametros=Array();
		$keys=array_keys($_GET);
		for($a=0;$a<count($keys);$a++){
			if($keys[$a]!='id' ){
				$parametros[]=$keys[$a].'='.$_GET[$keys[$a]];
			}
		}
		//continuamos con los parametros pasados en la url
		if (count($parametros)>0){
			$textoparametros='&'.implode($parametros,"&");
		}
		//si el valor pasado es un n�mero consideramos que es interno de lo contrario montamos la url pasada
		if(is_numeric($valor)){
			$enlace=$GLOBALS["TSFE"]->absRefPrefix.'index.php?id='.$valor.$textoparametros;
		}
		else{
			$enlace='http://'.$valor;
		}
		return $enlace;
	}
	//creamos los alt y titles en funcion de aquellos que se pasan
	// si no pasamos ninguno tomamos el nombre de la imagen
	//si solo se pasa uno de ellos, lo tomamos para los dos
	//si se pasan los dos se asigna cada uno al suyo
	function alt_title($img,$alt='',$title=''){

		if($alt=='' and $title==''){
			$datos['alt']=$datos['title']=$img;
		}
		else if($alt=='' and $title!=''){
			$datos['alt']=$datos['title']=trim($title);
		}
		else if($alt!='' and $title==''){
			$datos['alt']=$datos['title']=trim($alt);
		}
		else{
			$datos['alt']=trim($alt);
			$datos['title']=trim($title);
		}
		return $datos;
	}
}


if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/rotacion_img/pi1/class.tx_rotacionimg_pi1.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/rotacion_img/pi1/class.tx_rotacionimg_pi1.php']);
}

?>
