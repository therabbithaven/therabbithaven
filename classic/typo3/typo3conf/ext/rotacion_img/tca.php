<?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

$TCA["tx_rotacionimg_imagenes"] = Array (
	"ctrl" => $TCA["tx_rotacionimg_imagenes"]["ctrl"],
	"interface" => Array (
		"showRecordFieldList" => "hidden,starttime,endtime,imagenes,enlaces,nombre,rotacion_alt,rotacion_title"
	),
	"feInterface" => $TCA["tx_rotacionimg_imagenes"]["feInterface"],
	"columns" => Array (
		"hidden" => Array (		
			"exclude" => 1,
			"label" => "LLL:EXT:lang/locallang_general.php:LGL.hidden",
			"config" => Array (
				"type" => "check",
				"default" => "0"
			)
		),
		"starttime" => Array (		
			"exclude" => 1,
			"label" => "LLL:EXT:lang/locallang_general.php:LGL.starttime",
			"config" => Array (
				"type" => "input",
				"size" => "8",
				"max" => "20",
				"eval" => "date",
				"default" => "0",
				"checkbox" => "0"
			)
		),
		"endtime" => Array (		
			"exclude" => 1,
			"label" => "LLL:EXT:lang/locallang_general.php:LGL.endtime",
			"config" => Array (
				"type" => "input",
				"size" => "8",
				"max" => "20",
				"eval" => "date",
				"checkbox" => "0",
				"default" => "0",
				"range" => Array (
					"upper" => mktime(0,0,0,12,31,2020),
					"lower" => mktime(0,0,0,date("m")-1,date("d"),date("Y"))
				)
			)
		),
		"imagenes" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:rotacion_img/locallang_db.php:tx_rotacionimg_imagenes.imagenes",		
			"config" => Array (
				"type" => "group",
				"internal_type" => "file",
				"allowed" => $GLOBALS["TYPO3_CONF_VARS"]["GFX"]["imagefile_ext"],	
				"max_size" => 1000,	
				"uploadfolder" => "uploads/tx_rotacionimg",
				"show_thumbs" => 1,	
				"size" => 10,	
				"minitems" => 0,
				"maxitems" => 20,
			)
		),
		"enlaces" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:rotacion_img/locallang_db.php:tx_rotacionimg_imagenes.enlaces",		
			"config" => Array (
				"type" => "input",
				"size" => "15",
				"max" => "255",
				"checkbox" => "",
				"eval" => "trim",
				"wizards" => Array(
					"_PADDING" => 2,
					"link" => Array(
						"type" => "popup",
						"title" => "Link",
						"icon" => "link_popup.gif",
						"script" => "browse_links.php?mode=wizard",
						"JSopenParams" => "height=300,width=500,status=0,menubar=0,scrollbars=1"
					)
				)
			)
		),
		"nombre" => Array (		
			"exclude" => 0,		
			"label" => "LLL:EXT:rotacion_img/locallang_db.php:tx_rotacionimg_imagenes.nombre",
			"config" => Array (
				"type" => "input",	
				"size" => "30",	
				"max" => "100",	
				"eval" => "required",
			)
		),
		"rotacion_alt" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:rotacion_img/locallang_db.php:tx_rotacionimg_imagenes.rotacion_alt",
			"config" => Array (
				"type" => "text",
				"cols" => "20",
				"rows" => "5",
			)
		),
		"rotacion_title" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:rotacion_img/locallang_db.php:tx_rotacionimg_imagenes.rotacion_title",
			"config" => Array (
				"type" => "text",
				"cols" => "20",
				"rows" => "5",
			)
		),
	),
	"types" => Array (
		"0" => Array("showitem" => "hidden;;1;;2-2-2, nombre;;;;2-2-2,imagenes, enlaces;;2;;")
	),
	"palettes" => Array (
		"1" => Array("showitem" => "starttime, endtime"),
		"2" => Array("showitem" => "rotacion_alt,rotacion_title"),
	)
);
?>
