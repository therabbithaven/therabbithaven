<?php
/***************************************************************
*  Copyright notice
*
*  (c) 1999-2005 Kasper Skaarhoj (kasperYYYY@typo3.com)
*  All rights reserved
*
*  This script is part of the Typo3 project. The Typo3 project is 
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
* 
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
* 
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/** 
 * User extension of tslib_gifbuilder
 *
 * @author	Stefan Geith <typo3dev@geithware.de>
 *
 *============================================================================================
 * 
 * Changes are marked regarding v3.8.1 of class.t3lib_stdgraphic.php
 *
 * For working cropV/cropH, class.tslib_content.php ,
 * function getImgResource($file,$fileArray) must be changed:
 * 						
 * 4466		$fileArray['maxW']= intval($this->stdWrap($fileArray['maxW'],$fileArray['maxW.']));
 * 4447		$fileArray['maxH']= intval($this->stdWrap($fileArray['maxH'],$fileArray['maxH.']));
 * 4448		$fileArray['minW']= intval($fileArray['minW']);
 * 4449 	$fileArray['minH']= intval($fileArray['minH']);
 * +		$fileArray['cropH']= intval($this->stdWrap($fileArray['cropH'],$fileArray['cropH.']));
 * +		$fileArray['cropV']= intval($this->stdWrap($fileArray['cropV'],$fileArray['cropV.']));
 * +		$fileArray['cropH']= intval($fileArray['cropH']);
 * +		$fileArray['cropV']= intval($fileArray['cropV']);
 *
 * 4500		if ($fileArray['minW']) {$options['minW']=$fileArray['minW'];}
 * 4501		if ($fileArray['minH']) {$options['minH']=$fileArray['minH'];}
 * +		if ($fileArray['cropH']) {$options['cropH']=$fileArray['cropH'];}
 * +		if ($fileArray['cropV']) {$options['cropV']=$fileArray['cropV'];}
 *
 *================================================================================================= 
 */

require_once (PATH_tslib.'class.tslib_gifbuilder.php');

// tslib_gifbuilder extends class.t3lib_stdgraphic.php
class ux_tslib_gifbuilder extends tslib_gifbuilder {

	/***********************************
	 *
	 * Scaling, Dimensions of images
	 *
	 ***********************************/

	/**
	 * Converts $imagefile to another file in temp-dir of type $newExt (extension).
	 *
	 * @param	string		The image filepath
	 * @param	string		New extension, eg. "gif", "png", "jpg", "tif". If $newExt is NOT set, the new imagefile will be of the original format. If newExt = 'WEB' then one of the web-formats is applied.
	 * @param	string		Width. $w / $h is optional. If only one is given the image is scaled proportionally. If an 'm' exists in the $w or $h and if both are present the $w and $h is regarded as the Maximum w/h and the proportions will be kept
	 * @param	string		Height. See $w
	 * @param	string		Additional ImageMagick parameters.
	 * @param	string		Refers to which frame-number to select in the image. '' or 0 will select the first frame, 1 will select the next and so on...
	 * @param	array		An array with options passed to getImageScale (see this function).
	 * @param	boolean		If set, then another image than the input imagefile MUST be returned. Otherwise you can risk that the input image is good enough regarding messures etc and is of course not rendered to a new, temporary file in typo3temp/. But this option will force it to.
	 * @return	array		[0]/[1] is w/h, [2] is file extension and [3] is the filename.
	 * @see getImageScale(), typo3/show_item.php, fileList_ext::renderImage(), tslib_cObj::getImgResource(), SC_tslib_showpic::show(), maskImageOntoImage(), copyImageOntoImage(), scale()
	 */
	function imageMagickConvert($imagefile,$newExt='',$w='',$h='',$params='',$frame='',$options='',$mustCreate=0)	{
		if ($this->NO_IMAGE_MAGICK)	{
				// Returning file info right away
			return $this->getImageDimensions($imagefile);
		}

		if($info=$this->getImageDimensions($imagefile))	{
			$newExt=strtolower(trim($newExt));
			if (!$newExt)	{	// If no extension is given the original extension is used
				$newExt = $info[2];
			}
			if ($newExt=='web')	{
				if (t3lib_div::inList($this->webImageExt,$info[2]))	{
					$newExt = $info[2];
				} else {
					$newExt = $this->gif_or_jpg($info[2],$info[0],$info[1]);
					if (!$params)	{
						$params = $this->cmds[$newExt];
					}
				}
			}
			if (t3lib_div::inList($this->imageFileExt,$newExt))	{
				if (strstr($w.$h, 'm')) {$max=1;} else {$max=0;}

				$data = $this->getImageScale($info,$w,$h,$options);
				$w=$data['origW'];
				$h=$data['origH'];
				if ($data['cropH'] || $data['cropV']) {
					$options['cropH'] = $data['cropH'];
					$options['cropV'] = $data['cropV'];
				}

					// if no convertion should be performed
				$wh_noscale = (!$w && !$h) || ($data[0]==$info[0] && $data[1]==$info[1]);		// this flag is true if the width / height does NOT dictate the image to be scaled!! (that is if no w/h is given or if the destination w/h matches the original image-dimensions....

				if ($wh_noscale && !$params && !$frame && $newExt==$info[2] && !$mustCreate) {
					$info[3] = $imagefile;
					return $info;
				}
				$info[0]=$data[0];
				$info[1]=$data[1];

				$frame = $this->noFramePrepended ? '' : '['.intval($frame).']';

				if (!$params)	{
					$params = $this->cmds[$newExt];
				}

				$command = $this->scalecmd.' '.$info[0].'x'.$info[1].'! '.$params.' ';

				// vvvvv - new for cropscale; changing crop-parameters should change OutputName
				$md5ext = ($data['crs'] ? 'crs-V'.$options['cropV'].'H'.$options['cropH'] : '');
				// ^^^^^ - new for cropscale; changing crop-parameters should change OutputName

				if ($this->alternativeOutputKey)	{
					// new for cropcale: -------------------------vvvvvvvv
					$theOutputName = t3lib_div::shortMD5($command.$md5ext.basename($imagefile).$this->alternativeOutputKey.$frame);
				} else {
					// new for cropcale: -------------------------vvvvvvvv
					$theOutputName = t3lib_div::shortMD5($command.$md5ext.$imagefile.filemtime($imagefile).$frame);
				}
				if ($this->imageMagickConvert_forceFileNameBody)	{
					$theOutputName = $this->imageMagickConvert_forceFileNameBody;
					$this->imageMagickConvert_forceFileNameBody='';
				}

					// Making the temporary filename:
				$this->createTempSubDir('pics/');
				$output = $this->absPrefix.$this->tempPath.'pics/'.$this->filenamePrefix.$theOutputName.'.'.$newExt;

					// Register temporary filename:
				$GLOBALS['TEMP_IMAGES_ON_PAGE'][] = $output;

				// vvvvv - new for cropscale; crop image after scaling -------------------------------------------------
				if ($data['crs']) {
					// t3lib_div::debug(Array('$data'=>$data, '$params'=>$params, '$options'=>$options, '$imagefile'=>$imagefile, '$output'=>$output, '$command'=>$command, 'File:Line'=>__FILE__.':'.__LINE__));
					if ($this->dontCheckForExistingTempFile || !$this->file_exists_typo3temp_file($output,$imagefile))	{
						$crsOutput = str_replace('pics/','pics/crs-',$output);
						$this->imageMagickExec($imagefile.$frame,$crsOutput,$command);
						// t3lib_div::debug(Array('$crsOutput='=>$crsOutput, 'file_exists'=>file_exists($crsOutput), 'File:Line'=>__FILE__.':'.__LINE__));
						$gifCreator = t3lib_div::makeInstance('tslib_gifbuilder');
						$gifCreator->init();
						if ($GLOBALS['TYPO3_CONF_VARS']['GFX']['gdlib'])	{
							// $ofX = intval( ($data['origW'] - $data[0]) / 2);
							// $ofY = intval( ($data['origH'] - $data[1]) / 2);
							$ofX = intval( ($data['origW'] - $data[0]) * ($options['cropH']+100)/200);   
							$ofY = intval( ($data['origH'] - $data[1]) * ($options['cropV']+100)/200);
							$tmpParm = Array('XY'=>intval($data['origW']).','.intval($data['origH']),
								'backColor'=>'#00ff00',
								'10' => 'IMAGE',
								'10.' => Array( 'file'=> $crsOutput, 'offset'=> $ofX.','.$ofY),
								);
							$gifCreator->start($tmpParm,array());
							$newoutput = $gifCreator->gifBuild();
							if (!copy($newoutput,$output)) {
								$output = $newoutput;
							} 
						} else {
							$output=$crsOutput; 
						}
					}
					// t3lib_div::debug(Array('$newoutput'=>$newoutput, '$output'=>$output, 'File:Line'=>__FILE__.':'.__LINE__));
				// ^^^^^ - new for cropscale; crop image after scaling --------------------------------------
				} else if ($this->dontCheckForExistingTempFile || !$this->file_exists_typo3temp_file($output,$imagefile))	{
					$this->imageMagickExec($imagefile.$frame,$output,$command);
				}

				if (@file_exists($output))	{
					$info[3] = $output;
					$info[2] = $newExt;
					if ($params)	{	// params could realisticly change som imagedata!
						$info=$this->getImageDimensions($info[3]);
					}
					if ($info[2]==$this->gifExtension && !$this->dontCompress)	{
						t3lib_div::gif_compress($info[3],'');		// Compress with IM (lzw) or GD (rle)  (Workaround for the absence of lzw-compression in GD)
					}
					return $info;
				}
			}
		}
	}

	/**
	 * Get numbers for scaling the image based on input
	 *
	 * @param	array		Current image information: Width, Height etc.
	 * @param	integer		"required" width
	 * @param	integer		"required" height
	 * @param	array		Options: Keys are like "maxW", "maxH", "minW", "minH"
	 * @return	array
	 * @access private
	 * @see imageMagickConvert()
	 */
	function getImageScale($info,$w,$h,$options) {
		$out = Array();
		if (strstr($w.$h, 'm')) {$max=1;} else {$max=0;}
		// vvvvv - new for cropscale; set scropscale-flag 'crs' --------------------------------------
		if (strstr($w.$h, 'c')) {
			$out['cropH'] = intval(substr(strstr($w, 'c'),1));
			$out['cropV'] = intval(substr(strstr($h, 'c'),1));
			$crs=1;

		} else {
			$crs=0;
		}
		$out['crs'] = $crs;
		// ^^^^^ - new for cropscale; set scropscale-flag 'crs' --------------------------------------
		$w=intval($w);
		$h=intval($h);
			// if there are max-values...
		if ($options['maxW'])	{
			if ($w) {	// if width is given...
				if ($w>$options['maxW']) {
					$w=$options['maxW'];
					$max=1;	// height should follow
				}
			} else {
				if ($info[0]>$options['maxW']) {
					$w=$options['maxW'];
					$max=1; // height should follow
				}
			}
		}
		if ($options['maxH'])	{
			if ($h) {	// if height is given...
				if ($h>$options['maxH']) {
					$h=$options['maxH'];
					$max=1;	// height should follow
				}
			} else {
				if ($info[1]>$options['maxH']) {	// Changed [0] to [1] 290801
					$h=$options['maxH'];
					$max=1; // height should follow
				}
			}
		}
		$out['origW']=$w;
		$out['origH']=$h;
		$out['max'] = $max;

		if (!$this->mayScaleUp) {
			if ($w>$info[0]){$w=$info[0];}
			if ($h>$info[1]){$h=$info[1];}
		}
		if ($w || $h)	{	// if scaling should be performed
			if ($w && !$h)	{
				$info[1] = ceil($info[1]*($w/$info[0]));
				$info[0] = $w;
			}
			if (!$w && $h)	{
				$info[0] = ceil($info[0]*($h/$info[1]));
				$info[1] = $h;
			}
			if ($w && $h)	{
				if ($max)	{
					$ratio = $info[0]/$info[1];
					if ($h*$ratio > $w) {
						$h = round($w/$ratio);
					} else {
						$w = round($h*$ratio);
					}
				}
				// vvvvv - new for cropscale; scale to fit _around_ $h$w sized box -------------------
				if ($crs)	{
					$ratio = $info[0]/$info[1];
					if ($h*$ratio < $w) {
						$h = round($w/$ratio);
					} else {
						$w = round($h*$ratio);
					}
				}
				// ^^^^^ - new for cropscale; scale to fit _around_ $h$w sized box -------------------
				$info[0] = $w;
				$info[1] = $h;
			}
		}
		$out[0]=$info[0];
		$out[1]=$info[1];
			// Set minimum-measures!
		if ($options['minW'] && $out[0]<$options['minW'])	{
			if ($max && $out[0])	{
				$out[1]= round($out[1]*$options['minW']/$out[0]);
			// vvvvv - new for cropscale -----------------------------------
			} else if ($crs && $out[0])	{
				$out[1]= round($out[1]*$options['minW']/$out[0]);
			// ^^^^^ - new for cropscale -----------------------------------
			}
			$out[0]=$options['minW'];
		}
		if ($options['minH'] && $out[1]<$options['minH'])	{
			if ($max && $out[1])	{
				$out[0]= round($out[0]*$options['minH']/$out[1]);
			// vvvvv - new for cropscale -----------------------------------
			} else if ($crs && $out[1])	{
				$out[0]= round($out[0]*$options['minH']/$out[1]);
			// ^^^^^ - new for cropscale -----------------------------------
			}
			$out[1]=$options['minH'];
		}

		return $out;
	}

}

if (defined("TYPO3_MODE") && $TYPO3_CONF_VARS[TYPO3_MODE]["XCLASS"]["ext/sg_cropscale/class.ux_tslib_gifbuilder.php"])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]["XCLASS"]["ext/sg_cropscale/class.ux_tslib_gifbuilder.php"]);
}

?>