Changes of Descriptions in TSref, Funtions->imgResource

Change 'width' from
	If both the width and heigth is set and one of the numbers has an "m" after it, 
	the proportions will be preserved and thus width/height will work as max-dimensions 
	for the image.


Change 'width' to
	If both the width and heigth is set and one of the numbers has an "m" after it, 
	the proportions will be preserved and thus width/height will work as max-dimensions 
	for the image - image will be scaled to fit into width/height rectangle.

	If both the width and heigth is set and at least one of the numbers has an "c" after it, 
	cropscaling will be enabled; that means the proportions will be preserved and the
	image will be scaled to fit around (!) a rectangle with width/height dimensions.
	Then, a centered portion of the image (size defined by width/height) will be cut out.
	The "c" can have a percentage (-100 ... 100) after it, what defines, how much the cropping
	will be moved off the center to the border. 
	Example
	This crops 120x80px from the center of the scaled image:
	.width = 120c
	.height = 80c
	This crops 100x100px; from landscape-images  at the left and portrait-images centered:
	.width = 100c-100
	.height = 100c
	This crops 100x100px; from landscape-images at the left and portrait-images at the top:
	.width = 100c-100
	.height = 100c-100
	This crops 100x100px; from landscape-images a bit right of the center and portrait-images a bit upper than centered:
	.width = 100c30
	.height = 100c-25

Change 'height' to
	See width


