<?php

########################################################################
# Extension Manager/Repository config file for ext: "sg_cropscale"
# 
# Auto generated 01-08-2006 00:41
# 
# Manual updates:
# Only the data in the array - anything else is removed by next write
########################################################################

$EM_CONF[$_EXTKEY] = Array (
	'title' => 'CropScale Images (only 3.8.1 and before; in core s',
	'description' => 'Enables crop-scaling all images, created by IMAGE objects: extends tslib_gifbuilder by Xclass; width/height paramters in imgResource can now have a �c� after it, then the image gets scaled and after that a centered width/height part is cropped out.  (On',
	'category' => 'fe',
	'shy' => 1,
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'TYPO3_version' => '',
	'PHP_version' => '',
	'module' => '',
	'state' => 'beta',
	'internal' => '',
	'uploadfolder' => 1,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 1,
	'lockType' => '',
	'author' => 'Stefan Geith',
	'author_email' => 'typo3dev@geithware.de',
	'author_company' => '',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'version' => '0.4.0',	// Don't modify this! Managed automatically during upload to repository.
	'_md5_values_when_last_written' => 'a:10:{s:9:"ChangeLog";s:4:"d455";s:10:"README.txt";s:4:"ee2d";s:39:"class.t3lib_stdgraphic_v4b3-patched.php";s:4:"446a";s:29:"class.ux_tslib_gifbuilder.php";s:4:"bd1d";s:12:"ext_icon.gif";s:4:"1bdc";s:17:"ext_localconf.php";s:4:"def1";s:16:"stdgraphic.patch";s:4:"5457";s:14:"doc/readme.txt";s:4:"eac4";s:19:"doc/wizard_form.dat";s:4:"52e3";s:20:"doc/wizard_form.html";s:4:"7dbc";}',
);

?>