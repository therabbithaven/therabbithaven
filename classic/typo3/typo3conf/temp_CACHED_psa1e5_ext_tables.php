<?php
###########################
## EXTENSION: cms
## FILE:      /home/rabbit/public_html/typo3/typo3/sysext/cms/ext_tables.php
###########################

$_EXTKEY = 'cms';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
# TYPO3 CVS ID: $Id: ext_tables.php,v 1.18 2005/05/12 22:54:16 mundaun Exp $
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	{
	t3lib_extMgm::addModule('web','layout','top',t3lib_extMgm::extPath($_EXTKEY).'layout/');
	t3lib_extMgm::addLLrefForTCAdescr('_MOD_web_layout','EXT:cms/locallang_csh_weblayout.xml');
	t3lib_extMgm::addLLrefForTCAdescr('_MOD_web_info','EXT:cms/locallang_csh_webinfo.xml');

	t3lib_extMgm::insertModuleFunction(
		'web_info',
		'tx_cms_webinfo_page',
		t3lib_extMgm::extPath($_EXTKEY).'web_info/class.tx_cms_webinfo.php',
		'LLL:EXT:cms/locallang_tca.php:mod_tx_cms_webinfo_page'
	);
	t3lib_extMgm::insertModuleFunction(
		'web_info',
		'tx_cms_webinfo_lang',
		t3lib_extMgm::extPath($_EXTKEY).'web_info/class.tx_cms_webinfo_lang.php',
		'LLL:EXT:cms/locallang_tca.php:mod_tx_cms_webinfo_lang'
	);
}


// ******************************************************************
// Extend 'pages'-table
// ******************************************************************

if (TYPO3_MODE=='BE')	{
	// Setting ICON_TYPES (obsolete by the removal of the plugin_mgm extension)
	$ICON_TYPES = Array();
}

	// Adding pages_types:
		// t3lib_div::array_merge() MUST be used!
	$PAGES_TYPES = t3lib_div::array_merge(array(
		'3' => Array(
			'icon' => 'pages_link.gif'
		),
		'4' => Array(
			'icon' => 'pages_shortcut.gif'
		),
		'5' => Array(
			'icon' => 'pages_notinmenu.gif'
		),
		'7' => Array(
			'icon' => 'pages_mountpoint.gif'
		),
		'6' => Array(
			'type' => 'web',
			'icon' => 'be_users_section.gif',
			'allowedTables' => '*'
		),
		'199' => Array(		// TypoScript: Limit is 200. When the doktype is 200 or above, the page WILL NOT be regarded as a 'page' by TypoScript. Rather is it a system-type page
			'type' => 'sys',
			'icon' => 'spacer_icon.gif',
		)
	),$PAGES_TYPES);

	// Add allowed records to pages:
	t3lib_extMgm::allowTableOnStandardPages('pages_language_overlay,tt_content,sys_template,sys_domain');

	// Merging in CMS doktypes:
	array_splice(
		$TCA['pages']['columns']['doktype']['config']['items'],
		1,
		0,
		Array(
			Array('LLL:EXT:cms/locallang_tca.php:pages.doktype.I.0', '2'),
			Array('LLL:EXT:lang/locallang_general.php:LGL.external', '3'),
			Array('LLL:EXT:cms/locallang_tca.php:pages.doktype.I.2', '4'),
			Array('LLL:EXT:cms/locallang_tca.php:pages.doktype.I.3', '5'),
			Array('LLL:EXT:cms/locallang_tca.php:pages.doktype.I.4', '6'),
			Array('LLL:EXT:cms/locallang_tca.php:pages.doktype.I.5', '7'),
			Array('-----', '--div--'),
			Array('LLL:EXT:cms/locallang_tca.php:pages.doktype.I.7', '199')
		)
	);

	// Setting enablecolumns:
	$TCA['pages']['ctrl']['enablecolumns'] = Array (
		'disabled' => 'hidden',
		'starttime' => 'starttime',
		'endtime' => 'endtime',
		'fe_group' => 'fe_group',
	);

	// Adding default value columns:
	$TCA['pages']['ctrl']['useColumnsForDefaultValues'].=',fe_group,hidden';

	// Adding new columns:
	$TCA['pages']['columns'] = array_merge($TCA['pages']['columns'],Array(
		'hidden' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:cms/locallang_tca.php:pages.hidden',
			'config' => Array (
				'type' => 'check',
				'default' => '1'
			)
		),
		'starttime' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.php:LGL.starttime',
			'config' => Array (
				'type' => 'input',
				'size' => '8',
				'max' => '20',
				'eval' => 'date',
				'checkbox' => '0',
				'default' => '0'
			)
		),
		'endtime' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.php:LGL.endtime',
			'config' => Array (
				'type' => 'input',
				'size' => '8',
				'max' => '20',
				'eval' => 'date',
				'checkbox' => '0',
				'default' => '0',
				'range' => Array (
					'upper' => mktime(0,0,0,12,31,2020),
				)
			)
		),
		'layout' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.php:LGL.layout',
			'config' => Array (
				'type' => 'select',
				'items' => Array (
					Array('LLL:EXT:lang/locallang_general.php:LGL.normal', '0'),
					Array('LLL:EXT:cms/locallang_tca.php:pages.layout.I.1', '1'),
					Array('LLL:EXT:cms/locallang_tca.php:pages.layout.I.2', '2'),
					Array('LLL:EXT:cms/locallang_tca.php:pages.layout.I.3', '3')
				),
				'default' => '0'
			)
		),
		'fe_group' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.php:LGL.fe_group',
			'config' => Array (
				'type' => 'select',
				'items' => Array (
					Array('', 0),
					Array('LLL:EXT:lang/locallang_general.php:LGL.hide_at_login', -1),
					Array('LLL:EXT:lang/locallang_general.php:LGL.any_login', -2),
					Array('LLL:EXT:lang/locallang_general.php:LGL.usergroups', '--div--')
				),
				'foreign_table' => 'fe_groups'
			)
		),
		'extendToSubpages' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:cms/locallang_tca.php:pages.extendToSubpages',
			'config' => Array (
				'type' => 'check'
			)
		),
		'nav_title' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:cms/locallang_tca.php:pages.nav_title',
			'config' => Array (
				'type' => 'input',
				'size' => '30',
				'max' => '256',
				'checkbox' => '',
				'eval' => 'trim'
			)
		),
		'nav_hide' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:cms/locallang_tca.php:pages.nav_hide',
			'config' => Array (
				'type' => 'check'
			)
		),
		'subtitle' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:cms/locallang_tca.php:pages.subtitle',
			'config' => Array (
				'type' => 'input',
				'size' => '30',
				'max' => '256',
				'eval' => ''
			)
		),
		'target' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:cms/locallang_tca.php:pages.target',
			'config' => Array (
				'type' => 'input',
				'size' => '7',
				'max' => '20',
				'eval' => 'trim',
				'checkbox' => ''
			)
		),
		'alias' => Array (
			'label' => 'LLL:EXT:cms/locallang_tca.php:pages.alias',
			'config' => Array (
				'type' => 'input',
				'size' => '10',
				'max' => '20',
				'eval' => 'nospace,alphanum_x,lower,unique',
				'softref' => 'notify'
			)
		),
		'url' => Array (
			'label' => 'LLL:EXT:cms/locallang_tca.php:pages.url',
			'config' => Array (
				'type' => 'input',
				'size' => '25',
				'max' => '256',
				'eval' => 'trim',
				'softref' => 'url'
			)
		),
		'urltype' => Array (
			'label' => 'LLL:EXT:lang/locallang_general.php:LGL.type',
			'config' => Array (
				'type' => 'select',
				'items' => Array (
					Array('', '0'),
					Array('http://', '1'),
					Array('ftp://', '2'),
					Array('mailto:', '3')
				),
				'default' => '1'
			)
		),
		'lastUpdated' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:cms/locallang_tca.php:pages.lastUpdated',
			'config' => Array (
				'type' => 'input',
				'size' => '12',
				'max' => '20',
				'eval' => 'datetime',
				'checkbox' => '0',
				'default' => '0'
			)
		),
		'newUntil' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:cms/locallang_tca.php:pages.newUntil',
			'config' => Array (
				'type' => 'input',
				'size' => '8',
				'max' => '20',
				'eval' => 'date',
				'checkbox' => '0',
				'default' => '0'
			)
		),
		'cache_timeout' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:cms/locallang_tca.php:pages.cache_timeout',
			'config' => Array (
				'type' => 'select',
				'items' => Array (
					Array('LLL:EXT:lang/locallang_general.php:LGL.default_value', 0),
					Array('LLL:EXT:cms/locallang_tca.php:pages.cache_timeout.I.1', 60),
					Array('LLL:EXT:cms/locallang_tca.php:pages.cache_timeout.I.2', 5*60),
					Array('LLL:EXT:cms/locallang_tca.php:pages.cache_timeout.I.3', 15*60),
					Array('LLL:EXT:cms/locallang_tca.php:pages.cache_timeout.I.4', 30*60),
					Array('LLL:EXT:cms/locallang_tca.php:pages.cache_timeout.I.5', 60*60),
					Array('LLL:EXT:cms/locallang_tca.php:pages.cache_timeout.I.6', 4*60*60),
					Array('LLL:EXT:cms/locallang_tca.php:pages.cache_timeout.I.7', 24*60*60),
					Array('LLL:EXT:cms/locallang_tca.php:pages.cache_timeout.I.8', 2*24*60*60),
					Array('LLL:EXT:cms/locallang_tca.php:pages.cache_timeout.I.9', 7*24*60*60),
					Array('LLL:EXT:cms/locallang_tca.php:pages.cache_timeout.I.10', 31*24*60*60)
				),
				'default' => '0'
			)
		),
		'no_cache' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:cms/locallang_tca.php:pages.no_cache',
			'config' => Array (
				'type' => 'check'
			)
		),
		'no_search' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:cms/locallang_tca.php:pages.no_search',
			'config' => Array (
				'type' => 'check'
			)
		),
		'shortcut' => Array (
			'label' => 'LLL:EXT:lang/locallang_general.php:LGL.shortcut_page',
			'config' => Array (
				'type' => 'group',
				'internal_type' => 'db',
					'allowed' => 'pages',
				'size' => '3',
				'maxitems' => '1',
				'minitems' => '0',
				'show_thumbs' => '1'
			)
		),
		'shortcut_mode' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:cms/locallang_tca.php:pages.shortcut_mode',
			'config' => Array (
				'type' => 'select',
				'items' => Array (
					Array('', 0),
					Array('LLL:EXT:cms/locallang_tca.php:pages.shortcut_mode.I.1', 1),
					Array('LLL:EXT:cms/locallang_tca.php:pages.shortcut_mode.I.2', 2),
				),
				'default' => '0'
			)
		),
		'content_from_pid' => Array (
			'label' => 'LLL:EXT:cms/locallang_tca.php:pages.content_from_pid',
			'config' => Array (
				'type' => 'group',
				'internal_type' => 'db',
					'allowed' => 'pages',
				'size' => '1',
				'maxitems' => '1',
				'minitems' => '0',
				'show_thumbs' => '1'
			)
		),
		'mount_pid' => Array (
			'label' => 'LLL:EXT:cms/locallang_tca.php:pages.mount_pid',
			'config' => Array (
				'type' => 'group',
				'internal_type' => 'db',
					'allowed' => 'pages',
				'size' => '1',
				'maxitems' => '1',
				'minitems' => '0',
				'show_thumbs' => '1'
			)
		),
		'keywords' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.php:LGL.keywords',
			'config' => Array (
				'type' => 'text',
				'cols' => '40',
				'rows' => '3'
			)
		),
		'description' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.php:LGL.description',
			'config' => Array (
				'type' => 'input',
				'size' => '40',
				'eval' => 'trim'
			)
		),
		'abstract' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:cms/locallang_tca.php:pages.abstract',
			'config' => Array (
				'type' => 'text',
				'cols' => '40',
				'rows' => '3'
			)
		),
		'author' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.php:LGL.author',
			'config' => Array (
				'type' => 'input',
				'size' => '20',
				'eval' => 'trim',
				'max' => '80'
			)
		),
		'author_email' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.php:LGL.email',
			'config' => Array (
				'type' => 'input',
				'size' => '20',
				'eval' => 'trim',
				'max' => '80',
				'softref' => 'email[subst]'
			)
		),
		'media' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:cms/locallang_tca.php:pages.media',
			'config' => Array (
				'type' => 'group',
				'internal_type' => 'file',
				'allowed' => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'].',html,htm,ttf,txt,css',
				'max_size' => '2000',
				'uploadfolder' => 'uploads/media',
				'show_thumbs' => '1',
				'size' => '3',
				'maxitems' => '5',
				'minitems' => '0'
			)
		),
		'is_siteroot' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:cms/locallang_tca.php:pages.is_siteroot',
			'config' => Array (
				'type' => 'check'
			)
		),
		'mount_pid_ol' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:cms/locallang_tca.php:pages.mount_pid_ol',
			'config' => Array (
				'type' => 'check'
			)
		),
		'module' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:cms/locallang_tca.php:pages.module',
			'config' => Array (
				'type' => 'select',
				'items' => Array (
					Array('', ''),
					Array('LLL:EXT:cms/locallang_tca.php:pages.module.I.1', 'shop'),
					Array('LLL:EXT:cms/locallang_tca.php:pages.module.I.2', 'board'),
					Array('LLL:EXT:cms/locallang_tca.php:pages.module.I.3', 'news'),
					Array('LLL:EXT:cms/locallang_tca.php:pages.module.I.4', 'fe_users'),
					Array('LLL:EXT:cms/locallang_tca.php:pages.module.I.5', 'dmail'),
					Array('LLL:EXT:cms/locallang_tca.php:pages.module.I.6', 'approve')
				),
				'default' => ''
			)
		),
		'fe_login_mode' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:cms/locallang_tca.php:pages.fe_login_mode',
			'config' => Array (
				'type' => 'select',
				'items' => Array (
					Array('', 0),
					Array('LLL:EXT:cms/locallang_tca.php:pages.fe_login_mode.disable', 1),
					Array('LLL:EXT:cms/locallang_tca.php:pages.fe_login_mode.enable', 2),
				)
			)
		),
		'l18n_cfg' => Array (
			'exclude' => 1,
			'label' => 'LLL:EXT:cms/locallang_tca.php:pages.l18n_cfg',
			'config' => Array (
				'type' => 'check',
				'items' => Array (
					Array('LLL:EXT:cms/locallang_tca.php:pages.l18n_cfg.I.1', ''),
					Array($GLOBALS['TYPO3_CONF_VARS']['FE']['hidePagesIfNotTranslatedByDefault'] ? 'LLL:EXT:cms/locallang_tca.php:pages.l18n_cfg.I.2a' : 'LLL:EXT:cms/locallang_tca.php:pages.l18n_cfg.I.2', ''),
				),
			)
		),
	));

		// Add columns to info-display list.
	$TCA['pages']['interface']['showRecordFieldList'].=',alias,hidden,starttime,endtime,fe_group,url,target,no_cache,shortcut,keywords,description,abstract,newUntil,lastUpdated,cache_timeout';

		// Setting main palette
	$TCA['pages']['ctrl']['mainpalette']='1';

		// Totally overriding all type-settings:
	$TCA['pages']['types'] = Array (
		'1' => Array('showitem' => 'hidden;;;;1-1-1, doktype;;2;button, title;;3;;2-2-2, subtitle, nav_hide, TSconfig;;6;nowrap;5-5-5, storage_pid;;7, l18n_cfg'),
		'2' => Array('showitem' => 'hidden;;;;1-1-1, doktype;;2;button, title;;3;;2-2-2, subtitle, nav_hide, nav_title, --div--, abstract;;5;;3-3-3, keywords, description, media;;;;4-4-4, --div--, TSconfig;;6;nowrap;5-5-5, storage_pid;;7, l18n_cfg, fe_login_mode, module, content_from_pid'),
		'3' => Array('showitem' => 'hidden;;;;1-1-1, doktype, title;;3;;2-2-2, nav_hide, url;;;;3-3-3, urltype, TSconfig;;6;nowrap;5-5-5, storage_pid;;7, l18n_cfg'),
		'4' => Array('showitem' => 'hidden;;;;1-1-1, doktype, title;;3;;2-2-2, nav_hide, shortcut;;;;3-3-3, shortcut_mode, TSconfig;;6;nowrap;5-5-5, storage_pid;;7, l18n_cfg'),
		'5' => Array('showitem' => 'hidden;;;;1-1-1, doktype;;2;button, title;;3;;2-2-2, subtitle, nav_hide, nav_title, --div--, media;;;;4-4-4, --div--, TSconfig;;6;nowrap;5-5-5, storage_pid;;7, l18n_cfg, fe_login_mode, module, content_from_pid'),
		'7' => Array('showitem' => 'hidden;;;;1-1-1, doktype;;2;button, title;;3;;2-2-2, subtitle, nav_hide, nav_title, --div--, mount_pid;;;;3-3-3, mount_pid_ol, media;;;;4-4-4, --div--, TSconfig;;6;nowrap;5-5-5, storage_pid;;7, l18n_cfg, fe_login_mode, module, content_from_pid'),
		'199' => Array('showitem' => 'hidden;;;;1-1-1, doktype, title;;;;2-2-2, TSconfig;;6;nowrap;5-5-5, storage_pid;;7'),
		'254' => Array('showitem' => 'hidden;;;;1-1-1, doktype, title;LLL:EXT:lang/locallang_general.php:LGL.title;;;2-2-2, --div--, TSconfig;;6;nowrap;5-5-5, storage_pid;;7, module'),
		'255' => Array('showitem' => 'hidden;;;;1-1-1, doktype, title;;;;2-2-2')
	);
		// Merging palette settings:
		// t3lib_div::array_merge() MUST be used - otherwise the keys will be re-numbered!
	$TCA['pages']['palettes'] = t3lib_div::array_merge($TCA['pages']['palettes'],Array(
		'1' => Array('showitem' => 'starttime,endtime,fe_group,extendToSubpages'),
		'2' => Array('showitem' => 'layout, lastUpdated, newUntil, no_search'),
		'3' => Array('showitem' => 'alias, target, no_cache, cache_timeout'),
		'5' => Array('showitem' => 'author,author_email'),
	));






// ******************************************************************
// This is the standard TypoScript content table, tt_content
// ******************************************************************
$TCA['tt_content'] = Array (
	'ctrl' => Array (
		'label' => 'header',
		'label_alt' => 'subheader,bodytext',
		'sortby' => 'sorting',
		'tstamp' => 'tstamp',
		'title' => 'LLL:EXT:cms/locallang_tca.php:tt_content',
		'delete' => 'deleted',
		'versioning' => TRUE,
		'versioning_followPages' => TRUE,
		'type' => 'CType',
		'prependAtCopy' => 'LLL:EXT:lang/locallang_general.php:LGL.prependAtCopy',
		'copyAfterDuplFields' => 'colPos,sys_language_uid',
		'useColumnsForDefaultValues' => 'colPos,sys_language_uid',
		'transOrigPointerField' => 'l18n_parent',
		'transOrigDiffSourceField' => 'l18n_diffsource',
		'languageField' => 'sys_language_uid',
		'enablecolumns' => Array (
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
			'fe_group' => 'fe_group',
		),
		'typeicon_column' => 'CType',
		'typeicons' => Array (
			'header' => 'tt_content_header.gif',
			'textpic' => 'tt_content_textpic.gif',
			'image' => 'tt_content_image.gif',
			'bullets' => 'tt_content_bullets.gif',
			'table' => 'tt_content_table.gif',
			'splash' => 'tt_content_news.gif',
			'uploads' => 'tt_content_uploads.gif',
			'multimedia' => 'tt_content_mm.gif',
			'menu' => 'tt_content_menu.gif',
			'list' => 'tt_content_list.gif',
			'mailform' => 'tt_content_form.gif',
			'search' => 'tt_content_search.gif',
			'login' => 'tt_content_login.gif',
			'shortcut' => 'tt_content_shortcut.gif',
			'script' => 'tt_content_script.gif',
			'div' => 'tt_content_div.gif',
			'html' => 'tt_content_html.gif'
		),
		'mainpalette' => '1',
		'thumbnail' => 'image',
		'requestUpdate' => 'list_type',
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tbl_tt_content.php'
	)
);

// ******************************************************************
// fe_users
// ******************************************************************
$TCA['fe_users'] = Array (
	'ctrl' => Array (
		'label' => 'username',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'fe_cruser_id' => 'fe_cruser_id',
		'title' => 'LLL:EXT:cms/locallang_tca.php:fe_users',
		'delete' => 'deleted',
		'mainpalette' => '1',
		'enablecolumns' => Array (
			'disabled' => 'disable',
			'starttime' => 'starttime',
			'endtime' => 'endtime'
		),
		'useColumnsForDefaultValues' => 'usergroup,lockToDomain,disable,starttime,endtime',
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tbl_cms.php'
	),
	'feInterface' => Array (
		'fe_admin_fieldList' => 'username,password,usergroup,name,address,telephone,fax,email,title,zip,city,country,www,company',
	)
);

// ******************************************************************
// fe_groups
// ******************************************************************
$TCA['fe_groups'] = Array (
	'ctrl' => Array (
		'label' => 'title',
		'tstamp' => 'tstamp',
		'delete' => 'deleted',
		'prependAtCopy' => 'LLL:EXT:lang/locallang_general.php:LGL.prependAtCopy',
		'enablecolumns' => Array (
			'disabled' => 'hidden'
		),
		'title' => 'LLL:EXT:cms/locallang_tca.php:fe_groups',
		'useColumnsForDefaultValues' => 'lockToDomain',
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tbl_cms.php'
	)
);

// ******************************************************************
// sys_domain
// ******************************************************************
$TCA['sys_domain'] = Array (
	'ctrl' => Array (
		'label' => 'domainName',
		'tstamp' => 'tstamp',
		'sortby' => 'sorting',
		'title' => 'LLL:EXT:cms/locallang_tca.php:sys_domain',
		'iconfile' => 'domain.gif',
		'enablecolumns' => Array (
			'disabled' => 'hidden'
		),
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tbl_cms.php'
	)
);

// ******************************************************************
// pages_language_overlay
// ******************************************************************
$TCA['pages_language_overlay'] = Array (
	'ctrl' => Array (
		'label' => 'title',
		'tstamp' => 'tstamp',
		'title' => 'LLL:EXT:cms/locallang_tca.php:pages_language_overlay',
		'versioning' => TRUE,
		'versioning_followPages' => TRUE,
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'enablecolumns' => Array (
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime'
		),
		'transOrigPointerField' => 'pid',
		'transOrigPointerTable' => 'pages',
		'transOrigDiffSourceField' => 'l18n_diffsource',
		'languageField' => 'sys_language_uid',
		'mainpalette' => 1,
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tbl_cms.php'
	)
);


// ******************************************************************
// sys_template
// ******************************************************************
$TCA['sys_template'] = Array (
	'ctrl' => Array (
		'label' => 'title',
		'tstamp' => 'tstamp',
		'sortby' => 'sorting',
		'prependAtCopy' => 'LLL:EXT:lang/locallang_general.php:LGL.prependAtCopy',
		'title' => 'LLL:EXT:cms/locallang_tca.php:sys_template',
		'versioning' => TRUE,
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'delete' => 'deleted',
		'adminOnly' => 1,	// Only admin, if any
		'iconfile' => 'template.gif',
		'thumbnail' => 'resources',
		'enablecolumns' => Array (
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime'
		),
		'typeicon_column' => 'root',
		'typeicons' => Array (
			'0' => 'template_add.gif'
		),
		'mainpalette' => '1',
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tbl_cms.php'
	)
);

// ******************************************************************
// static_template
// ******************************************************************
$TCA['static_template'] = Array (
	'ctrl' => Array (
		'label' => 'title',
		'tstamp' => 'tstamp',
		'title' => 'LLL:EXT:cms/locallang_tca.php:static_template',
		'readOnly' => 1,	// This should always be true, as it prevents the static templates from being altered
		'adminOnly' => 1,	// Only admin, if any
		'rootLevel' => 1,
		'is_static' => 1,
		'default_sortby' => 'ORDER BY title',
		'crdate' => 'crdate',
		'iconfile' => 'template_standard.gif',
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tbl_cms.php'
	)
);

?><?php
###########################
## EXTENSION: sv
## FILE:      /home/rabbit/public_html/typo3/typo3/sysext/sv/ext_tables.php
###########################

$_EXTKEY = 'sv';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

// normal services should be added here

?><?php
###########################
## EXTENSION: css_styled_content
## FILE:      /home/rabbit/public_html/typo3/typo3/sysext/css_styled_content/ext_tables.php
###########################

$_EXTKEY = 'css_styled_content';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
# TYPO3 CVS ID: $Id: ext_tables.php,v 1.1 2004/11/16 15:21:46 typo3 Exp $
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

t3lib_extMgm::addStaticFile($_EXTKEY,'static/','CSS Styled Content');
?><?php
###########################
## EXTENSION: context_help
## FILE:      /home/rabbit/public_html/typo3/typo3/sysext/context_help/ext_tables.php
###########################

$_EXTKEY = 'context_help';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

t3lib_extMgm::addLLrefForTCAdescr('fe_groups','EXT:context_help/locallang_csh_fe_groups.xml');
t3lib_extMgm::addLLrefForTCAdescr('fe_users','EXT:context_help/locallang_csh_fe_users.xml');
t3lib_extMgm::addLLrefForTCAdescr('pages','EXT:context_help/locallang_csh_pages.xml');
t3lib_extMgm::addLLrefForTCAdescr('pages_language_overlay','EXT:context_help/locallang_csh_pageslol.xml');
t3lib_extMgm::addLLrefForTCAdescr('static_template','EXT:context_help/locallang_csh_statictpl.xml');
t3lib_extMgm::addLLrefForTCAdescr('sys_domain','EXT:context_help/locallang_csh_sysdomain.xml');
t3lib_extMgm::addLLrefForTCAdescr('sys_template','EXT:context_help/locallang_csh_systmpl.xml');
t3lib_extMgm::addLLrefForTCAdescr('tt_content','EXT:context_help/locallang_csh_ttcontent.xml');
?><?php
###########################
## EXTENSION: extra_page_cm_options
## FILE:      /home/rabbit/public_html/typo3/typo3/sysext/extra_page_cm_options/ext_tables.php
###########################

$_EXTKEY = 'extra_page_cm_options';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	{
	$GLOBALS['TBE_MODULES_EXT']['xMOD_alt_clickmenu']['extendCMclasses'][]=array(
		'name' => 'tx_extrapagecmoptions',
		'path' => t3lib_extMgm::extPath($_EXTKEY).'class.tx_extrapagecmoptions.php'
	);
}
?><?php
###########################
## EXTENSION: impexp
## FILE:      /home/rabbit/public_html/typo3/typo3/sysext/impexp/ext_tables.php
###########################

$_EXTKEY = 'impexp';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	{
	$GLOBALS['TBE_MODULES_EXT']['xMOD_alt_clickmenu']['extendCMclasses'][]=array(
		'name' => 'tx_impexp_clickmenu',
		'path' => t3lib_extMgm::extPath($_EXTKEY).'class.tx_impexp_clickmenu.php'
	);

	t3lib_extMgm::insertModuleFunction(
		'user_task',
		'tx_impexp_modfunc1',
		t3lib_extMgm::extPath($_EXTKEY).'modfunc1/class.tx_impexp_modfunc1.php',
		'LLL:EXT:impexp/app/locallang.xml:moduleFunction.tx_impexp_modfunc1'
	);

	t3lib_extMgm::addLLrefForTCAdescr('xMOD_tx_impexp','EXT:impexp/locallang_csh.xml');
}
?><?php
###########################
## EXTENSION: sys_note
## FILE:      /home/rabbit/public_html/typo3/typo3/sysext/sys_note/ext_tables.php
###########################

$_EXTKEY = 'sys_note';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	{
	$TCA['sys_note'] = Array (
		'ctrl' => Array (
			'label' => 'subject',
			'default_sortby' => 'ORDER BY crdate',
			'tstamp' => 'tstamp',
			'crdate' => 'crdate',
			'cruser_id' => 'cruser',
			'prependAtCopy' => 'LLL:EXT:lang/locallang_general.php:LGL.prependAtCopy',
			'delete' => 'deleted',
			'title' => 'LLL:EXT:sys_note/locallang_tca.php:sys_note',
			'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY).'ext_icon.gif',
		),
		'interface' => Array (
			'showRecordFieldList' => 'category,subject,message,author,email,personal'
		),
		'columns' => Array (
			'category' => Array (
				'label' => 'LLL:EXT:lang/locallang_general.php:LGL.category',
				'config' => Array (
					'type' => 'select',
					'items' => Array (
						Array('', '0'),
						Array('LLL:EXT:sys_note/locallang_tca.php:sys_note.category.I.1', '1'),
						Array('LLL:EXT:sys_note/locallang_tca.php:sys_note.category.I.2', '3'),
						Array('LLL:EXT:sys_note/locallang_tca.php:sys_note.category.I.3', '4'),
						Array('LLL:EXT:sys_note/locallang_tca.php:sys_note.category.I.4', '2')
					),
					'default' => '0'
				)
			),
			'subject' => Array (
				'label' => 'LLL:EXT:sys_note/locallang_tca.php:sys_note.subject',
				'config' => Array (
					'type' => 'input',
					'size' => '40',
					'max' => '256'
				)
			),
			'message' => Array (
				'label' => 'LLL:EXT:sys_note/locallang_tca.php:sys_note.message',
				'config' => Array (
					'type' => 'text',
					'cols' => '40',
					'rows' => '15'
				)
			),
			'author' => Array (
				'label' => 'LLL:EXT:lang/locallang_general.php:LGL.author',
				'config' => Array (
					'type' => 'input',
					'size' => '20',
					'eval' => 'trim',
					'max' => '80'
				)
			),
			'email' => Array (
				'label' => 'LLL:EXT:lang/locallang_general.php:LGL.email',
				'config' => Array (
					'type' => 'input',
					'size' => '20',
					'eval' => 'trim',
					'max' => '80'
				)
			),
			'personal' => Array (
				'label' => 'LLL:EXT:sys_note/locallang_tca.php:sys_note.personal',
				'config' => Array (
					'type' => 'check'
				)
			)
		),
		'types' => Array (
			'0' => Array('showitem' => 'category;;;;2-2-2, author, email, personal, subject;;;;3-3-3, message')
		)
	);

	t3lib_extMgm::allowTableOnStandardPages('sys_note');
}

t3lib_extMgm::addLLrefForTCAdescr('sys_note','EXT:sys_note/locallang_csh_sysnote.xml');
?><?php
###########################
## EXTENSION: tstemplate
## FILE:      /home/rabbit/public_html/typo3/typo3/ext/tstemplate/ext_tables.php
###########################

$_EXTKEY = 'tstemplate';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	t3lib_extMgm::addModule('web','ts','',t3lib_extMgm::extPath($_EXTKEY).'ts/');
?><?php
###########################
## EXTENSION: tstemplate_ceditor
## FILE:      /home/rabbit/public_html/typo3/typo3/ext/tstemplate_ceditor/ext_tables.php
###########################

$_EXTKEY = 'tstemplate_ceditor';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	{
	t3lib_extMgm::insertModuleFunction(
		'web_ts',
		'tx_tstemplateceditor',
		t3lib_extMgm::extPath($_EXTKEY).'class.tx_tstemplateceditor.php',
		'Constant Editor'
	);
}
?><?php
###########################
## EXTENSION: tstemplate_info
## FILE:      /home/rabbit/public_html/typo3/typo3/ext/tstemplate_info/ext_tables.php
###########################

$_EXTKEY = 'tstemplate_info';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	{
	t3lib_extMgm::insertModuleFunction(
		'web_ts',
		'tx_tstemplateinfo',
		t3lib_extMgm::extPath($_EXTKEY).'class.tx_tstemplateinfo.php',
		'Info/Modify'
	);
}
?><?php
###########################
## EXTENSION: tstemplate_objbrowser
## FILE:      /home/rabbit/public_html/typo3/typo3/ext/tstemplate_objbrowser/ext_tables.php
###########################

$_EXTKEY = 'tstemplate_objbrowser';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	{
	t3lib_extMgm::insertModuleFunction(
		'web_ts',
		'tx_tstemplateobjbrowser',
		t3lib_extMgm::extPath($_EXTKEY).'class.tx_tstemplateobjbrowser.php',
		'TypoScript Object Browser'
	);
}
?><?php
###########################
## EXTENSION: tstemplate_analyzer
## FILE:      /home/rabbit/public_html/typo3/typo3/ext/tstemplate_analyzer/ext_tables.php
###########################

$_EXTKEY = 'tstemplate_analyzer';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	{
	t3lib_extMgm::insertModuleFunction(
		'web_ts',
		'tx_tstemplateanalyzer',
		t3lib_extMgm::extPath($_EXTKEY).'class.tx_tstemplateanalyzer.php',
		'Template Analyzer'
	);
}
?><?php
###########################
## EXTENSION: tstemplate_styler
## FILE:      /home/rabbit/public_html/typo3/typo3/ext/tstemplate_styler/ext_tables.php
###########################

$_EXTKEY = 'tstemplate_styler';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	{
	t3lib_extMgm::insertModuleFunction(
		'web_ts',		
		'tx_tstemplatestyler_modfunc1',
		t3lib_extMgm::extPath($_EXTKEY).'modfunc1/class.tx_tstemplatestyler_modfunc1.php',
		'CSS Styler'
	);
}
?><?php
###########################
## EXTENSION: func_wizards
## FILE:      /home/rabbit/public_html/typo3/typo3/sysext/func_wizards/ext_tables.php
###########################

$_EXTKEY = 'func_wizards';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	{
	t3lib_extMgm::insertModuleFunction(
		'web_func',
		'tx_funcwizards_webfunc',
		t3lib_extMgm::extPath($_EXTKEY).'class.tx_funcwizards_webfunc.php',
		'LLL:EXT:func_wizards/locallang.php:mod_wizards'
	);
	t3lib_extMgm::addLLrefForTCAdescr('_MOD_web_func','EXT:func_wizards/locallang_csh.xml');
}
?><?php
###########################
## EXTENSION: wizard_crpages
## FILE:      /home/rabbit/public_html/typo3/typo3/sysext/wizard_crpages/ext_tables.php
###########################

$_EXTKEY = 'wizard_crpages';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	{
	t3lib_extMgm::insertModuleFunction(
		'web_func',
		'tx_wizardcrpages_webfunc_2',
		t3lib_extMgm::extPath($_EXTKEY).'class.tx_wizardcrpages_webfunc_2.php',
		'LLL:EXT:wizard_crpages/locallang.php:wiz_crMany',
		'wiz'
	);
	t3lib_extMgm::addLLrefForTCAdescr('_MOD_web_func','EXT:wizard_crpages/locallang_csh.xml');
}
?><?php
###########################
## EXTENSION: wizard_sortpages
## FILE:      /home/rabbit/public_html/typo3/typo3/sysext/wizard_sortpages/ext_tables.php
###########################

$_EXTKEY = 'wizard_sortpages';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	{
	t3lib_extMgm::insertModuleFunction(
		'web_func',
		'tx_wizardsortpages_webfunc_2',
		t3lib_extMgm::extPath($_EXTKEY).'class.tx_wizardsortpages_webfunc_2.php',
		'LLL:EXT:wizard_sortpages/locallang.php:wiz_sort',
		'wiz'
	);
	t3lib_extMgm::addLLrefForTCAdescr('_MOD_web_func','EXT:wizard_sortpages/locallang_csh.xml');
}
?><?php
###########################
## EXTENSION: lowlevel
## FILE:      /home/rabbit/public_html/typo3/typo3/sysext/lowlevel/ext_tables.php
###########################

$_EXTKEY = 'lowlevel';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	{
	t3lib_extMgm::addModule('tools','dbint','',t3lib_extMgm::extPath($_EXTKEY).'dbint/');
	t3lib_extMgm::addModule('tools','config','',t3lib_extMgm::extPath($_EXTKEY).'config/');
}
?><?php
###########################
## EXTENSION: install
## FILE:      /home/rabbit/public_html/typo3/typo3/sysext/install/ext_tables.php
###########################

$_EXTKEY = 'install';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	t3lib_extMgm::addModule('tools','install','',t3lib_extMgm::extPath($_EXTKEY).'mod/');
?><?php
###########################
## EXTENSION: belog
## FILE:      /home/rabbit/public_html/typo3/typo3/sysext/belog/ext_tables.php
###########################

$_EXTKEY = 'belog';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	{
	t3lib_extMgm::addModule('tools','log','',t3lib_extMgm::extPath($_EXTKEY).'mod/');
	t3lib_extMgm::insertModuleFunction(
		'web_info',
		'tx_belog_webinfo',
		t3lib_extMgm::extPath($_EXTKEY).'class.tx_belog_webinfo.php',
		'Log'
	);
}
?><?php
###########################
## EXTENSION: beuser
## FILE:      /home/rabbit/public_html/typo3/typo3/sysext/beuser/ext_tables.php
###########################

$_EXTKEY = 'beuser';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	t3lib_extMgm::addModule('tools','beuser','top',t3lib_extMgm::extPath($_EXTKEY).'mod/');
?><?php
###########################
## EXTENSION: phpmyadmin
## FILE:      /home/rabbit/public_html/typo3/typo3/ext/phpmyadmin/ext_tables.php
###########################

$_EXTKEY = 'phpmyadmin';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	t3lib_extMgm::addModule('tools','txphpmyadmin','',t3lib_extMgm::extPath($_EXTKEY).'modsub/');
?><?php
###########################
## EXTENSION: aboutmodules
## FILE:      /home/rabbit/public_html/typo3/typo3/sysext/aboutmodules/ext_tables.php
###########################

$_EXTKEY = 'aboutmodules';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	t3lib_extMgm::addModule('help','aboutmodules','top',t3lib_extMgm::extPath($_EXTKEY).'mod/');
?><?php
###########################
## EXTENSION: imagelist
## FILE:      /home/rabbit/public_html/typo3/typo3/ext/imagelist/ext_tables.php
###########################

$_EXTKEY = 'imagelist';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	t3lib_extMgm::addModule('file','images','after:list',t3lib_extMgm::extPath($_EXTKEY).'mod/');
?><?php
###########################
## EXTENSION: setup
## FILE:      /home/rabbit/public_html/typo3/typo3/sysext/setup/ext_tables.php
###########################

$_EXTKEY = 'setup';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	{
	t3lib_extMgm::addModule('user','setup','after:task',t3lib_extMgm::extPath($_EXTKEY).'mod/');
	t3lib_extMgm::addLLrefForTCAdescr('_MOD_user_setup','EXT:setup/locallang_csh_mod.xml');
}
?><?php
###########################
## EXTENSION: taskcenter
## FILE:      /home/rabbit/public_html/typo3/typo3/sysext/taskcenter/ext_tables.php
###########################

$_EXTKEY = 'taskcenter';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	t3lib_extMgm::addModule('user','task','top',t3lib_extMgm::extPath($_EXTKEY).'task/');
?><?php
###########################
## EXTENSION: sys_notepad
## FILE:      /home/rabbit/public_html/typo3/typo3/ext/sys_notepad/ext_tables.php
###########################

$_EXTKEY = 'sys_notepad';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	{
	t3lib_extMgm::insertModuleFunction(
		'user_task',
		'tx_sysnotepad',
		t3lib_extMgm::extPath($_EXTKEY).'class.tx_sysnotepad.php',
		'LLL:EXT:sys_notepad/locallang.php:mod_note'
	);
}
?><?php
###########################
## EXTENSION: taskcenter_recent
## FILE:      /home/rabbit/public_html/typo3/typo3/ext/taskcenter_recent/ext_tables.php
###########################

$_EXTKEY = 'taskcenter_recent';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	{
	t3lib_extMgm::insertModuleFunction(
		'user_task',
		'tx_taskcenterrecent',
		t3lib_extMgm::extPath($_EXTKEY).'class.tx_taskcenterrecent.php',
		'LLL:EXT:taskcenter_recent/locallang.php:mod_recent'
	);
}
?><?php
###########################
## EXTENSION: taskcenter_rootlist
## FILE:      /home/rabbit/public_html/typo3/typo3/ext/taskcenter_rootlist/ext_tables.php
###########################

$_EXTKEY = 'taskcenter_rootlist';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	{
	t3lib_extMgm::insertModuleFunction(
		'user_task',
		'tx_taskcenterrootlist',
		t3lib_extMgm::extPath($_EXTKEY).'class.tx_taskcenterrootlist.php',
		'LLL:EXT:taskcenter_rootlist/locallang.php:mod_rootlist'
	);
}
?><?php
###########################
## EXTENSION: info_pagetsconfig
## FILE:      /home/rabbit/public_html/typo3/typo3/sysext/info_pagetsconfig/ext_tables.php
###########################

$_EXTKEY = 'info_pagetsconfig';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	{
	t3lib_extMgm::insertModuleFunction(
		'web_info',
		'tx_infopagetsconfig_webinfo',
		t3lib_extMgm::extPath($_EXTKEY).'class.tx_infopagetsconfig_webinfo.php',
		'LLL:EXT:info_pagetsconfig/locallang.php:mod_pagetsconfig'
	);
}

t3lib_extMgm::addLLrefForTCAdescr('_MOD_web_info','EXT:info_pagetsconfig/locallang_csh_webinfo.xml');

?><?php
###########################
## EXTENSION: viewpage
## FILE:      /home/rabbit/public_html/typo3/typo3/sysext/viewpage/ext_tables.php
###########################

$_EXTKEY = 'viewpage';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE=='BE')	t3lib_extMgm::addModule('web','view','after:layout',t3lib_extMgm::extPath($_EXTKEY).'view/');
?><?php
###########################
## EXTENSION: lastupdate
## FILE:      /home/rabbit/public_html/typo3/typo3conf/ext/lastupdate/ext_tables.php
###########################

$_EXTKEY = 'lastupdate';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ("TYPO3_MODE")) 	die ("Access denied.");

t3lib_extMgm::allowTableOnStandardPages("tx_lastupdate_custom");


t3lib_extMgm::addToInsertRecords("tx_lastupdate_custom");

$TCA["tx_lastupdate_custom"] = Array (
	"ctrl" => Array (
		"title" => "LLL:EXT:lastupdate/locallang_tca.php:tx_lastupdate_custom",		
		"label" => "uid",	
		"tstamp" => "tstamp",
		"crdate" => "crdate",
		"cruser_id" => "cruser_id",
		"default_sortby" => "ORDER BY crdate",	
		"delete" => "deleted",
		'prependAtCopy' => 'LLL:EXT:lang/locallang_general.php:LGL.prependAtCopy',
		
		'versioning' => TRUE,
		
		'copyAfterDuplFields' => 'sys_language_uid',
		'useColumnsForDefaultValues' => 'sys_language_uid',
		'transOrigPointerField' => 'l18n_parent',
		'transOrigDiffSourceField' => 'l18n_diffsource',
		'languageField' => 'sys_language_uid',
			
		"enablecolumns" => Array (		
			"disabled" => "hidden",
		),
		"dynamicConfigFile" => t3lib_extMgm::extPath($_EXTKEY)."tca.php",
		"iconfile" => t3lib_extMgm::extRelPath($_EXTKEY)."icon_tx_lastupdate_update.gif",
	),
	"feInterface" => Array (
		"fe_admin_fieldList" => "hidden, author_prefix, date_prefix, date_format, separate",
	)
);

t3lib_div::loadTCA("tt_content");

$TCA["tt_content"]["types"]["list"]["subtypes_excludelist"][$_EXTKEY."_pi1"]="layout,select_key,pages,recursive";
$TCA['tt_content']['types']['list']['subtypes_addlist'][$_EXTKEY.'_pi1']='pi_flexform';

t3lib_extMgm::addPlugin(Array("LLL:EXT:lastupdate/locallang_tca.php:tt_content.list_type_pi1", $_EXTKEY."_pi1"),"list_type");
t3lib_extMgm::addPiFlexFormValue($_EXTKEY.'_pi1', 'FILE:EXT:lastupdate/flexform_ds.xml');

t3lib_extMgm::addStaticFile($_EXTKEY,"pi1/static/","Last update");
?><?php
###########################
## EXTENSION: cron_cssstyledimgtext
## FILE:      /home/rabbit/public_html/typo3/typo3conf/ext/cron_cssstyledimgtext/ext_tables.php
###########################

$_EXTKEY = 'cron_cssstyledimgtext';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

t3lib_extMgm::addStaticFile($_EXTKEY,'static/','CSS Styled IMGTEXT');

$tempColumns = Array (
	'tx_croncssstyledimgtext_renderMethod' => Array (
		'exclude' => 1,
		'label' => 'LLL:EXT:cron_cssstyledimgtext/locallang_db.php:tt_content.tx_croncssstyledimgtext_renderMethod',
		'config' => Array (
			'type' => 'select',
			'items' => Array (
				Array('LLL:EXT:cron_cssstyledimgtext/locallang_db.php:tt_content.tx_croncssstyledimgtext_renderMethod.I.default', 'default'),
				Array('LLL:EXT:cron_cssstyledimgtext/locallang_db.php:tt_content.tx_croncssstyledimgtext_renderMethod.I.dl', 'dl'),
				Array('LLL:EXT:cron_cssstyledimgtext/locallang_db.php:tt_content.tx_croncssstyledimgtext_renderMethod.I.ul', 'ul'),
				Array('LLL:EXT:cron_cssstyledimgtext/locallang_db.php:tt_content.tx_croncssstyledimgtext_renderMethod.I.div', 'div'),
				Array('LLL:EXT:cron_cssstyledimgtext/locallang_db.php:tt_content.tx_croncssstyledimgtext_renderMethod.I.table', 'table'),
				Array('LLL:EXT:cron_cssstyledimgtext/locallang_db.php:tt_content.tx_croncssstyledimgtext_renderMethod.I.custom', 'custom'),
			),
			'default' => 'default'
		)
	),
	'tx_croncssstyledimgtext_altText' => Array (
		'exclude' => 1,
		'label' => 'LLL:EXT:cron_cssstyledimgtext/locallang_db.php:tt_content.tx_croncssstyledimgtext_altText',
		'config' => Array (
			'type' => 'text',
			'cols' => '30',
			'rows' => '3',
		)
	),
	'tx_croncssstyledimgtext_titleText' => Array (
		'exclude' => 1,
		'label' => 'LLL:EXT:cron_cssstyledimgtext/locallang_db.php:tt_content.tx_croncssstyledimgtext_titleText',
		'config' => Array (
			'type' => 'text',
			'cols' => '30',
			'rows' => '3',
		)
	),
	'tx_croncssstyledimgtext_longText' => Array (
		'exclude' => 1,
		'label' => 'LLL:EXT:cron_cssstyledimgtext/locallang_db.php:tt_content.tx_croncssstyledimgtext_longText',
		'config' => Array (
			'type' => 'text',
			'cols' => '30',
			'rows' => '3',
		)
	),
);

t3lib_div::loadTCA('tt_content');
t3lib_extMgm::addTCAcolumns('tt_content',$tempColumns,1);
t3lib_extMgm::addToAllTCAtypes('tt_content',
	'tx_croncssstyledimgtext_altText,'.
	'tx_croncssstyledimgtext_titleText,'.
# not working yet, idea would be to have this text shown in showpic.php (click-enlarge)
#	'tx_croncssstyledimgtext_longText,'.
	'tx_croncssstyledimgtext_renderMethod;;;;1-1-1,'.
	'','textpic,image');

?><?php
###########################
## EXTENSION: tt_calender
## FILE:      /home/rabbit/public_html/typo3/typo3/ext/tt_calender/ext_tables.php
###########################

$_EXTKEY = 'tt_calender';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

$TCA['tt_calender'] = Array (
	'ctrl' => Array (
		'label' => 'title',
		'default_sortby' => 'ORDER BY date',
		'tstamp' => 'tstamp',
		'prependAtCopy' => 'LLL:EXT:lang/locallang_general.php:LGL.prependAtCopy',
		'delete' => 'deleted',
		'type' => 'type',
		'enablecolumns' => Array (
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime'
		),
		'mainpalette' => 1,
		'typeicon_column' => 'type',
		'typeicons' => Array (
			'0' => 'tt_calender.gif',
			'1' => 'tt_calender_todo.gif'
		),
		'title' => 'LLL:EXT:tt_calender/locallang_tca.php:tt_calender',
		'useColumnsForDefaultValues' => 'type',
		'mainpalette' => 1,
		'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY).'ext_icon.gif',
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tca.php'
	)
);
$TCA['tt_calender_cat'] = Array (
	'ctrl' => Array (
		'label' => 'title',
		'tstamp' => 'tstamp',
		'delete' => 'deleted',
		'prependAtCopy' => 'LLL:EXT:lang/locallang_general.php:LGL.prependAtCopy',
		'crdate' => 'crdate',
		'title' => 'LLL:EXT:tt_calender/locallang_tca.php:tt_calender_cat',
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tca.php'
	)
);
t3lib_extMgm::addPlugin(Array('LLL:EXT:tt_calender/locallang_tca.php:tt_calender', '7'));
t3lib_extMgm::allowTableOnStandardPages('tt_calender');
t3lib_extMgm::addToInsertRecords('tt_calender');

t3lib_extMgm::addLLrefForTCAdescr('tt_calender','EXT:tt_calender/locallang_csh_ttcalen.php');
t3lib_extMgm::addLLrefForTCAdescr('tt_calender_cat','EXT:tt_calender/locallang_csh_ttcalenc.php');
?><?php
###########################
## EXTENSION: tt_news
## FILE:      /home/rabbit/public_html/typo3/typo3/ext/tt_news/ext_tables.php
###########################

$_EXTKEY = 'tt_news';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

$TCA['tt_news'] = Array (
	'ctrl' => Array (
		'title' => 'LLL:EXT:tt_news/locallang_tca.php:tt_news',
		'label' => 'title',
		'default_sortby' => 'ORDER BY datetime DESC',
		'tstamp' => 'tstamp',
		'delete' => 'deleted',
		'prependAtCopy' => 'LLL:EXT:lang/locallang_general.php:LGL.prependAtCopy',
		'crdate' => 'crdate',
		'type' => 'type',
		'enablecolumns' => Array (
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
			'fe_group' => 'fe_group',
		),
		'typeicon_column' => 'type',
		'typeicons' => Array (
			'1' => 'tt_news_article.gif',
			'2' => 'tt_news_exturl.gif',
		),
		'thumbnail' => 'image',
		'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY).'ext_icon.gif',
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tca.php'
	)
);
$TCA['tt_news_cat'] = Array (
	'ctrl' => Array (
		'title' => 'LLL:EXT:tt_news/locallang_tca.php:tt_news_cat',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'delete' => 'deleted',
		'prependAtCopy' => 'LLL:EXT:lang/locallang_general.php:LGL.prependAtCopy',
		'crdate' => 'crdate',
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tca.php'
	)
);

t3lib_extMgm::addPlugin(Array('LLL:EXT:tt_news/locallang_tca.php:tt_news', '9'));
t3lib_extMgm::allowTableOnStandardPages('tt_news');
t3lib_extMgm::addToInsertRecords('tt_news');

t3lib_extMgm::addLLrefForTCAdescr('tt_news','EXT:tt_news/locallang_csh_ttnews.php');
t3lib_extMgm::addLLrefForTCAdescr('tt_news_cat','EXT:tt_news/locallang_csh_ttnewsc.php');
?><?php
###########################
## EXTENSION: dk_picntext
## FILE:      /home/rabbit/public_html/typo3/typo3conf/ext/dk_picntext/ext_tables.php
###########################

$_EXTKEY = 'dk_picntext';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

t3lib_extMgm::allowTableOnStandardPages("tx_dkpicntext_main");


t3lib_extMgm::addToInsertRecords("tx_dkpicntext_main");

$TCA["tx_dkpicntext_main"] = Array (
	"ctrl" => Array (
		"title" => "LLL:EXT:dk_picntext/locallang_db.php:tx_dkpicntext_main",		
		"label" => "uid",	
		"tstamp" => "tstamp",
		"crdate" => "crdate",
		"cruser_id" => "cruser_id",
		"versioning" => "1",	
		"languageField" => "sys_language_uid",	
		"transOrigPointerField" => "l18n_parent",	
		"transOrigDiffSourceField" => "l18n_diffsource",	
		"sortby" => "sorting",	
		"delete" => "deleted",	
		"enablecolumns" => Array (		
			"disabled" => "hidden",	
			"fe_group" => "fe_group",
		),
		"dynamicConfigFile" => t3lib_extMgm::extPath($_EXTKEY)."tca.php",
		"iconfile" => t3lib_extMgm::extRelPath($_EXTKEY)."icon_tx_dkpicntext_main.gif",
	),
	"feInterface" => Array (
		"fe_admin_fieldList" => "sys_language_uid, l18n_parent, l18n_diffsource, hidden, fe_group, header, picture, description, thumbsize, popupsize",
	)
);


t3lib_div::loadTCA('tt_content');
$TCA['tt_content']['types']['list']['subtypes_excludelist'][$_EXTKEY.'_pi1']='layout,select_key';


t3lib_extMgm::addPlugin(Array('LLL:EXT:dk_picntext/locallang_db.php:tt_content.list_type_pi1', $_EXTKEY.'_pi1'),'list_type');


t3lib_extMgm::addStaticFile($_EXTKEY,"pi1/static/","Weitblick-IT -- Picture and Text");
?><?php
###########################
## EXTENSION: kj_imagelightbox
## FILE:      /home/rabbit/public_html/typo3/typo3conf/ext/kj_imagelightbox/ext_tables.php
###########################

$_EXTKEY = 'kj_imagelightbox';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');
$tempColumns = Array (
	"tx_kjimagelightbox_imagelightbox" => Array (		
		"exclude" => 1,		
		"label" => "LLL:EXT:kj_imagelightbox/locallang_db.php:tt_content.tx_kjimagelightbox_imagelightbox",		
		"config" => Array (
			"type" => "check",
		)
	),
);


t3lib_div::loadTCA("tt_content");
t3lib_extMgm::addTCAcolumns("tt_content",$tempColumns,1);
$GLOBALS['TCA']['tt_content']['palettes']['7']['showitem'] = 'image_link, image_zoom, tx_kjimagelightbox_imagelightbox';
?><?php
###########################
## EXTENSION: rotacion_img
## FILE:      /home/rabbit/public_html/typo3/typo3conf/ext/rotacion_img/ext_tables.php
###########################

$_EXTKEY = 'rotacion_img';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

t3lib_extMgm::allowTableOnStandardPages("tx_rotacionimg_imagenes");


t3lib_extMgm::addToInsertRecords("tx_rotacionimg_imagenes");

$TCA["tx_rotacionimg_imagenes"] = Array (
	"ctrl" => Array (
		"title" => "LLL:EXT:rotacion_img/locallang_db.php:tx_rotacionimg_imagenes",		
		"label" => "nombre",
		"tstamp" => "tstamp",
		"crdate" => "crdate",
		"cruser_id" => "cruser_id",
		"default_sortby" => "ORDER BY nombre",	
		"delete" => "deleted",	
		"enablecolumns" => Array (		
			"disabled" => "hidden",	
			"starttime" => "starttime",	
			"endtime" => "endtime",
		),
		"dynamicConfigFile" => t3lib_extMgm::extPath($_EXTKEY)."tca.php",
		"iconfile" => t3lib_extMgm::extRelPath($_EXTKEY)."icon_tx_rotacionimg_imagenes.gif",
	),
	"feInterface" => Array (
		"fe_admin_fieldList" => "hidden, starttime, endtime, nombre,imagenes, enlaces,rotacion_alt,rotacion_title ",
	)
);


t3lib_div::loadTCA('tt_content');

t3lib_extMgm::addPlugin(Array('LLL:EXT:rotacion_img/locallang_db.php:tt_content.CType_pi1', $_EXTKEY.'_pi1'),'CType');

$tempColumns = Array (
	"tx_rotacionimg_reg_rotacion_img" => Array (		
		"exclude" => 1,
		"label" => "LLL:EXT:rotacion_img/locallang_db.php:tt_content.tx_rotacionimg_reg_rotacion_img",
		"config" => Array (
			"type" => "group",	
			"internal_type" => "db",	
			"allowed" => "tx_rotacionimg_imagenes",	
			"size" => 10,	
			"minitems" => 0,
			"maxitems" => 10,
		)
	),
	"tx_rotacionimg_tiempo_rotacion_img" => Array (		
		"exclude" => 1,		
		"label" => "LLL:EXT:rotacion_img/locallang_db.php:tt_content.tx_rotacionimg_tiempo_rotacion_img",
		"config" => Array (
			"type" => "input",
			"size" => "4",
			"max" => "4",
			"eval" => "int",
			"checkbox" => "0",
			"range" => Array (
				"upper" => "1000",
				"lower" => "1"
			),
			"default" => 0
		)
	),
);


t3lib_div::loadTCA("tt_content");
t3lib_extMgm::addTCAcolumns("tt_content",$tempColumns,1);
$TCA['tt_content']['types'][$_EXTKEY.'_pi1']['showitem']='CType;;4;button;1-1-1, header;;3;;2-2-2,tx_rotacionimg_reg_rotacion_img;;;;3-3-3, tx_rotacionimg_tiempo_rotacion_img';
//t3lib_extMgm::addToAllTCAtypes("tt_content","tx_rotacionimg_reg_rotacion_img;;;;1-1-1, tx_rotacionimg_tiempo_rotacion_img");

$TCA_DESCR['tx_rotacionimg_imagenes']=Array(
	'columns' => Array(
		'nombre' => Array(
			'description' => 'nombre identificativo',
			'details' => 'introduzca un nombre identificativo del grupo de imágenes',
			'seeAlso' => 'tx_rotacionimg_imagenes:imagenes,tx_rotacionimg_imagenes:enlaces,tx_rotacionimg_imagenes:rotacion_alt,tx_rotacionimg_imagenes:rotacion_title',
		),
		'imagenes' => Array(
			'description' => 'seleccion de imagenes',
			'details' => 'seleccione las imagenes que formarán parte de este grupo de imágenes, el orden que seleccione es en el que se presentaran las imagenes',
			'seeAlso' => 'tx_rotacionimg_imagenes:nombre,tx_rotacionimg_imagenes:enlaces,tx_rotacionimg_imagenes:rotacion_alt,tx_rotacionimg_imagenes:rotacion_title',
		),
		'enlaces' => Array(
			'description' => 'enlaces de imagenes',
			'details' => 'introduzca los enlaces para cada imagen separados por comas, los enlaces vacios dirigiran el enlace a la propia pagina en que se encuentre el contenido que las muestra',
			'seeAlso' => 'tx_rotacionimg_imagenes:nombre,tx_rotacionimg_imagenes:imagenes,tx_rotacionimg_imagenes:rotacion_alt,tx_rotacionimg_imagenes:rotacion_title',
		),
		'rotacion_alt' => Array(
			'description' => 'alt de las imagenes',
			'details' => 'introduzca el texto alternativo para las imagenes,cada linea corresponde con una imagen , si una imagen no tiene texto alternativo pero tiene su elemento correspondiente en el campo title, presentará este, y si no hay en ninguno presentará el nombre de la imagen',
			'seeAlso' => 'tx_rotacionimg_imagenes:nombre,tx_rotacionimg_imagenes:imagenes,tx_rotacionimg_imagenes:enlaces,tx_rotacionimg_imagenes:rotacion_title',
		),
		'rotacion_title' => Array(
			'description' => 'title de las imagenes',
			'details' => 'introduzca el titulo para las imagenes,cada linea corresponde con una imagen , si una imagen no tiene titulo pero tiene su texto alternativo correspondiente en el campo alt, presentará este, y si no hay en ninguno presentará el nombre de la imagen',
			'seeAlso' => 'tx_rotacionimg_imagenes:nombre,tx_rotacionimg_imagenes:imagenes,tx_rotacionimg_imagenes:enlaces,tx_rotacionimg_imagenes:rotacion_alt',
		),
	),
);

 $TCA_DESCR['tt_content']=Array(
	'columns' => Array(
		'tx_rotacionimg_reg_rotacion_img' => Array(
			'description' => 'seleccion del grupo de imagenes',
			'details' => 'seleccione los distintos grupos de imagenes pertenecientes al tipo de registro de rotación de imagenes, las imagenes se presentaran en el orden que se presenten los grupos seleccionados',
			'seeAlso' => 'tt_content:tx_rotacionimg_tiempo_rotacion_img'
		),
		'tx_rotacionimg_tiempo_rotacion_img' => Array(
			'description' => 'tiempo para cambio de imagenes',
			'details' => 'indique el tiempo en segundos durante el que se mostraran las distintas imagenes, pasado el cual se mostrara la siguiente imagen',
			'seeAlso' => 'tt_content:tx_rotacionimg_reg_rotacion_img'
		),
	),

);
?><?php
###########################
## EXTENSION: goof_fotoboek
## FILE:      /home/rabbit/public_html/typo3/typo3conf/ext/goof_fotoboek/ext_tables.php
###########################

$_EXTKEY = 'goof_fotoboek';
$_EXTCONF = $TYPO3_CONF_VARS['EXT']['extConf'][$_EXTKEY];

?><?php
if (!defined ("TYPO3_MODE")) 	die ("Access denied.");

if (TYPO3_MODE=="BE")	{
#experimental
#	t3lib_extMgm::addModule("tools","txgooffotoboekM1","",t3lib_extMgm::extPath($_EXTKEY)."mod1/");
#/experimental
}


$tempColumns = Array (
	'tx_gooffotoboek_path' => Array (
		'label' => 'LLL:EXT:goof_fotoboek/locallang_db.php:tt_content.tx_gooffotoboek_path',
		'config' => Array (
			'type' => 'input',
			'size' => '80',
			'max' => '128',
			'eval' => 'trim'
		)
	),
	'tx_gooffotoboek_webpath' => Array (
		'label' => 'LLL:EXT:goof_fotoboek/locallang_db.php:tt_content.tx_gooffotoboek_webpath',
		'config' => Array (
			'type' => 'input',
			'size' => '80',
			'max' => '128',
			'eval' => 'trim'
		)
	),

"tx_gooffotoboek_function" => Array (		
"exclude" => 0,		
"label" => "LLL:EXT:goof_fotoboek/locallang_db.php:tt_content.tx_gooffotoboek_function",		
"config" => Array (
	"type" => "select",
			"items" => Array (
				Array("LLL:EXT:goof_fotoboek/locallang_db.php:tt_content.tx_gooffotoboek_function.I.0", "show"),
				Array("LLL:EXT:goof_fotoboek/locallang_db.php:tt_content.tx_gooffotoboek_function.I.2", "comment")
		)
	)
)
);

t3lib_div::loadTCA("tt_content");
t3lib_extMgm::addTCAcolumns("tt_content",$tempColumns,1);

#mgmApi new style static template.
#t3lib_extMgm::addStaticFile($_EXTKEY,"pi1/static/","Photobook");

$TCA["tt_content"]["types"]["list"]["subtypes_addlist"][$_EXTKEY."_pi1"]="tx_gooffotoboek_function;;;;1-1-1,tx_gooffotoboek_path;;;;1-1-1,tx_gooffotoboek_webpath;;;;1-1-1";

t3lib_div::loadTCA("tt_content");
$TCA["tt_content"]["types"]["list"]["subtypes_excludelist"][$_EXTKEY."_pi1"]="layout,select_key,pages";

#typo3 3.5 didn't like this function.
if (function_exists('t3lib_extMgm::addLLrefForTCAdescr')) {
	t3lib_extMgm::addLLrefForTCAdescr('tt_content','EXT:goof_fotoboek/lang/locallang_csh.php');
}
t3lib_extMgm::addPlugin(Array("LLL:EXT:goof_fotoboek/locallang_db.php:tt_content.list_type", $_EXTKEY."_pi1"),"list_type");

#t3lib_div::debug($TCA_DESCR);

?>