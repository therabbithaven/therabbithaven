<?php
require 'db_connect.php';	// database connect script.
include('check_login.php');
if ($_SESSION['logged_in'] == 0) {
        header('Location: login.php');
	die('You are not logged in so you cannot log out.');
}

unset($_SESSION['username']);
unset($_SESSION['password']);
// kill session variables
$_SESSION = array(); // reset session array
session_destroy();   // destroy session.
header('Location: login.php');
// redirect them to anywhere you like.
//
// unauthorized modification of Swapware or using parts of the codes in Swapware to make your own software is strictly prohibited  
?>
<!-- Swapware, copyright by bookfinder.us.  You may not modify this file without our permission.
Any attempt to copy/delete/view data not belonging to you, will result to your Swapware account being terminated! 
-->
