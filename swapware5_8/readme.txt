Swapware Version 5.x 

Major improvments from previous versions:
-----------------------
Header is now added to submit_website.php.  
  
 

-----------------------
About Swapware:

Swapware is a free software that is written in PHP that allows you 
to easily manage your reciprocal links and helps you dynamically check 
your link partners' link back and update the status so that it will be
taken down or remains up in the next static pages update. Static pages 
are automatically updated when a new user submit a link or you can manually 
update the static pages by clicking the "update files" button in 
the admin area. With a single click, your Link Directory is built. 


-------------------------------------------------------------------------------

Installing Swapware:

1) Swapare requires PHP to run.  All you need to do is unzip the file 
   swapware.zip and ftp the folder named "swapware5_x" to your webhosting account.
   You may rename the folder "swapware5_x" to any name you wish.  
   
2) After that go to your "login.php" file: 
   http://www.your_domain.com/your_swapware_folder/login.php and click "install" to register an account.

4) Issue "chmod 777" command to your swapware folder and login with your email and password. 

5) Click "Install" and your static page will be created and may be viewed at: 
   http://www.yourdomain.com/swapare/index.html.
   
6) Customize the Swapware to the look of your site by removing every line in header_templates.htm and
   replace with your own header that you are using for you site.   
   When you are done, go to the file "login.php" again and click the "update files" button in the 
   "admin area"  to apply the modification.

7) Installation is now complete.

If problem occur, contact swapware@bookfinder.us for free support. 
---------------------------------------------------------------------------
You will need to issue "chmod 777" to your swapware folder.  
To issue "chmod 777" command is easy, you may using cuteftp, or wsftp.  
After you login to your webhositing account, right click 
on the Swapware folder and click the command CHMOD, then issue chmod 777 
by checking all three read,write, and execute checkboxes.
---------------------------------------------------------------------------

To see a detailed description of Swapware, please visit:
http://www.bookfinder.us/swapware_demo/about.htm

Uninstall - Swapware DO NOT and is not able to place any file, read or write 
            any file outside of your Swapware folder. To uninstall, delete the 
            Swapware folder and all the files in it.
            -> Swapware will now be cleanly uninstalled. 
---------------------------------------------------------------------------



Swapware Modification Policy
--------------------------------------------------------------------------
1. You may modify the files makefile.php, submit_website_header.php, header_template.htm 
   and footer_template.htm to customize the Swapware for your website provided that the 
   link to bookfinder.us appear on your pages.

2. The description and the link to bookfinder.us may be modified but may not be completly removed.

3. Bookfinder.us has the final said on wheather your modification is acceptable.  

4. When in doubt, email us at swapware@bookfinder.us


FAQ: 
---------------------------------------------------------------------------------------

Q: Can I removed the "powered by" and the link to bookfinder.us?
R: Yes, you may remove them if you place a link to bookfinder.us on your homepage or if 
   you make a $50 donation. To remove it, delete the line that contain "powered by" in the 
   file "makefile.php". 

Q: Who ownes the Swapware? 
R: Swapware is created and owned by Bookfinder USA.

Q: Why use Swapware? 
R: Swapware is a time saving solution for webmaster who don't want to add every link 
   by hand into whatever category it belongs. It is perfect for busy webmaster who don't 
   have the time to keep adding links, checking on back links, and keeping track of 
   who dropped you links. 


Q: I already have a high search engine ranking why do I need to use Swapware? 
R: Great! However, there is no guarantee that search engines will maintain your ranking. 
   As search engine renew itself, you may eventually be moved down the ranking system. 
   The best way is to utilize the high ranking you have is to swap links with other sites. 
   With enough sites linking to you, you can be sure that you will continue to move up 
   the search engine ranking system. Use Swapware to help you manage your links. 


Q: What files can I modify in Swapware to customize my websites? 
R: You may any files in Swapware to whatever you think is the best for your site. However, 
   you must keep a link to bookfinder.us on your Swapware pages. All the modification you
   made to Swapware automatically becomes the property of bookfinder.us. Bookfinder.us has 
   the final say on whether your modification is acceptable. When in doubt, email us your 
   modified code for approval at swapware@bookfinder.us.


Q: Do I have access to my database incase you no longer provide this service? 
R: Yes. We intend to host the database for all our existing users free for life. However, 
   in the unlikly even that we will no longer provide this service to you, we will notify you 
   and provide you with all the source code and detailed instruction on how to set your 
   own Swapware database. 


Q: Can I purchase rights to run the database on my website? 
R: No, currently, we do not offer this service. 


Q: How often do your robots spider the web for reciprocal links? 
R: The spider is set up to automatically update the status once a week. 


Q: When your spider finds that a reciprocal link that is down, as I understand it, that particular 
   site's link on my page is automatically changed to "down" and the next time the pages are updated, 
   does it gets deleted? 
R: No, the link will not be deleted when the link is down; only the status of the database record is
   changed to "down". 


Q: I own a webhosting company, I was wondering whether it would be okay for me to bundle Swapware in 
   my webhosting package? What I mean is, I host the application on my domain for them, but they would be 
   registering with you individually.
R: Yes, you may do this as long as you do not claim Swapware as your own and follow the Swapware copyright notice
   in the license.txt. 


-------------------------------------------------------------------------------------------------------------
To see a more detailed description of Swapware, please visit:
http://www.bookfinder.us/swapware_demo/about.htm

Uninstall - To uninstall, delete the Swapware folder and all the files in it -> Swapware is now cleanly uninstalled. 

Copyright 2003-2004 Bookfinder.us
---------------------------------NOTICE----------------------------------------------------------------------
1. Bookfinder USA is the exclusive owner of the copyrights in and to the Swapware Link Exchange Software.
2. Unauthorized using parts of the code in Swapware to make your own software infringe the exclusive rights 
   of Bookfinder USA. 
3. Do not remove this file and any copyright notice from this software.

