<?
//----------------------GERMAN----------------------------------------------------------------
//----------------------2.2.0 begin------------------------------------------------------------
$ccclogin_1="Sie m�ssen den Benutzernamen und das Passwort korrekt ausf�llen!";
$ccclogin_2="Passwort Wiederholung war nicht korrekt!";
$ccclogin_3="Applikationsentwicklung &amp; Site Management Software";
$ccclogin_4="BACKOFFICE BENUTZER INITIALISIERUNG";
$ccclogin_5="Um ins Constructioner Backoffice zu gelangen definieren Sie einen<br>
        <b>passenden Benutzernamen und ein Passwort</b><br>
        (diese Login-Daten k�nnen jederzeit im Backoffice ge�ndert werden):";
$ccclogin_6="Geben Sie einen Benutzernamen an:";
$ccclogin_7="Geben Sie ein Passwort an:";
$ccclogin_8="Wiederholen Sie das Passwort:";
$ccclogin_9="W�hlen Sie die \"Hauptsprache\"*:";
$ccclogin_10="Hauptsprache</b>\": Die Constructioner� Professional Edition<br>
        bietet unlimitierte Mehrsprachigkeit. Beginnen Sie mit einer \"Hauptsprache\" <br>
        und erweitern Sie Ihre Applikation Schritt f�r Schritt um weitere Sprachen - <br>
        Texte und Tabelleninhalte werden in der \"Hauptsprache\" angezeigt, wenn keine<br>
        �bersetzung in der aktuellen Sprache vorhanden ist.";
$ccclogin_11="Weiter";
$ccclogin_12="LOGIN";
$ccclogin_13="ZUGRIFF VERWEIGERT!";
$ccclogin_14="Development Software<br>&nbsp;<br> Um die Website �ffentlich zug�nglich zu machen<br>
              ben�tigen Sie einen ";
$ccclogin_15="Demo<br>&nbsp;<br> F�r die kommerzielle Nutzung dieser Solution ben�tigen Sie einen ";
$ccclogin_16="Backoffice";
$ccclogin_17="Sie haben die ENDBENUTZER-LIZENZBESTIMMUNGEN nicht akzepitiert. <br>
Um nochmals zum Setup zu gelangen klicken Sie ";
$ccclogin_18="hier";
$ccclogin_19="ENDBENUTZER-LIZENZBESTIMMUNGEN";
$ccclogin_20="CANCEL";
$ccclogin_21="JA, ICH AKZEPTIERE";
$ccclogin_22="WARNUNG [Das optionale Feature \"Bildgr��enkonvertierung\" ist nicht korrekt eingerichtet]";
$ccclogin_23="Systemaufruf f�r den Bildkonvertierungstest:";
$ccclogin_24="WARNUNG [Das optionale Feature \"Bildgr��enkonvertierung\" ist nicht korrekt eingerichtet";
$ccclogin_25="FEHLER [Kein Zugriff zu MySql und deshalb auch nicht zur Datenbank -> Constructioner kann nicht korrekt arbeiten]";
$ccclogin_26="FEHLER [Kein Zugriff zur Datenbank -> Constructioner kann nicht korrekt arbeiten]";
$ccclogin_27="FEHLER [Constructioner wird Webseiten nicht korrekt generieren]";
$ccclogin_28="FEHLER [Das Hochladen wird nicht korrekt funktionieren]";
$ccclogin_29="O.K.";
$ccclogin_30="SETUP �BERPR�FUNG DER SYSTEMVORAUSSETZUNGEN ";
$ccclogin_31="Zugriffsrechte";
$ccclogin_32="Verzeichnis";
$ccclogin_33="Fehlerbehebung";
$ccclogin_34="Bildgr��enkonvertierung";
$ccclogin_35="Datenbank-Verbindung";
$ccclogin_36="MySQL-Verbindung";
$ccclogin_37="<b>Browser</b> (benutzerseitig)";
$ccclogin_38="&quot;Content-Editable&quot; verf�gbar";
$ccclogin_39="Ihr Browser unterst�tzt nicht die Objekteigenschaft &quot;Content-Editable&quot oder &quot;Active Scripting&quot;!; Benutzen Sie den Microsoft Internet Explorer 5.5 oder h�her!";
$ccclogin_40="&quot;Javascript&quot;  verf�gbar";
$ccclogin_41="Ihr Browser unterst�tzt einige &quot;Javascript&quot; Funktionalit�ten nicht oder sie sind deaktiviert. Aktivieren Sie &quot;Javascript&quot; Funktionen in Ihrem Browser (\"Active Scripting\") oder benutzen Sie den Microsoft Internet Explorer 5.5 oder h�her!";
$ccclogin_42="Die Box auf der rechten Seite sollte vier Pfeile beinhalten - wenn Sie darin stattdessen Zahlen
oder andere Buchstaben oder Zeichen sehen, dann fehlt auf Ihrem Computer der Zeichensatz \"Webdings\". In diesem
Fall m�ssen Sie diesen Zeichensatz auf Ihrem Computer installieren, damit es im Backoffice keine Anzeigefehler gibt";
$ccclogin_43="Achtung: Alle bestehenden Tabellen der Datenbank";
$ccclogin_44="werden gel�scht und durch Constructioner Tabellen ersetzt. Dieser Vorgang kann ein paar Minuten dauern.";
$ccclogin_45="Datenbank installieren";
$ccclogin_46="FEHLER";
$ccclogin_47="Zeichensatz Webdings</b> (benutzerseitig)";
$ccclogin_48="Datenbank";
$ccclogin_49="eingespielt";
$ccclogin_51="";
//----------------------2.2.0 end------------------------------------------------------------

?>
