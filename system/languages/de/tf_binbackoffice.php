<?
//----------------------GERMAN----------------------------------------------------------------
//----------------------2.2.0 begin------------------------------------------------------------
$cccp0104_1="BACKOFFFICE";
//----------------------------------------------------------------------------------
$cccp0105_1="Statistik Speicherplatz:";
$cccp0105_2="Hauptsprache:";
$cccp0105_3="Aktuelle Sprache:";
$cccp0105_4="Login:";
$cccp0105_5="+ + + IHR INTERNET BROWSER ODER IHRE FIREWALL L�SST KEINE COOKIES ZU + + +<br></font></b><font color=\"#A42700\">OR</font><b><font color=\"#A42700\"><br>+ + + SESSION TIMEOUT WEGEN ZU LANGER INAKTIVIT�T + + +";
//----------------------------------------------------------------------------------
$cccp0106_1="WEBSITE DEVELOPMENT";
$cccp0106_2="Site Erstellen";
$cccp0106_3="Erstellen von grafischen und funktionalen \"Hintergr�nden\" f�r Seiten.";
$cccp0106_5="Erstellen von Seiten in Kombination mit Layouts.";
$cccp0106_7="Entwickler Bibliothek";
$cccp0106_8="Tabellen erstellen und konfigurieren.";
$cccp0106_10="Erstellen eigener Variablen und Anzeige der momentanen Werte aller Variablen.";
$cccp0106_12="Aufbau einer Farbbibliothek f�r das Erstellen der Site und das Editieren der Inhalte (CMS).";
$cccp0106_14="Aufbau einer Styles-Bibliothek f�r das Erstellen der Site.";
$cccp0106_16="Aufbau einer Bilder-Bibliothek f�r das Erstellen der Site.";
$cccp0106_18="Aufbau einer Template-Bibliothek f�r das Erstellen der Site.";
$cccp0106_20="Funktionen";
$cccp0106_21="+++ Ausw�hlen +++";
$cccp0106_22="GO";
$cccp0106_23="WEBSITE MANAGEMENT";
$cccp0106_24="Site Management";
$cccp0106_25="Erstellen und konfigurieren von Profilen und deren Zugangsbestimmungen.";
$cccp0106_27="Erstellen und konfigurieren von Backoffice-Benutzern.";
$cccp0106_29="Kommunikation mit anderen Backoffice-Benutzern.";
$cccp0106_31="Erstellen von Newslettern und Versand an vordefinierte Mailadressen einer Tabelle.";
$cccp0106_33="Sichern, Zur�cksetzen oder R�cksichern der Datenbank.";
$cccp0106_35="Hauptsprache der Website festlegen und Mehrsprachigkeit f�e die Website erm�glichen, falls n�tig.";
$cccp0106_37="Basiseinstellungen der Applikation.";
$cccp0106_39="Analysieren, exportieren und l�schen von Statistikdaten der Website-Besucher.";
$cccp0106_41="Content Management";
$cccp0106_42="Administrieren und Pflegen der Tabelleninhalte.";
$cccp0106_44="CONTENT MANAGEMENT";
$cccp0106_45="Inhalte";
$cccp0106_46="Aufbau einer Bilder-Bibliothek f�r das Editieren der Inhalte im CMS Editor(CMS).";
$cccp0106_48="Aufbau einer Bildergr��en-Bibliothek f�r das Hochladen von Bildern (CMS).";
$cccp0106_50="Aufbau einer Datei-Bibliothek f�r den Einbau von Datei-Downloads im CMS Editor (CMS).";
$cccp0106_52="Aufbau einer HTML-Teile-Bibliothek f�r den Einbau von HTML-Teilen im CMS Editor (CMS).";
$cccp0106_54="Komponenten";
$cccp0106_55="Kommunikation";
$cccp0106_56="Allgemeine Einstellungen";
$cccp0106_57="Sichern/Wiederherstellen";
$cccp0106_58="Analyse";
$cccp0106_59="Aufbau einer PHP Code-Bilbliothek f�r Entwicklung.";
$cccp0106_60="DEV Spezial";
$cccp0106_61="Debugging-Log-Datei ansehen und leeren.";
//----------------------------------------------------------------------------------
$cccp0107_1="+++ Seite w�hlen +++";
$cccp0107_2="Seite:";
$cccp0107_3="Layout:";
$cccp0107_4="Hilfe";
$cccp0107_5="Beenden";
$cccp0107_6="CMS Modus";
$cccp0107_7="Vorschau Modus";
$cccp0107_8="+++ Layout ausw�hlen +++";
$cccp0107_9="Funktionen einf�gen";
$cccp0107_10="Funktionen einf�gen";
$cccp0107_11="Website Vorschau";
$cccp0107_12="Zuerst eine Seite auw�hlen!";
$cccp0107_13="Zuerst eine Seite auw�hlen!";
$cccp0107_14="Zuerst eine Seite auw�hlen!";
$cccp0107_15="Zuerst eine Seite auw�hlen!";
//----------------------------------------------------------------------------------
//----------------------2.2.0 end------------------------------------------------------------

?>
