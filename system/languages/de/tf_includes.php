<?
//----------------------GERMAN----------------------------------------------------------------
//----------------------2.2.0 begin------------------------------------------------------------
//-------------- This File is included in every script -----------------------------
$cccsystempage_showname[1]="Layouts";
$cccsystempage_showname[2]="Seiten";
$cccsystempage_showname[3]="Tabellen";
$cccsystempage_showname[4]="Variablen";
$cccsystempage_showname[5]="Farben";
$cccsystempage_showname[6]="Styles";
$cccsystempage_showname[7]="Bilder";
$cccsystempage_showname[8]="HTML-Teile";
$cccsystempage_showname[9]="Tabellen Editor";
$cccsystempage_showname[10]="Bilder";
$cccsystempage_showname[11]="Bildergr��en";
$cccsystempage_showname[12]="Dateien";
$cccsystempage_showname[13]="HTML-Teile";
$cccsystempage_showname[14]="Newsletter";
$cccsystempage_showname[15]="Interne Nachrichten";
$cccsystempage_showname[16]="Berechtigungsprofile";
$cccsystempage_showname[17]="Backoffice Benutzer";
$cccsystempage_showname[18]="Sprachen";
$cccsystempage_showname[19]="Einstellungen";
$cccsystempage_showname[20]="Meta Tags";
$cccsystempage_showname[21]="Datenbank";
$cccsystempage_showname[22]="Statistik";
$cccsystempage_showname[23]="PHP-Code Bibliothek";
$cccsystempage_showname[24]="Debugging";

//----------------------------------------------------------------------------------
//              FUNCTIONS
//----------------------------------------------------------------------------------

$cccfunctions_showname[1]="CMSEditor";
$cccfunctions_showname[2]="GuestBook";
$cccfunctions_showname[3]="ImageGallery";
$cccfunctions_showname[4]="ImageGalleryPro";
$cccfunctions_showname[5]="ImageLink";
$cccfunctions_showname[6]="ItemListMenu";
$cccfunctions_showname[7]="LanguageSelector";
$cccfunctions_showname[8]="PullDownMenu";
$cccfunctions_showname[9]="Search";
$cccfunctions_showname[10]="TableFieldSelector";
$cccfunctions_showname[11]="TableListMenu";
$cccfunctions_showname[12]="TableView";
$cccfunctions_showname[13]="TextLink";
$cccfunctions_showname[14]="Ticker";
$cccfunctions_showname[15]="Web Page Login";




$cccfunctions_maintable[1]="CMSEditor";
$cccfunctions_maintable[2]="pguestbook";
$cccfunctions_maintable[3]="pimagegallery";
$cccfunctions_maintable[4]="pimagegallerypro";
$cccfunctions_maintable[5]="ImageLink";
$cccfunctions_maintable[6]="ItemListMenu";
$cccfunctions_maintable[7]="LanguageSelector";
$cccfunctions_maintable[8]="ppulldownmenu";
$cccfunctions_maintable[9]="Search";
$cccfunctions_maintable[10]="TableFieldSelector";
$cccfunctions_maintable[11]="ptablelistmenu";
$cccfunctions_maintable[12]="TableView";
$cccfunctions_maintable[13]="ptextlink";
$cccfunctions_maintable[14]="Ticker";
$cccfunctions_maintable[15]="WebPageLogin";
//----------------------------------------------------------------------------------
//              DATE [Pulldown]
//----------------------------------------------------------------------------------
$cccdatedaybo[0]="Tag";
$cccdateyearbo[0]="Jahr";
$cccdatemonthbo[0]="Monat";
$cccdatemonthbo[1]="J�nner";
$cccdatemonthbo[2]="Februar";
$cccdatemonthbo[3]="M�rz";
$cccdatemonthbo[4]="April";
$cccdatemonthbo[5]="Mai";
$cccdatemonthbo[6]="Juni";
$cccdatemonthbo[7]="July";
$cccdatemonthbo[8]="August";
$cccdatemonthbo[9]="September";
$cccdatemonthbo[10]="Oktober";
$cccdatemonthbo[11]="November";
$cccdatemonthbo[12]="Dezember";
//----------------------------------------------------------------------------------
$cccp0102_1="CMS Modus";
$cccp0102_2="Vorschau Modus";
$cccp0102_3="Funktionen einf�gen";
//----------------------------------------------------------------------------------
$cccp0120_1="Applikationsentwicklungs &amp; Site Management Software";
$cccp0120_2="ZUGRIFF VERWEIGERT - m�gliche Ursachen ...";
$cccp0120_3="keine Zugangsberechtigung zu diesem Bereich";
$cccp0120_4="Session Time-Out (z.B. wegen l�ngerer Inaktivit�t)";
$cccp0120_5="in diesem Fall m�ssen Sie sich wieder einloggen";
$cccp0120_6="hier";
//----------------------------------------------------------------------------------
$cccp0134_1="Definierte Farben:";
//----------------------------------------------------------------------------------
$cccp0135_1="FEHLER: Kontrollsumme ist falsch! Datei k�nnte fehlerhaft sein! Siehe Backoffice Einstellungen";
$cccp0135_2="Datens�tze importiert!";
$cccp0135_3="FEHLER: Sie m�ssen eine Datei f�r das Hochladen ausw�hlen!!!";
//----------------------------------------------------------------------------------
$cccp0137_1="ATTENTION! Hohes Sicherheitsrisiko! Der Standard-Benutzer 'constructioner' existiert immer noch mit dem Standard Passwort! �ndern Sie das Passwort des Benutzers 'constructioner' oder l�schen Sie diesen Benutzer!";
$cccp0137_2="Sie haben";
$cccp0137_3="neue";
$cccp0137_4="in Ihren internen Nachrichten!";
$cccp0137_5="f�r die Sprache:";
$cccp0137_6="Hilfe";
$cccp0137_7="Nachrichten";
$cccp0137_8="Nachricht";
//----------------------------------------------------------------------------------
$cccfunctionsearch_1="Suchergebnisse:";
$cccfunctionsearch_2="<hr>";
//----------------------2.2.0 end------------------------------------------------------------

?>
