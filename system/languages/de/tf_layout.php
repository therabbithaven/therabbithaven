<?
//----------------------2.2.0 begin------------------------------------------------------------
$cccp0155_1="LAYOUTS";
$cccp0155_2="Warnung: c-Area Namen sind innerhalb des Layouts nicht einzigartig! Achten Sie darauf, dass c-Area Namen innerhalb eines Layouts nur einmal vorkommen!";
$cccp0155_3="Anzahl der c-Areas:";
$cccp0155_4="Layout:";
$cccp0155_5="Hinzuf�gen";
$cccp0155_6="Bearbeiten";
$cccp0155_7="Layout Name";
$cccp0155_8="Konvertieren/Datei hochladen";
$cccp0155_9="Dateiname";
$cccp0155_10="Herunterladen";
$cccp0155_11="L�schen";
$cccp0155_12="Dieses Layout kann nicht gel�scht werden, da es derzeit noch von Seiten verwendet wird";
$cccp0155_13="Sind Sie sicher? Dieses Layout zu l�schen bewirkt auch, dass alle damit verbundenen Einstellungen/Zuweisungen von c-areas und Inhalte von CMS-Feldern gel�scht werden";
$cccp0155_14="Keine Datei bisher hochgeladen!";
$cccp0155_15="";
$cccp0155_16="";
$cccp0155_17="Funktionen einf�gen";
$cccp0155_18="Hochladen";
$cccp0155_19="Herunterladen";
$cccp0155_20="L�schen";
$cccp0155_21="Editor";
$cccp0155_22="Erstellen eines Layouts mit C-Areas</a> | ";
$cccp0155_23="Layouts";
$cccp0155_24="Arbeiten mit der Mehrsprachigkeit";
$cccp0155_25="Zeichen &amp; Symbole:<br>
                &nbsp;<br>
                </b>Die Symbole am Beginn und Ende einer &quot;C-Area&quot;
                geben wertvolle Hinweise <br>
                �ber den momentanen Zustand einer C-Area:";
$cccp0155_26="Start";
$cccp0155_27="Ende";
$cccp0155_28="C-Area Zustand";
$cccp0155_29="Keine Funktion eingef�gt - Area ist leer*";
$cccp0155_30="Funktion im &quot;Layout&quot;**";
$cccp0155_31="Funktion in der &quot;Seite&quot;**";
$cccp0155_32="Funktion in einer &quot;Seite&quot; wird beim dynamischen Zusammenbau <br>
            vor der Funktion im &quot;Layout&quot bevorzugt**<br>
            (dies wird nur beim Bearbeiten von &quot;Seiten&quot; angezeigt)";
$cccp0155_33="*Layout Platzhalter zwischen einem &quot;Start-&quot; und einem &quot;Ende-Symbol&quot;
                einer <br>
                &quot;C-Area&quot; (z.B.: text, images, etc.) werden - falls vorhanden - angezeigt. <br>
                &nbsp;<br>
                **Die Anzahl der angezeigten Zeichen des PHP-Codes innerhalb der &quot;C-Areas&quot;
                <br>
                kann im Backoffice-Bereich &quot;Einstellungen&quot; begrenzt werden.";
$cccp0155_34="*Achtung:";
$cccp0155_35="Zu 'Open HTML Code' konvertieren";
$cccp0155_36="Bild hochladen";
$cccp0155_37="Hochladen";
$cccp0155_38="Index von";
$cccp0155_39="L�schen";
$cccp0155_40="Layout";
$cccp0155_41="Creator";
$cccp0155_42="HTML";
$cccp0155_43="Code";
$cccp0155_44="Name:";
$cccp0155_45="F�r das Erstellen von Layouts au�erhalb des Constructioners beachten Sie die Hinweise in der Hilfe!";
$cccp0155_46="";
$cccp0155_47="Creator";
$cccp0155_48="Open HTML Code";
$cccp0155_49="Achtung: Nach der Konvertierung kann das Layout nicht mehr mit dem CREATOR bearbeitet werden.";
$cccp0155_50="";
$cccp0155_51="";
//-------------------------------------------------------------------------------------------
$cccp0230_1="LAYOUTS";
$cccp0230_2="Creator";
$cccp0230_3="Layout:";
$cccp0230_4="RESET";
$cccp0230_5="SPEICHERN";
$cccp0230_6="Vorlage:";
$cccp0230_8="Laden";
$cccp0230_9="advanced";
$cccp0230_10="Speichern als";
$cccp0230_11="SPEICHERN";
$cccp0230_13="Sind Sie sicher?";
$cccp0230_14="L�schen";
$cccp0230_15="Seite:";
$cccp0230_16="Horiz:";
$cccp0230_17="linksb�ndig";
$cccp0230_18="zentriert";
$cccp0230_19="rechtsb�ndig";
$cccp0230_20="Breite:";
$cccp0230_21="Hintergrundbild:";
$cccp0230_22="Hintergrundfarbe";
$cccp0230_23="Zuweisen";
$cccp0230_24="BoxEinstellungen:";
$cccp0230_25="Box-Einstellungen kopieren";
$cccp0230_26="KOPIEREN";
$cccp0230_27="Box-Einstellungen einf�gen";
$cccp0230_28="EINF�GEN";
$cccp0230_29="Neue Box";
$cccp0230_30="Eine neue Box rechts einf�gen - horizontal";
$cccp0230_31="Eine neue Box darunter einf�gen - vertikal";
$cccp0230_32="L�sche markierte Box";
$cccp0230_33="EINF�GEN X";
$cccp0230_34="EINF�GEN Y";
$cccp0230_35="L�SCHEN";
$cccp0230_36="Horiz:";
$cccp0230_37="linksb�ndig";
$cccp0230_38="zentriert";
$cccp0230_39="rechtsb�ndig";
$cccp0230_40="Vert:";
$cccp0230_41="Oben";
$cccp0230_42="Mitte";
$cccp0230_43="Unten";
$cccp0230_44="kein Umbruch";
$cccp0230_45="Hintergrundbild:";
$cccp0230_46="H�he:";
$cccp0230_47="Breite:";
$cccp0230_48="Hintergrundfarbe";
$cccp0230_49="RahmenStil:";
$cccp0230_50="RahmenFarbe:";
$cccp0230_51="RahmenBreite:";
$cccp0230_52="Spacing:";
$cccp0230_53="Cellpadding:";
$cccp0230_54="Anzahl an C-Areas*:";
$cccp0230_55="Markierter Box zuweisen";
$cccp0230_56="* C-Areas sind Platzhalter um Funktionen einzuf�gen.";
$cccp0230_57="<b>Hinweis:</b> Zur Bearbeitung selektieren Sie eine Box im Fenster.";
$cccp0230_58="W�hlen Sie zuerst eine Box!";
$cccp0230_59="Zumindest eine Box muss ausgew�hlt sein";
$cccp0230_60="Zumindest eine Box muss ausgew�hlt sein";
$cccp0230_61="Zumindest eine Box muss �brig bleiben!";
$cccp0230_62="Zeilen-Einstellungen:";
//-------------------------------------------------------------------------------------------
$cccp0231_1="SPEICHERN";
$cccp0231_2="HTML Code:";
$cccp0231_3="Einf�gen einer C-AREA:";
$cccp0231_4="Bezeichnung:";
$cccp0231_5="Einf�gen";
$cccp0231_6="Layout:";
$cccp0231_7="LAYOUTS";
//----------------------2.2.0 end------------------------------------------------------------
?>
