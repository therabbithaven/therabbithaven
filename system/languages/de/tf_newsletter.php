<?
//----------------------2.2.0 begin------------------------------------------------------------
$cccp0215_1="NEWSLETTER";
$cccp0215_2="FEHLER: Sie m�ssen einen Namen ausf�llen und eine Tabelle ausw�hlen!";
$cccp0215_3="";
$cccp0215_4="Newsletter:";
$cccp0215_5="Name:";
$cccp0215_6="Daten [Email,etc.] von Tabelle:";
$cccp0215_7="Mail-Typ:";
$cccp0215_8="ASCII Text";
$cccp0215_9="HTML";
$cccp0215_10="Hinzuf�gen";
$cccp0215_11="Einstellungen";
$cccp0215_12="Newsletter";
$cccp0215_13="Mail-Typ";
$cccp0215_14="L�schen";
$cccp0215_15="Einstellungen";
$cccp0215_16="Html";
$cccp0215_17="Text";
$cccp0215_18="Sind Sie sicher?";
$cccp0215_19="L�schen";
//----------------------------------------------------------------------------------
$cccp0216_1="NEWSLETTER";
$cccp0216_2="Newsletter:";
$cccp0216_14="";
$cccp0216_15="Newsletter:";
$cccp0216_16="Tabelle:";
$cccp0216_17="Mail-Typ:";
$cccp0216_18="SPEICHERN";
$cccp0216_19="EXIT";
$cccp0216_20="Mail von (Antwort geht an):*";
$cccp0216_21="Mail an:";
$cccp0216_22="[W�hlen Sie das Tabellenfeld, dass die Email-Adresse enth�lt.]";
$cccp0216_23="[Optional:] Einschr�nkende Bedingung:";
$cccp0216_24="z.B. tablefieldname01='TRUE'";        //Attention: Don�t translate 'TRUE'!
$cccp0216_25="Betreff:";
$cccp0216_26="Mail Editor:";
$cccp0216_27="*Achtung:</b> Die Benutzung dieses Feldes ist optional!
Es sollte eine Email-Adresse beinhalten, <br>
an die die Email-Empf�nger automatisch antworten k�nnen.
Einige SMTP Servereinstellungen <br> verhindern solche alternative Absendeadressen.
Dies kann zu unerwarteten Problemen beim<br> Versenden f�hren. In diesem Fall lassen Sie
dieses Feld einfach leer.";
$cccp0216_28="Editor";
$cccp0216_29="Einstellungen";
$cccp0216_30="Generiere Mails";
$cccp0216_31="Sende Mails";
//----------------------------------------------------------------------------------
$cccp0219_1="NEWSLETTER";
$cccp0219_2="Newsletter:";
$cccp0219_7="ACHTUNG: G�ltige Mailadressen konnten nict gefunden werden. �berpr�fen Sie den Inhalt der Tabelle";
$cccp0219_8="in der AKTUELLEN Sprache:";
$cccp0219_9="Newsletter:";
$cccp0219_10="Mails in die Tabelle";
$cccp0219_11="eingef�gt";
$cccp0219_12="Mails mit ung�ltiger Email-Adresse wurden nicht eingef�gt";
$cccp0219_13="Sie m�ssen zuerst 'Mail An' ausf�llen und das Mail editieren!";
$cccp0219_14="+++ Tabellenfeld ausw�hlen +++";
$cccp0219_15="Newsletter:";
$cccp0219_16="Tabelle:";
$cccp0219_17="Mail-Typ:";
$cccp0219_18="SPEICHERN";
$cccp0219_28="Mail Generator:";
$cccp0219_29="Achtung: W�hrend diesem Vorgang werden alle Daten der Tabelle";
$cccp0219_30="gel�scht und mit neuen Mails gef�llt.";
$cccp0219_31="Generieren";
$cccp0219_32="[Dieser Vorgang stellt die Tabelle ";
$cccp0219_33="(wieder) neu her und f�llt sie mit neuen Mails!]";
$cccp0219_34="Hinweis:</b> Eingef�gte Platzhalter werden w�hrend der Mail-Generierung ersetzt.";
$cccp0219_35="Hinweis:</b> Alle generierten Mails k�nnen einzeln nachbearbeitet werden bevor sie verschickt werden -<br> �ffnen Sie infach die Tabelle";
$cccp0219_36="durch Verwendung des \"Tabellen Editors\".";
$cccp0219_37="Editor";
$cccp0219_38="Einstellungen";
$cccp0219_39="Generiere Mails";
$cccp0219_40="Sende Mails";
//----------------------------------------------------------------------------------
$cccp0220_1="NEWSLETTER";
$cccp0220_2="Newsletter:";
$cccp0220_3="Mails gesendet";
$cccp0220_4="Mails mit ung�ltiger Email-Adresse nicht gesendet";
$cccp0220_5="FEHLER: Verbindung zu einem localen SMTP Dienst fehlgeschlagen!";
$cccp0220_6="Versenden ist nicht m�glich! �berpr�fen Sie den SMTP-Server!";
$cccp0220_15="Newsletter:";
$cccp0220_16="Tabelle:";
$cccp0220_17="Mail-Typ:";
$cccp0220_18="SPEICHERN";
$cccp0220_37="Mails versenden:";
$cccp0220_38="JETZT versenden!";
$cccp0220_39="[Alle Mails der Tabelle";
$cccp0220_40="werden zu den entsprechenden Empf�ngern versendet!]";
$cccp0220_41="Editor";
$cccp0220_42="Einstellungen";
$cccp0220_43="Generiere Mails";
$cccp0220_44="Sende Mails";
//----------------------2.2.0 end------------------------------------------------------------

?>
