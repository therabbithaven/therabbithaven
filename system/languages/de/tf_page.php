<?
//----------------------2.2.0 begin------------------------------------------------------------
$cccp0156_1="SEITE";
$cccp0156_2="+++ Ausw�hlen +++";
$cccp0156_3="Seite:";
$cccp0156_4="Name [technisch]:";
$cccp0156_5="Titel:";
$cccp0156_6="Layout:";
$cccp0156_7="Position:";
$cccp0156_8="Level:";
$cccp0156_9="Hinzuf�gen";
$cccp0156_10="Bearbeiten";
$cccp0156_11="Einstellungen";
$cccp0156_12="Seitenname";
$cccp0156_13="Position";
$cccp0156_14="Level";
$cccp0156_15="L�schen";
$cccp0156_16="[Kein Layout zugewiesen]";
$cccp0156_17="Diese Seite ist als Startseite (Home) definiert. Gehen Sie zu den Einstellungen und stelle dort eine andere Startseite ein!";
$cccp0156_18="Sie m�ssen der Seite zuerst ein Layout zuweisen. Klicken Sie daf�r auf Einstellungen...";
$cccp0156_19="Funktionen einf�gen";
$cccp0156_20="Einstellungen";
$cccp0156_21="verschieben";
$cccp0156_22="Sind Sie sicher? Diese Seite zu l�schen wird alle Einstellungen von eingef�gten Funktionen in C-Areas und darin enthaltene CMS Inhalte l�schen!";
$cccp0156_23="L�schen";
$cccp0156_24="Weiterf�hrende Tipps & Tricks dazu finden Sie im Detail<br>
                in der &quot;Hilfe&quot;:";
$cccp0156_25="Seiten";
$cccp0156_26="Funktionen in Layouts oder Seiten einf�gen?";
$cccp0156_27="Es gibt keine generelle Regel, die besagt, ob eine Funktion in
              <br>
                eine &quot;Seite&quot; oder in ein &quot;Layout&quot; eingef�gt werden soll.<br>
                Funktionen in &quot;Layouts&quot;
                werden auf allen &quot;Seiten&quot;,<br>
                welche dieses &quot;Layout&quot; als &quot;Hintergrund&quot;
                benutzen, wirksam, au�er wenn <br>
                eine Funktion in eine &quot;Seite&quot; an der gleichen Stelle eingebaut wurde.</p>
              <p><b>Hinweis: </b>&quot; Funktionen in Seiten werden gegen�ber Funktionen in Layouts bevorzugt!&quot;
                .";
$cccp0156_28="Siehe auch:";
$cccp0156_29="Funktionen in Layouts und Seiten einf�gen";
$cccp0156_30="Zeichen &amp; Symbole [&quot;Funktionen einf�gen&quot;]:<br>
                &nbsp;<br>
                </b>Die Symbole am Anfang und am Ende einer C-Area
                geben wertvolle <br>
                Hinweise �ber den Status einer C-Area:";
$cccp0156_31="Beginn";
$cccp0156_32="Ende";
$cccp0156_33="C-Area Einf�gestatus";
$cccp0156_34="Keine Funktion eingef�gt - Area ist immer noch leer*";
$cccp0156_35="Funktion im &quot;Layout&quot;**";
$cccp0156_36="Funktion in der &quot;Seite&quot;**";
$cccp0156_37="Funktion in der &quot;Seite&quot;
            �berschreibt die Funktion im &quot;Layout&quot;**<br>
            (kann nur auf der Seite angezeigt werden)";
$cccp0156_38="*Platzhalter zwischen &quot;Start-&quot; und &quot;Ende-Symbol&quot; einer <br>
                &quot;C-Area&quot; (z.B.: Text, Bilder, etc) werden - falls vorhanden - angezeigt.<br>
                &nbsp;<br>
                **Die Anzahl der angezeigten Buchstaben von PHP-Code innerhalb eines Rahmens<br>
                einer &quot;C-Area&quot; kann in den &quot;Einstellungen&quot;
                limitiert werden.";
//----------------------------------------------------------------------------------
$cccp0157_1="SEITE";
$cccp0157_2="--ausw�hlen--";
$cccp0157_3="Seite:";
$cccp0157_4="SPEICHERN";
$cccp0157_5="�berblick";
$cccp0157_6="Titel:*";
$cccp0157_7="Layout:";
$cccp0157_8="PHP-Code:";
$cccp0157_9="Hinweis:&nbsp;Dieser PHP-Code wird VOR dem dynamischen Zusammenbau dieser \"Seite\" ausgef�hrt.";
$cccp0157_10="Einstellungen";
$cccp0157_11="z.B. 'Das ist der Titel' <br>[max. 60 Zeichen empfohlen, die Verwendung von Variablen ist m�glich]";
$cccp0157_12="Beschreibung:*";
$cccp0157_13="z.B. 'Hier steht jener kurze Beschreibungstext, der bei den meisten Suchmaschinen in der Trefferliste angezeigt wird'<br>
                          [max. 150 Zeichen empfohlen, die Verwendung von Variablen ist m�glich]";
$cccp0157_14="Stichw�rter:*";
$cccp0157_15="z.B. 'Stichwort,noch ein Stichwort,Stichwort3,...'
[max. 874 Zeichen empfohlen, Stichworte durch Kommata trennen, die Verwendung von Variablen ist m�glich]";
$cccp0157_16="Suchmaschinen:*";
$cccp0157_17="Meta-Tag f�r die Suchmaschinenindizierung";
$cccp0157_18="<b>* Hinweis</b>: Keine Angabe (leeres Textfeld) bedeutet, dass die allgemeinen Einstellungen des Men�s 'Meta Tag' verwendet werden.";
//----------------------------------------------------------------------------------
$cccp0190_1="+++ausw�hlen+++";
$cccp0190_2="ParameterSet:";
$cccp0190_3="+++select+++";
$cccp0190_4="Hilfe";
$cccp0190_5="SPEICHERN";
$cccp0190_6="EXIT";
$cccp0190_7="Area:";
$cccp0190_8="Bearbeiten:";
$cccp0190_9="Standard";
$cccp0190_10="Advanced (Zeige PHP-Code)";
$cccp0190_11="Funktion bzw. Code dieser C-Area wird gel�scht und diese �nderung automatisch gespeichert!";
$cccp0190_12="L�schen";
$cccp0190_13="Funktion:";
$cccp0190_14="PHPCode:";
$cccp0190_15="Hinweis:</b> Funktionen k�nnen mittels PHP-Code folgenderma�en ausgef�hrt und das Ergebnis einer C-Area zugeordnet werden: <br>
        Die Variable <b>\$THIS_AREA </b>repr�sentiert die aktuelle Area. Was auch immer dieser Variabel zugewiesen wird
         wird an der Stelle angezeigt, an der die C-Area sich befindet, wenn die Seite erzeugt wird. So k�nnen Sie zus�tzlichen PHP-Code dieser Variable zuordnen.
         In den meisten F�llen werden Funktionen C-Areas zugeordnet. Funktionsaufrufe haben die Syntax: <br>
         <div  align='center'><b>Function(\"ParameterSet\")</b></div> Entwicklungsspezifische \"ParameterSets\" von \"Funktionen\" k�nnen innerhalb des Men�punktes \"Funktionen\" im Backoffice angelegt, bearbeitet oder gel�scht werden.
         </div>";
$cccp0190_16="Anlegen/Bearbeiten";
//----------------------2.2.0 end------------------------------------------------------------

?>
