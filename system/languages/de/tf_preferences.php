<?
//----------------------2.2.0 begin------------------------------------------------------------
$cccp0114_1="";
$cccp0114_2="";
$cccp0114_3="";
$cccp0114_4="WebSite URL:";
$cccp0114_5="SPEICHERN";
$cccp0114_6="PUBLIC ACCESS Key:";
$cccp0114_7="PUBLIC ACCESS Benutzerprofil:";
$cccp0114_8="Startseite (Home):";
$cccp0114_9="Allgemeiner Seitentitel:";
$cccp0114_10="[Wenn nicht im Layout oder in der Seite spezifiziert]";
$cccp0114_11="Datumsanzeige:";
$cccp0114_12="Tag Position:";
$cccp0114_13="Monat Position:";
$cccp0114_14="Jahr Position:";
$cccp0114_15="Einstellbares Dezimalzeichen:";
$cccp0114_16="in Tabellenfeldern vom Typ \"Zahl\":";
$cccp0114_17="Hauptsprache:";
$cccp0114_18="Mehrsprachige WebSite:";
$cccp0114_19="ja";
$cccp0114_20="nein";
$cccp0114_21="Bildgr��enkonvertierung:";
$cccp0114_22="ein";
$cccp0114_23="aus";
$cccp0114_24="Jpeg-Qualit�t:";
$cccp0114_25="[F�r die korrekte Funktionsweise pr�fen Sie den ImageMagick-Pfad in der config.inc.php]";
$cccp0114_26="Statistik-Aufzeichnung:";
$cccp0114_27="ein";
$cccp0114_28="aus";
$cccp0114_29="Aufzeichnung statistischer Daten beim Besuchen der WebSite via Backoffice";
$cccp0114_32="Vorschau beim Funktionseinbau:";
$cccp0114_33="Maximale Anzahl der Zeichen, die innerhalb des Rahmens einer C-Area angezeigt werden:";
$cccp0114_34="Timeout Limit:";
$cccp0114_35="Server erlaubt das �ndern des Timeout Limits [\"Save-Mode\"�ist \"off\"]";
$cccp0114_36="Scriptausf�hrung wird beendet nach";
$cccp0114_37="Sekunden";
$cccp0114_38="Tabellen Editor Anzeige:";
$cccp0114_39="Max. Anzahl der Datens�tze pro Backoffice-Seite:";
$cccp0114_40="Tabellen Export/Import:";
$cccp0114_41="CSV Trennzeichen:";
$cccp0114_42="Checksumme:";
$cccp0114_43="ja";
$cccp0114_44="nein";
$cccp0114_45="EINSTELLUNGEN";
$cccp0114_46="Datum [Pop-Up]";
$cccp0114_47="vom Jahr";
$cccp0114_48="bis zum Jahr";
$cccp0114_49="Ordner Berechtigung:";
$cccp0114_50="gestatte Benutzern Ordner im Verzeichnis \"upload/images/\" anzulegen";
$cccp0114_51="URL-Kodierung:";
$cccp0114_52="Update-Information abrufen:";
//----------------------2.2.0 end------------------------------------------------------------

?>
