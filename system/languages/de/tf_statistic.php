<?
//----------------------2.2.0 begin------------------------------------------------------------
$cccp0160_1="STATISTIK";
$cccp0160_2="Hilfe";
$cccp0160_3="EXIT";
$cccp0160_4="Site";
$cccp0160_5="Seiten";
$cccp0160_6="Verlinkt von";
$cccp0160_7="Besuche";
$cccp0160_8="Benutzer";
$cccp0160_9="Sprachen";
$cccp0160_10="Browser";
$cccp0160_11="IP-Adressen";
$cccp0160_12="WebSite: ";
$cccp0160_13="Direkter Aufruf";
$cccp0160_14="Intern";
$cccp0160_15="ALLE SEITEN";
$cccp0160_16="Zeitraum:";
$cccp0160_17="Analyse:";
$cccp0160_18="Zeitraum:";
$cccp0160_19="Zeitr�ume";
$cccp0160_20="Seitenaufrufe";
$cccp0160_21="Seitenaufrufe";
$cccp0160_22="[Insgesamt]";
$cccp0160_23="Seitenaufrufe";
$cccp0160_24="[in % pro Zeitraum]";
$cccp0160_25="SITE";
$cccp0160_26="Insgesamt:";
$cccp0160_27="Applikationsentwicklung & Site Management Software";
$cccp0160_28="Insgesamt";
$cccp0160_29="Monate";
$cccp0160_30="Wochen";
$cccp0160_31="Tage";
$cccp0160_28="[Gesamt]";
$cccp0160_29="[Monate]";
$cccp0160_30="[Wochen]";
$cccp0160_31="[Tage]";
//----------------------------------------------------------------------------------
$cccp0213_1="STATISTIK";
$cccp0213_2="Pr�fen Sie die Datumseinstellungen. Falls erforderlich geben Sie f�hrende Nullen bei Tag- und Monatsangaben hinzu, Jahre m�ssen vierstellig angegeben werden.";
$cccp0213_3="Statistikdaten:";
$cccp0213_4="Zeitraum";
$cccp0213_5="bis";
$cccp0213_6="Zuweisen";
$cccp0213_7="Statistikdaten anzeigen:";
$cccp0213_8="Site";
$cccp0213_9="Seiten";
$cccp0213_10="Verlinkt von";
$cccp0213_11="Besuche";
$cccp0213_12="Benutzer";
$cccp0213_13="Sprachen";
$cccp0213_14="Browser";
$cccp0213_15="IP-Addresses";
$cccp0213_16="Zeitraum:";
$cccp0213_17="Insgesamt";
$cccp0213_18="Monate";
$cccp0213_19="Wochen";
$cccp0213_20="Tage";
$cccp0213_21="Ansehen";
$cccp0213_22="Statistikdaten herunterladen:";
$cccp0213_23="Statistikdaten innerhalb des ausgew�hlten Zeitraums herunterladen";
$cccp0213_24="Herunterladen";
$cccp0213_25="Statistikdaten l�schen:";
$cccp0213_26="Aktueller Speicherplatz*:";
$cccp0213_27="verf�gbar";
$cccp0213_28="Sind Sie sichert? Daten innerhalb des ausgew�hlten Zeitraumes werden gel�scht!";
$cccp0213_29="Statistikdaten innerhalb des ausgew�hlten Zeitraums l�schen";
$cccp0213_30="L�schen";
$cccp0213_31="Achtung:</b> Speicherplatz &quot;0&nbsp;%&quot; verf�gbar bedeutet, dass keine Statistikdaten
        zus�tzlich gespeichert<br> werden k�nnen. Der Speicher ist voll. Um das zu vermeiden, l�schen Sie rechtzeitig alte Daten. </p>
      <p>Das Herunterladen der Daten erm�glicht die Analyse in anderen Programmen mit anderen Methoden.
        Die Gr��e <br>
        des Speicherplatzes h�ngt von der Konfiguration in den &quot;Einstellungen&quot;
        ab.</p>";
//----------------------------------------------------------------------------------
$cccp0233_1="STATISTIK";
$cccp0233_3="Statistikdaten:";
$cccp0233_4="Zeitraum";
$cccp0233_5="bis";
$cccp0233_6="Zuweisen";
$cccp0233_8="Site";
$cccp0233_9="Seiten";
$cccp0233_10="Verlinkt von";
$cccp0233_11="Besuche";
$cccp0233_12="";
$cccp0233_13="Sprachen";
$cccp0233_14="Browser";
$cccp0233_16="Summenbildung:";
$cccp0233_17="Insgesamt";
$cccp0233_18="Monate";
$cccp0233_19="Wochen";
$cccp0233_20="Tage";
$cccp0233_21="Ansehen";
//----------------------------------------------------------------------------------
$cccp0233_label[1]="Zeitraum:";
$cccp0233_label[2]="Summenbildung:";
$cccp0233_label[3]="Tabelle:";
$cccp0233_label[4]="Feld:";
$cccp0233_label[5]="Wert";
$cccp0233_label[6]="Grafikdarstellung";
$cccp0233_label[7]="Sortiere nach dieser Spalte";
$cccp0233_label[8]="Sortiere";
$cccp0233_label[9]="Sortiere nach dieser Spalte";
$cccp0233_label[10]="Grafikdarstellung";
$cccp0233_label[11]="Gesamt";
$cccp0233_label[12]="Gesamt";
$cccp0233_label[13]="Gesamt";
$cccp0233_label[14]="absteigend";
$cccp0233_label[15]="aufsteigend";
$cccp0233_label[16]="Gesamt";
$cccp0233_label[17]="Monate";
$cccp0233_label[18]="Wochen";
$cccp0233_label[19]="Tage";
//----------------------2.2.0 end------------------------------------------------------------

?>
