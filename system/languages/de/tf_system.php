<?
//----------------------2.2.0 begin------------------------------------------------------------
$cccp0163_1="DATENBANK";
$cccp0163_2="benutzte Datenbank:";
$cccp0163_3="Datenbank Sicherung";
$cccp0163_4="Upgrade Database-Dump";
$cccp0163_5="Nur f�r Versionen vor 2.5.0 notwendig!<br>Folgendes wird automatisch ersetzt:<br>
upload/dev_images/  -->     upload/images/Development/   <br>
upload/cms_images/  -->     upload/images/CMSEditor/   <br>
upload/cms_files/   -->     upload/files/CMSEditor/   <br>
CMS(                -->     CMSEditor(                 <br>
cmscontent          -->     cmseditorcontent                 <br>";
$cccp0163_11="Alle Einstellungen und Inhalte der Datenbank l�schen:";
$cccp0163_12="Sind Sie sicher? WICHTIG: Nach dem L�schen wird der gesamte Inhalt der Datenbank GEL�SCHT!";
$cccp0163_16="Datenbank auf NULL zur�cksetzen";
$cccp0163_18="Datenbank-Dump hochladen und einspielen:";
$cccp0163_19="Datenbank-Datei:";
$cccp0163_20="Optional - Suche und ersetze diese Zeichenkette in der Datenbank w�hrend dem Hochladen:";
$cccp0163_21="Suche nach:";
$cccp0163_22="Ersetze durch:";
$cccp0163_23="Sind Sie sicher? Alle aktuellen Daten werden gel�scht und die Datenbank wird hochgeladen und eingespielt.";
$cccp0163_24="Datenbank einspielen";
$cccp0163_25="Datenbank-Dump auf dem Server speichern:";
//-------------------------------------------------------------------------------------------
$cccp0228_1="Meta Tags";
$cccp0228_2="Meta-Tag Einstellungen";
$cccp0228_3="SPEICHERN";
$cccp0228_4="Wichtig:";
$cccp0228_5="<p>
Um die korrekte Funktionalit�t der nachstehenden Einstellungen zu gew�hrleisten, d�rfen die verwendeten HTML-Layouts KEINE
Titel-Angabe '&lt;title&gt;...&lt;/title&gt;' und KEINE Meta-Angaben wie z.B. '&lt;meta name=...&gt;' oder '&lt;meta http-equiv=...&gt;' enthalten.
Sollte Ihr HTML-Editor diese Tags automatisch erzeugen, so entfernen Sie diese bitte manuell aus den HTML-Kopfdaten Ihres Layouts.</p>";
$cccp0228_6="Titel:";
$cccp0228_7="z.B. 'Das ist der Titel'<br>
[max. 60 Zeichen empfohlen, die Verwendung von Variablen ist m�glich]";
$cccp0228_8="Beschreibung:";
$cccp0228_9="z.B. 'Hier steht jener kurze Beschreibungstext, der bei den meisten Suchmaschinen in der Trefferliste angezeigt wird'
[max. 150 Zeichen empfohlen, die Verwendung von Variablen ist m�glich]";
$cccp0228_10="Stichw�rter:";
$cccp0228_11="z.B. 'Stichwort,noch ein Stichwort,Stichwort3,...'
[max. 874 Zeichen empfohlen, Stichworte durch Kommata trennen, die Verwendung von Variablen ist m�glich]";
$cccp0228_12="Autor:";
$cccp0228_13="z.B. 'Artware Multimedia GmbH | www.artware.info'";
$cccp0228_14="Datum:";
$cccp0228_15="Erstellungsdatum der Website";
$cccp0228_16="Suchmaschinen:*";
$cccp0228_17="Meta-Tag f�r die Suchmaschinenindizierung";
$cccp0228_18="Ablaufzeit:";
$cccp0228_19="z.B. '0' f�r Null Sekunden [empfohlen] oder 'Fri, 22 Aug 2003 20:18:00 GMT' zur Angabe eines Ablaufdatums";
$cccp0228_20="Zeichensatz:";
$cccp0228_21="Die richtige Zeichensatz-Angabe wird analog zur gew�hlten Sprache definiert. Bei mehrsprachigen Websites<br>
wird der Zeichnsatz an die jeweilige 'Aktuelle Sprache' automatisch angepasst (siehe auch Men� 'Sprachen').";
$cccp0228_22="Hier k�nnen Sie bei Bedarf noch weitere Meta-Tags zu den HTML-Kopfdaten hinzuf�gen:";
$cccp0228_23="z.B. (System Dublin Core): '&lt;meta name=&quot;DC.Publisher&quot; content =&quot;Mustermann GmbH&quot;&gt;'";
$cccp0228_24="<b>* Hinweis</b>:  In den Seiteneinstellungen (Men� 'Seiten') k�nnen diese Tags auch seitenspezifisch definiert werden.";
//-------------------------------------------------------------------------------------------
$cccp0232_1="DEBUGGING";
$cccp0232_2="Log-Datei Bearbeiten";
$cccp0232_3="Datei:";
$cccp0232_4="SPEICHERN";
$cccp0232_5="L�schen & Speichern";
//----------------------2.2.0 end------------------------------------------------------------

?>
