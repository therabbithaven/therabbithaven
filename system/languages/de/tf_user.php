<?
//----------------------2.2.0 begin------------------------------------------------------------
$cccp0174_1="BERECHTIGUNGSPROFILE";
$cccp0174_2="Profil:";
$cccp0174_3="Name:";
$cccp0174_4="Hinzuf�gen";
$cccp0174_5="Profilname";
$cccp0174_6="Zugangsberechtigungen";
$cccp0174_7="L�schen";
$cccp0174_8="Sind Sie sicher?";
$cccp0174_9="Dieses Profil kann nicht gel�scht werden, weil es bei Benutzern noch eingetragen ist ";
$cccp0174_10="Seiten";
$cccp0174_11="Tabellen";
$cccp0174_12="Backoffice";
$cccp0174_13="Profile";
$cccp0174_14="L�schen";
//----------------------------------------------------------------------------------
$cccp0175_1="BERECHTIGUNGSPROFILE";
$cccp0175_2="Zugangsprofile:";
$cccp0175_3="SPEICHERN";
$cccp0175_4="EXIT";
$cccp0175_5="Seite";
$cccp0175_6="Kein Zugang";
$cccp0175_7="Zugang";
$cccp0175_8="Berechtigungen";
$cccp0175_9="C-Areas";
$cccp0175_10="Seiten";
$cccp0175_11="Tabellen";
$cccp0175_12="Backoffice";
$cccp0175_13="Profile";
//----------------------------------------------------------------------------------
$cccp0176_1="BACKOFFICE BENUTZER";
$cccp0176_2="FEHLER: Der Benutzer 'PUBLIC' wird vom System als Benutzer f�r den anonymen WebSite-Besucher benutzt!";
$cccp0176_3="Benutzer:";
$cccp0176_4="hinzuf�gen";
$cccp0176_5="Einstellungen";
$cccp0176_6="Benutzer";
$cccp0176_7="Online";
$cccp0176_8="L�schen";
$cccp0176_9="Einstellungen";
$cccp0176_10="Sind Sie sicher?";
$cccp0176_11="L�schen";
//----------------------------------------------------------------------------------
$cccp0177_1="BACKOFFICE BENUTZER";
$cccp0177_2="Backoffice";
$cccp0177_3="Backoffice Modus";
$cccp0177_4="CMS Modus";
$cccp0177_5="Vorschau Modus";
$cccp0177_6="SPEICHERN";
$cccp0177_7="�berblick";
$cccp0177_8="Passwort:";
$cccp0177_9="Passwort wiederholen:";
$cccp0177_10="Startseite nach dem Login:";
$cccp0177_11="Modus nach dem Login:";
$cccp0177_12="Aktuelle Sprache nach dem Login:";
$cccp0177_13="Hilfe erscheint nach dem Login:";
$cccp0177_14="Berechtigungsprofil:";
$cccp0177_15="E-Mailadresse f�r interne Nachrichten:";
$cccp0177_16="Bild Vorschau:";
$cccp0177_17="Das Wiederholen der Passworteingabe war nicht korrekt! Versuchen Sie es noch einmal!";
$cccp0177_18="Sprache f�r das Backoffice:";
$cccp0177_19="Einstellungen";
$cccp0177_20="Benutzer:";
//----------------------------------------------------------------------------------
$cccp0183_1="BERECHTIGUNGSPROFILE";
$cccp0183_2="Berechtigungsprofile:";
$cccp0183_3="SPEICHERN";
$cccp0183_4="EXIT";
$cccp0183_5="Tabellen";
$cccp0183_6="Unsichtbar";
$cccp0183_7="Sichtbar";
$cccp0183_8="Editierbar";
$cccp0183_9="Seiten";
$cccp0183_10="Tabellen";
$cccp0183_11="Backoffice";
$cccp0183_12="Profile";
//----------------------------------------------------------------------------------
$cccp0184_1="BERECHTIGUNGSPROFILE";
$cccp0184_2="Berechtigungsprofile:";
$cccp0184_3="SPEICHERN";
$cccp0184_4="EXIT";
$cccp0184_5="Backoffice Zonen";
$cccp0184_6="Verf�gbar";
$cccp0184_7="Spezialzug�nge";
$cccp0184_8="Seperater Zugang zu jeder Funktion";
$cccp0184_9="Seiten";
$cccp0184_10="Tabellen";
$cccp0184_11="Backoffice";
$cccp0184_12="Profile";
$cccp0184_13="Development - Site Erstellen";
$cccp0184_14="Development - Funktionen  [Plugins]";
$cccp0184_15="CMS - Inhalte";
$cccp0184_16="CMS - Komponenten";
$cccp0184_17="CMS - Kommunikation";
$cccp0184_18="Allgemeines - Allgemeine Einstellungen";
$cccp0184_19="Allgemeines - Sichern/Wiederherstellen";
$cccp0184_20="Allgemeines - Analyse";
$cccp0184_21="Development - DEV Spezial"; 
//----------------------------------------------------------------------------------
$cccp0185_1="BERECHTIGUNGSPROFILE [Pages / C-Areas]";
$cccp0185_2="Berechtigungsprofile:";
$cccp0185_3="Seite:";
$cccp0185_4="SPEICHERN";
$cccp0185_5="EXIT";
$cccp0185_6="C-Area*";
$cccp0185_7="Unsichtbar";
$cccp0185_8="Sichtbar";
$cccp0185_9="Editierbar";
$cccp0185_10="*Hinweis:</b> Es hat keine Auswirkungen, ob C-Areas <br>auf  \"Editierbar\" oder \"Sichtbar\" gestellt sind, solange nicht die<br>the \"CMS-Editor-Funktionalit�t\" dort eingebaut ist.<br><br><br>Im Moment bezieht sich \"Editierbar\" nur auf \"C-Areas\", die den<br>\"CMS-Editor\" eingebaut haben. C-Areas mit dieser Funktion <br> bekommen im \"CMS-Modus\" ein Bleistiftsymbol und k�nnen<br> f�r das Editieren der Inhalte ge�ffnet werden.";
$cccp0185_11="Seiten";
$cccp0185_12="Tabellen";
$cccp0185_13="Backoffice";
$cccp0185_14="Profile";
//----------------------------------------------------------------------------------
$cccp0186_1="BERECHTIGUNGSPROFILE";
$cccp0186_2="Berechtigungsprofile:";
$cccp0186_3="SPEICHERN";
$cccp0186_4="EXIT";
$cccp0186_5="Berechtigungsprofile";
$cccp0186_6="Sichtbar f�r";
$cccp0186_7="Unsichtbar f�r";
$cccp0186_8="Seiten";
$cccp0186_9="Tabellen";
$cccp0186_10="Backoffice";
$cccp0186_11="Profile";
//----------------------2.2.0 end------------------------------------------------------------

?>
