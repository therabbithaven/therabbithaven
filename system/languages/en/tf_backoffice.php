<?
//----------------------ENGLISH----------------------------------------------------------------
//----------------------2.2.0 begin------------------------------------------------------------
$ccclogin_1="You have to fill out username and password properly!";
$ccclogin_2="Password retyping was not correct!";
$ccclogin_3="Application Development &amp; Site Management Software";
$ccclogin_4="BACKOFFICE USER INITIALIZATION";
$ccclogin_5="To open the Constructioner Backoffice please define an <br>
        <b>arbitrary User Name and a Password</b><br>
        (these login data can be changed within the Backoffice at any time):";
$ccclogin_6="Define a User Name:";
$ccclogin_7="Define a Password:";
$ccclogin_8="Reenter your Password:";
$ccclogin_9="Choose the \"Main Content Language\"*:";
$ccclogin_10="Main Content Language</b>\": The Constructioner® Professional Edition<br>
        provides unlimited multilingualism. Start your website with a \"Main <br>
        Content Language\" and enhance further languages step by step - <br>
        the content of text-areas and tables will be displayed in the \"Main <br>
        Content Language\" until a translation has been added.";
$ccclogin_11="Next";
$ccclogin_12="LOGIN";
$ccclogin_13="ACCESS RESTRICTED!";
$ccclogin_14="Development Software<br>&nbsp;<br> To provide public access to this website get your ";
$ccclogin_15="Trial Version<br>&nbsp;<br> For commercial use get a ";
$ccclogin_16="Backoffice";
$ccclogin_17="You did not agreed to the END USER LICENCE AGREEMENT. For starting Setup again click ";
$ccclogin_18="here";
$ccclogin_19="END USER LICENCE AGREEMENT (EULA)";
$ccclogin_20="CANCEL";
$ccclogin_21="YES, I AGREE";
$ccclogin_22="WARNING [The optional feature \"Image size-conversion\" is not configured correctly]";
$ccclogin_23="Conversion Command for this Test:";
$ccclogin_24="WARNING [The optional feature \"Image size-conversion\" is not configured correctly]";
$ccclogin_25="ERROR [No Access to MySql and therefore to Database -> Constructioner will not work at all]";
$ccclogin_26="ERROR [No Access to Database -> Constructioner will not work at all]";
$ccclogin_27="ERROR [Constructioner will not generate WebPages correctly]";
$ccclogin_28="ERROR [Uploading Process will not work]";
$ccclogin_29="O.K.";
$ccclogin_30="SETUP PRECONDITIONS CHECKUP";
$ccclogin_31="Permission Rights";
$ccclogin_32="Directory";
$ccclogin_33="Troubleshooting";
$ccclogin_34="Image Resizing";
$ccclogin_35="Database-Connection";
$ccclogin_36="MySQL-Connection";
$ccclogin_37="<b>Browser</b> (client-side)";
$ccclogin_38="&quot;Content-Editable&quot; available";
$ccclogin_39="Your Browser does not support &quot;Content-Editable Objects&quot; or &quot;Active Scripting&quot;! We recommend to use Microsoft Internet Explorer 5.5 or higher!";
$ccclogin_40="&quot;Javascript&quot;  available";
$ccclogin_41="Your Browser does not support some &quot;Javascript&quot; functionalities or &quot;Javascript&quot; is disabled. Enable &quot;Javascript&quot; functionality in your Browser (\"Active Scripting\") or use Microsoft Internet Explorer 5.5 or higher!";
$ccclogin_42="The box on
      the right side should display four arrows - if there are numbers or other
      characters shown, the font \"Webdings\" is missing on your computer. In this
      case you have to install this font on your local operating system!";
$ccclogin_43="Attention: All current tables of the database";
$ccclogin_44="will be deleted and replaced with Constructioner tables. This process can take up to a few minutes.";
$ccclogin_45="Start Setup Database";
$ccclogin_46="ERROR";
$ccclogin_47="Font Webdings</b> (client-side)";
$ccclogin_48="Database";
$ccclogin_49="restored";
$ccclogin_51="for this solution!";
//----------------------2.2.0 end------------------------------------------------------------

?>
