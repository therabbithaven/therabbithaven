<?
//----------------------ENGLISH----------------------------------------------------------------
//----------------------2.2.0 begin------------------------------------------------------------
$cccp0104_1="BACKOFFFICE";
//----------------------------------------------------------------------------------
$cccp0105_1="Statistical Storage:";
$cccp0105_2="Main Language:";
$cccp0105_3="Current Language:";
$cccp0105_4="Login:";
$cccp0105_5="+ + + YOUR INTERNET BROWSER OR YOUR FIREWALL DOES NOT ACCEPT COOKIES + + +<br></font></b><font color=\"#A42700\">OR</font><b><font color=\"#A42700\"><br>+ + + SESSION TIMEOUT DUE TO LONG USER INACTIVITY + + +";
//----------------------------------------------------------------------------------
$cccp0106_1="DEVELOPMENT";
$cccp0106_2="Site Assembling";
$cccp0106_3="Create a graphical and functional \"background\" for Pages.";
$cccp0106_5="Create Pages and combine them with Layouts.";
$cccp0106_7="Development Library";
$cccp0106_8="Create and configurate the basical structure of a table.";
$cccp0106_10="Create own variables and inspect the current values of all variables.";
$cccp0106_12="Build up a library to make these colors available for developing and content managing.";
$cccp0106_14="Build up a library to make these fonts available for developing.";
$cccp0106_16="Build up a library to make these images available for developing.";
$cccp0106_18="Build up a library to make these templates available for developing.";
$cccp0106_20="Functions";
$cccp0106_21="+++ Select +++";
$cccp0106_22="GO";
$cccp0106_23="WEBSITE MANAGEMENT";
$cccp0106_24="Site Management";
$cccp0106_25="Create a profile and configurate its access permissions.";
$cccp0106_27="Add and configurate users of the Backoffice.";
$cccp0106_29="Communicate with other users of the Backoffice.";
$cccp0106_31="Create a newsletter and send it to a predefined address list of a table.";
$cccp0106_33="Backup, reset or restore the database.";
$cccp0106_35="Define the Main Language and enable multilingualism if desired.";
$cccp0106_37="Define the basic settings of the application.";
$cccp0106_39="Analyse, export and clear statistical data regarding the website usage.";
$cccp0106_41="Content Management";
$cccp0106_42="Administrate the data content of tables.";
$cccp0106_44="CONTENT MANAGEMENT";
$cccp0106_45="Content";
$cccp0106_46="Build up a library to make these images available for content managing.";
$cccp0106_48="Build up a library to make these images sizes available for content managing.";
$cccp0106_50="Build up a library to make these files available for content managing.";
$cccp0106_52="Build up a library to make these templates available for content managing.";
$cccp0106_54="Components";
$cccp0106_55="Communication";
$cccp0106_56="General Settings";
$cccp0106_57="Backup/Restore";
$cccp0106_58="Analysis";
$cccp0106_59="Build up a library to make PHP Code available for developing.";
$cccp0106_60="DEV Special";
$cccp0106_61="Check and clear log-file concerning debugging.";
//----------------------------------------------------------------------------------
$cccp0107_1="+++ Select a Page +++";
$cccp0107_2="Page:";
$cccp0107_3="Layout:";
$cccp0107_4="Help";
$cccp0107_5="Logout";
$cccp0107_6="CMS Mode";
$cccp0107_7="View Mode";
$cccp0107_8="+++ Select a Layout +++";
$cccp0107_9="Insert Functions";
$cccp0107_10="Insert Functions";
$cccp0107_11="Website Preview";
$cccp0107_12="You have to select a page!";
$cccp0107_13="You have to select a page!";
$cccp0107_14="You have to select a page!";
$cccp0107_15="You have to select a layout!";
//----------------------------------------------------------------------------------
//----------------------2.2.0 end------------------------------------------------------------

?>
