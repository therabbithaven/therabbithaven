<?
//----------------------ENGLISH----------------------------------------------------------------
//----------------------2.2.0 begin------------------------------------------------------------
$cccp0111_1="COLORS";
$cccp0111_2="New Color:";
$cccp0111_3="Name:";
$cccp0111_4="Color Hex-Code:";
$cccp0111_5="Add";
$cccp0111_6="Color Name";
$cccp0111_7="Color";
$cccp0111_8="Hex-Code";
$cccp0111_9="Delete";
$cccp0111_10="Are You Sure?";
$cccp0111_11="Delete";
$cccp0111_12="Deleting a color has no effect to all components, which use this color.";        
//----------------------2.2.0 end------------------------------------------------------------

?>
