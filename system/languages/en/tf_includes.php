<?
//----------------------ENGLISH----------------------------------------------------------------
//----------------------2.2.0 begin------------------------------------------------------------
//-------------- This File is included in every script -----------------------------
$cccsystempage_showname[1]="Layouts";
$cccsystempage_showname[2]="Pages";
$cccsystempage_showname[3]="Tables";
$cccsystempage_showname[4]="Variables";
$cccsystempage_showname[5]="Colors";
$cccsystempage_showname[6]="Styles";
$cccsystempage_showname[7]="Images";
$cccsystempage_showname[8]="HTML Parts";
$cccsystempage_showname[9]="Table Editor";
$cccsystempage_showname[10]="Images";
$cccsystempage_showname[11]="ImageSizes";
$cccsystempage_showname[12]="Files";
$cccsystempage_showname[13]="HTML Parts";
$cccsystempage_showname[14]="Newsletter";
$cccsystempage_showname[15]="Internal Messages";
$cccsystempage_showname[16]="Access Profiles";
$cccsystempage_showname[17]="User Settings";
$cccsystempage_showname[18]="Languages";
$cccsystempage_showname[19]="Preferences";
$cccsystempage_showname[20]="Meta Tags";
$cccsystempage_showname[21]="Database";
$cccsystempage_showname[22]="Statistics";
$cccsystempage_showname[23]="PHP-Code Library";
$cccsystempage_showname[24]="Debugging";

//----------------------------------------------------------------------------------
//              FUNCTIONS
//----------------------------------------------------------------------------------


$cccfunctions_showname[1]="CMSEditor";
$cccfunctions_showname[2]="GuestBook";
$cccfunctions_showname[3]="ImageGallery";
$cccfunctions_showname[4]="ImageGalleryPro";
$cccfunctions_showname[5]="ImageLink";
$cccfunctions_showname[6]="ItemListMenu";
$cccfunctions_showname[7]="LanguageSelector";
$cccfunctions_showname[8]="PullDownMenu";
$cccfunctions_showname[9]="Search";
$cccfunctions_showname[10]="TableFieldSelector";
$cccfunctions_showname[11]="TableListMenu";
$cccfunctions_showname[12]="TableView";
$cccfunctions_showname[13]="TextLink";
$cccfunctions_showname[14]="Ticker";
$cccfunctions_showname[15]="Web Page Login";




$cccfunctions_maintable[1]="CMSEditor";
$cccfunctions_maintable[2]="pguestbook";
$cccfunctions_maintable[3]="pimagegallery";
$cccfunctions_maintable[4]="pimagegallerypro";
$cccfunctions_maintable[5]="ImageLink";
$cccfunctions_maintable[6]="ItemListMenu";
$cccfunctions_maintable[7]="LanguageSelector";
$cccfunctions_maintable[8]="ppulldownmenu";
$cccfunctions_maintable[9]="Search";
$cccfunctions_maintable[10]="TableFieldSelector";
$cccfunctions_maintable[11]="ptablelistmenu";
$cccfunctions_maintable[12]="TableView";
$cccfunctions_maintable[13]="ptextlink";
$cccfunctions_maintable[14]="Ticker";
$cccfunctions_maintable[15]="WebPageLogin";

//----------------------------------------------------------------------------------
//              DATE [Pulldown-Backoffice]
//----------------------------------------------------------------------------------
$cccdateyearbo[0]="Year";
$cccdatedaybo[0]="Day";
$cccdatemonthbo[0]="Month";
$cccdatemonthbo[1]="Jan";
$cccdatemonthbo[2]="Feb";
$cccdatemonthbo[3]="Mar";
$cccdatemonthbo[4]="Apr";
$cccdatemonthbo[5]="May";
$cccdatemonthbo[6]="Jun";
$cccdatemonthbo[7]="Jul";
$cccdatemonthbo[8]="Aug";
$cccdatemonthbo[9]="Sep";
$cccdatemonthbo[10]="Okt";
$cccdatemonthbo[11]="Nov";
$cccdatemonthbo[12]="Dez";

//----------------------------------------------------------------------------------
$cccp0102_1="CMS Mode";
$cccp0102_2="View Mode";
$cccp0102_3="Insert Functions";
//----------------------------------------------------------------------------------
$cccp0120_1="Application Development &amp; Site Management Software";
$cccp0120_2="PERMISSION DENIED - possible reasons could be ...";
$cccp0120_3="no access permission to this area";
$cccp0120_4="session time-out (e.g. due to long user inactivity)";
$cccp0120_5="in this case you have to login again";
$cccp0120_6="here";
//----------------------------------------------------------------------------------
$cccp0134_1="Defined Colors:";
//----------------------------------------------------------------------------------
$cccp0135_1="ERROR: Checksum is wrong! File-Corruption possible!";
$cccp0135_2="Datasets imported!";
$cccp0135_3="ERROR: You have to select a file for uploading!!!";
//----------------------------------------------------------------------------------
$cccp0137_1="ATTENTION! High Secure Risk! The standard user 'constructioner' still exists in the database with the standard password! Please change the password of the user 'constructioner' or delete this user completely!";
$cccp0137_2="You have";
$cccp0137_3="new";
$cccp0137_4="in your Message Board!";
$cccp0137_5="in Language:";
$cccp0137_6="Help";
$cccp0137_7="Messages";
$cccp0137_8="Message";
//----------------------------------------------------------------------------------
$cccfunctionsearch_1="Results:";
$cccfunctionsearch_2="<hr>";
//----------------------2.2.0 end------------------------------------------------------------

?>
