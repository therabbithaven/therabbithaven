<?
//----------------------2.2.0 begin------------------------------------------------------------
$cccp0146_1="LANGUAGE";
$cccp0146_2="+++ Select a Language +++";
$cccp0146_3="Language Setup:";
$cccp0146_4="Enabled Languages:";
$cccp0146_5="Main Language:";
$cccp0146_6="SAVE";
$cccp0146_7="Multilingual WebSite:";
$cccp0146_8="Yes";
$cccp0146_9="No";
$cccp0146_12="Main Language:";
$cccp0146_13="<b>Attention:</b> Insert this Meta Tag line to the Head of your Layouts <br>
                to provide automatically insertion of the correct Charset definition <br>
                by using multilingualism:<br><br><font face=\"Courier New, Courier, mono\" size=\"2\">
                &lt;meta http-equiv=\"Content-Type\" content=\"$"."constructioner_charset\"&gt;";
$cccp0146_14="Settings";
$cccp0146_15="Enable / Disable Languages";
$cccp0146_16="Display";
//----------------------------------------------------------------------------------
$cccp0200_1="LANGUAGE";
$cccp0200_2="Language Setup:";
$cccp0200_3="Enabled Languages:";
$cccp0200_4="Main Language:";
$cccp0200_5="SAVE";
$cccp0200_6="EXIT";
$cccp0200_7="ID";
$cccp0200_8="Language";
$cccp0200_9="Charset";
$cccp0200_10="Active";
$cccp0200_11="Settings";
$cccp0200_12="Enable / Disable Languages";
$cccp0200_13="Display";
//----------------------------------------------------------------------------------
$cccp0229_1="LANGUAGE";
$cccp0229_3="Display in Language:";
$cccp0229_4="ID";
$cccp0229_5="Language";
$cccp0229_6="SAVE";
$cccp0229_7="Display Name";
$cccp0229_14="Settings";
$cccp0229_15="Enable / Disable Languages";
$cccp0229_16="Display";
//----------------------2.2.0 end------------------------------------------------------------

?>
