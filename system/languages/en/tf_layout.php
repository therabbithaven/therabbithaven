<?
//----------------------2.2.0 begin------------------------------------------------------------
$cccp0155_1="LAYOUTS";
$cccp0155_2="Warning: c-Area Names are not unique!";
$cccp0155_3="Number of c-Areas:";
$cccp0155_4="New Layout:";
$cccp0155_5="Add";
$cccp0155_6="Assembling";
$cccp0155_7="Layout Name";
$cccp0155_8="Convert/Upload File";
$cccp0155_9="File Name";
$cccp0155_10="Download";
$cccp0155_11="Delete";
$cccp0155_12="This Layout can not be deleted, because it is in use of pages";
$cccp0155_13="Are You Sure? Deleting this layout will also cause deletion of associated c-areas and cms-content";
$cccp0155_14="No file uploaded yet!";
$cccp0155_15="";
$cccp0155_16="";
$cccp0155_17="Insert Functions";
$cccp0155_18="Upload";
$cccp0155_19="Download";
$cccp0155_20="Delete";
$cccp0155_21="Editor";
$cccp0155_22="Create a Layout including C-Areas</a> | ";
$cccp0155_23="Layouts";
$cccp0155_24="Working with Multilingualism";
$cccp0155_25="Signs &amp; Symbols [&quot;Insert Functions&quot; Section]:<br>
                &nbsp;<br>
                </b>The icons at the beginning and the end of a &quot;C-Area&quot;
                will give helpful <br>
                information about the current state of the area:";
$cccp0155_26="Start";
$cccp0155_27="End";
$cccp0155_28="C-Area Insertion Status";
$cccp0155_29="No Function inserted - area is still empty*";
$cccp0155_30="Function in &quot;Layout&quot;**";
$cccp0155_31="Function in &quot;Page&quot;**";
$cccp0155_32="Function in &quot;Page&quot;
            will overrule Function in &quot;Layout&quot;**<br>
            (can only be indicated at &quot;Pages&quot;)";
$cccp0155_33="*Layout placeholders between a &quot;Start-&quot; and an &quot;End-Icon&quot;
                of a <br>
                &quot;C-Area&quot; (e.g. text, images, etc) will be displayed
                if existent.<br>
                &nbsp;<br>
                **The number of displayed PHP-code characters inside the framing
                <br>
                of a &quot;C-Area&quot; can be limited within the &quot;Preferences&quot;
                menu.";
$cccp0155_34="Note:";
$cccp0155_35="Convert to 'Open HTML Code'";
$cccp0155_36="Upload Image";
$cccp0155_37="Upload";
$cccp0155_38="Index of";
$cccp0155_39="Delete";
$cccp0155_40="Layout";
$cccp0155_41="Creator";
$cccp0155_42="HTML";
$cccp0155_43="Code";
$cccp0155_44="Name:";
$cccp0155_45="For the use of layouts created outside Constructioner, notice the advice given in our HELP!";
$cccp0155_46="";
$cccp0155_47="Creator";
$cccp0155_48="Open HTML Code";
$cccp0155_49="Attention: After conversion, the layout cannot be edited with the CREATOR anymore.";
$cccp0155_50="";
$cccp0155_51="";
//-------------------------------------------------------------------------------------------
$cccp0230_1="LAYOUTS";
$cccp0230_2="Creator";
$cccp0230_3="Layout:";
$cccp0230_4="RESET";
$cccp0230_5="SAVE";
$cccp0230_6="Template:";
$cccp0230_8="Load";
$cccp0230_9="advanced";
$cccp0230_10="Save as";
$cccp0230_11="SAVE";
$cccp0230_13="Are you sure?";
$cccp0230_14="Delete";
$cccp0230_15="PageSettings:";
$cccp0230_16="Alignment:";
$cccp0230_17="Left";
$cccp0230_18="Center";
$cccp0230_19="Right";
$cccp0230_20="Width:";
$cccp0230_21="BGImage:";
$cccp0230_22="BGColor";
$cccp0230_23="Apply";
$cccp0230_24="BoxSettings:";
$cccp0230_25="Copy this Box-Settings";
$cccp0230_26="COPY";
$cccp0230_27="Paste Box-Settings";
$cccp0230_28="PASTE";
$cccp0230_29="Apply to selection";
$cccp0230_30="Insert a new box right behind selected box - horizontal";
$cccp0230_31="Insert a new box right behind selected box - vertical";
$cccp0230_32="Delete selected box";
$cccp0230_33="INSERT X";
$cccp0230_34="INSERT Y";
$cccp0230_35="DELETE";
$cccp0230_36="Align:";
$cccp0230_37="Left";
$cccp0230_38="Center";
$cccp0230_39="Right";
$cccp0230_40="vAlign:";
$cccp0230_41="Top";
$cccp0230_42="Middle";
$cccp0230_43="Bottom";
$cccp0230_44="nowrap";
$cccp0230_45="BGImage:";
$cccp0230_46="Height:";
$cccp0230_47="Width:";
$cccp0230_48="BGColor";
$cccp0230_49="BorderStyle:";
$cccp0230_50="BorderColor:";
$cccp0230_51="BorderWidth:";
$cccp0230_52="Spacing:";
$cccp0230_53="Cellpadding:";
$cccp0230_54="Number of C-Areas*:";
$cccp0230_55="Apply to selected Box";
$cccp0230_56="* C-Areas are placeholders for 'Insert Functions'.";
$cccp0230_57="<b>Note:</b> For editing select a box within the window.";
$cccp0230_58="Select a box first!";
$cccp0230_59="Select just one box for splitting";
$cccp0230_60="Delete needs one box to be selected!";
$cccp0230_61="At least one box has to remain!";
$cccp0230_62="RowSettings:";
//-------------------------------------------------------------------------------------------
$cccp0231_1="SAVE";
$cccp0231_2="HTML Code:";
$cccp0231_3="Insert a C-AREA:";
$cccp0231_4="Name:";
$cccp0231_5="Insert";
$cccp0231_6="Layout:";
$cccp0231_7="LAYOUTS";
//----------------------2.2.0 end------------------------------------------------------------

?>
