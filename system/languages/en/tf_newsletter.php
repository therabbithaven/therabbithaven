<?
//----------------------2.2.0 begin------------------------------------------------------------
$cccp0215_1="NEWSLETTER";
$cccp0215_2="ERROR: You have to fill out a name AND select a table for data use!";
$cccp0215_3="";
$cccp0215_4="New Newsletter:";
$cccp0215_5="Name:";
$cccp0215_6="Select Data from Table:";
$cccp0215_7="Mail-Type:";
$cccp0215_8="Plain Text";
$cccp0215_9="HTML";
$cccp0215_10="Add";
$cccp0215_11="Settings";
$cccp0215_12="Newsletter";
$cccp0215_13="Mail-Type";
$cccp0215_14="Delete";
$cccp0215_15="Settings";
$cccp0215_16="Html";
$cccp0215_17="Text";
$cccp0215_18="Are You Sure?";
$cccp0215_19="Delete";
//----------------------------------------------------------------------------------
$cccp0216_1="NEWSLETTER";
$cccp0216_2="Newsletter:";
$cccp0216_14="";
$cccp0216_15="Newsletter:";
$cccp0216_16="Table:";
$cccp0216_17="Mail-Type:";
$cccp0216_18="SAVE";
$cccp0216_20="Mail From (Reply To):*";
$cccp0216_21="Mail To:";
$cccp0216_22="[Select the Table Field containing the email addresses.]";
$cccp0216_23="[Optional:] Restrictive Table Conditions:";
$cccp0216_24="e.g. tablefieldname01='TRUE'";        //Attention: Don�t translate 'TRUE'!
$cccp0216_25="Subject:";
$cccp0216_26="Message Editor:";
$cccp0216_27="*Attention:</b> Use this field optional! It should contain an e-mail address,<br>
to which recipients of a newsletter can reply their answer. However<br>
some SMTP server settings will prohibit arbitrary address definitions<br>
generally - so this will cause problems during mail sending. In this case<br>
leave this field empty or contact your webspace provider for help.";
$cccp0216_28="Editor";
$cccp0216_29="Settings";
$cccp0216_30="Generate Mails";
$cccp0216_31="Send Mails";
//----------------------------------------------------------------------------------
$cccp0219_1="NEWSLETTER";
$cccp0219_2="Newsletter:";
$cccp0219_7="ATTENTION: Correct Mailadresses have NOT been found. Check content of table";
$cccp0219_8="in CURRENT language:";
$cccp0219_9="Newsletter:";
$cccp0219_10="Mails into table";
$cccp0219_11="inserted";
$cccp0219_12="Mails with invalid mailadress not inserted";
$cccp0219_13="You have to fill out 'Mail To' and Edit Message first!";
$cccp0220_14="";
$cccp0219_15="Newsletter:";
$cccp0219_16="Table:";
$cccp0219_17="Mail-Type:";
$cccp0219_18="SAVE";
$cccp0219_28="Mail Generator:";
$cccp0219_29="Attention: During this process all current datasets of the Table";
$cccp0219_30="will be deleted and refilled with the new messages of the newsletter.";
$cccp0219_31="Generate";
$cccp0219_32="[This process will (re)create the Table";
$cccp0219_33="and fill it up with new messages!";
$cccp0219_34="Note:</b> Inserted placeholders will be replaced during this mail generation.]";
$cccp0219_35="Note:</b> All generated messages can be separately edited before sending if necessary -<br>just open the Table";
$cccp0219_36="by using the \"Table Editor\" menu.";
$cccp0219_37="Editor";
$cccp0219_38="Settings";
$cccp0219_39="Generate Mails";
$cccp0219_40="Send Mails";
//----------------------------------------------------------------------------------
$cccp0220_1="NEWSLETTER";
$cccp0220_2="Newsletter:";
$cccp0220_3="Mails sent";
$cccp0220_4="Mails with invalid mailadress not sent";
$cccp0220_5="ERROR: Connection to local SMTP failed!";
$cccp0220_6="Sending is not possible! Check SMTP-Server!";
$cccp0220_15="Newsletter:";
$cccp0220_16="Table:";
$cccp0220_17="Mail-Type:";
$cccp0220_18="SAVE";
$cccp0220_37="Send Mails:";
$cccp0220_38="Send now!";
$cccp0220_39="[All messages of the Table";
$cccp0220_40="will be sent to the according recipients!]";
$cccp0220_41="Editor";
$cccp0220_42="Settings";
$cccp0220_43="Generate Mails";
$cccp0220_44="Send Mails";
//----------------------2.2.0 end------------------------------------------------------------

?>
