<?
//----------------------2.2.0 begin------------------------------------------------------------
$cccp0156_1="PAGE";
$cccp0156_2="+++ Select +++";
$cccp0156_3="New Page:";
$cccp0156_4="Name [technical]:";
$cccp0156_5="Title:";
$cccp0156_6="Layout:";
$cccp0156_7="Position:";
$cccp0156_8="Level:";
$cccp0156_9="Add";
$cccp0156_10="Assembling";
$cccp0156_11="Settings";
$cccp0156_12="Page Name";
$cccp0156_13="Position";
$cccp0156_14="Level";
$cccp0156_15="Delete";
$cccp0156_16="[No Layout applied]";
$cccp0156_17="This Page is defined as Startpage (Home). Goto Preferences and apply another Startpage!";
$cccp0156_18="You have to apply a Layout to the Page first. Click Settings for applying...";
$cccp0156_19="Insert Functions";
$cccp0156_20="Settings";
$cccp0156_21="move";
$cccp0156_22="Are you sure? Canceling this Page will cause deletion of associated C-Area settings and filled in content!";
$cccp0156_23="Delete";
$cccp0156_24="Annotations, advices and term definitions concerning this menu
                are <br>
                described in detail within the &quot;Help Section&quot;:";
$cccp0156_25="Pages";
$cccp0156_26="Insert Function into Layout or Page?";
$cccp0156_27="There is no general rule whether a Function should be inserted
                into <br>
                a &quot;Page&quot; or into a &quot;Layout&quot;. Function in &quot;Layouts&quot;
                will be effective <br>
                to all &quot;Pages&quot; that use this &quot;Layout&quot; as &quot;background&quot;,
                except there <br>
                is a Function inserted in a &quot;Page&quot; at the same position.</p>
              <p><b>Note: </b>&quot;Page Function overrules Layout Function!&quot;
                A Function, that <br>
                is integrated into a Page, will overrule an existing &quot;Layout-Function&quot;
                <br>
                at the same position.";
$cccp0156_28="See also:";
$cccp0156_29="Insert Functions into Layouts and Pages";
$cccp0156_30="Signs &amp; Symbols [&quot;Insert Functions&quot; Section]:<br>
                &nbsp;<br>
                </b>The icons at the beginning and the end of a &quot;C-Area&quot;
                will give helpful <br>
                information about the current state of the area:";
$cccp0156_31="Start";
$cccp0156_32="End";
$cccp0156_33="C-Area Insertion Status";
$cccp0156_34="No Function inserted - area is still empty*";
$cccp0156_35="Function in &quot;Layout&quot;**";
$cccp0156_36="Function in &quot;Page&quot;**";
$cccp0156_37="Function in &quot;Page&quot;
            will overrule Function in &quot;Layout&quot;**<br>
            (can only be indicated at &quot;Pages&quot;)";
$cccp0156_38="*Layout placeholders between a &quot;Start-&quot; and an &quot;End-Icon&quot;
                of a <br>
                &quot;C-Area&quot; (e.g. text, images, etc) will be displayed
                if existent.<br>
                &nbsp;<br>
                **The number of displayed PHP-code characters inside the framing
                <br>
                of a &quot;C-Area&quot; can be limited within the &quot;Preferences&quot;
                menu.";
//----------------------------------------------------------------------------------
$cccp0157_1="PAGE";
$cccp0157_2="--select--";
$cccp0157_3="Page:";
$cccp0157_4="SAVE";
$cccp0157_5="Overview";
$cccp0157_6="Title:*";
$cccp0157_7="Layout:";
$cccp0157_8="PHP-Code:";
$cccp0157_9="Note:&nbsp;This PHP-Code will be executed before the \"Page\" is built together and displayed in the browser window.";
$cccp0157_10="Settings";
$cccp0157_11="e.g. 'This is my title' <br>
                          [a maximum of 60 chars is recommended, the use of variables is possible]";
$cccp0157_12="Description:*";
$cccp0157_13="e.g. 'This is the short description text, which will be shown in most search engines within
              the hit list.' <br>[a maximum of 150 chars is recommended, the use of variables is possible]";
$cccp0157_14="Keywords:*";
$cccp0157_15="e.g. 'keyword,another
              keyword,keyword 3,...'<br>
                          [a maximum of 874 chars is recommended, seperate keywords with commas,
                          the use of variables is possible]";
$cccp0157_16="Robots:*";
$cccp0157_17="Meta Tag for indexing the page within search engines";
$cccp0157_18="<b>* Hint</b>: Empty Fields mean, that general settings in section 'Meta Tags' will be used.";
//----------------------------------------------------------------------------------
$cccp0190_1="+++select+++";
$cccp0190_2="ParameterSet:";
$cccp0190_3="+++select+++";
$cccp0190_4="Help";
$cccp0190_5="SAVE";
$cccp0190_6="EXIT";
$cccp0190_7="Area:";
$cccp0190_8="Integration:";
$cccp0190_9="Standard";
$cccp0190_10="Advanced (Show PHP-Code)";
$cccp0190_11="Function resp. code of this C-Area will be deleted and saved automatically!";
$cccp0190_12="Delete";
$cccp0190_13="Function:";
$cccp0190_14="PHPCode:";
$cccp0190_15="Note:</b> Additional to normal PHP-Coding you can integrate functions in the following
        way: <br>
        The variable <b>\$THIS_AREA </b>represents the current area. Whatever you put to this variable
         will be shown, when the page will be constructed. So you can apply arbitrary PHP-Code to this variable.
         In most cases you will apply Functions to C-Areas. Functions have the syntax: <br>
         <div  align='center'><b>Function(\"ParameterSet\")</b></div> Developer-specific \"ParameterSets\" of \"Functions\" are created, edited or deleted within the \"Function\" section of the Backoffice.
         </div>";
$cccp0190_16="Create / Edit";
//----------------------2.2.0 end------------------------------------------------------------

?>
