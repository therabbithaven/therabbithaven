<?
//----------------------2.2.0 begin------------------------------------------------------------
$cccp0114_1="";
$cccp0114_2="";
$cccp0114_3="";
$cccp0114_4="WebSite URL:";
$cccp0114_5="SAVE";
$cccp0114_6="PUBLIC ACCESS Key:";
$cccp0114_7="PUBLIC ACCESS User Profile:";
$cccp0114_8="Startpage (Home):";
$cccp0114_9="General Page Title:";
$cccp0114_10="[If not defined in uploaded Layout or in Page Settings]";
$cccp0114_11="Date Display:";
$cccp0114_12="Day Position:";
$cccp0114_13="Month Position:";
$cccp0114_14="Year Position:";
$cccp0114_15="Adjustable Decimal Point:";
$cccp0114_16="in Table-Fields with Type \"Number\":";
$cccp0114_17="Main Language:";
$cccp0114_18="Multilingual WebSite:";
$cccp0114_19="yes";
$cccp0114_20="no";
$cccp0114_21="Image Resizing:";
$cccp0114_22="on";
$cccp0114_23="off";
$cccp0114_24="Jpeg-Quality:";
$cccp0114_25="[For resizing check the ImageMagick path in the config.inc.php file]";
$cccp0114_26="Statistic Recordings:";
$cccp0114_27="on";
$cccp0114_28="off";
$cccp0114_29="Recording statistical data while visiting site through backoffice";
$cccp0114_32="Insert Functions Preview:";
$cccp0114_33="Max. Number of PHP-Code Characters that are displayed inside the Framing of a C-Area:";
$cccp0114_34="Timeout Limit:";
$cccp0114_35="Server allows changing Timeout Limit [i.e. \"Save-Mode\" configuration is \"off\"]";
$cccp0114_36="Script execution is killed after";
$cccp0114_37="Seconds";
$cccp0114_38="Table Editor Display:";
$cccp0114_39="Max. Number of Datasets displayed per Backoffice-Page:";
$cccp0114_40="Table Export/Import:";
$cccp0114_41="CSV Delimiter:";
$cccp0114_42="Checksum:";
$cccp0114_43="yes";
$cccp0114_44="no";
$cccp0114_45="PREFERENCES";
$cccp0114_46="Date [Pop-Up]";
$cccp0114_47="from year";
$cccp0114_48="to year";
$cccp0114_49="Folder Permission:";
$cccp0114_50="allow users to create subfolder in directory \"upload/images/\"";
$cccp0114_51="URL-Coding:";
$cccp0114_52="Check for update information:";

//----------------------2.2.0 end------------------------------------------------------------

?>
