<?
//----------------------2.2.0 begin------------------------------------------------------------
$cccp0160_1="STATISTICS";
$cccp0160_2="Help";
$cccp0160_3="EXIT";
$cccp0160_4="Site";
$cccp0160_5="Pages";
$cccp0160_6="Referer";
$cccp0160_7="Sessions";
$cccp0160_8="User";
$cccp0160_9="Languages";
$cccp0160_10="Browser";
$cccp0160_11="IP-Addresses";
$cccp0160_12="WebSite: ";
$cccp0160_13="Direct Access";
$cccp0160_14="Internal";
$cccp0160_15="ALL PAGES";
$cccp0160_16="Intervall:";
$cccp0160_17="Analyse:";
$cccp0160_18="Periods:";
$cccp0160_19="Periods";
$cccp0160_20="Impressions";
$cccp0160_21="Impressions";
$cccp0160_22="[Total]";
$cccp0160_23="Impressions";
$cccp0160_24="[in % per Period]";
$cccp0160_25="SITE";
$cccp0160_26="Total:";
$cccp0160_27="WebApplication Development & Site Management Software";
$cccp0160_28="[Total]";
$cccp0160_29="[Months]";
$cccp0160_30="[Weeks]";
$cccp0160_31="[Days]";
//----------------------------------------------------------------------------------
$cccp0213_1="STATISTICS";
$cccp0213_2="Check date settings. If necessary enter left-hand zeros for one-digit day or month input, year enter must have four digits.";
$cccp0213_3="Statistical Data:";
$cccp0213_4="Intervall";
$cccp0213_5="to";
$cccp0213_6="Apply";
$cccp0213_7="View Statistical Data:";
$cccp0213_8="Site";
$cccp0213_9="Pages";
$cccp0213_10="Referer";
$cccp0213_11="Sessions";
$cccp0213_12="User";
$cccp0213_13="Languages";
$cccp0213_14="Browser";
$cccp0213_15="IP-Addresses";
$cccp0213_16="Period:";
$cccp0213_17="Total";
$cccp0213_18="Months";
$cccp0213_19="Weeks";
$cccp0213_20="Days";
$cccp0213_21="View";
$cccp0213_22="Download Statistical Data:";
$cccp0213_23="Download Statistical Date inbetween selected period";
$cccp0213_24="Download";
$cccp0213_25="Clear Statistical Data:";
$cccp0213_26="Current Storage Space*:";
$cccp0213_27="available";
$cccp0213_28="Are You Sure? Data inbetween selected Period will be lost!";
$cccp0213_29="Clear Statistical Date inbetween selected period";
$cccp0213_30="Clear";
$cccp0213_31="Attention:</b> Storage Space &quot;0&nbsp;%&quot; available means that no more statistical<br>
        data can be saved to the database. To avoid this, clear old data well timed. </p>
      <p>Downloading statistical data enables external disposal and evaluation.
        The size <br>
        of the storage space depends on the settings in the &quot;Preferences&quot;
        menu.</p>";
//----------------------------------------------------------------------------------
$cccp0233_1="STATISTICS";
$cccp0233_3="Statistical Data:";
$cccp0233_4="Intervall";
$cccp0233_5="to";
$cccp0233_6="Apply";
$cccp0233_8="PageImpressions";
$cccp0233_9="Pages";
$cccp0233_10="Referer";
$cccp0233_11="Visits";
$cccp0233_12="";
$cccp0233_13="Languages";
$cccp0233_14="Browser";
$cccp0233_16="Summarize:";
$cccp0233_17="Total";
$cccp0233_18="Months";
$cccp0233_19="Weeks";
$cccp0233_20="Days";
$cccp0233_21="View";
//-----------------------------------------------------------------------------------
$cccp0233_label[1]="Interval:";
$cccp0233_label[2]="Summarize:";
$cccp0233_label[3]="Table:";
$cccp0233_label[4]="Field:";
$cccp0233_label[5]="Value";
$cccp0233_label[6]="Chart";
$cccp0233_label[7]="Sort after this column";
$cccp0233_label[8]="Sorting";
$cccp0233_label[9]="Sort after this column";
$cccp0233_label[10]="Chart";
$cccp0233_label[11]="Total";
$cccp0233_label[12]="Total";
$cccp0233_label[13]="Total";
$cccp0233_label[14]="descending";
$cccp0233_label[15]="ascending";
//----------------------2.2.0 end------------------------------------------------------------

?>
