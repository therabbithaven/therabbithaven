<?
//----------------------2.2.0 begin------------------------------------------------------------
$cccp0163_1="DATABASE";
$cccp0163_2="Database in use:";
$cccp0163_3="Backup Database";
$cccp0163_4="Upgrade Database-Dump";
$cccp0163_5="Only for versions prior to 2.5.0!<br>Automatic replacement:<br>
upload/dev_images/  -->     upload/images/Development/   <br>
upload/cms_images/  -->     upload/images/CMSEditor/   <br>
upload/cms_files/   -->     upload/files/CMSEditor/   <br>
CMS(                -->     CMSEditor(                 <br>
cmscontent          -->     cmseditorcontent                 <br>";
$cccp0163_11="Delete Database:";
$cccp0163_12="Are You Sure? IMPORTANT: Whole Database will be DELETED!";
$cccp0163_16="Delete Database";
$cccp0163_18="Restore Database with uploaded File:";
$cccp0163_19="Database File:";
$cccp0163_20="Optional - Find and Replace a String in the Database File during Upload:";
$cccp0163_21="Search for:";
$cccp0163_22="Replace with:";
$cccp0163_23="Are you sure? All current data will be deleted and the database will be restored with the uploaded file.";
$cccp0163_24="Restore Database";
$cccp0163_25="Save Database to Server:";
//-------------------------------------------------------------------------------------------
$cccp0228_1="Meta Tags";
$cccp0228_2="Meta Tags Settings";
$cccp0228_3="SAVE";
$cccp0228_4="Important:";
$cccp0228_5="<p>In order to guarantee the accurate functionality of the following settings,
        title data '&lt;title&gt;...&lt;/title&gt;' and meta data '&lt;meta name=...&gt;'
        or<br>
                '&lt;meta http-equiv=...&gt;' must not be filled in within the HTML-Layout.
                In case your HTML-editor creates these tags automatically, please remove
                them from the HTML-header of your HTML-layout manually.</p>";
$cccp0228_6="Title:";
$cccp0228_7="e.g. 'This is my title' <br>
                          [a maximum of 60 chars is recommended, the use of variables is possible]";
$cccp0228_8="Description:";
$cccp0228_9="e.g. 'This is the short description text, which will be shown in most search engines within
              the hit list.' [a maximum of 150 chars is recommended, the use of variables is possible]";
$cccp0228_10="Keywords:";
$cccp0228_11="e.g. 'keyword,another
              keyword,keyword 3,...'<br>
                          [a maximum of 874 chars is recommended, seperate keywords with commas,
                          the use of variables is possible]";
$cccp0228_12="Author:";
$cccp0228_13="e.g. 'Artware Multimedia GmbH | www.artware.info'";
$cccp0228_14="Date:";
$cccp0228_15="Creation date of this website";
$cccp0228_16="Robots:*";
$cccp0228_17="Meta Tag for indexing the page within
              search engines";
$cccp0228_18="Expiry:";
$cccp0228_19="e.g. '0' for zero seconds [recommended]
                          or 'Fri, 22 Aug 2003 20:18:00 GMT' to define an expiration date";
$cccp0228_20="Character Set:";
$cccp0228_21="The character set for the chosen language
                          of the website will be defined. Multilingual websites will adjust
                          the character<br>
                          set according to the current language automatically (see also 'languages'
                          menu).";
$cccp0228_22="If required, additional Meta Tags can be added to the HTML-header:";
$cccp0228_23="e.g. (System Dublin Core): '&lt;meta name=&quot;DC.Publisher&quot; content
        =&quot;John Q. Public Ltd.&quot;&gt;'";
$cccp0228_24="<b>* Hint</b>: Within the settings of a page (Section: Development -> Pages -> Settings) it is possible to define these settings individual for each page.";
//-------------------------------------------------------------------------------------------
$cccp0232_1="DEBUGGING";
$cccp0232_2="Edit Log-File";
$cccp0232_3="File:";
$cccp0232_4="SAVE";
$cccp0232_5="Clear & Save";
//----------------------2.2.0 end------------------------------------------------------------

?>
