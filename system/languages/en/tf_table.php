<?
//----------------------2.2.0 begin------------------------------------------------------------
$cccp0165_1="TABLES";
$cccp0165_2="ERROR: Field Name [technical] and ANY Tablename [technical] have to be different!";
$cccp0165_3="ERROR: Field Name [technical] is in conflict with Variable-Names!";
$cccp0165_4="ERROR: Field Name [technical] is in conflict with System-Variable-Names!";
$cccp0165_5="Multilingual";
$cccp0165_6="Number of Characters:";
$cccp0165_7="Maxlenght:";
$cccp0165_8="Cols:";
$cccp0165_9="Rows:";
$cccp0165_10="Values From Table:";
$cccp0165_11="Field:";
$cccp0165_12="+++ Select a Color +++";
$cccp0165_13="Table Fields:";
$cccp0165_14="SAVE";
$cccp0165_15="Overview";
$cccp0165_16="New Table Field:";
$cccp0165_17="Type:";
$cccp0165_18="Check Box";
$cccp0165_19="Date";
$cccp0165_20="Email";
$cccp0165_21="Number [Double]";
$cccp0165_22="Password";
$cccp0165_23="PullDown [TableField]";
$cccp0165_24="Text";
$cccp0165_25="Textarea";
$cccp0165_26="Text/Image [CMS-Editor]";
$cccp0165_27="Upload Files";
$cccp0165_28="Username [unique]";
$cccp0165_29="TimeStamp [New Dataset]";
$cccp0165_30="TimeStamp [Update Dataset]";
$cccp0165_31="Overview [Table Editor]:";
$cccp0165_32="show";
$cccp0165_33="Background Color [Table Editor]:";
$cccp0165_34="Name [technical]:";
$cccp0165_35="Display&nbsp;Name [Table Editor]:";
$cccp0165_36="Add";
$cccp0165_37="Position";
$cccp0165_38="Type/Settings";
$cccp0165_39="Field Name</b><br>&nbsp;[technical]";
$cccp0165_40="Display Name </b><br>&nbsp;[Table Editor]";
$cccp0165_41="Background Color</b>&nbsp;<br>&nbsp;[Table Editor]";
$cccp0165_42="Overview</b> <br>&nbsp;[Table Editor]";
$cccp0165_43="Delete";
$cccp0165_44="";
$cccp0165_45="Integer";
$cccp0165_46="";
$cccp0165_47="multilingual";
$cccp0165_48="monolingual";
$cccp0165_49="Are You Sure?";
$cccp0165_50="Delete";
$cccp0165_51="Access Profile";
$cccp0165_52="Table Fields";
$cccp0165_53="Date [PullDown]";
$cccp0165_54="Image";
$cccp0165_55="Image Gallery Folder";
$cccp0165_56="Upload Images";
$cccp0165_57="PHP Code";
$cccp0165_58="Files will be located in this folder:";
$cccp0165_59="Images will be located in this folder:";
//----------------------------------------------------------------------------------
$cccp0166_1="TABLES";
$cccp0166_2="ERROR: Table Names [technical] have to be different!";
$cccp0166_3="ERROR: Table Names [technical] and ANY  Field Names [technical] have to be different!";
$cccp0166_4="New Table:";
$cccp0166_5="Name [technical]:";
$cccp0166_6="Display Name [Table Editor]:";
$cccp0166_7="Add";
$cccp0166_8="SAVE";
$cccp0166_9="Table Fields";
$cccp0166_10="Table Name</b>[technical]";
$cccp0166_11="Display Name </b>[Table Editor]";
$cccp0166_12="Delete";
$cccp0166_13="Table Fields";
$cccp0166_14="Are You Sure? Check for use of this table in TableView, Search, Newsletter,...";
$cccp0166_15="Delete";
$cccp0166_16="Statistical Recording";
$cccp0166_17="Show field in statistical analyses";
//----------------------------------------------------------------------------------
$cccp0167_1="TABLE EDITOR";
$cccp0167_2="Datasets";
$cccp0167_3="Table </b>[Current Number of Datasets]";
$cccp0167_4="Clear";
$cccp0167_5="Import</b>&nbsp;[FileFormat: CSV]";
$cccp0167_6="Export";
$cccp0167_7="Table data of the CURRENT language will be exported!";
$cccp0167_8="Are you sure? Imported data of the uploaded file will overwrite all table data of the CURRENT language!";
$cccp0167_9="Table data will be exported!";
$cccp0167_10="Are you sure? Imported data of the uploaded file will overwrite all table data!";
$cccp0167_11="Open Datasets";
$cccp0167_12="Import";
$cccp0167_13="Are You Sure? This action clears all datasets in this table!!!";
$cccp0167_14="Clear all Data Sets";
$cccp0167_15="Clear";
$cccp0167_16="Export";
$cccp0167_17="View Datasets";
//----------------------------------------------------------------------------------
$cccp0226_1="TABLE FIELD";
$cccp0226_2="Table Fields";
$cccp0226_3="Table:";
$cccp0226_4="Fieldname:";
$cccp0226_5="SAVE";
$cccp0226_6="PHP Code to modify VALUE for <b>DATABASE-INPUT</b>:";
$cccp0226_7="<b>Note:</b> Fieldvalue is applied to variable ";
$cccp0226_8="PHP Code to modify VALUE for <b>BROWSER-DISPLAY</b>:";
$cccp0226_9="The variable <b>\$TYPE</b> consists the actual type of database-access:<br>
INSERT = insert a new dataset;
UPDATE = update an existing dataset;
SELECT = show an existing dataset!";
//----------------------2.2.0 end------------------------------------------------------------




?>
