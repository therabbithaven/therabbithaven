<?
//----------------------2.2.0 begin------------------------------------------------------------
$cccp0196_1="IMAGES";
$cccp0196_2="+ + + No Resizing + + +";
$cccp0196_3="Upload Image:";
$cccp0196_4="[+++ Resizing not available +++]";
$cccp0196_5="Upload Image";
$cccp0196_6="Upload";
$cccp0196_7="Preview";
$cccp0196_8="Image";
$cccp0196_9="Download";
$cccp0196_10="Delete";
$cccp0196_11="Preview";
$cccp0196_12="Download";
$cccp0196_13="Are You Sure? All inserted images on the WebSite will be missing!!!";
$cccp0196_14="Delete";
$cccp0196_15="Index of";
$cccp0196_16="New Folder";
$cccp0196_17="Add";
$cccp0196_18="Parent Directory";
$cccp0196_19="in CMS Editor available";
$cccp0196_20="SAVE";
//----------------------------------------------------------------------------------
$cccp0197_1="FILES";
$cccp0197_2="Upload File:";
$cccp0197_3="Upload File";
$cccp0197_4="Upload";
$cccp0197_5="Preview";
$cccp0197_6="Index of";
$cccp0197_7="in CMS Editor available";
$cccp0197_8="Delete";
$cccp0197_9="Preview";
$cccp0197_10="Download";
$cccp0197_11="Are You Sure? All inserted files on the WebSite will be missing!!!";
$cccp0197_12="Delete";
//----------------------------------------------------------------------------------
$cccp0198_1="HTML PARTS [CMS-Editor]";
$cccp0198_2="Upload HTML Parts:";
$cccp0198_3="Upload HTML Part";
$cccp0198_4="Upload";
$cccp0198_5="Preview";
$cccp0198_6="HTML Part [available within CMS-Editor]";
$cccp0198_7="Download";
$cccp0198_8="Delete";
$cccp0198_9="Preview";
$cccp0198_10="Download";
$cccp0198_11="Are You Sure?";
$cccp0198_12="Delete";
//----------------------------------------------------------------------------------
$cccp0199_1="Help";
$cccp0199_2="Are You Sure? All inserted images on the WebSite will be missing!!!";
$cccp0199_3="Delete";
$cccp0199_4="Image:";
//----------------------------------------------------------------------------------
$cccp0202_1="IMAGES";
$cccp0202_2="+ + + No Resizing + + +";
$cccp0202_3="Upload Image:";
$cccp0202_4="[+++ Resizing not available +++]";
$cccp0202_5="Upload Image";
$cccp0202_6="Upload";
$cccp0202_7="Preview";
$cccp0202_8="Image";
$cccp0202_9="Download";
$cccp0202_10="Delete";
$cccp0202_11="Preview";
$cccp0202_12="Download";
$cccp0202_13="Are You Sure? All inserted images on the WebSite will be missing!!!";
$cccp0202_14="Delete";
$cccp0202_15="Index of";
$cccp0202_16="New Folder";
$cccp0202_17="Add";
$cccp0202_18="Parent Directory";
$cccp0202_19="Available in<br>CMS section";
$cccp0202_20="SAVE";
$cccp0202_21="Available in<br>CMS Editor";
$cccp0202_22="Available in<br>Development section";
//----------------------------------------------------------------------------------
$cccp0203_1="Help";
$cccp0203_2="Are You Sure?";
$cccp0203_3="Delete";
$cccp0203_4="EXIT";
$cccp0203_5="Image:";
//----------------------------------------------------------------------------------
$cccp0211_1="HTML Parts [Development]";
$cccp0211_2="Upload HTML Parts:";
$cccp0211_3="Upload HTML Part";
$cccp0211_4="Upload";
$cccp0211_5="Preview";
$cccp0211_6="HTML Part [available within Development section]";
$cccp0211_7="Download";
$cccp0211_8="Delete";
$cccp0211_9="Preview";
$cccp0211_10="Download";
$cccp0211_11="Are You Sure?";
$cccp0211_12="Delete";
//----------------------2.2.0 end------------------------------------------------------------

?>
