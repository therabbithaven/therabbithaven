<?
//----------------------2.2.0 begin------------------------------------------------------------
$cccp0174_1="ACCESS PROFILES";
$cccp0174_2="New Profile:";
$cccp0174_3="Name:";
$cccp0174_4="Add";
$cccp0174_5="Profile Name";
$cccp0174_6="Access Permissions";
$cccp0174_7="Delete";
$cccp0174_8="Are You Sure?";
$cccp0174_9="It is not possible to delete this profile, because of us in assoziated user-settings ";
$cccp0174_10="Pages";
$cccp0174_11="Tables";
$cccp0174_12="Backoffice";
$cccp0174_13="Profiles";
$cccp0174_14="Delete";
//----------------------------------------------------------------------------------
$cccp0175_1="ACCESS PROFILES";
$cccp0175_2="Access Profile:";
$cccp0175_3="SAVE";
$cccp0175_4="Overview";
$cccp0175_5="Page";
$cccp0175_6="Inaccessible";
$cccp0175_7="Accessible";
$cccp0175_8="Permissions";
$cccp0175_9="C-Areas";
$cccp0175_10="Pages";
$cccp0175_11="Tables";
$cccp0175_12="Backoffice";
$cccp0175_13="Profiles";
//----------------------------------------------------------------------------------
$cccp0176_1="USER SETTINGS";
$cccp0176_2="ERROR: The user 'PUBLIC' is already used by system as the public-access-user!";
$cccp0176_3="New User:";
$cccp0176_4="Add";
$cccp0176_5="Settings";
$cccp0176_6="User";
$cccp0176_7="Online";
$cccp0176_8="Delete";
$cccp0176_9="Settings";
$cccp0176_10="Are You Sure?";
$cccp0176_11="Delete";
//----------------------------------------------------------------------------------
$cccp0177_1="USER SETTINGS";
$cccp0177_2="Backoffice";
$cccp0177_3="Backoffice Mode";
$cccp0177_4="CMS Mode";
$cccp0177_5="View Mode";
$cccp0177_6="SAVE";
$cccp0177_7="Overview";
$cccp0177_8="Password:";
$cccp0177_9="Retype Password:";
$cccp0177_10="Startpage at Login:";
$cccp0177_11="Mode at Login:";
$cccp0177_12="Current Language at Login:";
$cccp0177_13="Help welcomes at Login:";
$cccp0177_14="Access Profile:";
$cccp0177_15="Forward Message To E-Mail:";
$cccp0177_16="Image Preview:";
$cccp0177_17="Retyping of password was not correct! Please try again!";
$cccp0177_18="Backoffice Language:";
$cccp0177_19="Settings";
$cccp0177_20="User:";
//----------------------------------------------------------------------------------
$cccp0183_1="ACCESS PROFILES";
$cccp0183_2="Access Profile:";
$cccp0183_3="SAVE";
$cccp0183_4="Overview";
$cccp0183_5="Tables";
$cccp0183_6="Invisible";
$cccp0183_7="Visible";
$cccp0183_8="Editable";
$cccp0183_9="Pages";
$cccp0183_10="Tables";
$cccp0183_11="Backoffice";
$cccp0183_12="Profiles";
//----------------------------------------------------------------------------------
$cccp0184_1="ACCESS PROFILES";
$cccp0184_2="Access Profile:";
$cccp0184_3="SAVE";
$cccp0184_4="Overview";
$cccp0184_5="Backoffice Zones";
$cccp0184_6="Available";
$cccp0184_7="Special Access";
$cccp0184_8="Seperate Access to Each Function";
$cccp0184_9="Pages";
$cccp0184_10="Tables";
$cccp0184_11="Backoffice";
$cccp0184_12="Profiles";
$cccp0184_13="Development - Site Assembling";
$cccp0184_14="Development - Functions [Plugins]";
$cccp0184_15="CMS - Content";
$cccp0184_16="CMS - Components";
$cccp0184_17="CMS - Communication";
$cccp0184_18="General - Settings";
$cccp0184_19="General - Backup/Restore";
$cccp0184_20="General - Analyse";
$cccp0184_21="Development - DEV Special";
//----------------------------------------------------------------------------------
$cccp0185_1="ACCESS PROFILES";
$cccp0185_2="Access Profile:";
$cccp0185_3="Page:";
$cccp0185_4="SAVE";
$cccp0185_5="Pages";
$cccp0185_6="C-Area*";
$cccp0185_7="Invisible";
$cccp0185_8="Visible";
$cccp0185_9="Editable";
$cccp0185_10="*Note:</b> It has no consequences of any kind whether the setting is<br>adjusted to \"Editable\" or \"Visible\" for \"C-Areas\" that do not contain<br>the \"CMS-Editor\" functionality.<br><br><br>Actually the term \"Editable\" refers only to \"C-Areas\" with built-in<br>\"CMS-Editor\" functionality. Content areas with this adjustment will<br>display the pencil icon within the \"CMS-Mode\" and can be opened<br>for content managing subsequently.";
$cccp0185_11="Pages";
$cccp0185_12="Tables";
$cccp0185_13="Backoffice";
$cccp0185_14="Profiles";
//----------------------------------------------------------------------------------
$cccp0186_1="ACCESS PROFILES";
$cccp0186_2="Access Profile:";
$cccp0186_3="SAVE";
$cccp0186_4="Overview";
$cccp0186_5="Access Profile";
$cccp0186_6="Visible for";
$cccp0186_7="Invisible for";
$cccp0186_8="Pages";
$cccp0186_9="Tables";
$cccp0186_10="Backoffice";
$cccp0186_11="Profiles";
//----------------------2.2.0 end------------------------------------------------------------

?>
