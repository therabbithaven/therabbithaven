<?
//--------------------------------------------------------------------------
//This file can be used to include the correct translation-file
//depending on the current language
//---------------------------------
//Dieses File kann f�r die Steuerung der �bersetzungsfiles verwendet werden
//abh�ngig von der jeweils aktuellen Sprache
//steht ein und dieselbe Variable in beiden Dateien, wird die sprachspezifischere Variante gezogen
//steht eine Variable nur in dem File f�r die Hauptsprache, dann wird nur dieser Wert genommen
//Tip: Erstellen Sie zuerst alle ben�tigten Vokabeln in dem File f�r die Hauptsprache
// und kopieren Sie dieses mit der richtigen Endung (de...Deutsch; en...Englisch) um eine Nebensprache
// nach der anderen zu �bersetzen! Weitere Abk�rzungen (Sprach-IDs) siehe im
// Backoffice-Men� Sprachen/Languages -> Sprachen Aktivieren/Deaktivieren
//--------------------------------------------------------------------------
$mainlanguagefile="upload/phplibrary/cms_language_en.php";   //File for main language
$currentlanguagefile="upload/phplibrary/cms_language_$language.php"; //File for current language

if (file_exists($mainlanguagefile)){
    include ("$mainlanguagefile");
}
if ($language!="en"){
    if (file_exists($currentlanguagefile)){
        include ("$currentlanguagefile");
    }
}

?>