<?
//Before Page-Construction
//-------statistic definition:
//--PageImpressions--
$cccstatistic_analyse_name[1]="PageImpressions";         //Name which will be used as a showname in statistical anaylses
$cccstatistic_file_shortcut[1]="pi";                   //Look for existing filenames in folder 'upload/statistic/' to prevent name conflicts
//--Sessions--
$cccstatistic_analyse_name[2]="Sessions";         //Name which will be used as a showname in statistical anaylses
$cccstatistic_file_shortcut[2]="se";                   //Look for existing filenames in folder 'upload/statistic/' to prevent name conflicts
//--Browsers--
$cccstatistic_analyse_name[3]="Browsers";         //Name which will be used as a showname in statistical anaylses
$cccstatistic_file_shortcut[3]="br";                   //Look for existing filenames in folder 'upload/statistic/' to prevent name conflicts
//--Pages--
$cccstatistic_analyse_name[4]="Pages";         //Name which will be used as a showname in statistical anaylses
$cccstatistic_file_shortcut[4]="pa";                   //Look for existing filenames in folder 'upload/statistic/' to prevent name conflicts
//--Languages--
$cccstatistic_analyse_name[5]="Languages";         //Name which will be used as a showname in statistical anaylses
$cccstatistic_file_shortcut[5]="la";                   //Look for existing filenames in folder 'upload/statistic/' to prevent name conflicts
//--Referrer--
$cccstatistic_analyse_name[6]="Referrer";         //Name which will be used as a showname in statistical anaylses
$cccstatistic_file_shortcut[6]="re";                   //Look for existing filenames in folder 'upload/statistic/' to prevent name conflicts


?>